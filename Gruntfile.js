module.exports = function(grunt) {
    grunt.initConfig({
        less: {
            development: {
                options: {
                    paths: [
                        'bower/bootstrap/less',
                        'app/Resources/public/less'
                    ],
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    'app.css': 'app/Resources/public/less/app.less'
                }
            }
        },

        clean: ['app.css'],

        concat: {
            options: {
                separator: ';\n'
            },
            js: {
                src: [
                    'bower/offline/offline.min.js',
                    'bower/jquery/dist/jquery.min.js',
                    'bower/spin.js/spin.min.js',
                    'bower/underscore/underscore-min.js',
                    'bower/underscore.string/dist/underscore.string.min.js',
                    'bower/sprintf/dist/sprintf.min.js',
                    'bower/twig.js/twig.min.js',
                    'bower/parsleyjs/dist/parsley.min.js',
                    'bower/parsleyjs/dist/i18n/es.js',
                    'bower/jquery-form/jquery.form.js',
                    'bower/datatables/media/js/jquery.dataTables.min.js',
                    'app/Resources/public/js/jquery.uploadfile.min.js',
                    'bower/bootstrap/dist/js/bootstrap.min.js',
                    'bower/bootbox.js/bootbox.js',
                    'web/bundles/easyadmin/javascript/select2.full.min.js',
                    'bower/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'bower/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js',
                    'bower/moment/min/moment.min.js',
                    'bower/moment/locale/es.js',
                    'bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                    'bower/hinclude/hinclude.js',
                    'bower/jquery.inputmask/dist/jquery.inputmask.bundle.js',
                    'app/Resources/public/js/app.js'
                ],
                dest: 'web/assets/app.js'
            },

            css: {
                src: [
                    'app/Resources/public/less/uploadfile.css',
                    'bower/offline/themes/offline-theme-default.css',
                    'bower/offline/themes/offline-language-spanish-indicator.css',
                    'bower/datatables/media/css/jquery.dataTables.min.css',
                    'bower/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
                    'bower/font-awesome/css/font-awesome.min.css',
                    'web/bundles/easyadmin/stylesheet/select2-bootstrap.min.css',
                    'app.css'
                ],
                dest: 'web/assets/app.css'
            }
        },

        copy: {
            assets: {
                files: [
                    {expand: true, flatten: true, src: ['app/Resources/public/files/*'], dest: 'web/assets/files/', filter: 'isFile'},
                    {expand: true, flatten: true, src: ['app/Resources/public/img/*'], dest: 'web/assets/img/', filter: 'isFile'},
                    {expand: true, flatten: true, src: ['bower/bootstrap/fonts/*'], dest: 'web/assets/fonts/', filter: 'isFile'},
                    {expand: true, flatten: true, src: ['bower/font-awesome/fonts/*'], dest: 'web/assets/fonts/', filter: 'isFile'},
                    {expand: true, flatten: true, src: ['app/Resources/public/fonts/*'], dest: 'web/assets/fonts/', filter: 'isFile'},
                    {expand: true, flatten: true, src: ['app/Resources/public/js/DataTable/*'], dest: 'web/assets/', filter: 'isFile'},
                ]
            }
        },

        watch: {
            // http://stackoverflow.com/questions/16748737
            // echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
            styles: {
                files: ['app/Resources/public/less/*.less'],
                tasks: ['compile:css', 'clean'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },
            scripts: {
                files: ['app/Resources/public/js/*.js'],
                tasks: ['compile:js'],
                options: {
                    spawn: false,
                    livereload: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('compile:css', ['less', 'concat:css']);
    grunt.registerTask('compile:js', ['concat:js']);
    grunt.registerTask('compile:watch', ['default', 'watch']);
    grunt.registerTask('default', ['compile:css', 'compile:js', 'copy:assets', 'clean'])
};
