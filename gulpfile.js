var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');

gulp.task('vendors-js', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/datatables.net/js/jquery.dataTables.js',
        './node_modules/select2/dist/js/select2.js',
        './node_modules/bootstrap-filestyle/src/bootstrap-filestyle.js',
        './bower/spin.js/spin.min.js',
        './bower/underscore/underscore-min.js',
        './bower/underscore.string/dist/underscore.string.min.js',
        './bower/sprintf/dist/sprintf.min.js',
        './bower/twig.js/twig.min.js',
        './bower/parsleyjs/dist/parsley.min.js',
        './bower/parsleyjs/dist/i18n/es.js',
        './bower/jquery-form/jquery.form.js',
        './app/Resources/public/js/jquery.uploadfile.min.js',
        './bower/bootstrap/dist/js/bootstrap.min.js',
        './bower/bootbox.js/bootbox.js',
        './bower/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        './bower/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js',
        './bower/moment/min/moment.min.js',
        './bower/moment/locale/es.js',
        './bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        './bower/hinclude/hinclude.js',
        './bower/jquery.inputmask/dist/jquery.inputmask.bundle.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./web/dist/'));
});

gulp.task('vendors-css', function() {
    return gulp.src([
        './node_modules/bootstrap/dist/css/bootstrap.css',
        './node_modules/select2/dist/css/select2.css',
        './app/Resources/public/less/uploadfile.css',
        './bower/datatables/media/css/jquery.dataTables.min.css',
        './bower/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
        './bower/font-awesome/css/font-awesome.min.css',
    ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./web/dist/'));
});

gulp.task('css', function() {
    return gulp.src('./assets/scss/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./web/dist/'));
});

gulp.task('js', function() {
    return gulp.src([
        './assets/js/**.js'
    ])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./web/dist/'));
});

gulp.task('icons', function() {
    return gulp.src([
        './bower/bootstrap/dist/fonts/**.*',
        './bower/font-awesome/fonts/**.*'
    ])
        .pipe(gulp.dest('./web/fonts/'));
});


gulp.task('images', function() {
    return gulp.src([
        './assets/img/**.*',
    ])
        .pipe(gulp.dest('./web/dist/img/'));
});


gulp.task('default', [
    'vendors-js',
    'vendors-css',
    'icons',
    'css',
    'js',
    'images'
]);