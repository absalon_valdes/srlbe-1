<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class AppKernel extends BaseKernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new JMS\DiExtraBundle\JMSDiExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Vich\UploaderBundle\VichUploaderBundle(),
            new Lexik\Bundle\WorkflowBundle\LexikWorkflowBundle(),
            new Lexik\Bundle\MaintenanceBundle\LexikMaintenanceBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new JavierEguiluz\Bundle\EasyAdminBundle\EasyAdminBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Ivory\OrderedFormBundle\IvoryOrderedFormBundle(),
            new cspoo\Swiftmailer\MailgunBundle\cspooSwiftmailerMailgunBundle(),
            new APY\BreadcrumbTrailBundle\APYBreadcrumbTrailBundle(),
            new Snc\RedisBundle\SncRedisBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
            new Jmikola\WildcardEventDispatcherBundle\JmikolaWildcardEventDispatcherBundle(),
            //new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),

            new Http\HttplugBundle\HttplugBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle(),
            new Bazinga\Bundle\HateoasBundle\BazingaHateoasBundle(),
            new Sg\DatatablesBundle\SgDatatablesBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new Tetranz\Select2EntityBundle\TetranzSelect2EntityBundle(),

            new App\App(),
            new ApiBundle\ApiBundle(),
            new \Tionvel\InteropBundle\TionvelInteropBundle(),
            new \Tionvel\SurveyBundle\TionvelSurveyBundle(),
            new \Tionvel\DeliveryBundle\TionvelDeliveryBundle(),
            new \Tionvel\SuperadminBundle\TionvelSuperadminBundle(),
            new \Tionvel\BiddingBundle\TionvelBiddingBundle(),
            new \Tionvel\WaterBundle\TionvelWaterBundle(),
            new \BE\FAQBundle\BEFAQBundle(),
            new \BE\UniformBundle\BEUniformBundle(),
            new \BE\CoreBundle\BECoreBundle(),
        ];

        if (in_array($this->environment, ['dev', 'qa', 'stage'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            //$bundles[] = new Elao\WebProfilerExtraBundle\WebProfilerExtraBundle();
        }
        
        return $bundles;
    }

    /**
     * @return string
     */
    public function getRootDir()
    {
        return __DIR__;
    }

    /**
     * @return string
     */
    public function getVarDir()
    {
        return dirname($this->getRootDir()).'/var';
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return sprintf('%s/cache/%s', $this->getVarDir(), $this->environment);
    }

    /**
     * @return string
     */
    public function getLogDir()
    {
        return sprintf('%s/logs', $this->getVarDir());
    }

    /**
     * @return string
     */
    public function getConfigDir()
    {
        return sprintf('%s/config', $this->getRootDir());
    }

    /**
     * @param LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->environment.'.yml');
    }

    /**
     * @return array
     */
    public function getKernelParameters()
    {
        return array_merge([
            'kernel.var_dir' => $this->getVarDir(),
            'kernel.config_dir' => $this->getConfigDir()
        ], parent::getKernelParameters());
    }
}
