<?php

namespace App\Notification\Notifier;

use App\Notification\MessageInterface;
use Doctrine\Common\Inflector\Inflector;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class Notifier
 * @package App\Notification\Notifier
 */
abstract class Notifier implements NotifierInterface
{
    /**
     * @Di\Inject("twig")
     */
    public $twig;

    /**
     * @Di\Inject("%notification_templates%")
     */
    public $path;

    /**
     * @Di\Inject("templating")
     */
    public $templating;

    /**
     * @param MessageInterface $message
     * @return string Rendered message
     */
    public function renderBody(MessageInterface $message)
    {
        return $this->twig->render($this->getTemplateName($message), [
            'data' => $message->getData(),
            'message' => $message,
            'uuid' => md5(time().uniqid(md5(rand()), true)),
            'channel' => $this->getName(),
        ]);
    }

    /**
     * @return mixed|string
     */
    public function getName()
    {
        $ref = new \ReflectionObject($this);

        $name = preg_replace('/Notifier$/', '', $ref->getShortName());
        $name = Inflector::tableize($name);

        return $name;
    }

    /**
     * @param MessageInterface $message
     * @return mixed
     * @throws \Exception
     */
    private function getTemplateName(MessageInterface $message)
    {
        $name = str_replace('.reached', '', $message->getSource());

        $templates = [
            sprintf('%s/%s/%s.twig', $this->path, $this->getName(), $name),
            sprintf('%s/%s.twig', $this->path, $name)
        ];

        foreach ($templates as $template) {
            if ($this->templating->exists($template)) {
                return $template;
            }
        }

        throw new \Exception(sprintf('Template for event source "%s" not found (tried %s)', $name, implode(', ', $templates)));
    }
}
