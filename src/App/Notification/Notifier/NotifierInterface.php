<?php

namespace App\Notification\Notifier;

use App\Notification\MessageInterface;

/**
 * Interface NotifierInterface
 * @package App\Notification\Notifier
 */
interface NotifierInterface
{
    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param MessageInterface $message
     * @return mixed
     */
    public function notify(MessageInterface $message);
}
