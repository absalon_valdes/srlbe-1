<?php

namespace App\Notification\Process;

use App\Entity\{
    Requisition
};
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class VcardNotifier
 * @package App\Notification\Process
 * @Di\Service("vcard_notifier")
 */
class VcardNotifier extends ProcessNotifier
{
    public function getData($event)
    {
        /** @var Requisition $requisition */
        $requisition = $event->getSubject();
        $assigne = $requisition->getAssignee();
        $requester = $requisition->getRequester();

        return [
            'requisition' => $requisition,
            'assignee' => $assigne,
            'requester' => $requester
        ];
    }

    public function getLookupTable()
    {
        return [
            'vcard.approver' => ['all', 'requester', 'assignee'],
            'vcard.evaluation' => ['all', 'requester', 'assignee'],
            'vcard.appeal' => ['all', 'requester', 'assignee'],
            'vcard.closed' => ['all', 'assignee', 'requester'],
            'vcard.rejected' => ['all', 'assignee', 'requester'],
            'vcard.deny' => ['all', 'assignee', 'requester']
        ];
    }
}