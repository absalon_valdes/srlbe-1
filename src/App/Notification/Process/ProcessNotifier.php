<?php

namespace App\Notification\Process;

use App\Entity\Requisition;
use App\Entity\User;
use App\Notification\MessageInterface;
use App\Notification\NotificationManager;
use App\Repository\UserRepository;
use JMS\DiExtraBundle\Annotation as Di;
use Lexik\Bundle\WorkflowBundle\Event\StepEvent;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ProcessNotifier
 * @package App\Notification\Process
 */
abstract class ProcessNotifier
{
    /**
     * @var NotificationManager
     * @Di\Inject("notification_manager")
     */
    public $notificator;

    /**
     * @var UserRepository
     * @Di\Inject("user_repo")
     */
    public $userRepo;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @param $event
     * @return array|null
     */
    abstract public function getData($event);

    /**
     * @return array
     */
    abstract public function getLookupTable();

    /**
     * @param $event
     * @param $eventName
     */
    public function sendNotifications($event, $eventName)
    {
        $lookupTable = $this->getLookupTable();

        if (!isset($lookupTable[$eventName])) {
            return;
        }

        $data = $this->getData($event);

        $getSubject = function() use ($eventName,$data){
            if ($data["requisition"]) {
                /** @var Requisition $requisition */
                $requisition = $data["requisition"];
            }else {
                $subject = str_replace('reached', 'event', $eventName);
                $subject = $this->translator->trans($subject);
                return $subject;
            }

            return $this->translator->trans(
                $requisition->getStatus().'.'.
                str_replace('requisition.', '', $requisition->getType()).'.'.
                ($requisition->getContract()->getCentralized() ? 'centralized' : 'descentralized')
            );

        };

        $getChannel = function ($eventName) use ($lookupTable) {
            return $lookupTable[$eventName][0];
        };

        $getProperty = function ($event, $eventName, $index) use ($lookupTable) {
            $data = $this->getData($event);
            $key = $lookupTable[$eventName][$index];

            return is_array($key) ? array_intersect_key($data, array_flip($key)) : $data[$key];
        };

        /** @var MessageInterface $message */
        $message = $this->notificator->createMessage()
            ->setSubject($getSubject())
            ->setSource($eventName)
            ->setData($data)
            ->setChannel($getChannel($eventName))
            ->setSender($getProperty($event, $eventName, 1))
        ;

        $receivers = $getProperty($event, $eventName, 2);
        $receivers = is_array($receivers) ? $receivers : [$receivers];

        // Parche temporal reenvio de correos
        if ($data['requisition']) {
            /** @var Requisition $requisition */
            $requisition = $data['requisition'];
            if ($requisition->isType('requisition.delivery')) {
                $usernames = array_map(function ($user) {
                    if (!$user || !$user instanceof User) {
                        return null;
                    }

                    return in_array($user->getUsername(), ['mortuzar', 'yjara1', 'dtapia2']);
                }, $receivers);

                if (!empty(array_filter($usernames))) {
                    if ($extra = $this->userRepo->findOneBy([
                        'username' => 'dtapia2'
                    ])) {
                        $receivers[] = $extra;
                    }
                }
            }
        }

        foreach ($receivers as $receiver) {
            if (null !== $receiver) {
                $message->addReceiver($receiver);
            }
        }

        try{
            $this->notificator->notify($message);
        }catch (\Exception $e){
//            dump($e);
        }
    }
}
