<?php
/**
 * Created by PhpStorm.
 * User: steven
 * Date: 22-12-16
 * Time: 14:11
 */

namespace App\Notification\Process;

use App\Entity\SpecialRequest;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class SpecialRequestNotifier
 * @package App\Notification\Process
 * @Di\Service("special_request_notifier")
 */
class SpecialRequestNotifier extends ProcessNotifier
{

    public function getData($event)
    {
        /** @var SpecialRequest $request */
        $request = $event->getSubject();
        $assigne = $request->getAssignee();
        $requester = $request->getRequester();

        return [
            'request' => $request,
            'assignee' => $assigne,
            'requester' => $requester
        ];
    }

    public function getLookupTable()
    {
        return [
            'special.request.evaluation'        => ['all', 'requester', 'assignee'],
            'special.request.closed'            => ['all', 'assignee', 'requester'],
            'special.request.rejected'          => ['all', 'assignee', 'requester'],
            'special.request.appealed'          => ['all', 'requester', 'assignee'],
            'special.request.rejected_final'    => ['all', 'assignee', 'requester'],
            'special.request.comment'           => ['all', 'assignee', [ 'requester', 'assignee' ]],
        ];
    }


    public function sendNotifications($event, $eventName)
    {
        $lookupTable = $this->getLookupTable();

        if (!isset($lookupTable[$eventName])) {
            return;
        }

        $data = $this->getData($event);

        $getSubject = function() use ($eventName,$data){
            $request = $data["request"];
            return $this->translator->trans($request->getState().'.special_request.centralized');

        };

        $getChannel = function ($eventName) use ($lookupTable) {
            return $lookupTable[$eventName][0];
        };

        $getProperty = function ($event, $eventName, $index) use ($lookupTable) {
            $data = $this->getData($event);
            $key = $lookupTable[$eventName][$index];

            return is_array($key) ? array_intersect_key($data, array_flip($key)) : $data[$key];
        };

        /** @var MessageInterface $message */
        $message = $this->notificator->createMessage()
            ->setSubject($getSubject())
            ->setSource($eventName)
            ->setData($data)
            ->setChannel($getChannel($eventName))
            ->setSender($getProperty($event, $eventName, 1))
        ;

        $receivers = $getProperty($event, $eventName, 2);
        $receivers = is_array($receivers) ? $receivers : [$receivers];


        foreach ($receivers as $receiver) {
            if (null !== $receiver) {
                $message->addReceiver($receiver);
            }
        }

        try{
            $this->notificator->notify($message);
        }catch (\Exception $e){
//            dump($e);
        }
    }

}