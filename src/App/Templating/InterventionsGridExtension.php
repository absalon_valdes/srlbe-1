<?php

namespace App\Templating;

use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\User;
use App\Helper\UserHelper;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class InterventionsGridExtension
 * @package App\Templating
 *
 * @Di\Service @Di\Tag("twig.extension")
 */
class InterventionsGridExtension extends \Twig_Extension
{
    /**
     * @var \Twig_Environment
     * @Di\Inject("twig")
     */
    public $twig;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;
    
    /**
     * @Di\Inject("%kernel.var_dir%")
     */
    public $varDir;
    
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'interventions_grid';
    }

    /**
     * @inheritDoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('interventions_grid', [$this, 'renderGrid'], ['is_safe' => ['html']])
        ];
    }

    public function renderGrid()
    {
        if (!$code = $this->getOfficeCode()) {
            return '';
        }
                
        return $this->twig->render('interventions_grid.twig', [
            'data' => $this->loadProjectsData($code)
        ]);
    }

    private function getOfficeCode()
    {
        /** @var User $user */
        $user = $this->userHelper->getCurrentUser();
        
        /** @var Employee $employee */
        if (!$employee = $user->getEmployee()) {
            return null;
        }
        
        /** @var Office $office */
        if (!$office = $employee->getOffice()) {
            return null;
        } 
        
        return $office->getCode();
    }

    public function loadProjectsData($code)
    {
        $filename = sprintf('%s/data/intervenciones.xls', $this->varDir);
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($filename);
        $worksheet = $excel->getSheet();
        $toArray = $worksheet ? $worksheet->toArray() : array();
        $toArray = array_filter($toArray, function ($row) use ($code) {
            return (int) $row[2] === $code; 
        });
        
        return array_values($toArray);
    }
}