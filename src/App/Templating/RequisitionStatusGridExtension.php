<?php

namespace App\Templating;

use Behat\Transliterator\Transliterator;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class RequisitionStatusGrid
 * @package App\Templating
 *
 * @Di\Service @Di\Tag("twig.extension")
 */
class RequisitionStatusGridExtension extends \Twig_Extension
{
    /**
     * @var \Twig_Environment
     * @Di\Inject("twig")
     */
    public $twig;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('requisition_status_grid', [$this, 'renderStatusGrid'], ['is_safe' => ['html']])
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'requisition_status_grid';
    }

    /**
     * @param $requisitions
     * @param string|null $viewMoreLink
     * @param string|null $gridTitle
     * @param bool $hideIfEmpty
     * @param string|null $emptyText
     * @param int $dataSlice
     * @param string $titleTag
     * @param string $itemRoute
     * @param string $customTemplate
     * @param bool $hideViewMore
     * @param array $attributes
     * @param string $type
     * @return string
     */
    public function renderStatusGrid(
        $requisitions,
        string $viewMoreLink = null,
        string $gridTitle = null,
        bool $hideIfEmpty = true,
        string $emptyText = null,
        int $dataSlice = 5,
        string $titleTag = 'h3',
        string $itemRoute = 'requisition_detail',
        string $customTemplate = '_status_grid.twig',
        bool $hideViewMore = false,
        array $attributes = [],
        string $type = 'requisition'
    ) {
        /** Normalize data */
        $requisitions = $requisitions ?: [];
        
        $slug = Transliterator::urlize($gridTitle, '_').'_help';
        $help = $this->translator->trans($slug);
        
        // TODO: Parametrize & inject template name
        return $this->twig->render($customTemplate, [
            'grid_title'     => $gridTitle,
            'grid_title_tag' => $titleTag,
            'title_help'     => $help, // !== $slug ? $help : null,
            'empty_text'     => $emptyText,
            'hide_if_empty'  => $hideIfEmpty,
            'requisitions'   => $requisitions,
            'view_more_link' => $viewMoreLink,
            'data_slice'     => $dataSlice,
            'item_route'     => $itemRoute,
            'hide_view_more' => $hideViewMore,
            'attributes'     => $attributes,
            'type'           => $type,
        ]);
    }
}
