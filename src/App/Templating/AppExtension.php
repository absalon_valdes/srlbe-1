<?php

namespace App\Templating;

use App\Entity\Contract;
use App\Entity\Requisition;
use App\Entity\SpecialRequests;
use App\Entity\User;
use App\Helper\SlaCalculator;
use App\Helper\SlaEvaluator;
use App\Helper\SpecialOfficeHelper;
use App\Helper\UserHelper;
use App\Repository\EmployeeRepository;
use App\Repository\MessageRepository;
use App\Repository\RequisitionRepository;
use App\Repository\SpecialRequestsRepository;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("app_extension")
 * @Di\Tag("twig.extension")
 */
class AppExtension extends \Twig_Extension
{
    /**
     * @var MessageRepository
     * @Di\Inject("message_repo")
     */
    public $messages;

    /**
     * @var EmployeeRepository
     * @Di\Inject("employee_repo")
     */
    public $employees;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var SpecialOfficeHelper
     * @Di\Inject("special_office_helper")
     */
    public $specialOfficeHelper;

    /**
     * @var \Twig_Environment
     * @Di\Inject("twig")
     */
    public $twig;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
        ];

        return [
            new \Twig_Function('workflow_indicator', [$this, 'workflowIndicator'], $options),
            new \Twig_Function('get_real_username', [$this, 'getRealUsername'], $options),
            new \Twig_Function('product_is_descentralized', [$this, 'productIsDescentralized'], $options),
            new \Twig_Function('get_notification_count', [$this, 'getNotificationCount'], $options),
            new \Twig_Function('get_cart_items_count', [$this, 'getCartItemsCount'], $options),
            new \Twig_Function('get_requests_items_count', [$this, 'getRequestsItemsCount'], $options),
            new \Twig_Function('get_roles_sanetize', [$this, 'getRolesSanetize'], $options),
            new \Twig_Function('impersonate_user', [$this, 'impersonateUserLink'], $options),
            new \Twig_Function('has_active_vcard_requisitions', [$this, 'hasActiveVcardRequisitions'], $options),
            new \Twig_Function('get_special_office_config', [$this, 'getSpecialOfficeConfig'], $options),
        ];
    }

    public function hasActiveVcardRequisitions(User $user = null)
    {
        $user = $user ?: $this->userHelper->getCurrentUser();
        
        return $this->requisitionRepo->findCountActiveVcards($user);
    }

    public function impersonateUserLink(User $user)
    {
        return $this->twig->render('impersonate_link.twig', [
            'user' => $user
        ]);
    }

    /**
     * @param array $contracts
     * @return bool
     */
    public function productIsDescentralized($contracts)
    {
        $check = function (Contract $c) {
            return !$c->getCentralized();
        };

        if (is_array($contracts)) {
            return (bool)count(array_filter($contracts, $check));
        }

        if ($contracts instanceof Contract) {
            return $check($contracts);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_extension';
    }

    /**
     * @return mixed
     */
    public function getNotificationCount()
    {
        $user = $this->userHelper->getCurrentUser();

        return $this->messages->getUnreadCount($user);
    }

    /**
     * @return mixed
     */
    public function getCartItemsCount()
    {
        return $this->requisitionRepo->getCartItemsCount();
    }

    /**
     * @return mixed
     */
    public function getRequestsItemsCount()
    {
        return $this->requestsRepo->getRequestsItemsCount();
    }

    /**
     * @return mixed
     */
    public function getRealUsername()
    {
        $user = $this->userHelper->getCurrentUser();

        return $user->getUsername();
    }

    /**
     * @param Requisition $requisition
     * @return mixed
     */
    public function workflowIndicator(Requisition $requisition, $text = null)
    {
        return $this->twig->render('requisition/indicator.twig', [
            'requisition' => $requisition,
            'text' => $text,
        ]);
    }

    /**
     * @return mixed
     */
    public function getRolesSanetize()
    {
        $roles = $this->userHelper->getCurrentUser()->getRoles();

        // si el usuario tiene solo un rol no es necesario eliminar los roles heredados.
        if (1 == count($roles)) {
            return $roles;
        }

        if (2 == count($roles) && in_array('ROLE_USER', $roles) && in_array('ROLE_EMPLOYEE', $roles)) {
            return ['ROLE_EMPLOYEE'];
        }

        $rroles = array();
        foreach ($roles as $role) {
            // eliminar los roles heredados si el usuario tiene mas de un rol.
            if ('ROLE_EMPLOYEE' != $role && 'ROLE_USER' != $role) {
                $rroles[] = $role;
            }
        }

        return $rroles;
    }

    public function getSpecialOfficeConfig($config){
        return $this->specialOfficeHelper->getConfig($config);
    }
}
