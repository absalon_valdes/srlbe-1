<?php

namespace App\DependencyInjection;

use App\Workflow\ProcessInstance;
use App\Workflow\ProcessModel;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('app');
        
        $rootNode
            ->children()
                ->arrayNode('products')
                    ->children()
                        ->arrayNode('by_category')
                            ->normalizeKeys(false)
                            ->useAttributeAsKey(true)
                            ->prototype('array')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('type')->end()
                                        ->scalarNode('label')->end()
                                        ->arrayNode('options')
                                            ->prototype('variable')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end() 
                        ->end()
            
                        ->arrayNode('by_product')
                            ->normalizeKeys(false)
                            ->prototype('array')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('type')->end()
                                        ->scalarNode('label')->end()
                                        ->arrayNode('options')
                                            ->prototype('variable')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end() 
                        ->end()
            
                        ->arrayNode('by_group')
                            ->prototype('array')
                                ->children()
                                    ->arrayNode('codes')
                                        ->prototype('scalar')->end()
                                    ->end()
                                    ->arrayNode('fields')
                                        ->normalizeKeys(false)
                                            ->prototype('array')
                                                ->children()
                                                    ->scalarNode('type')->end()
                                                    ->scalarNode('label')->end()
                                                    ->arrayNode('options')
                                                        ->prototype('variable')->end()
                                                    ->end() 
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
            
                    ->end()
                ->end()
            ->end()
        ;
        $this->addNotificationSection($rootNode);
        $this->addWorkflowSection($rootNode);
        $this->addHolidaysSection($rootNode);
        $this->addSpecialOfficesSection($rootNode);

        return $treeBuilder;
    }

    protected function addNotificationSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('notification')
                    ->children()
                        ->scalarNode('sender')->end()
                        ->scalarNode('templates')->end()
                        ->arrayNode('subjects')
                            ->normalizeKeys(false)
                            ->useAttributeAsKey('source')
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    public function addWorkflowSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('workflow')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('process_class')
                            ->defaultValue(ProcessInstance::class)
                        ->end()
                        ->scalarNode('process_model_class')
                            ->defaultValue(ProcessModel::class)
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    public function addHolidaysSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('holidays')
                    ->prototype('scalar')->end()
                ->end()
            ->end()
        ;  
    }


    public function addSpecialOfficesSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('special_offices')
                    ->prototype('array')
                        ->children()
                            ->arrayNode('blocks')
                                ->prototype('scalar')->end()
                            ->end()
                            ->scalarNode('coordinators')->end()
                            ->scalarNode('filter_offices')->end()
                            ->scalarNode('manual')->end()
                            ->scalarNode('hide_vcard')->end()
                    ->end()
                ->end()
            ->end()
        ->end()
        ;
    }
}
