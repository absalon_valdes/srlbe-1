<?php

namespace App\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class RegisterDashboardQueryCompilerPass
 * @package App\DependencyInjection\Compiler
 */
class RegisterDashboardQueryCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('dashboard_query_aggregator')) {
            return;
        }

        /** @var Definition $aggregator */
        $aggregator = $container->getDefinition('dashboard_query_aggregator');

        foreach ($container->findTaggedServiceIds('dashboard.query') as $id => $tags) {
            $aggregator->addMethodCall('addQuery', array(new Reference($id)));
        }
    }
}