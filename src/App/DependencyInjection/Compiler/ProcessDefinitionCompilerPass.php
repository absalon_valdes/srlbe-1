<?php

namespace App\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ProcessDefinitionCompilerPass
 * @package App\DependencyInjection\Compiler
 */
class ProcessDefinitionCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $processClass = $container->getParameter('app.workflow.process_class');
        $processModelClass = $container->getParameter('app.workflow.process_model_class');

        foreach ($container->findTaggedServiceIds('lexik_workflow.process') as $id => $tags) {
            foreach ($tags as $attributes) {
                $alias = $attributes['alias'];

                $definition = new Definition($processClass, [
                    new Reference(sprintf('lexik_workflow.handler.%s', $alias)),
                    $alias,
                    new Reference('doctrine.orm.entity_manager'),
                    $processModelClass
                ]);

                $container->setDefinition(sprintf('workflow.%s', $alias), $definition);
            }
        }
    }
}
