<?php

namespace App\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class AppExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {   
        $config = $this->processConfiguration(
            $this->getConfiguration($configs, $container), 
            $configs
        );

        $workflow = $config['workflow'];
        $container->setParameter('app.workflow.process_class', $workflow['process_class']);
        $container->setParameter('app.workflow.process_model_class', $workflow['process_model_class']);

        $container->setParameter('app.holidays', $config['holidays']);
        $container->setParameter('app.products.config', $config['products']);
        $container->setParameter('app.special_offices', $config['special_offices']);
    }
}
