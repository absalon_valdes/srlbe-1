<?php

namespace App\Command;

use App\Entity\Requisition;
use App\Helper\UserHelper;
use App\Import\ProductImporter;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Workflow\Workflow;

class CloseOnHoldSelfmanagementCommand extends ContainerAwareCommand
{
    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var RequisitionRepository
     */
    public $requisitionRepo;

    /**
     * @var Requisition[]
     */
    public $requisitions;
    /**
     * @var string[]
     */
    public $data;

    /**
     * @var OutputInterface $outG
     */
    public $outG;

    /**
     * @var []
     */
    public $statusMap;

    /**
     * @var ProcessInstance
     */
    protected $workflow;

    /**
     * @var Workflow
     */
    protected $workflowVcard;
    /**
     * @var UserHelper
     */
    protected $userHelper;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('srl:close:onhold:req')
            ->addOption('file', null, InputOption::VALUE_OPTIONAL)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getOption('file');

        $this->outG = $output;

        if (null === $source) {
            throw new \InvalidArgumentException;
        }

        if (null !== $source) {
            $this->data = $this->processSource($source);
        }

        /** @var ContainerInterface $container */
        $container = $this->getContainer();

        /** @var LoggerInterface $logger */
        $this->logger = $container->get("logger");

        /** @var EntityManager $em **/
        $this->em = $container->get("doctrine.orm.default_entity_manager");

        /** @var RequisitionRepository requisitionRepo **/
        $this->requisitionRepo = $container->get('requisition_repo');

        /** @var ProcessInstance workflow **/
        $this->workflow = $container->get('workflow.product');

        /** @var Workflow workflowVcard */
        $this->workflowVcard = $container->get('workflow.vcards');

        /** @var UserHelper userHelper */
        $this->userHelper = $container->get('user_helper');

        $this->requisitions = $this->requisitionRepo->findByNumber(array_keys($this->data));

        $this->updateRequisition();
    }
    /**
     * @param $filename
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    private function processSource($filename)
    {
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($filename);
        $worksheet = $excel->getSheet();
        $toArray = $worksheet ? $worksheet->toArray() : array();
        array_shift($toArray);

        $toArray = array_filter($toArray, function ($item) {
            return !empty($item[0]);
        });

        $requisitions = [];
        foreach (array_filter($toArray) as $item) {

            if (!isset($nuevos[$item[0]])) {
                $nuevos[$item[0]] = [];
            }

            $requisitions[$item[0]] = [
                'req_number' => $item[0],
                'comment'    => $item[1]
            ];
        }

        return array_filter($requisitions);
    }

    private function updateRequisition()
    {
        foreach ($this->requisitions as $key => $requisition){
            $this->outG->writeln("{$key} -> {$requisition->getNumber()}-{$requisition->getStatus()}");
            $closedInfo = ["comment" => $this->data[$requisition->getNumber()]["comment"]];
            $this->userHelper->impersonateUser($requisition->getRequester()->getUsername());
            if($requisition->getStatus() == 'product.completed' || $requisition->getStatus() == 'product.draft') {
                continue;
            }
            if($requisition->getStatus() !== 'product.closed' && $requisition->getStatus() !== 'product.timeout'){
                $state = $this->workflow->proceed($requisition, 'close');
                if ($state->getSuccessful()) {
                    $this->logger->info("{$key}. Cerrando la solicitud: {$requisition->getNumber()}");
                    $requisition->setClosedAt($state->getCreatedAt());
                    $requisition->addMetadata('closed',$closedInfo);
                    $this->requisitionRepo->save($requisition);
                }
            }else{
                $requisition->addMetadata('closed',$closedInfo);
                $this->requisitionRepo->save($requisition);
            }

        }
    }
}