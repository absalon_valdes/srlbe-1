<?php

namespace App\Command;

use App\Entity\Category;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Dump\Container;

class CreateCategoryCommand extends ContainerAwareCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('create-cat')
            ->addArgument('name', InputArgument::REQUIRED)
            ->addArgument('parent-id', InputArgument::REQUIRED)
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $parentId = $input->getArgument('parent-id');
        $name = $input->getArgument('name');
        
        /** @var Container $c */
        $c = $this->getContainer();
        
        /** @var EntityManager $em */
        $em = $c->get('doctrine.orm.entity_manager');
        
        $parent = $em->getRepository(Category::class)
            ->find($parentId);
        
        $child = new Category();
        $child->setName($name);
        $child->setParent($parent);
        
        $em->persist($child);
        $em->flush($child);
    }

}