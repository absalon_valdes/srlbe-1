<?php

namespace App\Command;

use App\Entity\Requisition;
use App\Entity\SLA;
use App\Helper\SlaCalculator;
use App\Helper\UserHelper;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\EntityManager;
use DoctrineExtensions\Query\Mysql\Date;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Workflow\Workflow;

class CloseRequisitionsCommand extends ContainerAwareCommand
{


    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var RequisitionRepository
     */
    public $requisitionRepo;

    /**
     * @var Requisition[]
     */
    public $requisitions;

    /**
     * @var []
     */
    public $statusMap;

    /**
     * @var ProcessInstance
     */
    protected $workflow;

    /**
     * @var Workflow
     */
    protected $workflowVcard;
    /**
     * @var UserHelper
     */
    protected $userHelper;

    /**
     * @var SlaCalculator
     */
    public $slaCalculator;

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('srl:close:requisition');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var Container $container */
        $container = $this->getContainer();

        /** @var LoggerInterface $logger */
        $this->logger = $container->get("logger");

        /** @var SlaCalculator $slaCalculator */
        $this->slaCalculator = $container->get('sla_calculator');

        /** @var EntityManager $em **/
        $this->em = $container->get("doctrine.orm.default_entity_manager");

        /** @var RequisitionRepository requisitionRepo **/
        $this->requisitionRepo = $container->get('requisition_repo');

        /** @var ProcessInstance workflow **/
        $this->workflow = $container->get('workflow.product');

        /** @var UserHelper userHelper */
        $this->userHelper = $container->get('user_helper');
        $statusToSearch = [ProductStatus::EVALUATED,ProductStatus::REVIEWED,ProductStatus::REJECTED];

        $this->requisitions = $this->requisitionRepo->findBy(['status' => $statusToSearch],['createdAt' => 'ASC']);

        $this->closeRequisitions($output);

    }

    private function closeRequisitions(OutputInterface $output){
        $closed = 0;
        foreach ($this->requisitions as $requisition){
            if($this->isCloseable($requisition)){
                ++$closed;
                $this->logger->info("Cerrando la solicitud: {$requisition->getNumber()}");
                $this->userHelper->impersonateUser($requisition->getRequester()->getUsername());

                $state = $this->workflow->proceed($requisition, 'expire');
                if ($state->getSuccessful()) {
                    $requisition->setClosedAt($state->getCreatedAt());
                    $this->requisitionRepo->save($requisition);
                }

            }
        }
        $this->logger->info("FIN Cierre de solicitudes [{$closed}]");
    }

    private function isCloseable(Requisition $requisition){
        $now = new \DateTime();
        $sla_test = SLA::create(16,SLA::BUSINESS_HOUR);
        $this->logger->info("Evaluando la solicitud: {$requisition->getNumber()}|{$requisition->getStatus()}|{$requisition->getType()}");

        $transition = $requisition->getLastTransition();
        $sla = $this->slaCalculator->getSla($sla_test,$transition->getDatetime());
        if($sla < $now) {
            return true;
        }

        return false;
    }

}