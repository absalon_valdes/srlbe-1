<?php 

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportEmployeesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('srl:import:employees')
            ->addArgument('file', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!file_exists($file = $input->getArgument('file'))) {
            throw new \InvalidArgumentException('Fixtures file not found');
        }

        $container = $this->getContainer();
        $processor = $container->get('excel_batch_processor');
        $importer = $container->get('employees_importer');

        $processor->process($file, 'employees_', [$importer, 'import']);
    }
}