<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\LockHandler;

class RunQueueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('queue:run');
    }
   
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lock = new LockHandler('queue_run_id');
        if (!$lock->lock()) {
            $output->writeln('This command is already running in another process.');
            return 0;
        }
        $this->getContainer()->get('queue')->run();

        $lock->release();
    }
}