<?php

namespace App\Command;

use App\Entity\Requisition;
use App\Helper\SlaCalculator;
use App\Helper\UserHelper;
use App\Import\ProductImporter;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Workflow\Workflow;
use Symfony\Component\Translation\TranslatorInterface;

class ListRequisitionsInfoCommand extends ContainerAwareCommand
{
    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var RequisitionRepository
     */
    public $requisitionRepo;

    /**
     * @var Requisition[]
     */
    public $requisitions;
    /**
     * @var string[]
     */
    public $data;

    /**
     * @var OutputInterface $outG
     */
    public $outG;

    /**
     * @var []
     */
    public $statusMap;

    /**
     * @var ProcessInstance
     */
    protected $workflow;

    /**
     * @var Workflow
     */
    protected $workflowVcard;
    /**
     * @var UserHelper
     */
    protected $userHelper;


    /**
     * @var SlaCalculator
     */
    public $slaCalculator;

    /**
     * @var TranslatorInterface
     */
    public $translator;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('srl:list:req')
            ->addOption('file', null, InputOption::VALUE_OPTIONAL)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getOption('file');

        $this->outG = $output;

        if (null === $source) {
            throw new \InvalidArgumentException;
        }

        if (null !== $source) {
            $this->data = $this->processSource($source);
        }

        /** @var ContainerInterface $container */
        $container = $this->getContainer();

        /** @var LoggerInterface $logger */
        $this->logger = $container->get("logger");

        /** @var EntityManager $em **/
        $this->em = $container->get("doctrine.orm.default_entity_manager");

        /** @var RequisitionRepository requisitionRepo **/
        $this->requisitionRepo = $container->get('requisition_repo');

        /** @var ProcessInstance workflow **/
        $this->workflow = $container->get('workflow.product');

        /** @var Workflow workflowVcard */
        $this->workflowVcard = $container->get('workflow.vcards');

        /** @var SlaCalculator $slaCalculator */
        $this->slaCalculator = $container->get('sla_calculator');

        /** @var UserHelper userHelper */
        $this->userHelper = $container->get('user_helper');

        $this->translator = $container->get('translator');

        $this->requisitions = $this->requisitionRepo->findByNumber(array_keys($this->data));

        $this->updateRequisition();
    }
    /**
     * @param $filename
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    private function processSource($filename)
    {
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($filename);
        $worksheet = $excel->getSheet();
        $toArray = $worksheet ? $worksheet->toArray() : array();

        $toArray = array_filter($toArray, function ($item) {
            return !empty($item[0]);
        });

        $requisitions = [];
        foreach (array_filter($toArray) as $item) {

            if (!isset($nuevos[$item[0]])) {
                $nuevos[$item[0]] = [];
            }

            $requisitions[$item[0]] = [
                'req_number' => $item[0],
//                'comment'    => $item[1]
            ];
        }

        return array_filter($requisitions);
    }

    private function updateRequisition()
    {
        $this->outG->writeln("Numero solicitud,Fecha Inicio,Fecha estimada,Fecha entrega proveedor, fecha fin / cierre sistema, Estado final,En SLA");

        $getRequisitionStateString = function ($state, Requisition $requisition){
            $cent = $requisition->getContract()->getCentralized();
            $type = $requisition->getType();

            return $this->translator->trans(
                $state.'.'.str_replace('requisition.', '', $type).'.'.($cent ? 'centralized' : 'descentralized')
            );
        };


        foreach ($this->requisitions as $key => $requisition){
            $workflowId = Uuid::uuid5(Uuid::NAMESPACE_DNS, Requisition::class.$requisition->getId())->toString();
            $steps = $this->getRequisitionHistory($workflowId);
            $startDates = $this->searchStateDates($steps,ProductStatus::APPROVED);
            $clonedSteps = $steps;

            $estimateSLA = $this->slaCalculator->getEstimate(
                $startDates['created_at'],
                $requisition->getSLA(ProductStatus::DELIVERED),
                $requisition->getSLA(ProductStatus::APPROVED),
                $requisition->getSLA(ProductStatus::CREATED)
//                ,$requisition->getSLA(ProductStatus::CONFIRMED)
            );

            $lastStep = end($clonedSteps);
            $certification = $this->searchStateDates($steps,ProductStatus::REVIEWED);
            $inSLA = $certification !== null ? $certification['created_at'] <= $estimateSLA : true;
            $end = $this->searchStateDates($steps,$lastStep['process_step_full']);
            $inSlaText = $inSLA ? 'Dentro SLA' : 'Fuera SLA';
            $certDate = $certification ? $certification['created_at']->format('d-m-Y H:i:s') : 'No aplica';
            $lastStateString = $getRequisitionStateString($lastStep['process_step_full'],$requisition);
            $this->outG->writeln("{$requisition->getNumber()},{$startDates['created_at']->format('d-m-Y H:i:s')},{$estimateSLA->format('d-m-Y H:i:s')},{$certDate},{$end['created_at']->format('d-m-Y H:i:s')},{$lastStateString},{$inSlaText}");

        }
    }

    private function getRequisitionHistory($wId){
        return  $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('s.workflow_identifier as process_uid')
            ->addSelect('s.process_name as process_name')
            ->addSelect('s.step_name as process_step')
            ->addSelect('concat_ws(".", s.process_name, s.step_name) as process_step_full')
            ->addSelect('s.created_at as process_created_at')
            ->addSelect('s.finished_at as process_finished_at')
            ->from('workflow_state', 's')
            ->where('s.successful <> 0')
            ->andWhere('s.workflow_identifier = :wId')
            ->andWhere("s.step_name <> 'draft'")
            ->setParameter('wId',$wId)
            ->orderBy('s.created_at','ASC')
            ->execute()
            ->fetchAll( \PDO::FETCH_ASSOC)
            ;
    }

    private function getWorkflowDates($step){
        $mysqlDatetimeToObject = function ($datetime,$format = 'Y-m-d H:i:s') {
            if(!$datetime){
                return null;
            }
            return \DateTime::createFromFormat($format, $datetime);
        };

        return [
            'created_at' => $mysqlDatetimeToObject($step['process_created_at']),
            'finished_at' => $mysqlDatetimeToObject($step['process_finished_at']),
        ];
    }

    private function searchStateDates($steps,$stepName){

        foreach ($steps as $step){
            if($step['process_step_full'] === $stepName){
                return $this->getWorkflowDates($step);
            }
        }
        return null;
    }
}