<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportPepsCommand extends ContainerAwareCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('srl:import:peps')
            ->addArgument('file', InputArgument::REQUIRED)
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!file_exists($filename = $input->getArgument('file'))) {
            throw new \InvalidArgumentException('Data file not found.');
        }
        
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($filename);

        $peps = [];
        foreach ($excel->getAllSheets() as $sheet) {
            $title = strtolower($sheet->getTitle());
            $key = 'main';
            
            if (is_int(stripos($title, 'titular'))) {
                $key = 'main';
            } elseif (is_int(stripos($title, 'relacionado'))) {
                $key = 'related';
            } elseif (is_int(stripos($title, 'banco'))) {
                $key = 'bank';
            }

            $pepsData = $sheet->toArray();
            array_shift($pepsData);
            $rutKey = ($key === 'related') ? 6 : 0;

            foreach ($pepsData as $pep) {
                $peps[$key][(int) $pep[$rutKey]] = implode(',', $pep);
            }
        }
        
        $varDir = $this->getContainer()->getParameter('kernel.var_dir');
        $pepCache = sprintf('%s/data/peps.php.cache', $varDir);

        file_put_contents($pepCache, sprintf('<?php return %s;', var_export($peps, true)));
    }
}