<?php

namespace App\Command;

use App\Entity\Contract;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Helper\UserHelper;
use App\Model\Transition;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\DateTime;

class FixRequisitionsWithoutApproverCommand extends ContainerAwareCommand
{
    /**
     * @var ProcessInstance
     */
    protected $workflow;

    protected function configure()
    {
        $this->setName('fix-requisitions-without-approver');
        $this->setDescription("Comando que arregla solicitudes que se saltaron el evaluador por el producto y contrato estar marcado como no centralizado por error");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Container $container */
        $container = $this->getContainer();

        /** @var ProcessInstance workflow **/
        $this->workflow = $container->get('workflow.product');

        /** @var EntityManager $em */
        $em = $container->get('doctrine.orm.entity_manager');

        /** @var UserHelper $userHelper */
        $userHelper = $container->get('user_helper');

//        $helper = $this->getHelper('question');

        $qb = $em->createQueryBuilder();
        $qb->select('r')
            ->from(Requisition::class,'r')
            ->join(Contract::class,'c',Join::WITH,$qb->expr()->eq('r.contract', 'c'))
            ->join(Product::class,'p',Join::WITH,$qb->expr()->eq('c.product', 'p'))
            ->where($qb->expr()->andX(
                $qb->expr()->in('r.number', ['42749','42740']))//Agregar numero de solicitudes dañadas
//                $qb->expr()->in('r.number', ['42879','42863','42834','42831','42825','42822','42821','42805','42804','42749','42740','42739','42728','42726','42710','42680','42666','42631','42615','42599','42557','42510','42481','42480','42463','42462','42461','42460','42397','42367','42366','42350','42316','42312','42307','42301','42299','42293','42292','42291','42290','42284','42283','42282','42281','42215','42208','42206','42205','42204','42203','42201','42185','42184','42183','42181','42180','42178','42160','42159','42152','42125','42116','42115','42106','42092','42073','42071','42069','42050','42048','42035','42000','41921','41898','41863','41862','41821','41735','41706','41652','41649','41577','41573','41570','41561','41559','41558','41551','41545','41544','41525','41523','41515','41477','41476','41468','41430','41425','41349','41348','41340','41326','41325','41322','41320','41318','41309','41305','41286','41281','41244','41241','41234','41203','41147','41146','41142','41140','41139','41137','41133','41048','41025','41023','41013','41012','40997','40981','40972','40971','40970','40947','40946','40942','40940','40937','40922','40917','40916','40914','40913','40912','40911','40869','40868','40867','40865','40749','40746','40745','40687','40680','40676'/*,'40675'*/,'40673','40637','40079','38834','38755','37786','37273','37141','35728']))//Agregar numero de solicitudes dañadas
            )
//            ->andWhere('r.createdAt > :date')
//            ->andWhere('r.status = :status')
//            ->setParameters([
////                'date'=>\DateTime::createFromFormat('d-m-Y','19-07-2017'),
//                'status'=>ProductStatus::APPROVED
//            ])
        ;

        $requisitions = $qb->getQuery()->getResult();

        /** @var Requisition $requisition */
        foreach ($requisitions as $requisition) {
            $output->writeln('Modificando solicitud '.$requisition->getNumber().'['.$requisition->getStatus().']...');
            $requisition->setStatus(ProductStatus::CREATED);
            $requisition->setAssignee($requisition->getProduct()->getApprover());
            $metadata = $requisition->getMetadata();
            if(array_key_exists('declined',$metadata)){
                unset($metadata["declined"]);
            }
            if(array_key_exists('decline',$metadata)){
                unset($metadata["decline"]);
            }
            $requisition->setMetadata($metadata);

            /** @var Transition $transition */
            $requisition->setTransitions(null);

            $oid = Uuid::uuid5(Uuid::NAMESPACE_DNS, Requisition::class.$requisition->getId())->toString();
            $sql = 'delete from workflow_state where workflow_identifier = ? and step_name <> ?';
            $em->getConnection()->executeQuery($sql,[$oid,'draft']);
            $em->persist($requisition);
            $em->flush();
            $userHelper->impersonateUser($requisition->getRequester()->getUsername());
            $this->workflow->proceed($requisition,'create');
        }
    }
}
