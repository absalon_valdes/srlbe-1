<?php

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

class TestCommand extends ContainerAwareCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('test');
    }
    
    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Container $c */
        $c = $this->getContainer();
        
        /** @var Connection $em */
        $pdo = $c->get('database_connection');
    
        $codSucursal  = 'C00009646-9034';
        
        $baseQuery = $pdo->createQueryBuilder()
            ->select('c.id')
            ->from('contracts', 'c')
            ->leftJoin('c', 'offices', 'o', 'o.id = c.office_id')
            ->leftJoin('c', 'suppliers', 's', 's.id = c.supplier_id')
            ->leftJoin('c', 'products', 'p', 'p.id = c.product_id')
        ;
        
        $rutProveedor = '9251728-4';
        $codProductos = ['MB-010', 'MB-011', 'MB-012', 'MB-013', 'MB-014', 'MB-015', 'MB-018', 'MB-019', 'MB-021'];
        
        $queryA = (clone $baseQuery)
            ->where('s.rut = :rut_proveedor')
            ->andWhere('p.code in (:cod_productos)')
            ->andWhere('o.code = :cod_sucursal')
            ->setParameter('rut_proveedor', $rutProveedor)
            ->setParameter('cod_productos', $codProductos, Connection::PARAM_STR_ARRAY)
            ->setParameter('cod_sucursal', $codSucursal)
        ;
    
        $codProductos2 = ['MGE-010', 'MGE-011', 'MGE-014', 'MGE-15'];
        
        $queryB = (clone $baseQuery)
            ->where('p.code in (:cod_productos)')
            ->andWhere('o.code = :cod_sucursal')
            ->setParameter('cod_productos', $codProductos2, Connection::PARAM_STR_ARRAY)
            ->setParameter('cod_sucursal', $codSucursal)
        ;
    
        $rutProveedores = ['76298180-7', '90960000-6'];
        $codProductos3 = ['MMC-010', 'MMC-011', 'MMC-013', 'MMC-014', 'MMC-015', 'MMC-016'];
        
        $queryC = (clone $baseQuery)
            ->where('p.code in (:cod_productos)')
            ->andWhere('s.rut in (:rut_proveedores)')
            ->andWhere('o.code = :cod_sucursal')
            ->setParameter('cod_productos', $codProductos3, Connection::PARAM_STR_ARRAY)
            ->setParameter('rut_proveedores', $rutProveedores, Connection::PARAM_STR_ARRAY)
            ->setParameter('cod_sucursal', $codSucursal)
        ;
        
        $rutProveedores2 = ['76844920-1', '76151806-2', '76165285-0'];
        $codProductos4 = ['ME-01', 'ME-02', 'ME-03', 'ME-04', 'ME-05', 'ME-06', 'ME-07'];
        
        $queryD = (clone $baseQuery)
            ->where('p.code in (:cod_productos)')
            ->andWhere('s.rut in (:rut_proveedores)')
            ->andWhere('o.code = :cod_sucursal')
            ->setParameter('cod_productos', $codProductos4, Connection::PARAM_STR_ARRAY)
            ->setParameter('rut_proveedores', $rutProveedores2, Connection::PARAM_STR_ARRAY)
            ->setParameter('cod_sucursal', $codSucursal)
        ;
    
        $rutProveedores3 = ['76844920-1', '76151806-2', '76165285-0'];
        $codProductos5 = ['MEC-01', 'MEC-02', 'MEC-03', 'MEC-04', 'MEC-05'];
    
        $queryE = (clone $baseQuery)
            ->where('p.code in (:cod_productos)')
            ->andWhere('s.rut in (:rut_proveedores)')
            ->andWhere('o.code = :cod_sucursal')
            ->setParameter('cod_productos', $codProductos5, Connection::PARAM_STR_ARRAY)
            ->setParameter('rut_proveedores', $rutProveedores3, Connection::PARAM_STR_ARRAY)
            ->setParameter('cod_sucursal', $codSucursal)
        ;
        
        $ids = [];
        $ids = array_merge($ids, $queryA->execute()->fetchAll(\PDO::FETCH_COLUMN));
        $ids = array_merge($ids, $queryB->execute()->fetchAll(\PDO::FETCH_COLUMN));
        $ids = array_merge($ids, $queryC->execute()->fetchAll(\PDO::FETCH_COLUMN));
        $ids = array_merge($ids, $queryD->execute()->fetchAll(\PDO::FETCH_COLUMN));
        $ids = array_merge($ids, $queryE->execute()->fetchAll(\PDO::FETCH_COLUMN));
        
        $stmt = $pdo->executeQuery('update contracts set expired = 1 where id in (?)', [$ids], [Connection::PARAM_INT_ARRAY]);
        $total = $stmt->rowCount();
        
        $output->writeln('Total: '.$total);
    }
    
}