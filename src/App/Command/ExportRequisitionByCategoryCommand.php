<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\DateTime;

class ExportRequisitionByCategoryCommand extends ContainerAwareCommand
{
    /**
     * @var LoggerInterface
     * @Di\Inject("logger")
     */
    public $logger;

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('export:requisition:bycategory')
            ->addOption('from',null,InputOption::VALUE_OPTIONAL)
            ->addOption('slug', null,InputOption::VALUE_OPTIONAL);
    }
    
    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try{
            $c = $this->getContainer();
            $env = $c->getParameter('kernel.environment');
            $this->logger = $c->get('logger');
            $now = new DateTime();
            $from = $input->getOption('from') ?? $now->format('d-m-Y');
            $slug =  $input->getOption('slug') ?? null;
            $reportsDir = sprintf('%s/categories', $c->getParameter('kernel.var_dir'));

            if (!@mkdir($reportsDir, 0777, true) && !is_dir($reportsDir)) {
                throw new \RuntimeException(sprintf(
                    'Cannot create folder %s', $reportsDir
                ));
            }

            $normalized = str_replace(['-','/','productos'],'',$slug);

            $filename = sprintf('%s/reporte-bi-%s-%s-%s.csv', $reportsDir, $normalized, date('YmdHis'), $env);
            $c->get('requisition.bycategory.exporter')->export($slug,$from)->getFile($filename);
        }
        catch (\Exception $e) {
            $this->logger->error("REPORTE: {$e->getMessage()}");
        }


    }
}