<?php

namespace App\Command;

use App\Entity\Employee;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Dump\Container;

class NormalizeSpecialCharsCommand extends ContainerAwareCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('normalize:special:chars')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $c = $this->getContainer();
        $em = $c->get('doctrine')->getManager();
        $employees = $em->getRepository('App:Employee')->findAll();

        /** @var Employee $emp */
        foreach ($employees as $emp) {
            $flag = false;
            $fields = [
                "setName"=>$emp->getName(),
                "setLastName"=>$emp->getLastName(),
                "setSecondLastName"=>$emp->getSecondLastName()
            ];
            foreach($fields as $key=>$field) {
                if($newValue = $this->replaceSpecialChar($field,'Ñ','ñ')) {
                    $emp->$key($newValue);
                    $flag = true;
                    $output->writeln("update ".$emp->getRut()." - ".$field." => ".$newValue);
                }
            }

            if($flag) {
                $em->flush();
            };
        }
    }

    function replaceSpecialChar($string,$char,$replace) {

        $flag = false;
        $words = explode(" ",$string);
        foreach($words as $key=>$word) {
            $withoutFirst = substr($word,1);
            if(strpos($withoutFirst,$char)!=0) {
                $words[$key] = $string[0] . str_replace($char, $replace, $withoutFirst);
                $flag = !$flag;
            }
        }
        return $flag ? ucwords(strtolower(implode(" ",$words))) : $flag;
    }

}