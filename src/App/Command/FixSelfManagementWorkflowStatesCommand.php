<?php

namespace App\Command;

use App\Entity\Requisition;
use App\Entity\SLA;
use App\Model\Transition;
use App\Repository\RequisitionRepository;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Ramsey\Uuid\Uuid;

class FixSelfManagementWorkflowStatesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('fix-self-management');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Container $c */
        $c = $this->getContainer();

        /** @var RequisitionRepository $repo */
        $repo = $c->get('requisition_repo');

        /** @var EntityManager $em */
        $em = $c->get('doctrine')->getManager();

        /** @var Connection $db */
        $db = $c->get('database_connection');

        /** @var Requisition[] $toChange */
        $toChange = $repo->createQueryBuilder('r')
            ->where('r.type in (:type)')
            ->setParameter('type', 'requisition.self_management')
            ->getQuery()
            ->getResult()
        ;

        $getWorkFlowHistory = function($id) use ($db) {
            $qwfs = $db->executeQuery("select * from workflow_state where workflow_identifier = '".$id."' ORDER BY created_at ASC ");
            return $qwfs->fetchAll();
        };

        $getCreatedState = function ($states){
            foreach ($states as $state){
                if($state["step_name"] == 'created'){
                    return $state;
                }
            }
            return;
        };
        
        $toUpdate = [];
        foreach ($toChange as $item) {

            $oid = Uuid::uuid5(Uuid::NAMESPACE_DNS, Requisition::class.$item->getId())->toString();
            $states = $getWorkFlowHistory($oid);

            if(!$createdState = $getCreatedState($states)){
                continue;
            }

            $newT = Transition::create($item->getRequester()->getUsername(),ProductStatus::APPROVED);
            $newT->setDatetime(new \DateTime($createdState["created_at"]));
            $item->setTransitions([]);
            $item->addTransition($newT);
            if($item->getStatus() == ProductStatus::CREATED){
                $item->setStatus(ProductStatus::APPROVED);
            }
            $toUpdate[$item->getId()] = $createdState;
        }
        $toUpdate = array_filter($toUpdate);
        $output->writeln(sprintf('Actualizando Solicitudes...%s', $total = count($toUpdate)));
        $progress = new ProgressBar($output, $total);
        $progress->setFormat('very_verbose');

        $em->getConnection()->beginTransaction();
        try {
            foreach ($toUpdate as  $data) {
                $db->update('workflow_state', ['step_name' => 'approved'], ['id' => $data["id"]]);
                $progress->advance();
            }
            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            $output->writeln($e->getMessage());
        }
        
        $progress->finish();
        $output->writeln(['', 'Solicitudes actualizadas']);
    }
}