<?php

namespace App\Command;

use App\Entity\Requisition;
use App\Helper\UserHelper;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\EntityManager;
use DoctrineExtensions\Query\Mysql\Date;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Workflow\Workflow;

class FixClosedRequisitionsCommand extends ContainerAwareCommand
{


    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var RequisitionRepository
     */
    public $requisitionRepo;

    /**
     * @var Requisition[]
     */
    public $requisitions;

    /**
     * @var []
     */
    public $statusMap;

    /**
     * @var ProcessInstance
     */
    protected $workflow;

    /**
     * @var Workflow
     */
    protected $workflowVcard;
    /**
     * @var UserHelper
     */
    protected $userHelper;

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('srl:fix:close:requisition');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var Container $container */
        $container = $this->getContainer();

        /** @var LoggerInterface $logger */
        $this->logger = $container->get("logger");

        /** @var EntityManager $em **/
        $this->em = $container->get("doctrine.orm.default_entity_manager");

        /** @var RequisitionRepository requisitionRepo **/
        $this->requisitionRepo = $container->get('requisition_repo');

        /** @var ProcessInstance workflow **/
        $this->workflow = $container->get('workflow.product');

        /** @var Workflow workflowVcard */
        $this->workflowVcard = $container->get('workflow.vcards');

        /** @var UserHelper userHelper */
        $this->userHelper = $container->get('user_helper');
        $statusToSearch = [ProductStatus::TIMEOUT];

        $this->requisitions = $this->requisitionRepo->findBy(['status' => $statusToSearch],['createdAt' => 'ASC']);
        $this->openRequisitions($output);

    }

    private function openRequisitions(OutputInterface $output){
        $closed = 0;
        $test = [];
        foreach ($this->requisitions as $requisition){
            if($this->isOpenable($requisition)){
                ++$closed;
                $this->logger->info("Abriendo la solicitud: {$requisition->getNumber()}");
                $test[] = $requisition->getNumber();
                $this->userHelper->impersonateUser($requisition->getRequester()->getUsername());
                $states = $this->workflow->getHistory($requisition);
                $lastTransition = $requisition->getLastTransition();
                foreach ($states as $key => $state){
                    $trans = str_replace ('product.', '',  $lastTransition->getStatus() );
                    if(strpos($state->getStepName(), $trans) !== false && $key >= (count($states) - 2)){
                        $state->setFinishedAt(NULL);
                    }
                    if(strpos($state->getStepName(), 'timeout') !== false){
                        $this->em->remove($state);

                    }
                }
                $requisition->setClosedAt(NULL);
                $requisition->setStatus($lastTransition->getStatus());
            }
            $this->em->flush();
        }
        $opened =  implode(",", $test);
        $this->logger->info("FIN Abriendo de solicitudes [{$closed}]");
        $this->logger->info("Se abrieron las solicitudes ({$opened})");
    }

    private function isOpenable(Requisition $requisition)
    {
//        $now = new \DateTime();
        //if fecha de cierre es mayor o igual a (fecha indicada) o son las que estaban es estado X
        if($requisition->getLastTransition()->getStatus() == ProductStatus::CONFIRMED){
            return true;
        }
        return false;
    }

}