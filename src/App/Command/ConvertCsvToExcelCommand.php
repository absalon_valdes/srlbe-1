<?php

namespace App\Command;

use App\Entity\Requisition;
use App\Helper\UserHelper;
use App\Import\ProductImporter;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Workflow\Workflow;

class ConvertCsvToExcelCommand extends ContainerAwareCommand
{
    /**
     * @var LoggerInterface
     */
    public $logger;
    /**
     * @var string
     */
    public $env;

    /**
     * @var OutputInterface $outG
     */
    public $outG;

    /**
     * @var $excelFactory
     * @Di\Inject("phpexcel")
     */
    public $excelFactory;

    /**
     * @var string
     * @Di\Inject("%kernel.cache_dir%")
     */
    public $varDir;

    /**
     * @var string
     */
    public $finalDir;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('srl:convert:csv')
            ->addOption('file', null, InputOption::VALUE_OPTIONAL)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getOption('file');

        $this->outG = $output;

        if (null === $source || !(file_exists($source))) {
            throw new \InvalidArgumentException;
        }

        /** @var ContainerInterface $container */
        $container = $this->getContainer();

        /** @var LoggerInterface $logger */
        $this->logger = $container->get("logger");

        $this->env = $container->getParameter("kernel.environment");

        $this->varDir = $container->getParameter("kernel.cache_dir");
        $cacheDir = $this->varDir.'/exporter_cache';
        $cacheFilename = $cacheDir.'/'.$this->getLastFileName($source);
        $time = $this->getTimeFile($source);

        if (!@mkdir($cacheDir, 0777, true) && !is_dir($cacheDir)) {
            throw new \RuntimeException(sprintf('Cannot create folder %s', $cacheDir));
        }

        $reader = \PHPExcel_IOFactory::createReader('CSV');
        $excel = $reader->load($source);

        $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $writer->save($cacheFilename);
        touch($cacheFilename, $time->getTimestamp());
        rename($cacheFilename,$container->getParameter("kernel.var_dir").'/reports/'.$this->getLastFileName($source,true));

    }

    function getFileName($source){
        $array = explode('/',$source);
        $last = end($array);
        $last = explode('.',$last);
        return $last[0];
    }

    function getTimeFile($source){
        $filename = $this->getFileName($source);
        $names = explode('-',$filename);
        $time = end($names);
        $createdAt = \DateTime::createFromFormat ( 'YmdHi' , $time );
        return $createdAt;
    }

    function getLastFileName($source,$addSeconds = false){
        $time = $this->getTimeFile($source);
        $timeName = $time->format('YmdHi');
        if($addSeconds){
            $timeName = $time->format('YmdHis');
        }
        return 'reporte-bi-general-'.$timeName.'-'.$this->env.'.xlsx';
    }
}