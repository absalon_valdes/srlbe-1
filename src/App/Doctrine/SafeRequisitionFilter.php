<?php

namespace App\Doctrine;

use App\Entity\Requisition;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class SafeRequisitionFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if ($targetEntity->getReflectionClass()->getName() !== Requisition::class) {
            return '';
        }
        
        return sprintf('%s.product_id is not null', $targetTableAlias);
    }
}