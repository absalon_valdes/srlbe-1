<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ValidationDocType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextareaType::class, [
                'label' => 'Descripción del documento o comentario'
            ])
            ->add('document', FileType::class,  [
                'data_class' => null,
                'label' => 'Ingreso de documento',
                'required' => false,
            ])
        ;
    }
}