<?php

namespace App\Form;

use App\Model\QueueUpload;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ImporterType
 * @package App\Form
 * 
 * @DI\FormType()
 */
class ImporterType extends AbstractType
{
    /**
     * @var array
     * @DI\Inject("%workers%")
     */
    public $workers;
    
    /**
     * @var string
     * @DI\Inject("%kernel.var_dir%")
     */
    public $varDir;
    
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label'=>'Acción',
                'choices' => array_values(array_column($this->workers, 'id')),
                'choice_label' => function ($j, $k) {
                    return $this->workers[$k]['name'];
                },
                'placeholder' => false,
                'empty_data' => null
            ])
            ->add('file', HiddenType::class)
            ->add('original', HiddenType::class)
        ;
        
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            /** @var QueueUpload $data */
            $data = $event->getData(); 
            $data->setFile(sprintf('%s/queue_files/%s', $this->varDir, $data->getFile()));
        });
    }
    
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', QueueUpload::class);
    }
}