<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProductDeniedResponseType extends AbstractType
{
    const DENIED_ACCEPT = 'denied_accept';
    const DENIED_DONT   = 'denied_dont';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('evaluation', ChoiceType::class, [
                'expanded' => true,
                'choices' => [
                    self::DENIED_ACCEPT,
                    self::DENIED_DONT
                ],
                'choice_label' => function ($v) {
                    return $v;
                },
                'data' => self::DENIED_ACCEPT
            ])
            ->add('comment', TextareaType::class, [
                'required' => false
            ])
        ;
    } 
}