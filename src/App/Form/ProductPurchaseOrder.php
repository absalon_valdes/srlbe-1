<?php

namespace App\Form;

use App\Controller\Helper\FormUploadHelper;
use App\Entity\Requisition;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductPurchaseOrder
 * @package App\Form
 * 
 * @DI\FormType()
 */
class ProductPurchaseOrder extends AbstractType
{
    /**
     * @var FormUploadHelper
     * @DI\Inject("form_upload_helper")
     */
    public $uploadHelper;
    
    /**
     * @var Requisition
     */
    private $requisition;
    
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Requisition $requisition */
            $requisition = $this->requisition = $event->getData();
            
            $event->getForm()
                ->add('purchase_order', FileType::class, [
                    'label' => false,
                    'data_class' => null,
                    'required' => !$requisition->isType(Requisition::TYPE_MAINTENANCE),
                    'mapped' => false,
                ])
            ;
        });
        
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $purchaseOrder = $event->getData(); 
            
            $this->requisition->addMetadata('purchase_order', [
                'files' => $this->uploadHelper->upload($purchaseOrder)
            ]);
        });
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Requisition::class);
    }
}