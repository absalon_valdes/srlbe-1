<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Office;
use App\Entity\Requisition;
use App\Entity\Supplier;
use App\Entity\User;
use App\Helper\BuildArrayByConstants;
use App\Helper\RolesHelper;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * @FormType
 */
class BackOfficeSearchRequisitionType extends AbstractType
{
    /**
     * @var CategoryRepository
     * @Inject("category_repo")
     */
    public $categoryRepo;

    /**
     * @var RolesHelper
     * @Inject("app_roles_helper")
     */
    public $rolesHelper;

    /**
     * @var BuildArrayByConstants
     * @Inject("build_array_by_constants")
     */
    public $buildArray;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $regions = $this->categoryRepo->getChildren($this->categoryRepo->findOneByName('Geografico'), true);
        $cities = $this->categoryRepo->getLeafs($this->categoryRepo->findOneByName('Geografico'));
        $roles = array_map(function ($item) {
            $excludedRoles = [
                'ROLE_ADMIN',
                'ROLE_DEVELOPER',
                'ROLE_HRM',
                'ROLE_EMPLOYEE',
                'ROLE_HELP_DESK',
                'ROLE_SYSTEM',
                'ROLE_BACKOFFICE',
            ];
            if (!in_array($item, $excludedRoles)) {
                return $item;
            }
        }, $this->rolesHelper->getRoles());

        $requisitionStates = array_map(function ($value) {

            //Removes the internal workflow states in the array
            $invalidTypes = [
                ProductStatus::DRAFT,
                ProductStatus::RETURNED,
                ProductStatus::SHIPPED
            ];
            if (!in_array($value, $invalidTypes)) {
                return $value;
            }
        }, $this->buildArray->getConstantsArray(ProductStatus::class, '.event'));

        $builder
            ->add('orderNumber', TextType::class)
//            ->add('office', TextType::class)
            ->add('office', EntityType::class, [
                'class' => Office::class,
                'attr' => ['class' => 'betterSelect'],
                'label' => 'Sucursal',
                'placeholder'=> 'Todas',
//                'required' => true,
                'multiple'=> false,
            ])
            ->add('createdAt', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false,
                'attr' => [
                    'class' => 'datepicker',
                    'data-date-autoclose' => 'true',
                    'data-date-format' => 'dd/mm/yyyy',
                ],
            ])
            ->add('region', ChoiceType::class, [
                'attr' => ['class' => 'betterSelect'],
                'choices' => array_filter(['Todas' => 'region.all'] + $regions),
                'choice_label' => function ($category, $key, $index) {
                    if (!is_string($category)) {
                        return $category->getName();
                    }

                    return $key;
                },
            ])
            ->add('city', ChoiceType::class, [
                'attr' => ['class' => 'betterSelect'],
                'choices' => array_filter(['Todas' => 'cities.all'] + $cities),
                'choice_label' => function ($category, $key, $index) {
                    if (!is_string($category)) {
                        return $category->getName();
                    }

                    return $key;
                },
            ])
            ->add('in_sla', ChoiceType::class, [
                'attr' => ['class' => 'betterSelect'],
                'label' => 'SLA',
                'choices' => array_filter(['all_sla' => 'all_sla','in_sla' => 'in_sla','out_sla' => 'out_sla'])
            ])
            ->add('status', ChoiceType::class, array(
                'attr' => ['class' => 'betterSelect'],
                'choices' => array_filter(['Todos' => 'requisition.all'] + $requisitionStates),
            ))
            ->add('type', ChoiceType::class, [
                'attr' => ['class' => 'betterSelect'],
                'label' => 'Categoría tipo',
                'choices' => [
                    'Todas'                            => '',
                    'Pedidos'                          => Requisition::TYPE_PRODUCT,
                     Requisition::TYPE_SELF_MANAGEMENT => Requisition::TYPE_SELF_MANAGEMENT,
                    'Mantenciones'                     => Requisition::TYPE_MAINTENANCE
                ] ,
                'choice_label' => function ($category, $key, $index) {
                    if (!is_string($category)) {
                        return $category->getName();
                    }

                    return $key;
                },
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'attr' => ['class' => 'betterSelect'],
                'query_builder' => function (CategoryRepository $repo) {
                    $products = $repo->findOneBy(['slug'=>'productos']);
                    return $repo->childrenQueryBuilder($products,false,'name','asc');
                },
                'choice_attr' => function ($val) {
                    return [
                        'data-category' => json_encode($val)
                    ];
                },
                'required'=> false,
                'placeholder'=> 'Selecciona una categoría',
                'multiple'=> false,
            ])
            ->add('sub_category_1', EntityType::class, [
                'class' => Category::class,
                'attr' => ['class' => 'betterSelect'],
                'label' => 'Subcategoría nivel 1',
                'required'=> false,
                'placeholder'=> 'Selecciona una sub categoría nivel 1',
                'multiple'=> false,
            ])
            ->add('sub_category_2', EntityType::class, [
                'class' => Category::class,
                'attr' => ['class' => 'betterSelect'],
                'label' => 'Subcategoría nivel 2',
                'required'=> false,
                'placeholder'=> 'Selecciona una sub categoría nivel 2',
                'multiple'=> false,
            ])
            ->add('sub_category_3', EntityType::class, [
                'class' => Category::class,
                'attr' => ['class' => 'betterSelect'],
                'label' => 'Subcategoría nivel 3',
                'required'=> false,
                'placeholder'=> 'Selecciona una sub categoría nivel 3',
                'multiple'=> false,
            ])
            //->add('evaluation', TextType::class)
            /*->add('userType', ChoiceType::class, [
                'choices' => array_filter($roles),
            ])*/
            ->add('requesterName', EntityType::class, [
                'class' => User::class,
                'label' => 'requester.name',
                'required' => false
            ])
            ->add('requesterUserName', EntityType::class, [
                'class' => User::class,
                'label' => 'requester.username',
                'required' => false,
                //'choices' => []
            ])
            ->add('supplier', EntityType::class, [
                'class' => Supplier::class,
                'placeholder'=> 'Selecciona un proveedor',
                'attr' => ['class' => 'betterSelect'],
                'required' => false,
            ])
            ->add('approver', EntityType::class, [
                'class' => User::class,
                'attr' => ['class' => 'betterSelect'],
                'label' => 'approver',
                'placeholder'=> 'Selecciona un evaluador',
                'required' => false,
                'query_builder' => function (UserRepository $repo) {
                    $qb = $repo->createQueryBuilder('u')
                        ->where('u.roles LIKE :role')
                        ->setParameter('role', '%ROLE_APPROVER%');
                    return $qb;
                }
            ])
            ->add('reset', ButtonType::class, [
                'label' => 'Limpiar filtros',
                'attr' => [
                    'class' => 'pull-left btn btn-sm btn-success',
                    'id' => 'btn_search_requisition_reset',
                    'name' => 'btn_search_requisition_reset',
//                    'data-loading-text' => 'Buscando...'
                ],
            ])
            ->add('search', SubmitType::class, [
                'attr' => [
                    'class' => 'pull-right btn btn-sm btn-success',
                    'id' => 'btn_search_requisition',
                    'name' => 'btn_search_requisition',
                    'data-loading-text' => 'Buscando...'
                ],
            ]);
    }

    public function getName()
    {
        return 'search_requisition';
    }
}
