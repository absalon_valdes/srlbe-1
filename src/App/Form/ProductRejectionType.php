<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProductRejectionType extends AbstractType
{
    const REJECTION_STANDARD   = 'fuera_estandar';
    const REJECTION_DONT_APPLY = 'no_aplica';
    const REJECTION_TECHNICAL  = 'motivos_tecnicos';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reason', ChoiceType::class, [
                'expanded' => true,
                'choices' => [
                    self::REJECTION_STANDARD,
                    self::REJECTION_DONT_APPLY,
                    self::REJECTION_TECHNICAL,
                ],
                'choice_label' => function ($v) {
                    return $v;
                },  
            ])
            ->add('comment', TextareaType::class)
            ->add('send', SubmitType::class)
        ;
    } 
}