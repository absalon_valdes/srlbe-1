<?php

namespace App\Form;

use App\Model\Shareholder;
use App\Validator\Constraints\Rut;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PepShareholderType extends PepRepresentativeType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('share', NumberType::class, [
                'label' => false,
                'scale' => 2,
                'attr' => ['class' => 'share-input','step'  => 'any'],
                'constraints' => new Range([
                    'min' => 1, 
                    'max' => 100,
                    'groups' => 'all'
                ])
            ])
        ;
    } 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shareholder::class
        ]);
    }
}