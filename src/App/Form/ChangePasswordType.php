<?php

namespace App\Form;

use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @FormType
 */
class ChangePasswordType extends AbstractType
{
    /**
     * @Inject("user_helper")
     */ 
    public $userHelper;

    /**
     * @Inject("security.password_encoder")
     */
    public $encoder;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $callback = function($object, ExecutionContextInterface $context) {
            $user = $this->userHelper->getCurrentUser();
            $newPassword = $this->encoder->encodePassword($user, $object);

            if ($newPassword === $user->getPassword()) {
                $context
                    ->buildViolation('La nueva contraseña no puede ser igual a la anterior')
                    ->addViolation();
            }
        };

        $builder
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'constraints' => [
                    new Callback($callback),
                    new Length([
                        'min' => 8,
                        'minMessage' => 'La contraseña debe tener un mínimo de 8 caracteres'
                    ])
                ],
                'invalid_message' => 'Las contraseñas no coinciden.',
                'required' => true,
                'first_options'  => ['label' => 'Nueva contraseña'],
                'second_options' => ['label' => 'Repita nueva contraseña']
            ]) 
            ->add('send', SubmitType::class)
        ;
    }

    public function getName()
    {
        return 'change_password';
    } 
}