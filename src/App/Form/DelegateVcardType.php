<?php 

namespace App\Form;

use App\Entity\Employee;
use App\Entity\Requisition;
use App\Entity\User;
use App\Helper\UserHelper;
use App\Model\VcardRequisition;
use App\Repository\EmployeeRepository;
use App\Repository\RequisitionRepository;
use App\Validator\Constraints\Rut;
use JMS\DiExtraBundle\Annotation\FormType;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\Serializer\Serializer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @FormType
 */
class DelegateVcardType extends AbstractType
{
    /**
     * @Inject("serializer")
     * @var Serializer
     */
    public $serializer;

    /**
     * @Inject("employee_repo")
     * @var EmployeeRepository
     */
    public $employeeRepo;

    /**
     * @Inject("user_helper")
     * @var UserHelper
     */
    public $userHelper;

    /**
     * @Inject("requisition_repo")
     * @var RequisitionRepository
     */
    public $requisitionRepo;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $callback = function($rut, ExecutionContextInterface $context) {
            /** @var Employee $empl */
            if (!$empl = $this->employeeRepo->findOneByRut($rut)) {
                $context
                    ->buildViolation('No existe ningún funcionario con ese RUT')
                    ->atPath('rut')
                    ->addViolation()
                ;

                return;
            }

            /** @var User $user */
            if (!$user = $this->userHelper->getCurrentUser()) {
                return;
            }

            /** @var Employee $employee */
            if (!$employee = $user->getEmployee()) {
                return;
            }

            if (strtoupper($employee->getRut()) === strtoupper($rut)) {
                $context
                    ->buildViolation('Solo puedes pedir tarjetas para un tercero')
                    ->atPath('rut')
                    ->addViolation()
                ;

                return;
            }

            $requisitions = $this->requisitionRepo->createQueryBuilder('o')
                ->select('o.metadata')
                ->where('o.type = :type')
                ->andWhere('o.status not in (:statuses)')
                ->setParameter('type', Requisition::TYPE_VCARD)
                ->setParameter('statuses', [
                    'solicitud_completa',
                    'cerrado',
                    'cerrada_sistema'
                ])
                ->getQuery()
                ->getArrayResult()
            ;

            $requisitions = array_map(function($item) {
                return $this->serializer->deserialize(
                    $item['metadata']['vcards'], VcardRequisition::class, 'json'
                );
            }, $requisitions);

            $requisitions = array_filter(
                $requisitions,
                function (VcardRequisition $item) use ($empl) {
                    if (!$e = $item->getEmployee()) {
                        return false;
                    }

                    return $e->getRut() === $empl->getRut();
                }
            );

            if (count($requisitions)) {
                $context
                    ->buildViolation(sprintf(
                        'El usuario %s ya tiene pedidos activos', $empl->getFullName()
                    ))
                    ->atPath('rut')
                    ->addViolation()
                ;
            }
        };

        $builder
            ->add('rut', TextType::class, [
                'constraints' => [new Rut, new NotBlank, new Callback($callback)],
                'attr' => ['help' => 'El RUT debe tener el formato 00000000-0']
            ])
            ->add('send', SubmitType::class)
        ;
    }

    public function getName()
    {
        return 'vcard_delegation';
    }
}