<?php

namespace App\Form;

use App\Model\Metadata\Requisition\SupplierCertification;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SupplierCertificationType
 * @package App\Form
 * 
 * @DI\FormType()
 */
class SupplierCertificationType extends AbstractType 
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('supplier_datetime', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'choice',
                'date_format' => 'dd/MM/yyyy',
            ])
            ->add('info', CollectionType::class, [
                'entry_type' => SupplierCertificationInfoType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'prototype_name' => '__supplier_certification_info__',
            ])
            ->add('requisition', RequisitionIdType::class)
        ;
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            //dump($event);    
        });

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            //dump($event);    
        });
        
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            //dump($event);    
        });
        
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            //dump($event);    
        });
        
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            //dump($event);    
        });

//        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
//            /** @var FormInterface $form */
//            $form = $event->getForm();
//
//            /** @var SupplierCertification $supplierCertification */
//            $supplierCert = $event->getData();
//
//            /** @var Requisition $requisition */
//            $requisition = $supplierCert->getRequisition();
//
//            $questions = $requisition->getMetadata('more_info');
//
//            foreach ($questions as $i => $question) {
//                $form->add(sprintf('more_info_%s', $i), TextareaType::class, [
//                    'mapped' => false,
//                    'attr' => [
//                        'data-index' => $i,
//                        'readonly' => isset($question['response']),
//                    ],
//                    'label' => false,
//                    'data' => $question['response'] ?? null,
//                ]);
//            }
//        });
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', SupplierCertification::class);
    }
}