<?php

namespace App\Form;

use App\Entity\User;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @FormType
 */
class EmployeeProfileType extends AbstractType
{
    /**
     * @Inject("user_repo")
     */
    public $userRepo;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rut', TextType::class, ['attr' => ['readonly' => true ] ])
            ->add('name', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('lastName', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('phone', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('secondLastName', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('position', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('username', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('email', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'el password debe coincidir.',
                'options' => array('attr' => [
                                                'class' => 'password-field',
                                             ]),
                'constraints' => [
                                    new Length([
                                        'min' => 8,
                                        'minMessage' => 'La contraseña debe tener un mínimo de 8 caracteres'
                                    ])
                                 ],
                'required' => false,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repetir Password'),
            ))
            ->add('send', SubmitType::class, ['label' => 'Cambiar Contraseña', 'attr' => ['class' => 'btn btn-warning pull-left']]);
    }

    public function getName()
    {
        return 'employee_profile';
    }
}
