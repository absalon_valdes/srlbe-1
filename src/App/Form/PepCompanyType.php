<?php

namespace App\Form;

use App\Model\Company;
use App\Validator\Constraints\Rut;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PepCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Persona jurídica' => Company::TYPE_COMPANY,
                    'Persona natural'  => Company::TYPE_PERSON,
                ],
            ])
            ->add('rut', TextType::class, [
                'attr' => [
                    'class' => 'rut-input',
                    'help' => 'Introduzca RUT sin puntos. Ej: 9999999-9',
                ],
                'constraints' => [
                    new Rut(['groups' => ['person', 'all']]), 
                    new NotBlank(['groups' => ['person', 'all']])
                ],
            ])
            ->add('name', TextType::class, [
                'constraints' => new NotBlank(['groups' => ['person', 'all']]),
            ])
            ->add('representatives', CollectionType::class, [
                'entry_type' => PepRepresentativeType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'prototype_name' => '__company_representative_name__',
            ])
            ->add('shareholders', CollectionType::class, [
                'entry_type' => PepShareholderType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'prototype_name' => '__company_shareholder_name__',
            ])
            ->add('documents', CollectionType::class, [
                'entry_type' => FileType::class,
                'entry_options' => [
                    'required' => false,
                    'data_class' => null,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'prototype_name' => '__company_document_name__',
            ])
            ->add('template', FileType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
        ;
    } 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Company::class);   
        
        $resolver->setDefault('validation_groups', function (FormInterface $form) {
            if (Company::TYPE_PERSON === $form->getData()->getType()) {
                return ['person'];
            } 

            return ['all'];
        });
    }
}