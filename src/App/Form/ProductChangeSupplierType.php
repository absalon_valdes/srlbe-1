<?php

namespace App\Form;

use App\Entity\Contract;
use App\Entity\Requisition;
use App\Repository\ContractRepository;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class ProductChangeSupplierType
 *
 * @Di\FormType()
 */
class ProductChangeSupplierType extends AbstractType
{
    /**
     * @var ContractRepository
     * @Di\Inject("contract_repo")
     */
    public $contractRepo;
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Requisition $requisition */
            $requisition = $event->getData();       

            $declined = $requisition->getMetadata('declined') ?? [];
            
            $rejectedContracts = $this->contractRepo->createQueryBuilder('c')
                ->select('c', 's')
                ->leftJoin('c.supplier', 's')
                ->where('c.id in (:ids)')
                ->setParameter('ids', $declined)
                ->getQuery()
                ->getResult()
            ;
            
            $suppliers = array_map(function (Contract $contract) {
                return $contract->getSupplier();
            }, $rejectedContracts);
           
            $event->getForm()
                ->add('contract', EntityType::class, [
                    'expanded' => true,
                    'multiple' => false,
                    'class' => Contract::class,
                    'label' => false,
                    'choice_label' => null,
                    'position' => 'first',
                    'choice_attr' => function (Contract $value) use ($declined) {
                        return [
                            'disabled' => in_array($value->getSupplierUser()->getId(), $declined, true)
                        ];
                    },
                    'query_builder' => function (ContractRepository $cr) use ($requisition) {
                        return $cr->queryByProduct(
                            $requisition->getProduct(), 
                            $requisition->getRequester()
                        );
                    }
                ])
            ;
        });
    }
}