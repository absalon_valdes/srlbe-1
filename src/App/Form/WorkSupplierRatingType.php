<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class WorkSupplierRatingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $options = [
            'expanded' => true,
            'choices' => [1, 2, 3, 4, 5, 6, 7],
            'label_attr' => ['class' => 'radio-inline'],
            'choice_label' => function ($v) {
                return $v;
            }
        ];

        $builder
            ->add('punctuality', ChoiceType::class, $options)
            ->add('quality', ChoiceType::class, $options)
            ->add('service_level', ChoiceType::class, $options)
        ;
    } 
}