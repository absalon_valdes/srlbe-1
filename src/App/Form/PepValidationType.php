<?php

namespace App\Form;

use App\Entity\PepGrant;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PepValidationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('purchase_mode', ChoiceType::class, [
                'expanded' => true,
                'choices' => [
                    PepGrant::MODE_PRIVATE,
                    PepGrant::MODE_DIRECT,
                ],
                'choice_label' => function($v) {
                    return $v;
                },  
            ])
            ->add('purchase_range', ChoiceType::class, [
                'choices' => [
                    PepGrant::RANGE_LT_2KUF,
                    PepGrant::RANGE_GE_2KUF
                ],
                'choice_label' => function($v) {
                    return $v;
                }
            ])
            ->add('description', TextareaType::class, [
                'constraints' => new NotBlank
            ])
            ->add('companies', CollectionType::class, [
                'entry_type' => PepCompanyType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'prototype_name' => '__company_name__',
            ])
            ->add('submit', SubmitType::class)
        ;

        $builder
            ->get('companies')
            ->addModelTransformer(new CallbackTransformer(
                function ($original) {
                    return $original;
                },
                function ($submitted) {
                    // http://stackoverflow.com/questions/11084209
                    return clone $submitted;
                }
            ))
        ;
    } 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', PepGrant::class);
    }
}