<?php

namespace App\Form;

use App\Entity\User;
use App\Helper\UserHelper;
use App\Repository\UserRepository;
use JMS\DiExtraBundle\Annotation\FormType;
use JMS\DiExtraBundle\Annotation\Inject;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @FormType
 */
class UserType extends AbstractType
{
    /**
     * @var UserRepository $userRepo
     * @Inject("user_repo")
     */
    public $userRepo;

    /**
     * @var UserHelper $userHelper
     * @Inject("user_helper")
     */
    public $userHelper;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'username',
                'label' => false,
                'choices' => [] // ajax
            ])
        ;
    }

    public function getName()
    {
        return 'user_redirection';
    }
}
