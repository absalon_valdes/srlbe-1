<?php
namespace App\Form\Type;

use App\Entity\Address;
use App\Entity\Category;
use App\Entity\Taxonomy;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street', TextType::Class)
            ->add('region', EntityType::class, array(
                'class' => 'App:Category',
                'choice_label' => 'name',
                'query_builder' => function ($er) {
                    return $er->createQueryBuilder('c')
                              ->leftJoin("App\Entity\Taxonomy", "ON", "taxonomy")
                              ->where("c.parent IS NULL")
                              ->orderBy('c.name', 'ASC');
                }

            ))
            ->add('city', EntityType::class, array(
                'class' => 'App:Category',
                'choice_label' => 'name',
                'disabled' => 'true',
                ))
            ->add('province', EntityType::class, array(
                'class' => 'App:Category',
                'choice_label' => 'name',
                'disabled' => 'true',
                ))
            ;
    }
}
