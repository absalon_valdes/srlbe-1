<?php

namespace App\Form;

use App\Entity\Contract;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Repository\CategoryRepository;
use App\Repository\ContractRepository;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RequisitionType
 * @package App\Form
 * @Di\FormType()
 */
class RequisitionType extends AbstractType
{
    /**
     * @var array
     * @DI\Inject("%app.products.config%")
     */
    public $config;
    
    /**
     * @var CategoryRepository
     * @Di\Inject("category_repo")
     */
    public $categoryRepo;
    
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            /** @var FormInterface $form */
            $form = $event->getForm();

            /** @var Requisition $requisition */
            $requisition = $event->getData();
           
            $form->add('details', DynamicType::class, [
                'property_path' => 'metadata[details]',
                'config' => $this->resolveAttrConfig($requisition->getProduct())
            ]);

            $form->add('contract', EntityType::class, [
                'expanded' => true,
                'multiple' => false,
                'class' => Contract::class,
                'label' => false,
                'choice_label' => null,
                'query_builder' => function (ContractRepository $cr) use ($requisition, $options) {
                    if (empty($options['contracts']) && is_array($options['contracts'])) {
                        return $cr->queryByProduct(
                            $requisition->getProduct(),
                            $requisition->getRequester()
                        );
                    }

                    return $cr->createQueryBuilder('o')
                        ->where('o.id in (:ids)')
                        ->setParameter('ids', $options['contracts']);
                }
            ]);
        });
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('contracts', []);
        $resolver->setDefault('data_class', Requisition::class);
    }
    
    /**
     * @param Product $product
     * @return array
     */
    private function resolveAttrConfig(Product $product)
    {
        $config = [];
        $code = $product->getCode();
        
        $prodConfig  = $this->config['by_product'];
        $catsConfig  = $this->config['by_category'];
        $groupConfig = $this->config['by_group'];
        
        foreach ($catsConfig as $catSlug => $catConfig) {
            if ($category = $this->categoryRepo->findOneBySlug($catSlug)) {
                if (!$product->getCategory()) {
                    continue;
                }
                
                if ($product->belongsToCategory($category->getName())) {
                    $config = array_replace_recursive($config, $catConfig);
                }
            }
        }
        foreach ($groupConfig as $group) {
            if (!in_array($code, $group['codes'], true)) {
                continue;
            }

            $config = array_replace_recursive($config, $group['fields']);
        }
        
        return array_replace_recursive($config, $prodConfig[$code] ?? []);
    }
}