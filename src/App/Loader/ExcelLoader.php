<?php

namespace App\Loader;

use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("file_loader")
 */
class ExcelLoader implements LoaderInterface
{
    /**
     * @return string
     */
    public function getType()
    {
        return 'excel';
    }

    /**
     * @param $filename
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function load($filename)
    {
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);

        $excel = $reader->load($filename);
        $worksheet = $excel->getSheet();
        //Warnings about the params in the toArray function in PHP to excel:
        //http://stackoverflow.com/questions/8583915/phpexcel-read-all-values-date-time-numbers-as-strings
        $toArray = $worksheet ? $worksheet->toArray(null, true, true) : [];

        array_shift($toArray);

        return $toArray;
    }
}
