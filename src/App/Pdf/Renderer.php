<?php

namespace App\Pdf;

use JMS\DiExtraBundle\Annotation as Di;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class RenderPdf
 * @package App\Pdf
 *
 * @Di\Service("pdf_renderer")
 */
class Renderer
{
    /**
     * @var string
     * @Di\Inject("%kernel.cache_dir%")
     */
    public $cacheDir;
    
    /**
     * @var LoggerInterface
     * @Di\Inject("logger")
     */
    public $logger;

    /**
     * @param $html
     * @return null|string
     */
    public function render($html)
    {
        $filename = Uuid::uuid5(Uuid::NAMESPACE_DNS, $html)->toString();
        $filename = sprintf('%s/pdfs/%s.pdf', $this->cacheDir, $filename);

        try {
            $htmlToPdf = new \HTML2PDF('P', 'A4', 'es', true, 'UTF-8', [10, 10, 10, 20]);
            $htmlToPdf->setTestTdInOnePage(false);
            $htmlToPdf->writeHTML($html);
            $htmlToPdf->Output($filename);

            return $filename;
        } catch (\HTML2PDF_exception $e) {
            $this->logger->critical($e->getMessage().PHP_EOL.PHP_EOL.$html);
        }

        return null;
    }
}