<?php 

namespace App\Model;

/**
 * Class Shareholder
 * @package App\Model
 */
class Shareholder extends Person
{
    /**
     * @var
     */
    protected $share;

    /**
     * @return mixed
     */
    public function getShare()
    {
        return $this->share;
    }

    /**
     * @param $share
     */
    public function setShare($share)
    {
        $this->share = $share;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['rut', 'pep', 'name', 'share'];
    }
}