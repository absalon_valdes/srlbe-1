<?php

namespace App\Model;

use App\Entity\Position;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class VcardContact
 * 
 * @Assert\Callback({"App\Validator\Callback\VcardValidationCallback", "validate"})
 * @Serializer\ExclusionPolicy("ALL")
 */
class VcardContact
{
    /** 
     * @Assert\NotBlank() 
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $fullName;
    
    /** 
     * @Assert\NotBlank()
     * @Serializer\Type("App\Entity\Position")
     * @Serializer\Expose()
     */
    protected $position;
    
    /**
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $areaCode;
    
    /** 
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $phone;
    
    /** 
     * @Assert\Email() 
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $email;
    
    protected $requisition;

    public function getFullName()
    {
        return $this->fullName;
    }

    public function setFullName($fullName)
    {
        $this->fullName = ucwords($fullName);
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition(Position $position)
    {
        $this->position = $position;
    }

    public function getAreaCode()
    {
        return $this->areaCode;
    }

    public function setAreaCode($areaCode)
    {
        $this->areaCode = $areaCode;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getRequisition()
    {
        return $this->requisition;
    }

    public function setRequisition(VcardRequisition $requisition)
    {
        $this->requisition = $requisition;
        
        return $this;
    }
}