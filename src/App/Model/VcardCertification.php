<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class VcardCertification
{
    /**
     * @Assert\LessThanOrEqual("now")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $deliveredAt;

    /**
     * @Assert\Length(min=7)
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $receiver;

    /**
     * @Assert\Valid()
     * @Serializer\Expose()
     * @Serializer\Type("ArrayCollection<App\Model\VcardCertificationExtra>")
     */
    protected $extras;

    public function __construct()
    {
        $this->deliveredAt = new \DateTime;
        $this->extras = new ArrayCollection;
    }

    public function uploadFiles($uploadDir)
    {
        $this->extras->forAll(function ($i, $extra) use ($uploadDir) {
            $extra->upload($uploadDir);
        });
    }

    public function getDeliveredAt()
    {
        return $this->deliveredAt;
    }

    public function setDeliveredAt(\DateTime $deliveredAt)
    {
        $this->deliveredAt = $deliveredAt;
        
        return $this;
    }

    public function getReceiver()
    {
        return $this->receiver;
    }

    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
        
        return $this;
    }

    public function getExtras()
    {
        return $this->extras;
    }

    public function setExtras(ArrayCollection $extras)
    {
        $this->extras = $extras;
        
        return $this;
    }

    public function addExtra(VcardCertificationExtra $extra)
    {
        $this->extras->add($extra);
        
        return $this;
    }
}