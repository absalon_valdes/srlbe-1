<?php

namespace App\Model;
/**
 * Class QueueUpload
 * @package App\Model
 */
class QueueUpload
{
    protected $type;
    protected $file;
    protected $original;
    
    /**
     * @return mixed
     */
    public function getOriginal()
    {
        return $this->original;
    }
    
    /**
     * @param mixed $original
     */
    public function setOriginal($original)
    {
        $this->original = $original;
    }
    
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
    
    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }
    
    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }
    
    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'type' => $this->type,
            'file' => $this->file,
            'original' => $this->original,
        ];
    }
}