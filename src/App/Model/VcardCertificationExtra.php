<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class VcardCertificationExtra
 * @Serializer\ExclusionPolicy("ALL")
 */
class VcardCertificationExtra
{
    /**
     * @var string
     * @Assert\File()
     */
    protected $file;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $comment;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $fileName;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $filePath;

    /**
     * @param $uploadDir
     */
    public function upload($uploadDir)
    {
        if (!$this->file) {
            return;
        }

        $this->fileName = $this->file->getClientOriginalName();
        $extension = $this->file->getClientOriginalExtension();
        $this->filePath = sha1($this->fileName.time()).($extension ? '.'.$extension : null);
        $this->file->move($uploadDir, $this->filePath);
        $this->file = null;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }
}