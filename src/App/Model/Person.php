<?php 

namespace App\Model;

/**
 * Class Person
 * @package App\Model
 */
class Person
{
    /**
     * @var
     */
    protected $name;

    /**
     * @var
     */
    protected $rut;

    /**
     * @var null
     */
    protected $pep = null;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param $rut
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    /**
     * @return null
     */
    public function isPep()
    {
        return $this->pep;
    }

    /**
     * @param $pep
     */
    public function setPep($pep)
    {
        $this->pep = $pep;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['pep', 'rut', 'name'];
    }
}