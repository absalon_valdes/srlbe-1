<?php
/**
 * Created by PhpStorm.
 * User: rmoreno
 * Date: 23-05-18
 * Time: 19:23
 */

namespace App\Model;

/**
 * Interface RequisitionInterface
 * Si se requiere obtener los nombres legacy debe usarse \App\Model\RequisitionTrail
 *
 * @package App\Model
 */

interface RequisitionInterface
{
    const STATUS_DRAFT              = 'draft';     // en borrador (carro)
    const STATUS_CREATED            = 'created';   // emitida
    const STATUS_APPROVED           = 'approved';  // aprobada por el evaluador
    const STATUS_ORDERED            = 'ordered';   // emisión de orden de compra
    const STATUS_REJECTED           = 'rejected';  // rechazada por el evaluador
    const STATUS_APPEAL             = 'appeal';    // en apelación
    const STATUS_DENIED             = 'denied';    // apelación rechazada
    const STATUS_ACCEPTED           = 'accepted';  // aceptada por el proveedor
    const STATUS_CONFIRMED          = 'confirmed'; // recepción confirmada por el proveedor
    const STATUS_DECLINED           = 'declined';  // rechazada por el proveedor
    const STATUS_HOLD               = 'hold';      // esperando que evaluador acepte rechazo de proveedor
    const STATUS_DELIVERED          = 'delivered'; // completada y entregada por el proveedor
    const STATUS_REVIEWED           = 'reviewed';  // certificada por el evaluador
    const STATUS_RETURNED           = 'returned';  // certificacion rechazada por el evaluador
    const STATUS_EVALUATED          = 'evaluated'; // certificada por el demandante
    const STATUS_CLOSED             = 'closed';    // proceso cerrado incompleto
    const STATUS_CLOSED_BY_APPROVER = 'closed_by_approver'; // Cerrado por el evaluador
    const STATUS_COMPLETED          = 'completed'; // proceso completado con éxito
    const STATUS_TIMEOUT            = 'timeout';   // cerrado por timeout
    const STATUS_SHIPPED            = 'shipped';   // en camino

    const CLASS_PRODUCT           = 'product';     // pedidos
    const CLASS_MAINTENANCE       = 'maintenance';   // mantencion
    const CLASS_VCARD             = 'vcard';  // tarjetas
    const CLASS_SELF_MANAGEMENT   = 'self_management';   // autogestión
    const CLASS_UNIFORM           = 'uniform';  // uniformes
    const CLASS_DELIVERY          = 'delivery';    // paquetería
    const CLASS_WATER             = 'water'; // aguas

    const STATUSES_CLOSED = [
        self::STATUS_CLOSED,
        self::STATUS_TIMEOUT,
        self::STATUS_COMPLETED,
        self::STATUS_CLOSED_BY_APPROVER
    ];
}