<?php

namespace App\Model;

/**
 * Class Transition
 * @package App\Model
 */
class Transition 
{
    /**
     * @var \DateTime
     */
    private $datetime;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $status;

    /**
     * Transition constructor.
     * 
     * @param string $username
     * @param string $status
     */
    public function __construct($username, $status)
    {
        $this->username = $username;
        $this->status = $status;
        
        $this->datetime = new \DateTime;
    }

    /**
     * @param $username
     * @param $status
     * @return Transition
     */
    public static function create($username, $status)
    {   
        return new self($username, $status);
    }

    /**
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param \DateTime $datetime
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function serialize()
    {
        return ['username', 'status'];
    }
}