<?php

namespace App\Model;

/**
 * Class StatusLabel
 * @package App\Model
 */
class StatusLabel
{
    /**
     * @var array
     */
    private $viewed = array();

    /**
     * @var string
     */
    private $status;

    /**
     * @var bool
     */
    private $sticky = false;

    /**
     * StatusLabel constructor.
     * @param $viewed
     * @param $status
     * @param $sticky
     */
    public function __construct($viewed, $status, $sticky)
    {
        $this->viewed = $viewed;
        $this->status = $status;
        $this->sticky = $sticky;
    }

    /**
     * @param $viewed
     * @param $status
     * @param bool $sticky
     * @return StatusLabel
     */
    public static function create($viewed, $status, $sticky = false)
    {
        return new self($viewed, $status, $sticky);
    }

    /**
     * @return mixed
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * @param mixed $viewed
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSticky()
    {
        return $this->sticky;
    }

    /**
     * @param mixed $sticky
     */
    public function setSticky($sticky)
    {
        $this->sticky = $sticky;
    }
}