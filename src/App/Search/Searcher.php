<?php
namespace App\Search;

use App\Entity\Contract;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\RequisitionRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\Service;

/**
 * @Service("searcher")
 */
class Searcher
{
    /**
     * @var ProductRepository
     * @Inject("product_repo")
     */
    public $productRepo;

    /**
     * @var CategoryRepository
     * @Inject("category_repo")
     */
    public $categoryRepo;

    /**
     * @var RequisitionRepository
     * @Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var string
     * @Inject("%kernel.environment%")
     */
    public $environment;

    public function findProducts($needle, $user)
    {        
        /** @var QueryBuilder $qb */
        $qb = $this->productRepo->createQueryBuilder('p');
        $qb->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('c.product', 'p'));
        $qb->join(Office::class, 'o', Join::WITH, $qb->expr()->eq('o', 'c.office'));
        $qb->join(Employee::class, 'e', Join::WITH, $qb->expr()->eq('e.office', 'o'));
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq('e.user', ':user'),
            $qb->expr()->eq('c.expired', 0),
            $qb->expr()->orX(
                $qb->expr()->like('p.code', ':needle'),
                $qb->expr()->like('p.name', ':needle')
            )
        ));
        $qb->setParameter('user', $user);
        $qb->setParameter('needle', '%'.$needle.'%');
        
        $query = $qb->getQuery();
        $query->setQueryCacheLifetime(86400);  //24 Hours
        $query->setResultCacheLifetime(86400); //24 Hours
        
        $result = $query->getResult();

        // HACK DEL OOOOOORTO
        if ('prod' === $this->environment) {
            $result = array_filter($result, function ($product) {
                if (!$category = $product->getCategory()) {
                    return true;
                }
                
                return (bool) !preg_match('#^productos/manten#', $category->getSlug());
            });
        }

        return $result;
    }

    public function findRequisitions($needle, $user)
    {
        $qb = $this->requisitionRepo
	    ->getEntityManager()
            ->getConnection()
            ->createQueryBuilder();

        return $qb->select('p.id', 'p.name as pname', 'p.code', 'p.images', 'c.id as cat_id', 'c.slug', 'c.name as cat_name', 'r.id rq_id', 'r.status', 'r.id as req_id', 'o.name office_name')
                 ->from('requisitions', 'r')
                 ->leftJoin('r', 'products', 'p', 'r.product_id = p.id')
                 ->leftJoin('p', 'product_categories', 'pc', 'p.id = pc.product_id')
                 ->leftJoin('p', 'product_approvers', 'pa', 'p.id = pa.product_id')
                 ->leftJoin('pa', 'users', 'u', 'u.id = pa.user_id')
                 ->leftJoin('pc', 'categories', 'c', 'c.id = pc.category_id')
                 ->leftJoin('r', 'employees', 'e', 'e.user_id = r.requester_id')
                 ->leftJoin('e', 'offices', 'o', 'e.office_id = o.id')
                 ->where(
                     $qb->expr()->orX(
                         $qb->expr()->like('r.id', ':needle'),
                         $qb->expr()->like('p.name', ':needle'),
                         $qb->expr()->like('p.code', ':needle')
                     )
                 )
                 ->andWhere('u.id = :user')
                 ->setParameter('needle', '%'.$needle.'%')
                 ->setParameter('user', $user->getId())
                 ->execute()
                 ->fetchAll();
    }
}
