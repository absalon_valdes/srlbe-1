<?php
namespace App\Import;

use App\Entity\Contract;
use App\Entity\Money;
use App\Entity\SLA;
use App\Util\RutUtils;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use JMS\DiExtraBundle\Annotation as Di;
use Psr\Log\LoggerInterface;

/**
 * @Di\Service("contracts_importer")
 */
class ContractImporter extends Importer
{
    const EXCEL_DATE_FORMAT = 'd/m/Y';  // i.e: 13/01/2016
    
    /**
     * @var Connection
     * @Di\Inject("database_connection")
     */
    public $pdo;

    /**
     * @var LoggerInterface
     * @Di\Inject("logger")
     */
    public $logger;
    
    private $suppliers;
    private $products;
    private $pCentralized;
    private $officeByCode;

    public function transform(array $row)
    {
        if (array_key_exists('id_producto', $row[0])) {
            return $row;
        }

        return [
            'id_producto' => function ($row) {
                return strtoupper(trim($row[0]));
            },
            'nombre_producto' => function ($row) {
                return '';
            },
            'id_proveedor' => function ($row) {
                $rut = trim($row[1]); 
                if (preg_match('/\d*/', trim($row[1]), $completeRut)) {
                    $rut = sprintf('%s-%s', $completeRut[0], RutUtils::calculateVerifier($completeRut[0]));
                }
                return $rut;
            },
            'id_unidad' => function ($row) {
                return trim($row[6]);
            },
            'fecha_inicio' => function ($row) {
                return \DateTime::createFromFormat(self::EXCEL_DATE_FORMAT, trim($row[3]));
            },
            'fecha_fin' => function ($row) {
                return \DateTime::createFromFormat(self::EXCEL_DATE_FORMAT, trim($row[4]));
            },
            'precio' => function ($row) {
                return trim($row[9]);
            },
            'moneda' => function ($row) {
                return Money::getMoneyType($row[8]);
            },
            'numero_contrato' => function ($row) {
                return trim($row[2]);
            },
            'acuerdo_compra' => function ($row) {
                return trim($row[5]);
            },
            'centralizado' => function ($row) {
                return  !empty(trim($row[10]));
            },
            'centro_costo' => function ($row) {
                return trim($row[7]);
            },
            'sla_prod_tipo'    => function ($row) {
                return Contract::getSlaType($row[11]);
            },
            'sla_prod_valor'   => function ($row) {
                return Contract::getSlaValue($row[11]);
            },
            'sla_eval_tipo'    => function ($row) {
                return Contract::getSlaType($row[10]);
            },
            'sla_eval_valor'   => function ($row) {
                return Contract::getSlaValue($row[10]);
            },
            'sla_accept_tipo'  => function ($row) {
                return  Contract::getSlaType('diashabiles');
            },
            'sla_accept_valor' => function ($row) {
                return  2;
            },
            'sla_key'  => function ($row) {
                return Contract::getSlaType($row[11]);
            },
        ];
    }

    protected function doImport(array $data)
    {
        $this->preload();
        $this->pdo->beginTransaction();

        $violations = [];
        try {
            foreach ($data as $contract) {
                $violations[] = $this->createContract($contract);
            }
            $this->pdo->commit();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            $this->pdo->rollBack();
        } finally {
            unset($this->suppliers, $this->products, $this->officeByCode);
        }
        
        return $violations;
    }

    private function createContract(array $row)
    {
        if (!isset($row['id_proveedor'], $row['id_producto'], $row['id_unidad'])) {
            $this->logger->info('[Fixture contratos] - IDS inválidos');
            return [
                'entity' => $row,
                'error' => 'IDS inválidos',
            ];
        }
        
        // Normalizar codigos de producto
        $this->products = array_change_key_case($this->products, CASE_UPPER);
        
        $supplierId = $this->suppliers[$row['id_proveedor']] ?? null;
        $productId  = $this->products[$row['id_producto']] ?? null;
        $pCentralized  = $this->pCentralized[$row['id_producto']] ?? 1;
        $officeId   = $this->officeByCode[$row['id_unidad']] ?? null;

        $start = !$row['fecha_inicio'] instanceof \DateTime ? 
            \DateTime::createFromFormat('Y-m-d', trim($row['fecha_inicio'])) : 
            $row['fecha_inicio'];

        $end = !$row['fecha_inicio'] instanceof \DateTime ?
            \DateTime::createFromFormat('Y-m-d', trim($row['fecha_fin'])) :
            $row['fecha_fin'];

        if (!$supplierId) {
            $this->logger->info('[Fixture contratos] - Proveedor: '. $row['id_proveedor'] . ' No existe');
            return [
                'entity' => $row,
                'error'  => 'Proveedor: '. $row['id_proveedor'] . ' No existe',
            ];
        }

        if (!$productId) {
            $this->logger->info('[Fixture contratos] - Producto: '. $row['id_producto'] . ' No existe');
            return [
                'entity' => $row,
                'error'  => 'Producto: '. $row['id_producto'] . ' No existe',
            ];
        }

        if (!$officeId) {
            $this->logger->info('[Fixture contratos] - Sucursal: '. $row['id_unidad'] . ' No existe');
            return [
                'entity' => $row,
                'error'  => 'Sucursal: '. $row['id_unidad'] . ' No existe',
            ];
        }

        if (!$start || !$end) {
            return [
                'entity' => $row,
                'error' => 'Formato de fecha invalido: '. trim($row['fecha_inicio']) . ' '. trim($row['fecha_fin']),
            ];    
        }

        $columnTypes = [
            \PDO::PARAM_INT,  // product_id
            \PDO::PARAM_INT,  // supplier_id
            \PDO::PARAM_INT,  // office_id
            \PDO::PARAM_STR,  // contract_number
            'datetime',       // validity_start
            'datetime',       // validity_end
            \PDO::PARAM_BOOL, // expired
            \PDO::PARAM_STR,  // purchase_agreement
            \PDO::PARAM_INT,  // price
            \PDO::PARAM_STR,  // currency
            
            \PDO::PARAM_INT,  // sla_approver_value
            \PDO::PARAM_STR,  // sla_approver_type

            \PDO::PARAM_INT,  // sla_product_value
            \PDO::PARAM_STR,  // sla_product_type

            \PDO::PARAM_INT,  // sla_accept_value
            \PDO::PARAM_STR,  // sla_accept_type

            \PDO::PARAM_INT,  // sla_order_value
            \PDO::PARAM_STR,  // sla_order_type

            \PDO::PARAM_INT,  // sla_approver_eval_value
            \PDO::PARAM_STR,  // sla_approver_eval_type

            \PDO::PARAM_INT,  // sla_requester_eval_value
            \PDO::PARAM_STR,  // sla_requester_eval_type

            \PDO::PARAM_INT,  // centralized
            \PDO::PARAM_STR,  // cost_center
        ];
        
        $registerData = [
            'product_id'         => $productId,
            'supplier_id'        => $supplierId,
            'office_id'          => $officeId,
            'contract_number'    => $row['numero_contrato'],
            'validity_start'     => $start,
            'validity_end'       => $end,
            'expired'            => $end < new \DateTime(),
            'purchase_agreement' => $row['acuerdo_compra'],
            'centralized'        => $pCentralized,
            'price_value'        => $row['precio'],
            'price_currency'     => $row['moneda'],
            'cost_center'        => $row['centro_costo'],
            'slas' => $slas = serialize([
                ProductStatus::CREATED   => SLA::create($row['sla_eval_valor'],   $row['sla_eval_tipo']),
                ProductStatus::ORDERED   => SLA::create($row['sla_order_valor'],  $row['sla_order_tipo']),
                ProductStatus::APPROVED  => SLA::create($row['sla_accept_valor'], $row['sla_accept_tipo']),
                ProductStatus::DELIVERED => SLA::create($row['sla_prod_valor'],   $row['sla_prod_tipo']),
                ProductStatus::CONFIRMED => SLA::create(2, SLA::BUSINESS_DAY),
                ProductStatus::REVIEWED  => SLA::create(2, SLA::BUSINESS_DAY),
                'en_despacho'            => SLA::create($row['sla_delivery_valor'], $row['sla_delivery_tipo']),
            ]),
            'deleted_at' => NULL
        ];
            
        if ($id = $this->checkContractExist($supplierId, $productId, $officeId)) {
            unset(
                $columnTypes[0],
                $columnTypes[1],
                $columnTypes[2],
                $registerData['product_id'],
                $registerData['supplier_id'],
                $registerData['office_id']
            );

            $this->logger->warning('Actualizando contrato id '.$id.' '.$row['id_unidad']. ' - '.$row['id_producto'].' - '.$row['id_proveedor']);
            $this->pdo->update('contracts', $registerData, ['id' => $id], array_values($columnTypes));
        } else {
            $this->logger->warning('Creando contrato '.$row['id_unidad']. ' - '.$row['id_producto'].' - '.$row['id_proveedor']);
            $this->pdo->insert('contracts', $registerData, $columnTypes);
        }
        
         return [
            'entity' => $row,
            'error'  => false,
         ];
    }

    private function checkContractExist($idSupplier, $idProduct, $idOffice)
    {
        return $this->pdo->fetchColumn(
            'select id from contracts where supplier_id = ? and product_id = ? and office_id = ?',
            [$idSupplier, $idProduct, $idOffice],
            0 // column index ==> id 
        );
    }

    private function preload()
    {
        $this->pdo->setFetchMode(\PDO::FETCH_KEY_PAIR);

        $this->suppliers    = $this->pdo->fetchAll('select rut, id from suppliers');
        $this->products     = $this->pdo->fetchAll('select code, id from products');
        $this->pCentralized = $this->pdo->fetchAll('select code, centralized from products');
        $this->officeByCode = $this->pdo->fetchAll('select code, id from offices');
    }
}
