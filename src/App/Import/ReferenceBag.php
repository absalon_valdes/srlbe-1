<?php
namespace App\Import;

/**
 * Class ReferenceBag
 * @package App\Import
 */
class ReferenceBag
{
    /**
     * @var array
     */
    private $references = array();

    /**
     * @param $id
     * @return bool
     */
    public function hasReference($id)
    {
        return array_key_exists($id, $this->references);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getReference($id)
    {
        return $this->references[$id];
    }

    /**
     * @param $id
     * @param $object
     */
    public function setReference($id, $object)
    {
        $this->references[$id] = $object;
    }

    /**
     * @param $id
     * @param $object
     */
    public function addReference($id, $object)
    {
        if ($this->hasReference($id)) {
            return;
        }

        $this->setReference($id, $object);
    }
}
