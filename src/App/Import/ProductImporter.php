<?php

namespace App\Import;

use App\Entity\Employee;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\EmployeeRepository;
use App\Repository\ProductRepository;
use App\Util\RutUtils;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use JMS\DiExtraBundle\Annotation as Di;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ProductImporter
 * @package App\Import
 * 
 * @Di\Service("product_importer")
 */
class ProductImporter
{
    const FILE_DOCUMENT     = 'file.doc';
    const FILE_IMAGE        = 'file.img';
    const OLD_SYSTEM_URL    = 'http://sistemaderequerimientosbe.cl/';
    const OLD_SYSTEM_FOLDER = '/www/drupal/';

    /**
     * @var LoggerInterface
     * @Di\Inject("logger")
     */
    public $logger;

    /**
     * @var string
     * @Di\Inject("%products_files_dir%")
     */
    public $uploadDir;
    
    /**
     * @var string
     * @Di\Inject("%kernel.root_dir%")
     */
    public $rootDir;

    /**
     * @var UserManager
     * @Di\Inject("fos_user.user_manager")
     */
    public $userManager;

    /**
     * @var ProductRepository
     * @Di\Inject("product_repo")
     */
    public $productRepo;

    /**
     * @var CategoryRepository
     * @Di\Inject("category_repo")
     */
    public $categoryRepo;

    /**
     * @var EmployeeRepository
     * @Di\Inject("employee_repo")
     */
    public $employeeRepo;

    /**
     * @var string
     */
    public $filesPath;

    /**
     * @var string
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $manager;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var array
     */
    private $references;

    /**
     * @var Connection
     * @Di\Inject("database_connection")
     */
    public $database;

    /**
     * @var Filesystem
     * @Di\Inject("filesystem")
     */
    public $filesystem;

    /**
     * @var string
     */
    public static $imagesDirectory;

    /**
     * @var bool
     */
    public static $onlyImages = false;

    /**
     * @param $data
     */
    public function import($data)
    {
        $this->filesPath = sprintf('%s/data/fixtures/drupal', $this->rootDir);
        
        foreach ($data as $categorySlug => $products) {
            if (!preg_match('#^[Pp]roductos/#', $categorySlug)) {
                $categorySlug = 'Productos/'.$categorySlug;
            }
            
            foreach ($products as $product) {
                if (empty($product['codigo_tecnico'])) {
                    continue;
                }

                $this->product = $this->getOrCreateProduct($product['codigo_tecnico']);
                
                $this->addFile($product, self::FILE_IMAGE);
                $this->addFile($product, self::FILE_DOCUMENT);
                
                if (self::$onlyImages) {
                    $this->logger->info(sprintf('Imagen actualizada para %s', $this->product->getCode())); 
                    continue;
                }

                $this->product->setName($this->cleanName($product['nombre']));
                $this->product->setCode($product['codigo_tecnico']);
                $this->product->setDescription($product['descripcion']);
                $this->product->setTechnicalSpecs($product['especificaciones_tecnicas']);
                $this->product->setApplySpecs($product['normas_aplicacion']);
                $this->product->setWarnings($product['precauciones']);
                $this->product->setInstructions($product['instrucciones']);
                $this->product->setCentralized($product['centralizado']);
                
                $this->product->setCategory($this->categoryRepo->getOrCreateFromSlug($categorySlug));
                
                if ($product['evaluador']) {
                    $rut = RutUtils::normalize($product['evaluador']);
                    
                    /** @var Employee $approverEmployee */
                    if ($approverEmployee = $this->employeeRepo->findOneByRut($rut)) {
                        
                        /** @var User $approverUser */
                        if ($approverUser = $approverEmployee->getUser()) {
                            $approverUser->addRole('ROLE_APPROVER');
                            
                            $this->manager->persist($approverUser);
                            $this->product->setApprover($approverUser);

                            $this->logger->info(sprintf('Approver assigned %s -> %s [%scentralized]', 
                                $approverUser->getUsername(),
                                $this->product->getCode(),
                                !$this->product->isCentralized() ? 'des' : ''
                            ));
                        }
                    }
                }

                $this->manager->persist($this->product);
            }
        }
        
        $this->manager->flush();
    }

    /**
     * @param $imagesDirectory
     */
    public static function setImagesDirectory($imagesDirectory)
    {
        self::$imagesDirectory = $imagesDirectory;
    }

    /**
     * @param boolean $onlyImages
     */
    public static function setOnlyImages($onlyImages)
    {
        self::$onlyImages = $onlyImages;
    }
    
    /**
     * @param $code
     * @return Product
     */
    private function getOrCreateProduct($code)
    {
        if (null === $this->references) {
            $this->prefetchReferences();
        }
        
        $id = $this->references['products'][$code] ?? null;
        
        if ($id && ($product = $this->productRepo->find($id))) {
            return $product;
        }

        $this->logger->info(sprintf('Product %s not found', $code));
        
        return new Product;
    }

    /**
     * Prefetch products is by code for later use
     */
    private function prefetchReferences()
    {
        $this->database->setFetchMode(\PDO::FETCH_KEY_PAIR);

        $productsTable = $this->manager
            ->getClassMetadata(Product::class)
            ->getTableName();

        $this->references = [
            'products' => $this->database->fetchAll('select code, id from '.$productsTable),
        ];
    }

    /**
     * @param $productName
     * @return string
     */
    private function cleanName($productName)
    {
        $arrName = explode('|', $productName);

        if (count($arrName) === 1) {
            return trim(ucfirst($arrName[0]));
        }

        return trim(ucfirst($arrName[1]));
    }

    /**
     * @param $rowProduct
     * @param $fileType
     * @return void
     */
    private function addFile($rowProduct, $fileType)
    {
        $file = $fileType === self::FILE_IMAGE ? 
            trim($rowProduct['imagenes']) : trim($rowProduct['documentos']);
        
        $maintenance = false;
        
        if (empty($file)) {
            $code = strtoupper($rowProduct['codigo_tecnico']);
            $altCode = str_replace('-', '_', $code);
            $altCode2 = str_replace('_', '-', $code);
            $path = sprintf('%s/{%s,%s,%s}.*', self::$imagesDirectory, $code, $altCode, $altCode2);
            
            foreach (array_unique(glob($path, GLOB_BRACE)) as $item) {
                $maintenance = true;
                $file = $item;
            }
        }
        
        $expName = explode('/', $file);
        if (count($expName) <= 1) {
            return;
        }
        
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $name      = sprintf('%s.%s', md5($file), $extension);

        if ($this->uploadFileExist($name, $fileType)) {
            return;
        }
        
        $filesPath = sprintf('%s/prod_img/%s', $this->filesPath, $name);
        
        $this->moveFile($file, $filesPath);
        $attachment = $this->generateAttachments($filesPath, $name);
        
        if ($maintenance) {
            if (!in_array($attachment, $this->product->getImages() ?? [], true)) {
                $this->product->addImage($attachment);
                return;
            }
        }
        
        // if there's an issue with the file upload
        if (!$attachment) {
            return;
        }

        if ($fileType === self::FILE_IMAGE) {
            $this->product->addImage($attachment);
            return;
        }

        $this->product->addAttachment(pathinfo($file, PATHINFO_FILENAME), $attachment);
    }

    /**
     * @param $file
     * @param $fileType
     * @return bool|void
     */
    private function uploadFileExist($file, $fileType)
    {
        //Verify if the file already exist in the uploads directory
        $uploadFile = sprintf('%s/%s', $this->uploadDir, $file);
        
        if (!is_file($uploadFile)) {
            return;
        }

        if ($fileType === self::FILE_IMAGE) {
            $this->product->addImage($file);
        } else {
            $this->product->addAttachment(pathinfo($file, PATHINFO_FILENAME), $file);
        }
        return true;
    }

    /**
     * @param $file
     * @param $filesPath
     */
    private function moveFile($file, $filesPath)
    {
        //Try to copy the file from the old system path
        if (!$this->copyFile($file, $filesPath)) {
            //If the copy fails, download from the old system url
            $urlFile = $this->urlGetContents($file);
            if (!$urlFile) {
                $this->logger->info("Error en la descarga de la imagen del archivo $file");
                return;
            }
            file_put_contents($filesPath, $urlFile);
        }
    }

    /**
     * @param $filesPath
     * @param $name
     * @return bool|void
     */
    private function generateAttachments($filesPath, $name)
    {
        // Generate the attachments for the file
        if (!is_file($filesPath)) {
            return false;
        }

        if (!$this->checkValidMimeType($filesPath)) {
            unlink($filesPath);
            return;
        }

        $upload = new UploadedFile($filesPath, $name, null, null, null, true);
        $upload->move($this->uploadDir, $name);
        
        return $name;
    }

    /**
     * @param $path
     * @param $newPath
     * @return bool
     */
    private function copyFile($path, $newPath)
    {
        $path = urldecode(str_replace(self::OLD_SYSTEM_URL, self::OLD_SYSTEM_FOLDER, $path));

        $result = false;
        try {
            if (!$result = $this->filesystem->copy($path, $newPath)) {
                //$this->logger->warning((bool) $result.' - '.$newPath);
            }
        } catch (\Exception $e) {
            //$this->logger->warning($e->getMessage());
        } finally {
            return $result;
        }
    }

    /**
     * @param $file
     * @return bool
     */
    private function checkValidMimeType($file)
    {
        $mime = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $file);
        $validMime = [
            'application/pdf',
            'application/x-pdf',
            'image/gif',
            'image/jpg',
            'image/jpeg',
            'image/png',
        ];

        if (in_array($mime, $validMime)) {
            return true;
        }

        $this->logger->info("Tipo de mime: $mime invalido para el archivo: $file");
        
        return false;
    }

    /**
     * @param $url
     * @return bool|mixed|void
     */
    private function urlGetContents($url)
    {
    }
}