<?php

namespace App\Import;

use App\Entity\Office;
use App\Import\Helper\RenderImporterErrorMessage;
use App\Repository\CategoryRepository;
use App\Repository\OfficeRepository;
use Behat\Transliterator\Transliterator;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\Service;

/**
 * @Service("office_importer")
 */
class OfficeImporter extends Importer
{
    /**
     * @var OfficeRepository $officeRepo
     * @Inject("office_repo")
     */
    public $officeRepo;

    /**
     * @var CategoryRepository $categoryRepo
     * @Inject("category_repo")
     */
    public $categoryRepo;

    /**
     * @var RenderImporterErrorMessage $errorMessageRender
     * @Inject("render_importer_error_message")
     */
    public $errorMessageRender;

    private $data;

    private $cities;

    /**
     * @param array $data
     * @return mixed
     */
    protected function transform(array $data)
    {
        $this->data = $data;
        return $data;
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function doImport(array $data)
    {
        $violations = [];
        
        $geoCategories = $this->categoryRepo->findOneByName('Geografico');
        $this->cities = $this->categoryRepo->getLeafs($geoCategories, 'slug');
        $this->createOfficesByType($this->data, 'gerencia_general');
        $this->createOfficesByType($this->data, 'gerencia');
        $this->createOfficesByType($this->data, 'subgerencia');
        $this->createOfficesByType($this->data, 'nombre_dependencia');
        $this->createOfficesByType($this->data, 'nombre_unidad');

        return $violations;
    }

    private function createOfficesByType($data, $type)
    {
        $parent = $this->getParentByType($type);
        foreach ($data as $offices) {
            if (array_search($offices[$type], $offices, true) === 'nombre_unidad') {
                $parentOffice = !$parent ? null : $offices[$parent];
                $this->importOffices($offices, $parentOffice);
            }
        }
    }

    private function getParentByType($type) {
        $parent = [
            'gerencia_general'   => null,
            'gerencia'           => 'gerencia_general',
            'subgerencia'        => 'gerencia',
            'nombre_dependencia' => 'subgerencia',
            'nombre_unidad'      => 'nombre_dependencia',
        ];

        return $parent[$type];
    }

    private function importOffices($officeInfo, $parentName)
    {
        $code = $officeInfo['id_sucursal'];

        $officeExist  = $this->officeRepo->findOneByCode($code);
        if ($officeExist) {
            //$office = $officeExist;
            $message  = sprintf('[Importador Sucursales] - Error unidad %s ya existe', $code);
            return $this->errorMessageRender->renderError($officeInfo, $message);
        }

        $slugCity = Transliterator::urlize($officeInfo['comuna']);
        $city = array_filter($this->cities, function ($item) use ($slugCity) {
            return explode('/', $item->getSlug())[2] === $slugCity;
        });
        $city = reset($city);

        if (!$city) {
            $message  = sprintf('[Importador Sucursales] - Error en %s %s, Comuna: %s no existe', $officeInfo['id_sucursal'], $officeInfo['nombre_unidad'], $officeInfo['comuna']);
            return $this->errorMessageRender->renderError($officeInfo, $message);
        }

        $type = Office::TYPE_OFFICE;

        if ($officeInfo['id_dependencia'] === $officeInfo['id_unidad']) {
            //$code = $officeInfo['id_dependencia'];
            $type = Office::TYPE_CENTRAL;
        }

        /*if (($type === Office::TYPE_OFFICE) && strlen($officeInfo['id_sucursal']) < 4) {
            $code = trim($officeInfo['id_unidad']);
        }*/


        $office = $this->officeRepo->create();
        $office->setName($officeInfo['nombre_unidad']);
        $office->setCode($code);
        $office->setDependencyCode($officeInfo['id_unidad']);
        $office->setAddress(trim($officeInfo['direccion']));
        $office->setCostCenter($officeInfo['centro_costo']);
        $office->setCity($city);
        $office->setType($type);

        if ($parentName) {
            $parent = $this->officeRepo->findOneByName($parentName);

            if (!$parent) {
                if (!$parent = $this->officeRepo->findOneByName($officeInfo['subgerencia'])) {
                    if (!$parent = $this->officeRepo->findOneByName($officeInfo['gerencia'])) {
                        if (!$parent = $this->officeRepo->findOneByName($officeInfo['gerencia_general'])) {
                            $message = sprintf('[Importador Sucursales] - Error en %s - %s Unidad padre: %s no existe', $officeInfo['id_sucursal'], $officeInfo['nombre_unidad'], $parentName);
                            return $this->errorMessageRender->renderError($officeInfo, $message);
                        }
                    }
                }
            }

            $office->setParent($parent);
        }

        if ($officeInfo['id_dependencia'] === $officeInfo['id_unidad']) {
            $office->setDependencyCode($officeInfo['id_dependencia']);
        }

        $this->officeRepo->persist($office, true);
    }


}