<?php

namespace App\Import;

/**
 * Interface ImporterInterface
 * @package App\Import
 */
interface ImporterInterface
{
    /**
     * @param $filename
     * @return mixed
     */
    public function import($filename);
}
