<?php

namespace App\Import\Helper;

use JMS\DiExtraBundle\Annotation as Di;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;

/**
 * @Di\Service("render_importer_error_message")
 */
class RenderImporterErrorMessage
{

    /**
     * @var LoggerInterface $logger
     * @Di\Inject("logger")
     */
    public $logger;

    public function renderError(array $info, $logMessage, $isWarn = false, $uiMessage = false)
    {
        $uiMessage = !$uiMessage ? $logMessage : $uiMessage;
        $this->logger->log($isWarn ? Logger::WARNING : Logger::INFO, $logMessage);

        return [
            'entity' => $info,
            'error'  => $uiMessage,
        ];
    }
}
