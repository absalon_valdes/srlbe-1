<?php

namespace App\Exception;

use FOS\RestBundle\View\ExceptionWrapperHandlerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ExceptionWrapperHandler implements ExceptionWrapperHandlerInterface
{
    public function wrap($data)
    {
        $exception = $data['exception'];
        $message = $data['message'];

        if ($exception instanceof AccessDeniedHttpException) {
            $message = 'Access Denied';
        }

        return [
            'code' => $data['code'],
            'message' => $message
        ];
    }
}