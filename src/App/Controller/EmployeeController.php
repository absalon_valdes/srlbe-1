<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Form\ChangeEmployeeDataType;
use App\Form\ChangeOfficeType;
use App\Menu\MainMenu;
use App\Model\ChangeOffice;
use App\Repository\EmployeeRepository;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EmployeeController
 * @package App\Controller
 */
class EmployeeController extends FOSRestController
{
    /**
     * @var EmployeeRepository
     * @DI\Inject("employee_repo")
     */
    public $employeeRepo;

    /**
     * @var \Redis
     * @DI\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var ContainerInterface
     * @DI\Inject("service_container")
     */
    public $container;

    /**
     * @DI\Inject("doctrine.orm.default_query_cache")
     */
    public $doctrineQueryCache;

    /**
     * @DI\Inject("doctrine.orm.default_result_cache")
     */
    public $doctrineResultCache;

    /**
     * @Get("/update-office-phone", name="employee_update_office_phone")
     * @View("_partial/office_data.twig")
     * @return array
     */
    public function officeDetailAction()
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();
        
        if (null === $employee) {
            return [
                'employee' => null,
                'change_office' => null,
            ];
        }

        /** @var FormInterface $form */
        $form = $this->createForm(
            ChangeOfficeType::class,
            ChangeOffice::createForEmployee($employee)
        );
        
        return [
            'employee' => $employee,
            'change_office' => $form->createView(),
        ];
    }

    /**
     * @Get("/update-employee-data", name="employee_update_data")
     */
    public function changeDataAction()
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();

        if (!$employee->getDeliveryAddress()) {
            $employee->setDeliveryAddress($employee->getOffice());
        }

        /** @var FormInterface $form */
        $form = $this->createForm(
            ChangeEmployeeDataType::class,$employee
        );

        return $this->render('_partial/address_data.twig',
        [   'employee' => $employee,
            'change_data' => $form->createView(),
        ]);
    }

    /**
     * @Post("/change-office", name="employee_office_change")
     * @param Request $request
     * @return array|\FOS\RestBundle\View\View
     */
    public function changeOfficeAction(Request $request)
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();

        /** @var FormInterface $form */
        $form = $this->createForm(
            ChangeOfficeType::class,
            ChangeOffice::createForEmployee($employee)
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $employee->setOffice($form->getData()->getOffice());
            $employee->setPhone($form->getData()->getPhone());
            $this->employeeRepo->save($employee);
            $this->cacheClear();

            return $this->view(null, 200);
        }

        return $this->view(null, 500);
    }

    /**
     * @Post("/change-data", name="employee_data_change")
     * @param Request $request
     * @return array|\FOS\RestBundle\View\View
     */
    public function changeAddressAction(Request $request)
    {
        /** @var Employee $employee */
        $employee = $this->getUser()->getEmployee();

        /** @var FormInterface $form */
        $form = $this->createForm(
            ChangeEmployeeDataType::class, $employee
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->employeeRepo->save($employee);
            $this->cacheClear();
            $address = $employee->getDeliveryAddress()->getAddress();
            if($employee->getAlternateAddress()){
                $address.=','.($employee->getAlternateAddress());
            }
            $phone = $employee->getPhone();
            if($employee->getSecondPhone()){
                $phone = $employee->getSecondPhone();
            }
            return $this->view([
                'address' => $address,
                'phone' =>$phone,
            ], 200);
        }

        return $this->view(null, 500);
    }

    private function cacheClear()
    {
        $this->doctrineQueryCache->flushAll();
        $this->doctrineResultCache->flushAll();
        $this->redis->del(MainMenu::MANT_KEY, MainMenu::PROD_KEY, MainMenu::REPR_KEY);
    }
}
