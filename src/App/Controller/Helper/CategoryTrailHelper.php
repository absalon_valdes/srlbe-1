<?php 

namespace App\Controller\Helper;

use App\Entity\Category;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\Service;

/**
 * @Service("category_trail_helper")
 */
class CategoryTrailHelper
{
    /**
     * @var \APY\BreadcrumbTrailBundle\BreadcrumbTrail\Trail
     * @Inject("apy_breadcrumb_trail")
     */
    public $breadcrumb;

    public function getTrail(Category $category)
    {
	    if (!$category) {
            return;
        }

        $ancestors = [$category];

        while ($category = $category->getParent()) {
            $ancestors[] = $category;
        }

        /** @var Category $category */
        foreach (array_reverse($ancestors) as $category) {
            $this->breadcrumb->add($category->getName(), 'requisitions_catalog', [
                'slug' => $category->getSlug()
            ]);
        }
    } 
}