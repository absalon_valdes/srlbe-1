<?php
/**
 * Created by PhpStorm.
 * User: steven
 * Date: 15-12-16
 * Time: 15:59
 */

namespace App\Controller;

use App\Repository\SupplierRepository;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/proveedor")
 */
class Supplier
{
    /**
     * @var SupplierRepository
     * @Inject("supplier_repo")
     */
    public $supplierRepo;

    /**
     * @Get("/encontrar-usuario", name="find_supplier")
     * @param Request $request
     * @return View
     */
    public function findSupplier(Request $request)
    {
        $result = [];

        if (!empty($q = $request->query->get('r'))) {
            $result = $this->supplierRepo->findOneByRut($q);
        }
            
        return ['results' => $result];
    }
}