<?php

namespace App\Controller;

use App\Contact\Factory;
use App\Contact\Mailer;
use App\Form\ContactType;
use App\Helper\SpecialOfficeHelper;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Contact
 * @package App\Controller
 * @Breadcrumb("Portada", route={"name"="dashboard"})
 */
class Contact extends FOSRestController
{

    /**
     * @var array
     */
    private $coordinators;

    /**
     * @var SpecialOfficeHelper
     * @Di\Inject("special_office_helper")
     */
    public $specialOfficeHelper;

    /**
     * @Di\Inject("%kernel.root_dir%")
     */
    public $rootDir;

    /**
     * @Breadcrumb("Formulario de contacto")
     * @Extra\Route("/contacto", name="contact_index")
     * @Rest\View("contact.twig")
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {

        $filename = 'coordinadores.php.cache';

        if($coordinators = $this->specialOfficeHelper->getConfig('coordinators') ){
            if(file_exists(sprintf('%s/../var/data/'.$coordinators, $this->rootDir))){
                $filename = $coordinators;
            }
        }

        $this->coordinators = require_once sprintf('%s/../var/data/'.$filename, $this->rootDir);

        return ["data" => $this->coordinators];
    }
}
