<?php

namespace App\Controller;

use App\Controller\Helper\CategoryTrailHelper;
use App\Controller\Helper\FormUploadHelper;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Form\ApproverCloseType;
use App\Form\ChangeEmployeeDataType;
use App\Form\ProductAppealRejectionType;
use App\Form\ProductAppealType;
use App\Form\ProductChangeSupplierType;
use App\Form\ProductDeniedResponseType;
use App\Form\ProductPurchaseOrder;
use App\Form\ProductRejectionType;
use App\Form\ProductServiceResponsibleType;
use App\Form\ProductSupplierDeclineType;
use App\Form\RequisitionType;
use App\Form\RolType;
use App\Form\SupplierCertificationType;
use App\Form\WorkEvaluationType;
use App\Form\WorkMoreInfoType;
use App\Form\WorkSupplierRatingType;
use App\Form\WorkValidationType;
use App\Helper\RequisitionTypeResolver;
use App\Repository\ContractRepository;
use App\Repository\DeclinedRequisitionRepository;
use App\Repository\EmployeeRepository;
use App\Repository\OfficeRepository;
use App\Repository\ProductRepository;
use App\Repository\RequisitionRepository;
use App\Repository\SupplierRepository;
use App\Repository\UserRepository;
use App\Workflow\ProcessInstance;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as Di;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Breadcrumb("Portada", route={"name"="dashboard"})
 */
class Requisitions extends FOSRestController
{
    const EVENT_AFTER_EVALUATE = 'requisition.after.evaluate';
    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    protected $workflow;

    /**
     * @var \APY\BreadcrumbTrailBundle\BreadcrumbTrail\Breadcrumb
     * @Di\Inject("apy_breadcrumb_trail")
     */
    protected $breadcrumb;

    /**
     * @var CategoryTrailHelper
     * @Di\Inject("category_trail_helper")
     */
    protected $categoryTrail;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    protected $requisitionRepo;

    /**
     * @var EmployeeRepository
     * @Di\Inject("employee_repo")
     */
    protected $employeeRepo;
    /**
     * @var SupplierRepository
     * @Di\Inject("supplier_repo")
     */
    protected $supplierRepo;

    /**
     * @var FormUploadHelper
     * @Di\Inject("form_upload_helper")
     */
    protected $uploadHelper;

    /**
     * @var ContractRepository
     * @Di\Inject("contract_repo")
     */
    protected $contractRepo;

    /**
     * @var EventDispatcherInterface
     * @Di\Inject("event_dispatcher")
     */
    protected $dispatcher;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    protected $typeResolver;

    /**
     * @var OfficeRepository
     * @Di\Inject("office_repo")
     */
    protected $officeRepo;

    /**
     * @var UserRepository
     * @Di\Inject("user_repo")
     */
    protected $userRepo;

    /**
     * @var DeclinedRequisitionRepository
     * @Di\Inject("declined_requisition_repo")
     */
    public $declinedRequisitionRepo;

    /**
     * @var EventDispatcherInterface
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    /**
     * @Extra\Security("is_granted('ROLE_REQUESTER')")
     * @Extra\ParamConverter("category", class="App:Category")
     * @Extra\ParamConverter("products", class="App:Product", options={
     *     "map_method_signature" = true,
     *     "repository_method" = "findByCategory"
     * })
     * @Rest\Get("/catalogos/{slug}", name="requisitions_catalog", requirements={"slug"=".+"})
     * @Rest\View("requisition/catalog.twig")
     *
     * @param Category $category
     * @param $products
     *
     * @return array
     */
    public function catalog(Category $category, $products)
    {
        $this->categoryTrail->getTrail($category);

        return [
            'category' => $category,
            'products' => $products,
        ];
    }

    /**
     * @Extra\Security("is_granted('ROLE_EMPLOYEE')")
     * @Breadcrumb("Carro de solicitudes")
     * @Rest\Get("/carro-solicitudes", name="requisitions_cart")
     * @Rest\View("requisition/cart.twig")
     */
    public function cart()
    {
        $checkoutForm = $this->createFormBuilder()
            ->add('submit', SubmitType::class)
            ->getForm();

        $deleteForm = $this->createFormBuilder()
            ->add('id', HiddenType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        return [
            'requisitions' => $this->requisitionRepo->findInCart(),
            'delete_form' => $deleteForm->createView(),
            'checkout_form' => $checkoutForm->createView()
        ];
    }

    /**
     * @Rest\Post("/crear-pedido", name="requisitions_create_order")
     * @throws \Exception
     */
    public function create()
    {
        $success = [];
        $requisitions = $this->requisitionRepo->findInCart();

        /** @var Requisition $requisition */
        foreach($requisitions as $requisition) {

            if ($requisition->isType('requisition.virtual')) {
                $this->requisitionRepo->save($requisition);
                continue;
            }

            try {
                $this->requisitionRepo->setNextNumber($requisition);
                $state = $this->workflow->proceed($requisition, 'create');

                if ($state->getSuccessful()) {
                    $this->requisitionRepo->save($requisition);
                    $success[] = $requisition;
                }
                else {
                    $requisition->setNumber(0);
                    $this->requisitionRepo->save($requisition);
                }
            }
            catch (\Exception $e) {
                throw $e;
            }
        }

        return $this->renderView('requisition/createdmsg.twig', [
            'orders' => $success,
        ]);
    }

    /**
     * @Breadcrumb("Solicitudes", route="requisitions_index")
     * @Breadcrumb("Solicitud n° {requisition.number}")
     * @Rest\Get("/solicitud/{id}", name="requisition_detail")
     * @Rest\Get("/solicitud/{id}/orden-trabajo", name="requisition_work_order")
     * @Rest\Get("/solicitud/{id}/cert-evaluador", name="requisition_approver_proxy_cert")
     * @Rest\View("requisition/detail.twig")
     * @Extra\Security("is_granted('requisition_view', requisition) or is_granted('ROLE_BACKOFFICE')")
     *
     * @param Requisition $requisition
     *
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function detail(Requisition $requisition)
    {
        $this->eventDispatcher->dispatch(
            'product.requisition.view',
            new GenericEvent($requisition)
        );

        return [
            'requisition' => $requisition,
            'history' => $this->workflow->getHistory($requisition),
            //'current_state' => $this->workflow->getCurrentState($requisition),
            //'is_complete' => $this->workflow->isComplete($requisition),
            'user' => $user = $requisition->getRequester(),
            'employee' => $employee = $this->employeeRepo->findOneByUser($user),
            'office' => $office = $employee->getOffice(),
            'alternate' => $this->officeRepo->findAlternateRequester($office, $user),

            'rejection' => $this->createForm(ProductRejectionType::class)->createView(),
            'appeal' => $this->createForm(ProductAppealType::class)->createView(),
            'deny' => $this->createForm(ProductAppealRejectionType::class)->createView(),
            'response' => $this->createForm(ProductDeniedResponseType::class)->createView(),
            'more_info' => $this->createForm(WorkMoreInfoType::class)->createView(),
            'supplier_rating' => $this->createForm(WorkSupplierRatingType::class)->createView(),

            'decline' => $this->createForm(ProductSupplierDeclineType::class, $requisition)->createView(),
            'product_service_responsible' => $this->createForm(ProductServiceResponsibleType::class, $requisition)->createView(),
            'evaluation' => $this->createForm(WorkEvaluationType::class, $requisition)->createView(),
            'change_sup' => $this->createForm(ProductChangeSupplierType::class, $requisition)->createView(),
            'purchase_order' => $this->createForm(ProductPurchaseOrder::class, $requisition)->createView(),
            'work_validation' => $this->createForm(WorkValidationType::class, $requisition)->createView(),
            'provider' =>  $this->createForm(RolType::class)->createView(),

            'approver_close' => $this->createForm(ApproverCloseType::class,$requisition)->createView()
        ];
    }

    /**
     * @Extra\Security("is_granted('ROLE_REQUESTER')")
     * @Extra\ParamConverter("category", class="App:Category")
     * @Extra\ParamConverter("product", class="App:Product", options={
     *     "repository_method" = "findOneByCode"
     * })
     * @Extra\ParamConverter("contracts", class="App:Contract", options={
     *     "map_method_signature" = true,
     *     "repository_method" = "findByProduct"
     * })
     * @Extra\Method({"GET", "POST"})
     * @Rest\Route("/pedidos/{slug}/{code}", name="requisitions_request", requirements={"slug"=".+", "code"=".+"})
     * @Rest\Route("/pedidos/{code}", name="requisitions_request_product", requirements={"code"=".+"})
     * @Rest\View("requisition/product/request.twig")
     *
     * @param Request $request
     * @param Category $category
     * @param Product $product
     * @param $contracts
     *
     * @return array|\FOS\RestBundle\View\View|Response
     * @throws \LogicException
     */
    public function request(Request $request, Product $product, $contracts, Category $category = null)
    {
        if ($category) {
            $this->categoryTrail->getTrail($category);
        }

        $this->breadcrumb->add($product->getName());

        /** @var Requisition $requisition */
        $type = $this->typeResolver->getType($product);
        if($type == Requisition::TYPE_VCARD){
            return $this->routeRedirectView('vcard_available');
        }

        if(empty($contracts)){
            return $this->redirectToRoute('requisitions_catalog',
                array('slug' => $product->getCategory()->getSlug())
            );
        }
        $requisition = $this->requisitionRepo->create();
        $requisition->setType($type);
        $requisition->setProduct($product);
        $requisition->setRequester($this->getUser());
        $requisition->setStateDueDate(new \DateTime);
        $requisition->setContract($contracts[0]);

        $form = $this->createForm(RequisitionType::class, $requisition);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->requisitionRepo->save($requisition);
            if ($this->workflow->start($requisition)->getSuccessful()) {
                $this->addFlash('success', 'requisition.cart.new_item');

                return $this->routeRedirectView('requisitions_cart');
            }
        }

        if ($lock = $this->requisitionRepo->findCountPendingEval()) {
            $this->addFlash('danger', 'requisition.lock.pending_eval');
        }

        $response = [
            'category' => $category,
            'product' => $product,
            'contracts' => $contracts,
            'requisition' => $requisition,
            'form' => $form->createView(),
            'lock' => $lock > 0,
        ];

        //TODO: remover variable una vez ordenada todas las implementaciones de los distintos proveedores
        $response['lockUniform'] = false;

        return $response;
    }

    /**
     * @Extra\Security("user == requisition.getRequester()")
     * @Rest\Delete("/solicitud/{id}/eliminar", name="requisition_delete")
     *
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function delete(Requisition $requisition)
    {
        $this->requisitionRepo->remove($requisition, true);
        $this->addFlash('success', 'La solicitud ha sido eliminada');

        return $this->routeRedirectView('requisitions_cart');
    }

    /**
     * @Breadcrumb("Editar solicitud")
     * @Breadcrumb("Solicitud n° {requisition.number}")
     * @Extra\Security("is_granted('ROLE_REQUESTER')")
     * @Extra\ParamConverter("requisition", class="App:Requisition")
     * @Extra\Method({"GET", "POST"})
     * @Extra\Route("/solicitud/{id}/editar", name="requisition_edit")
     * @Rest\View("requisition/product/request.twig")
     *
     * @param Requisition $requisition
     * @param Request     $request
     *
     * @return array|\FOS\RestBundle\View\View
     */
    public function edit(Requisition $requisition, Request $request)
    {
        /** @var Product $product */
        $product = $requisition->getProduct();

        /** @var ArrayCollection $contracts */
        $contracts = $this->contractRepo->findByProduct($product);

        $form = $this->createForm(RequisitionType::class, $requisition);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->requisitionRepo->save($requisition);
            $this->addFlash('success', 'requisition.request.updated');

            return $this->routeRedirectView('requisitions_cart');
        }

        $response = [
            'category' => $product->getCategory(),
            'product' => $product,
            'contracts' => $contracts,
            'requisition' => $requisition,
            'form' => $form->createView(),
            'lock' => false,
            'lockUniform'=>false
        ];

        //TODO: remover variable una vez ordenada todas las implementaciones de los distintos proveedores
        $response['lockUniform'] = false;

        return $response;
    }

    /**
     * @Rest\Post("/solicitud/{id}/evaluador-cambiar", name="product_approver_change")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function approverChangeSupplier(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ProductChangeSupplierType::class, $requisition);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->requisitionRepo->save($requisition);

            return $this->view($form->getErrors(), 200);
        }

        return $this->view($form, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/justificar-cambio", name="supplier_change_justify")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function changeSupplierJustify(Request $request, Requisition $requisition)
    {
        $data = $request->request->get('supplier_change');
        $requisition->addMetadata('supplier_change', $data);
        $this->requisitionRepo->save($requisition);

        return $this->view(null, 200);
    }

    /**
     * @Rest\Post("/solicitud/{id}/rechazar", name="product_reject")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function reject(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ProductRejectionType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $requisition->addMetadata('rejection', $form->getData());

            $this->requisitionRepo->save($requisition);

            if ($this->workflow->proceed($requisition, 'reject')->getSuccessful()) {
                return $this->view(null, 200);
            }
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/apelar", name="product_appeal")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function appeal(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ProductAppealType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $requisition->addMetadata('appeal', [
                'comment' => $data['comment'],
                'files' => $this->uploadHelper->upload($data['files']),
            ]);

            $this->requisitionRepo->save($requisition);

            if ($this->workflow->proceed($requisition, 'appeal')->getSuccessful()) {
                return $this->view(null, 200);
            }
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/rechazar-apelacion", name="product_appeal_reject")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function deny(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ProductAppealRejectionType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $requisition->addMetadata('denial', [
                'reason' => $data['reason'],
                'comment' => $data['comment'],
                'files' => $this->uploadHelper->upload($data['files']),
            ]);

            $this->requisitionRepo->save($requisition);

            if ($this->workflow->proceed($requisition, 'deny')->getSuccessful()) {
                return $this->view(null, 200);
            }
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/evaluar-respuesta", name="product_response")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function response(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ProductDeniedResponseType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $requisition->addMetadata('response', $form->getData());

            $this->requisitionRepo->save($requisition);

            return $this->view(null, 200);
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/aceptar", name="requisition_accept")
     *
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function accept(Requisition $requisition)
    {
        if (!$this->workflow->proceed($requisition, 'accept')->getSuccessful()) {
            return $this->view(null, 500);
        }

        $this->requisitionRepo->save($requisition);

        if (!$this->workflow->proceed($requisition, 'deliver')->getSuccessful()) {
            return $this->view(null, 500);
        }

        $requisition->addMetadata('work_order', $workOrder = time());
        $this->requisitionRepo->save($requisition);

        return $this->view($workOrder, 200);
    }

    /**
     * @Rest\Post("/solicitud/{id}/cambiar", name="requisition_change_supplier")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function changeSupplier(Request $request, Requisition $requisition)
    {

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        $form = $this->createForm(ProductChangeSupplierType::class, $requisition);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->requisitionRepo->save($requisition);
            $result = $this->workflow->proceed($requisition, 'create');
            if(!$result){
                $em->getConnection()->rollBack();
                return $this->view($result->getErrors(), 500);
            }
            $em->getConnection()->commit();
            return $this->routeRedirectView('dashboard',[],200);
        }

        return $this->view($form->getErrors(), 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/proveedor-rechazar", name="requisition_decline")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View|array
     */
    public function decline(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ProductSupplierDeclineType::class, $requisition);
        $form->handleRequest($request);

        $result = $this->workflow->proceed($requisition, 'decline');
        if ($form->isValid() && $result->getSuccessful()) {
            $declined = $this->declinedRequisitionRepo->create();
            $declined->setUser($requisition->getContract()->getSupplier()->getRepresentatives()->get(0));
            $declined->setRequisition($requisition);

            $this->declinedRequisitionRepo->save($declined);
            $this->requisitionRepo->save($requisition);
        }

        return [
            'decline' => $form->createView(),
        ];
    }

    /**
     * @param Requisition $requisition
     *
     * @Rest\Post("/solicitud/{id}/rechazar-rechazo", name="decline_reject")
     * @return \FOS\RestBundle\View\View
     */
    public function declineReject(Requisition $requisition)
    {
        $declined = $this->declinedRequisitionRepo->findOneBy([
            'requisition' => $requisition->getId(),
            'user' => $uid = $requisition->getSupplier()->getId()
        ]);

        if (!$declined) {
            return $this->view('Declined requisition record not found', Response::HTTP_NOT_FOUND);
        }

        $this->declinedRequisitionRepo->remove($declined, true);
        $declinedUids = $requisition->getMetadata('declined');

        if (false !== ($pos = array_search($uid, $declinedUids, true))) {
            unset($declinedUids[$pos]);

            $requisition->addMetadata('declined', $declinedUids);
            $requisition->addMetadata('declined_rejection_uid', $uid);
            $this->requisitionRepo->save($requisition, true);
        }

        return $this->view(null, 200);
    }

    /**
     * @Rest\Post("/solicitud/{id}/proveedor-certificar", name="requisition_confirm")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function confirm(Request $request, Requisition $requisition)
    {
        if ($this->workflow->proceed($requisition, 'confirm')->getSuccessful()) {
            $form = $this->createForm(WorkValidationType::class, $requisition);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->requisitionRepo->save($requisition);

                return $this->view(null, 200);
            }
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/demandante-evaluar", name="requisition_evaluate")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function evaluate(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(WorkEvaluationType::class, $requisition);
        $form->handleRequest($request);

        $result = $this->workflow->proceed($requisition, 'evaluate');
        if ($form->isValid() && $result->getSuccessful()) {
            $this->requisitionRepo->save($requisition);

            $this->eventDispatcher->dispatch(
                self::EVENT_AFTER_EVALUATE, new GenericEvent($requisition)
            );

            return $this->view(null, 200);
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/demandante-calificar", name="requisition_rate")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function rate(Request $request, Requisition $requisition)
    {
        if ($this->workflow->proceed($requisition, 'complete')->getSuccessful()) {
            $form = $this->createForm(WorkSupplierRatingType::class);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $supplier = $requisition->getContract()->getSupplier();
                $supplier->addRating($data);

                $requisition->addMetadata('supplier_rating', $data);

                $this->requisitionRepo->save($requisition);
                $this->requisitionRepo->save($supplier);

                return $this->routeRedirectView('dashboard');
            }
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/mas-informacion", name="requisition_ask_info")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function askMoreInfo(Request $request, Requisition $requisition)
    {
        if ($this->workflow->proceed($requisition, 'return')->getSuccessful()) {
            $form = $this->createForm(WorkMoreInfoType::class);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $requisition->addSubMetadata('more_info', [
                    'timestamp' => new \DateTime(),
                    'comments' => $data['comment'],
                ]);

                $event = new GenericEvent($requisition);
                $this->eventDispatcher->dispatch('product.more_info_ask.reached', $event);

                $this->requisitionRepo->save($requisition);

                return $this->view(null, 200);
            }
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/proveedor-revalidar", name="requisition_revalidate")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return \FOS\RestBundle\View\View
     */
    public function revalidate(Request $request, Requisition $requisition)
    {
        $status = $this->workflow->proceed($requisition, 'revalidate');

        if ($status->getSuccessful()) {
            $form = $this->createForm(WorkValidationType::class, $requisition);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->requisitionRepo->save($requisition);

                return $this->view(null, 200);
            }
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/proveedor-responsable", name="requisition_work_responsible")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return array
     */
    public function workResponsible(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ProductServiceResponsibleType::class, $requisition);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->requisitionRepo->save($requisition);
        }

        return [
            'product_service_responsible' => $form->createView(),
        ];
    }

    /**
     * @Rest\Post("/solicitud/{id}/adjuntar-oc", name="requisition_attach_po")
     *
     * @param Request     $request
     * @param Requisition $requisition
     *
     * @return array
     */
    public function attachPurchaseOrder(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ProductPurchaseOrder::class, $requisition);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->requisitionRepo->save($requisition);
        }

        return [
            'product_purchase_order' => $form->createView(),
        ];
    }

    /**
     * @Rest\Post("/solicitud/{id}/evaluador-cerrar", name="requisition_approver_close")
     * @Extra\Security("is_granted('ROLE_APPROVER')")
     *
     * @param Request $request
     * @param Requisition $requisition
     * @return array
     */
    public function approverClose(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(ApproverCloseType::class, $requisition);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $status = $this->workflow->proceed($requisition, 'approver_close');

            if ($status->getSuccessful()) {
                $this->requisitionRepo->save($requisition);
            }
            $this->addFlash('success', "La solicitud N°{$requisition->getNumber()} fue cerrada con éxito." );
            return $this->routeRedirectView('dashboard',[],200);
        }

        return [
            'approver_close' => $form->createView()
        ];
    }

    /**
     * @Rest\Post("/proceder/{process}/{id}/{step}", name="requisition_proceed")
     * @param Requisition $requisition
     * @param $process
     * @param null $step
     * @return \FOS\RestBundle\View\View
     */
    public function proceed(Requisition $requisition, $process, $step = null)
    {
        if ($this->workflow->proceed($requisition, $step)->getSuccessful()) {
            $this->requisitionRepo->save($requisition);

            return $this->view(null, 200);
        }

        return $this->view(null, 500);
    }

    /**
     * @Rest\Post("/solicitud/{id}/agregar-meta", name="requisition_add_meta")
     * @param Request $request
     * @param Requisition $requisition
     * @return \FOS\RestBundle\View\View
     */
    public function registerMetadata(Request $request, Requisition $requisition)
    {
        $requisition->addMetadata(
            $request->request->get('key'),
            $request->request->get('value')
        );

        $this->requisitionRepo->save($requisition);

        return $this->view(null, 200);
    }

    /**
     * @Rest\Get("/orden-trabajo-pdf/{id}", name="requisition_work_order_pdf")
     * @Rest\View("requisition/work_order.pdf.twig")
     * @param Requisition $requisition
     * @return Response
     */
    public function workOrderPdf(Requisition $requisition)
    {
        $html = $this->renderView('requisition/work_order.pdf.twig', [
            'requisition' => $requisition,
            'requester' => $this->employeeRepo->findOneByUser($requisition->getRequester())
        ]);

        if (!$document = $this->get('pdf_renderer')->render($html)) {
            throw new NotFoundHttpException('Error al renderizar orden de trabajo');
        }

        return new Response(file_get_contents($document), Response::HTTP_OK, [
            'Content-Type' => 'application/octect-stream',
            'Content-Disposition' => 'attachment; filename="file.pdf"',
        ]);
    }
}
