<?php

namespace App\Controller;

use App\Entity\SpecialRequest;
use App\Entity\UploadedFile;
use App\Entity\User;
use App\Form\ProviderType;
use App\Form\SpecialRequestType;
use App\Helper\FormBuilder;
use App\Repository\SpecialRequestRepository;
use App\Repository\SupplierRepository;
use App\Repository\UserRepository;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SpecialRequestController
 * @package App\Controller
 * @Route("/solicitudes-especiales")
 * @Breadcrumb("Portada", route={"name"="dashboard"})
 * @Breadcrumb("Solicitudes especiales")
 */
class SpecialRequestController extends FOSRestController
{
    /**
     * @var SpecialRequestRepository
     * @DI\Inject("special_request_repo")
     */
    protected $specialRequestRepo;

    /**
     * @var UserRepository
     * @DI\Inject("user_repo")
     */
    protected $userRepo;

    /**
     * @var SupplierRepository
     * @DI\Inject("supplier_repo")
     */
    protected $supplierRepo;

    /**
     * @var EventDispatcherInterface
     * @DI\Inject("event_dispatcher")
     */
    protected $dispatcher;

    /**
     * @Method({"GET", "POST"})
     * @Route("/", name="solicitudes_especiales")
     * @View("special_request/index.twig")
     * @Security("has_role('ROLE_REQUESTER')")
     * @Breadcrumb("Crear solicitud")
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        $specialRequest = new SpecialRequest();
        $specialRequest->setRequester($this->getUser());

        $form = $this->createForm(SpecialRequestType::class);

        try {
            $this->resolveAssignee($specialRequest);
            $this->specialRequestRepo->setNextNumber($specialRequest);
        } catch (\Exception $e) {
            $this->addFlash('danger', $e->getMessage());

            return [
                'form' => $form->createView(),
                'special_request' => $specialRequest,
            ];
        }

        if ($this->handleForm($request, $form, $specialRequest)) {
            $this->addFlash('success', 'Se ha agregado la solicitud al carro');

            return $this->routeRedirectView('solicitudes_especiales_carro');
        }

        return [
            'form' => $form->createView(),
            'special_request' => $specialRequest,
        ];
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/{id}/editar", name="solicitud_especial_editar")
     * @View("special_request/index.twig")
     * @param Request $request
     * @param SpecialRequest $specialRequest
     * @return array
     */
    public function editAction(Request $request, SpecialRequest $specialRequest)
    {
        $form = $this->createForm(SpecialRequestType::class);

        if ($this->handleForm($request, $form, $specialRequest)) {
            $this->addFlash('success', 'La solicitud ha sido guardada correctamente');

            return $this->routeRedirectView('solicitudes_especiales_carro');
        }

        return [
            'form' => $form->createView(),
            'special_request' => $specialRequest,
        ];
    }

    /**
     * @Get("/carro", name="solicitudes_especiales_carro")
     * @View("special_request/cart.twig")
     * @Breadcrumb("Carro de solicitudes especiales")
     */
    public function cartAction()
    {
        return [
            'cart' => $this->specialRequestRepo->findUserCart($this->getUser())
        ];
    }

    /**
     * @Post("/carro/enviar", name="solicitudes_especiales_enviar")
     */
    public function sentRequestsAction()
    {
        $requests = $this->specialRequestRepo->findUserCart(
            $this->getUser()
        );

        try {
            /** @var SpecialRequest $request */
            foreach ($requests as $request) {
                $request->setState(SpecialRequest::STATE_EVALUATION);
                $this->resolveAssignee($request);

                $this->specialRequestRepo->persist($request);
                $this->dispatcher->dispatch('special.request.evaluation', new GenericEvent($request));

            }

            $this->specialRequestRepo->flush();

            return $this->view(null, Response::HTTP_OK);
        } catch (\Exception $e) {
            return $this->view($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Post("/{id}/certificar", name="solicitud_especial_certificar")
     * @param SpecialRequest $specialRequest
     * @return \FOS\RestBundle\View\View
     */
    public function certifyAction(SpecialRequest $specialRequest)
    {
        try {
            $specialRequest->setClosedAt(new \DateTime);
            $specialRequest->setState(SpecialRequest::STATE_CLOSED);

            $this->specialRequestRepo->save($specialRequest);
            $this->dispatcher->dispatch('special.request.closed', new GenericEvent($specialRequest));

            return $this->view(null, Response::HTTP_OK);
        } catch (\Exception $e) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/{id}", name="solicitud_especial_detalle")
     * @View("special_request/show.twig")
     * @param Request $request
     * @param SpecialRequest $specialRequest
     * @return array
     */
    public function showAction(Request $request, SpecialRequest $specialRequest)
    {
        $form = $this->createForm(ProviderType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $supplier = $this->supplierRepo->findOneByRut($data['rut']);
            $costCenter = $data['cost_center'];
            $cost = $data['cost'];

            $specialRequest->setCost($cost);
            $specialRequest->setCostCenter($costCenter);
            $specialRequest->setSupplier($supplier);
            $specialRequest->setState(SpecialRequest::STATE_IN_PROGRESS);

            $this->specialRequestRepo->save($specialRequest);
            $this->addFlash('success', 'Proveedor asignado correctamente');

            return $this->routeRedirectView('dashboard');
        }

        return [
            'form' => $form->createView(),
            'special_request' => $specialRequest
        ];
    }

    /**
     * @Post("/{id}/rechazar", name="solicitud_especial_rechazar")
     * @param Request $request
     * @param SpecialRequest $specialRequest
     * @return \FOS\RestBundle\View\View
     */
    public function rejectAction(Request $request, SpecialRequest $specialRequest)
    {
        try {
            if ($rejection = $request->request->get('rejection')) {
                $specialRequest->setRejectionComment($rejection);
                $specialRequest->setState(SpecialRequest::STATE_REJECTED);

                $this->specialRequestRepo->save($specialRequest);
                $this->dispatcher->dispatch('special.request.rejected', new GenericEvent($specialRequest));
                $this->addFlash('success', 'Solicitud rechazada correctamente');
            }

            return $this->view(null, Response::HTTP_OK);
        } catch (\Exception $e) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Post("/{id}/apelar", name="solicitud_especial_apelar")
     * @param Request $request
     * @param SpecialRequest $specialRequest
     * @return \FOS\RestBundle\View\View
     */
    public function appealAction(Request $request, SpecialRequest $specialRequest)
    {
        try {
            if ($apeal = $request->request->get('apeal')) {
                $specialRequest->setRejectionApeal($apeal);
                $specialRequest->setState(SpecialRequest::STATE_APPEALED);

                $this->specialRequestRepo->save($specialRequest);
                $this->dispatcher->dispatch('special.request.appealed', new GenericEvent($specialRequest));
                $this->addFlash('success', 'Solicitud apelada correctamente');
            }

            return $this->view(null, Response::HTTP_OK);
        } catch (\Exception $e) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Post("/{id}/rechazar-final", name="solicitud_rechazar_final")
     * @param SpecialRequest $specialRequest
     * @return \FOS\RestBundle\View\View
     */
    public function rejectFinal(SpecialRequest $specialRequest)
    {
        try {
            $specialRequest->setState(SpecialRequest::STATE_REJECTED_FINAL);

            $this->specialRequestRepo->save($specialRequest);
            $this->addFlash('success', 'Apelación rechazada. Solicitud cerrada correctamente');
            $this->dispatcher->dispatch('special.request.rejected_final', new GenericEvent($specialRequest));
            return $this->view(null, Response::HTTP_OK);
        } catch (\Exception $e) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Post("/{id}/agregar-comentario", name="solicitud_agregar_comentario")
     * @param Request $request
     * @param SpecialRequest $specialRequest
     * @return \FOS\RestBundle\View\View
     */
    public function addCommentAction(Request $request, SpecialRequest $specialRequest)
    {
        try {
            if ($comment = $request->request->get('comment')) {
                $user = $this->getUser();
                $fullName = $user->getEmployee() ? $user->getEmployee()->getFullName() : '';

                $specialRequest->addComment($comment = [
                    'comment' => $comment,
                    'datetime' => date('d-m-Y H:i:s'),
                    'user' => $user->getUsername(),
                    'full_name' => $fullName,
                ]);

                $this->specialRequestRepo->save($specialRequest);
                $this->dispatcher->dispatch('special.request.comment', new GenericEvent($specialRequest));
            }

            return $this->view($comment, Response::HTTP_OK);
        } catch (\Exception $e) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function handleForm(Request $request, FormInterface $form, SpecialRequest $specialRequest)
    {
        $files = $specialRequest->getFiles();

        $form->setData($specialRequest);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if (!$this->uploadFiles($specialRequest, $files)) {
                return false;
            }

            $this->specialRequestRepo->save($specialRequest);

            return true;
        }

        return false;
    }

    private function uploadFiles(SpecialRequest $specialRequest, array $uploadedFiles = [])
    {
        $dest = $this->getParameter('uploads_dir');
        $files = [];

        try {
            foreach ($specialRequest->getFiles() as $file) {
                $new = UploadedFile::createFromFile($file, $dest);
                $newName = $this->generateNewFileName($new, $uploadedFiles);
                $files[$newName] = $new;
            }

            $specialRequest->setFiles(array_merge($uploadedFiles, $files));

            return true;
        } catch (\Exception $e) {
            $this->addFlash('danger', sprintf(
                'Hubo un error al guardar la solicitud (%s)', $e->getMessage()
            ));

            return false;
        }
    }

    private function generateNewFileName(UploadedFile $file, array $uploadedFiles)
    {
        return $file->getOriginalName();
    }

    /**
     * @param SpecialRequest $specialRequest
     * @return User
     */
    private function resolveAssignee(SpecialRequest $specialRequest)
    {
        $assignee = $this->userRepo->findByRole('ROLE_APPROVER_SPECIAL');

        if (SpecialRequest::STATE_CART === $specialRequest->getState()) {
            $assignee = $specialRequest->getRequester();
        }

        if (!$assignee) {
            throw new \RuntimeException('No se ha podido determinar el usuario a asignar');
        }

        $specialRequest->setAssignee($assignee);
    }
}