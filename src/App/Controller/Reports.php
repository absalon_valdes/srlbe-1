<?php

namespace App\Controller;

use App\Dashboard\Query\Requisition\UniformSupplierQuery;
use App\Dashboard\Query\Requisition\VCardApproverQuery;
use App\Dashboard\Query\Requisition\VCardSupplierQuery;
use App\Export\ApproverRequisitionExporter;
use App\Export\ApproverRequisitionExporterV2;
use App\Export\ApproverVCardRequisitionExporter;
use App\Export\GeneralVCardRequisitionExporter;
use App\Export\PepExporter;
use App\Export\RequirementExporter;
use App\Export\RequisitionExporter;
use App\Export\SingleRequisitionExporter;
use App\Export\SpecialRequestExporter;
use App\Export\SupplierRequisitionExporter;
use App\Export\SupplierUniformRequisitionExporter;
use App\Export\SupplierVCardRequisitionExporter;
use App\Export\UserExporter;
use App\Helper\ReportsRepository;
use App\Helper\UserHelper;
use JMS\DiExtraBundle\Annotation as Di;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Process\Process;

/**
 * Class Reports
 * @package App\Controller
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_SUPPLIER') or has_role('ROLE_APPROVER') or has_role('ROLE_APPROVER_VCARD')  or has_role('ROLE_BACKOFFICE')")
 * @Route("/reportes")
 */
class Reports extends FOSRestController
{
    /**
     * @var ReportsRepository
     * @Di\Inject("reports_repository")
     */
    public $reportsRepository;

    /**
     * @var RequisitionExporter $requisitionExporter ;
     * @Di\Inject("requisition.exporter")
     */
    public $requisitionExporter;

    /**
     * @var SpecialRequestExporter $specialRequestExporter ;
     * @Di\Inject("special.request.exporter")
     */
    public $specialRequestExporter;

    /**
     * @var SingleRequisitionExporter $singleRequisitionExporter;
     * @Di\Inject("single.requisition.exporter")
     */
    public $singleRequisitionExporter;

    /**
     * @var SupplierRequisitionExporter $supplierRequisitionExporter;
     * @Di\Inject("supplier.requisition.exporter.v2")
     */
    public $supplierRequisitionExporter;

    /**
     * @var SupplierVCardRequisitionExporter $supplierVCardRequisitionExporter;
     * @Di\Inject("supplier.vcard.requisition.exporter")
     */
    public $supplierVCardRequisitionExporter;

    /**
     * @var SupplierUniformRequisitionExporter $supplierUniformRequisitionExporter;
     * @Di\Inject("supplier.uniform.requisition.exporter")
     */
    public $supplierUniformRequisitionExporter;

    /**
     * @var ApproverRequisitionExporterV2 $approverRequisitionExporter;
     * @Di\Inject("approver.requisition.exporter.v2")
     */
    public $approverRequisitionExporter;

    /**
     * @var ApproverVCardRequisitionExporter $approverVCardRequisitionExporter;
     * @Di\Inject("approver.vcard.requisition.exporter")
     */
    public $approverVCardRequisitionExporter;

    /**
     * @var GeneralVCardRequisitionExporter $generalVCardRequisitionExporter;
     * @Di\Inject("general.vcard.requisition.exporter")
     */
    public $generalVCardRequisitionExporter;

    /**
     * @var PepExporter $pepExporter;
     * @Di\Inject("pep.exporter")
     */
    public $pepExporter;

    /**
     * @var UserExporter $userExporter;
     * @Di\Inject("user.exporter")
     */
    public $userExporter;

    /**
     * @var VCardSupplierQuery
     * @Di\Inject("app.dashboard.query.requisition.v_card_supplier_query")
     */
    public $queryVCardSupplier;

    /**
     * @var UniformSupplierQuery
     * @Di\Inject("app.dashboard.query.requisition.uniform_supplier_query")
     */
    public $queryUniformSupplier;

    /**
     * @var string
     * @Di\Inject("%kernel.root_dir%")
     */
    public $rootDir;

    /**
     * @var string
     * @Di\Inject("%kernel.environment%")
     */
    public $env;


    /**
     * @Method({"GET", "POST"})
     * @Route("/pedidos-general", name="report_requisition_bi_general")
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        return $this->requisitionExporter->export($request->query->get('type'))->getResponse();
    }

    /**
     * @Method({"GET"})
     * @Route("/reporte-general-command", name="report_general_comando")
     * @return Response
     */
    public function reporteGeneral()
    {
        $lockHandler = new LockHandler('lock.gen');
        if ($lockHandler->lock()) {
            $binPath = realpath(dirname(__DIR__, 3));
            $binPhpPath = PHP_BINDIR;
            $command = "$binPhpPath/php $binPath/bin/console re:gen --env=$this->env > /dev/null 2>&1 &";
            exec($command);
        }

        return new Response("Command succesfully executed from the controller");
    }

        /**
     * @Method({"GET", "POST"})
     * @Route("/reporte-proveedor", name="report_requisition_supplier")
     * @param Request $request
     * @return Response
     */
    public function supplierRequisitions(Request $request)
    {
        if($this->isVCardSupplier()){
            return $this->supplierVCardRequisitionExporter->export($request->query->get('type'))->getResponse();
        }

        if($this->isUniformSupplier()){
            return $this->supplierUniformRequisitionExporter->export()->getResponse();
        }

        return $this->supplierRequisitionExporter->export()->getResponse();
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/reporte-evaluador", name="report_requisition_approver")
     * @param Request $request
     * @return Response
     */
    public function approverRequisitions(Request $request)
    {
        return $this->approverRequisitionExporter->export($request->query->get('type'))->getResponse();
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/reporte-evaluador-vcard", name="report_requisition_approver_vcard")
     * @param Request $request
     * @return Response
     */
    public function approverVCardRequisitions(Request $request)
    {
        return $this->approverVCardRequisitionExporter->export($request->query->get('type'))->getResponse();
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/reporte-general-vcard", name="report_requisition_general_vcard")
     * @param Request $request
     * @return Response
     */
    public function generalVCardRequisitions(Request $request)
    {
        return $this->generalVCardRequisitionExporter->export()->getResponse();
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/pep-general", name="report_pep_bi_general")
     * @return array
     */
    public function pepReport()
    {
        return $this->pepExporter->export()->getResponse();
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/reporte-general-uniformes", name="report_uniformes_bi_general")
     * @return array|BinaryFileResponse
     */
    public function uniformBiGeneral()
    {
        return $this->supplierUniformRequisitionExporter->export()->getResponse();
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/usuarios", name="report_users_general")
     * @return array
     */
    public function reportUsersGeneral()
    {
        return $this->userExporter->export()->getResponse();
    }

    private function isVCardSupplier(){
        if($this->queryVCardSupplier->getResult()){
            return true;
        }
        return false;
    }

    private function isUniformSupplier(){
        if($this->queryUniformSupplier->getResult()){
            return true;
        }
        return false;
    }

    /**
     * @Rest\Get("/general-bi-diario/{filename}", name="report_bi_general_daily_download")
     * @param $filename
     * @return BinaryFileResponse
     */
    public function downloadGeneralDaily($filename)
    {
        if (!$filename = $this->reportsRepository->find($filename)) {
            throw $this->createNotFoundException();
        }

        BinaryFileResponse::trustXSendfileTypeHeader();

        $fileId = basename($filename);

        $response = new BinaryFileResponse($filename);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileId,
            iconv('UTF-8', 'ASCII//TRANSLIT', $fileId)
        );

        return $response;
    }

    /**
     * @Rest\Get("/por-tipo/{filename}", name="report_bi_by_type_download")
     * @param $filename
     * @return BinaryFileResponse
     */
    public function downloadByType($filename)
    {
        if (!$filename = $this->reportsRepository->find($filename, true)) {
            throw $this->createNotFoundException();
        }

        BinaryFileResponse::trustXSendfileTypeHeader();

        $fileId = basename($filename);

        $response = new BinaryFileResponse($filename);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileId,
            iconv('UTF-8', 'ASCII//TRANSLIT', $fileId)
        );

        return $response;
    }


    /**
     * @Method({"GET","POST"})
     * @Route("/exportar-solicitud", name="export_single_requisition")
     * @param Request $request
     * @return array
     */
    public function exportSingleRequisition(Request $request)
    {
        return $this->singleRequisitionExporter->export($request->query->get('uid'))->getResponse();
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/reporte-oficina-especial", name="report_requisition_special_office")
     * @param Request $request
     * @return Response
     */
    public function specialOfficesRequisitions(Request $request)
    {
        return $this->requisitionExporter->export(null, ['special_type' => $request->get('special_type')])->getResponse();
    }

    /**
     * @Method({"GET"})
     * @Route("/reporte-pedido-especial", name="report_request_special_request")
     * @param Request $request
     * @return Response
     */
    public function specialRequests(Request $request)
    {
        return $this->specialRequestExporter->export()->getResponse();
    }
}
