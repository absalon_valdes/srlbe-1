<?php

namespace App\Controller;

use App\Entity\Office;
use App\Form\ChangeOfficeType;
use App\Repository\OfficeRepository;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/Office")
 */
class Offices extends FOSRestController
{
    /**
     * @var OfficeRepository
     * @Inject("office_repo")
     */
    public $officeRepo;

    /**
     * @Get("/encontrar-oficina", name="office_index")
     * @param Request $request
     * @internal param $name
     * @return array
     */
    public function findOffice(Request $request)
    {
        $result = [];
        
        if (!empty($q = $request->get('q', null))) {
            $result = $this->officeRepo->createQueryBuilder('o')
                ->select('o.id, CONCAT(o.costCenter, \' - \', o.name) as text')
                ->where('o.name LIKE :name')
                ->orWhere('o.costCenter LIKE :costCenter')
                ->orWhere('o.code LIKE :code')
                ->orderBy('o.name', 'ASC')
                ->setParameter('name', '%'.$q.'%')
                ->setParameter('costCenter', '%'.$q.'%')
                ->setParameter('code', '%'.$q.'%')
                ->getQuery()
                ->useQueryCache(true)
                ->useResultCache(true)
                ->getArrayResult()
            ;
        }
        
        return ['results' => $result];
    }

    /**
     * @param Office $office
     * @Get("/obtener-oficina/{id}", name="get_office")
     * @return Office
     */
    public function getOffice(Office $office)
    {
        return [
            'id' => $office->getId(),
            'address' => $office->getAddress(),
            'city' => $office->getCity()->getName(),
            'region' => $office->getCity()->getParent()->getName(),
            'cost_center' => $office->getCostCenter(),
        ];
    }
    
    /**
     * @Get("/officeList", name="office_data_view")
     * @View("_box/change_office.twig")
     * @return array
     */
    public function officeListAction()
    {
        /** @var FormInterface $form */
        $form = $this->createForm(
            ChangeOfficeType::class
        );

        return [
            'change_office' => $form->createView(),
        ];
    }
}