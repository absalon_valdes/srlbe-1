<?php

namespace App\Helper;

use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("rating_calculator")
 */
class RatingCalculator
{
    /**
     * @param array $ratings
     * @return array
     */
    public static function getAverage($ratings)
    {
        if (null === $ratings || !is_array($ratings)) {
            return [
                'punctuality' => 0,
                'quality' => 0,
                'service_level' => 0,
                'total' => 0,
            ];
        }
        
        // BC fix
        unset(
            $ratings['punctuality'], 
            $ratings['quality'], 
            $ratings['service_level']
        );

        if (!$total = count($ratings)) {
            return [
                'punctuality' => 0,
                'quality' => 0,
                'service_level' => 0,
                'total' => 0,
            ];
        }

        $average = function ($column) use ($ratings, $total) {
            return (int) (array_sum(array_column($ratings, $column)) / $total);
        };
        
        return [
            'punctuality'   => $punctuality = $average('punctuality'),
            'quality'       => $quality = $average('quality'),
            'service_level' => $serviceLevel = $average('service_level'),
            'total'         => ($punctuality + $quality + $serviceLevel) / 3,
        ];
    }

    /**
     * @param array $ratings
     * @param $key
     * @return int
     */
    public static function getKeyAverage(array $ratings, $key)
    {
        if (0 === count($ratings)) {
            return 0;
        }
        
        return (int) (array_sum(array_column($ratings, $key)) / count($ratings));
    }
}