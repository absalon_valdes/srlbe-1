<?php
namespace App\Helper;

use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("redis_handler")
 */
class RedisHandler
{
    const STACK_DEFAULT_LIMIT = 5;

    private $limit;

    /**
     * @var \Redis
     * @Di\Inject("snc_redis.default_client")
     */
    public $redis;

    public function __construct($limit = self::STACK_DEFAULT_LIMIT)
    {
        $this->limit = $limit;
    }

    public function push($key, $msg)
    {
        $this->redis->lPush($key, serialize($msg));
        $this->redis->lTrim($key, 0, $this->limit);
    }

    public function read($key, $start = 0, $limit = self::STACK_DEFAULT_LIMIT)
    {
        if (!$results = $this->redis->lRange($key, $start, $limit)) {
            return [];
        }

        return array_map('unserialize', $results);
    }
}
