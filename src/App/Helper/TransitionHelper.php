<?php

namespace App\Helper;

use App\Entity\Requisition;
use App\Model\Transition;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class TransitionHelper
 * @package App\Helper
 * 
 * @DI\Service("transition_helper")
 */
class TransitionHelper
{
    /**
     * @var UserHelper
     * @DI\Inject("user_helper")
     */
    public $userHelper;
    
    /**
     * @param Requisition $requisition
     */
    public function pushTransition(Requisition $requisition)
    {
        $user = $this->userHelper->getCurrentUser();
        
        $requisition->addTransition(Transition::create(
            $user->getUsername(),
            $requisition->getStatus()
        ));
    }
}