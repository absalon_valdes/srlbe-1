<?php

namespace App\Helper;

use App\Entity\Position;
use App\Entity\Office;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("vcard_query_helper")
 */
class VcardQuery
{
    /**
     * @var array
     */
    private $vcards;

    /**
     * @Di\Inject("%kernel.root_dir%")
     */
    public $rootDir;

    public function getVcardCodes(Position $position, Office $office)
    {
        $this->loadVcardDatabase();
        $title = strtoupper(str_replace([' ', '-', '_'], '', $position->getTitle()));
        
        return $this->vcards[$title] ?? ['TBL1', 'TBL2'];
    }

    private function loadVcardDatabase()
    {
        if (!$this->vcards) {
            $this->vcards = require_once sprintf('%s/../var/data/vcards.php.cache', $this->rootDir);
        }

        return $this->vcards;
    }
}