<?php

namespace App\Helper;

use App\Entity\Contract;
use App\Entity\Requisition;
use App\Entity\Supplier;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class RequisitionSupplierChanger
 * 
 * @DI\Service("requisition_supplier_changer")
 */
class RequisitionSupplierChanger
{
    /**
     * @var EntityManager
     */
    protected $em;
    
    /**
     * RequisitionSupplierChanger constructor.
     *
     * @param EntityManager $em
     *
     * @DI\InjectParams({
     *     "em"=@DI\Inject("doctrine.orm.entity_manager")
     * })
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * @param array $requisitions
     * @param Supplier $newSupplier
     */
    public function changeSupplier(array $requisitions, Supplier $newSupplier = null)
    {
        /** @var Requisition $requisition */
        foreach ($requisitions as $requisition) {
            /** @var Contract $newContract */
            if (!$newContract = $this->findMatchingContract($requisition->getContract(), $newSupplier)) {
                continue;
            }
            
            $requisition->setContract($newContract);

            if (in_array('ROLE_SUPPLIER', $requisition->getAssignee()->getRoles(), true)) {
                $requisition->setAssignee($newSupplier->getRepresentatives()->first());
            }
        }
        
       	$this->em->flush();
    }
    
    /**
     * @param Contract $contract
     * @param Supplier $supplier
     * @return mixed
     */
    private function findMatchingContract(Contract $contract, Supplier $supplier)
    {
        return $this->em->createQueryBuilder()
            ->select('c')
            ->from(Contract::class, 'c')
            ->where('c.office = :office')
            ->andWhere('c.product = :product')
            ->andWhere('c.supplier = :supplier')
            ->setParameter('office', $contract->getOffice())
            ->setParameter('product', $contract->getProduct())
            ->setParameter('supplier', $supplier)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
