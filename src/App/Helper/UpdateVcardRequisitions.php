<?php

namespace App\Helper;

use App\Entity\Requisition;
use App\Repository\RequisitionRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Psr\Log\LoggerInterface;
use Symfony\Component\Workflow\Workflow;

/**
 * Class UpdateVcardRequisitions
 * @DI\Service("update_vcard_requisitions")
 */
class UpdateVcardRequisitions
{
    /**
     * @var LoggerInterface
     * @DI\Inject("logger")
     */
    public $logger;
    
    /**
     * @var RequisitionRepository
     * @DI\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var Workflow
     * @DI\Inject("workflow.vcards")
     */
    public $workflow;

    public function update()
    {
        $requisitions = $this->requisitionRepo->createQueryBuilder('r')
            ->where('r.type = :type')
            ->andWhere('r.status = :status')
            ->setParameter('type', Requisition::TYPE_VCARD)
            ->setParameter('status', 'en_despacho')
            ->getQuery()
            ->getResult()
        ;

        foreach ($requisitions as $requisition) {
            $this->updateState($requisition);
        }
    }

    /**
     * @param Requisition $requisition
     */
    private function updateState(Requisition $requisition)
    {
        $loggerCtx = [
            'number' => $requisition->getNumber(),
            'id' => $requisition->getId(),
        ];
        
        $this->logger->info('Checking requisition {number} ({id})...', $loggerCtx);
        
        /** @var \DateTime[] $timestamps */
        $timestamps = $requisition->getMetadata('timestamps');
        $timeout = array_values($timestamps)[0]['completion'];
        
        if ((new \DateTime) < $timeout) {
            return;
        }
        
        if ($this->workflow->can($requisition, 'recibir_despacho')) {
            $this->workflow->apply($requisition, 'recibir_despacho');
            $this->requisitionRepo->save($requisition);

            $this->logger->info('Updating requisition {number} ({id})...', $loggerCtx);
        }
    }
}