<?php

namespace App\Helper;

use App\Entity\User;
use App\Repository\UserRepository;
use FOS\UserBundle\Model\UserManager;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Di\Service("user_helper")
 */
class UserHelper
{
    /**
     * @var TokenStorage
     * @Di\Inject("security.token_storage")
     */
    public $tokenStorage;

    /**
     * @Di\Inject("security.authorization_checker")
     */
    public $authorizationChecker;

    /**
     * @var UserManager
     * @Di\Inject("fos_user.user_manager")
     */
    public $userManager;

    /**
     * @var UserRepository
     * @Di\Inject("user_repo")
     */
    public $userRepo;

    /**
     * @param $role
     * @return boolean
     */
    public function isGranted($role)
    {
        return $this->authorizationChecker->isGranted($role);
    }

    /**
     * @return null|User
     */
    public function getCurrentUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }

    /**
     * @param $newPassword
     */
    public function changePassword($newPassword)
    {
        $user = $this->getCurrentUser();
        $user->setPlainPassword($newPassword);
        $this->userManager->updateUser($user);
    }

    /**
     * @param $username
     */
    public function impersonateUser($username)
    {
        /** @var UserInterface $user */
        if (!$user = $this->userRepo->findOneByUsername($username)) {
            throw new \RuntimeException('Cannot initialize system security token');
        }
        
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles()); 
        $this->tokenStorage->setToken($token);
    }
}
