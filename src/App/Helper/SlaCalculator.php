<?php

namespace App\Helper;

use App\Entity\SLA;
use Business\Business;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class SlaCalculator
 * @package App\Helper
 *
 * @Di\Service("sla_calculator")
 */
class SlaCalculator
{    
    /**
     * @var Business
     */
    public $business;

    /**
     * @var array
     * @Di\Inject("%app.holidays%")
     */
    public $holidays;

    /**
     * SlaCalculator constructor.
     * @param BusinessFactory $businessFactory
     * @Di\InjectParams({
     *     "businessFactory"=@Di\Inject("business_factory")
     * })
     */
    public function __construct(BusinessFactory $businessFactory)
    {
        $this->business = $businessFactory->getBusiness();
    }

    /**
     * @param SLA $sla
     * @param \DateTime $start
     * @return \DateTime
     */
    public function getSla(SLA $sla, \DateTime $start = null)
    {
        if ($sla->inDays()) {
            return $this->getDaysSla($sla, $start);
        }
        
        if ($sla->inHours()) {
            return $this->getHoursSla($sla, $start);
        }
        
        throw new \InvalidArgumentException('Invalid SLA');
    }

    /**
     * @param SLA $sla
     * @param \DateTime|null $start
     * @return \DateTime
     */
    private function getDaysSla(SLA $sla, \DateTime $start = null)
    {
        $days = $sla->getValue();
        $calculated = clone ($start ?? new \DateTime);
        
        while ($days) {
            $calculated->modify('+1 day');

            if ($calculated->format('w') % 6 === 0 && $sla->isBusiness()) {
                continue;
            }

            if(in_array($calculated->format('d/m'),$this->holidays)){
                continue;
            }

            $days--;
        }

        return $calculated;       
    }

    /**
     * @param SLA $sla
     * @param \DateTime|null $start
     * @return \DateTime
     */
    private function getHoursSla(SLA $sla, \DateTime $start = null)
    {
        $hours = $sla->getValue();
        $calculated = clone ($start ?? new \DateTime);

        if ($sla->isCalendar()) {
            return $calculated->add(new \DateInterval('PT'.$hours.'H'));
        }

        while ($hours) {
            $calculated->modify('+1 hour');
            
            if (!$this->business->within($calculated)) {
                continue;
            }
            
            $hours--;
        }
        
        if (!$this->business->within($start ?? new \DateTime)) {
            $calculated->modify('+1 hour');
            $calculated = date('Y-m-d H:00:00', $calculated->getTimestamp());
            $calculated = \DateTime::createFromFormat('Y-m-d H:i:s', $calculated);
        }
        
        
        return $calculated;
    }

    /**
     * @param SLA $sla
     * @param \DateTime $ref
     * @return \DateInterval
     */
    public function slaToDateInterval(SLA $sla, \DateTime $ref = null)
    {
        $ref = clone ($ref ?? new \DateTime);
        
        return $ref->diff($this->getSla($sla, $ref));
    }

    /**
     * @param \DateInterval $interval
     * @param string $type
     * @return SLA
     */
    public function dateIntervalToSla(\DateInterval $interval, $type = SLA::BUSINESS_DAY)
    {
        $amount = $interval->y * 365 + $interval->m * 30 + $interval->d;
        $amount += $interval->h > 4 ? 1 : 0; // redondear un día si hay más de 4 horas
        
        if (SLA::BUSINESS_HOUR === $type) {
            $amount *= 24;
        }

        return SLA::create($amount, $type);
    }

    /**
     * @param \DateTime $start
     * @param SLA[] $slas
     * @return \DateTime
     */
    public function getEstimate(\DateTime $start = null, ...$slas)
    {
        $start = clone ($start ?? new \DateTime);
        
        foreach (array_filter($slas) as $sla) {
            if (!$sla instanceof SLA) {
                continue;
            }
            $interval = $this->slaToDateInterval($sla, $start);
            
            $start->add($interval);
        }

        return $start;
    }
}
