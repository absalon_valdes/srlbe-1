<?php

namespace App\Util;


class PhpExcelUtils
{
    const FIRST_LETTER  = 65;
    const LAST_LETTER   = 26;

    public static function getNextLetterByNumber(int $num) {
        $numeric = $num % self::LAST_LETTER;
        $letter = chr(self::FIRST_LETTER + $numeric);
        $isAfterZ = (int)($num / self::LAST_LETTER);
        if ($isAfterZ > 0) {
            return self::getNextLetterByNumber  ($isAfterZ - 1) . $letter;
        }
        
        return $letter;
    }
}
