<?php

namespace App\Util;

/**
 * Class FormUtils
 * @package App\Util
 */
class FormUtils
{
    /**
     * It returns the FQCN of the given short type name.
     * Example: 'text' -> 'Symfony\Component\Form\Extension\Core\Type\TextType'
     *
     * @param string $shortType
     *
     * @return string
     */
    public static function getFormTypeFqcn($shortType)
    {
        $builtinTypes = array(
            'birthday', 'button', 'checkbox', 'choice', 'collection', 'country',
            'currency', 'datetime', 'date', 'email', 'entity', 'file', 'form',
            'hidden', 'integer', 'language', 'locale', 'money', 'number',
            'password', 'percent', 'radio', 'range', 'repeated', 'reset',
            'search', 'submit', 'textarea', 'text', 'time', 'timezone', 'url',
        );

        if (!in_array($shortType, $builtinTypes, true)) {
            return $shortType;
        }

        $irregularTypeFqcn = array(
            'entity' => 'Symfony\\Bridge\\Doctrine\\Form\\Type\\EntityType',
            'datetime' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\DateTimeType',
        );

        if (array_key_exists($shortType, $irregularTypeFqcn)) {
            return $irregularTypeFqcn[$shortType];
        }

        return sprintf('Symfony\\Component\\Form\\Extension\\Core\\Type\\%sType', ucfirst($shortType));
    }
}