<?php

namespace App\Util;

class FunctionUtils
{
    public static function memoize($function)
    {
        return function() use ($function)
        {
            static $cache = [];

            $key = md5(serialize($args = func_get_args()));

            if (!isset($cache[$key])) {
                $cache[$key] = call_user_func_array($function, $args);
            }

            return $cache[$key];
        };
    }
}