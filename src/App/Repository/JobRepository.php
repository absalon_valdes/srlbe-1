<?php

namespace App\Repository;

use App\Entity\Job;

/**
 * Class JobRepository
 * @package App\Repository
 */
class JobRepository extends Repository
{
    /**
     * @return mixed
     */
    public function findNext()
    {
        return $this->findOneByState(Job::QUEUED, ['createdAt' => 'ASC']);
    }
    
    /**
     * @param $states
     * @return array
     * @internal param $state
     */
    public function findByState($states)
    {
        return $this->createQueryBuilder('o')
            ->where('o.state in (:states)')
            ->orderBy('o.createdAt', 'asc')
            ->setParameter('states', $states)
            ->getQuery()
            ->useResultCache(false)
            ->getArrayResult()
        ;
    }
    
    /**
     * @return array
     */
    public function findActive()
    {
        return $this->findByState([
            Job::QUEUED,
            Job::LOADING,
            Job::VALIDATING,
            Job::PREPARING,
            Job::RUNNING,
            Job::STOPPED,
            Job::FAILED,
            Job::RETRY,
        ]);
    }
    
    /**
     * @return array
     */
    public function findHistory()
    {
        return $this->findByState([
            Job::CANCELLED,
            Job::COMPLETED,
        ]);
    }
}