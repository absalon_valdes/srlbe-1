<?php

namespace App\Repository;

use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Position;
use Doctrine\ORM\Query\Expr\Join;

class UserRepository extends Repository
{
    public function findAssigneeQuery($role, $office, $positionNeedle = null)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->leftJoin(Employee::class, 'e', Join::WITH, $qb->expr()->eq('u', 'e.user'))
            ->leftJoin(Office::class, 'o', Join::WITH, $qb->expr()->eq('e.office', 'o'))
            ->where($qb->expr()->eq('o', ':office'))
            ->andWhere($qb->expr()->like('u.roles', ':role'))
            ->setParameter('role', '%' . $role . '%')
            ->setParameter('office', $office);
        if ($positionNeedle) {
            $qb->leftJoin(Position::class, 'p', Join::WITH, $qb->expr()->eq('e.position', 'p'))
                ->andWhere($qb->expr()->like('p.title', ':position'))
                ->setParameter('position', '%' . $positionNeedle . '%');
        }

        return $qb->getQuery();
    }


    public function findByRole($role)
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE :role')
            ->setParameter('role', '%'.$role.'%')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()
        ;
    }
}
