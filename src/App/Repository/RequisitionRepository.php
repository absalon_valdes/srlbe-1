<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\Contract;
use App\Entity\SpecialRequest;
use App\Entity\Supplier;
use App\Entity\User;
use App\Entity\Requisition;
use App\Model\VcardRequisition;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\DBAL\Types\Type;

class RequisitionRepository extends Repository
{
    public function findByOrderNumber($number)
    {
        return $this->findBy(['orderNumber' => $number]);
    }

    public function findLastMonthRequisitions(User $user)
    {
        return (int) $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->where('r.requester = :user')
            ->andWhere('r.createdAt between :from and :to')
            ->orderBy('r.createdAt', 'desc')
            ->setParameter('user', $user)
            ->setParameter('from', new \DateTime('first day of last month'), Type::DATETIME)
            ->setParameter('to', new \DateTime('last day of last month'), Type::DATETIME)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function findCountPendingEval()
    {
        $qb = $this->createQueryBuilder('r');
        $qb->select($qb->expr()->count('r.id'));
        $qb->where($qb->expr()->andX(
            $qb->expr()->in('r.status', [
                ProductStatus::REVIEWED,
                ProductStatus::EVALUATED,
            ]),
            $qb->expr()->eq('r.requester', ':user')
        ));
        $qb->orderBy($qb->expr()->desc('r.updatedAt'));
        $qb->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findCountActiveVcards(User $user)
    {
        if (!$employee = $user->getEmployee()) {
            return null;
        }

        $active = $this->createQueryBuilder('r')
            ->select('r')
            ->where('r.type = :type')
            ->andWhere('r.status not in (:statuses)')
            ->setParameter('type', Requisition::TYPE_VCARD)
            ->setParameter('statuses', [
                'solicitud_completa',
                'cerrado',
                'cerrada_sistema'
            ])
            ->getQuery()
            ->getResult()
        ;

        $forMe = [];
        $serializer = $this->get('serializer');

        /** @var Requisition $act */
        foreach ($active as $act) {
            $meta = $act->getMetadata('vcards');
            /** @var VcardRequisition $vcardData */
            $vcardData = $serializer->deserialize($meta, VcardRequisition::class, 'json');
            if ($vcardData->getEmployee()->getId() === $employee->getId()) {
                $forMe[] = $act;
            }
        }

        return $forMe;
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountActiveUniform()
    {
        $qb = $this->createQueryBuilder('r');
        $qb->select($qb->expr()->count('r.id'));
        $qb->where($qb->expr()->andX(
            $qb->expr()->notIn('r.status', Requisition::CLOSED_STATUSES),
            $qb->expr()->eq('r.requester', ':user'),
            $qb->expr()->eq('r.type', ':type')
        ));
        $qb->orderBy($qb->expr()->desc('r.updatedAt'));
        $qb->setParameter('user', $this->get('user_helper')->getCurrentUser());
        $qb->setParameter('type',Requisition::TYPE_UNIFORM);
        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findByParticipant(User $user)
    {
        $qb = $this->createQueryBuilder('r');

        if ($user->hasRole('ROLE_REQUESTER')) {
            $qb->where($qb->expr()->eq('r.requester', ':user'));
            $qb->setParameter('user', $user);
        } elseif ($user->hasRole('ROLE_APPROVER')) {
            $qb->join(Product::class, 'p', Join::WITH, $qb->expr()->eq('r.product', 'p'));
            $qb->where($qb->expr()->eq(':user', 'p.approver'));
            $qb->setParameter('user', $user);
        } elseif ($user->hasRole('ROLE_SUPPLIER')) {
            $qb->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'));
            $qb->join(Supplier::class, 's', Join::WITH, $qb->expr()->eq('c.supplier', 's'));
            $qb->where($qb->expr()->isMemberOf(':user', 's.representatives'));
            $qb->andWhere($qb->expr()->in('r.status', [
                ProductStatus::APPROVED,
                ProductStatus::ACCEPTED,
                ProductStatus::DELIVERED,
            ]));
            $qb->setParameter('user', $user);
        } else {
            return null;
        }

        $qb->orderBy($qb->expr()->desc('r.updatedAt'));

        return $this->paginate($qb->getQuery());
    }

    public function findUserHistory(User $user = null)
    {
        $user = $user ?? $this->get('user_helper')->getCurrentUser();

        $qb = $this->createQueryBuilder('r');

        if ($user->hasRole('ROLE_REQUESTER')) {
            $this->requesterHistory($qb);
            $qb->setParameter('user', $user);
        } elseif ($user->hasRole('ROLE_APPROVER')) {
            $this->approverHistory($qb);
            $qb->setParameter('user', $user);
        } elseif ($user->hasRole('ROLE_SUPPLIER')) {
            $this->supplierHistory($qb);
            $qb->setParameter('user', $user);
        } else {
            return null;
        }

        $qb->orderBy($qb->expr()->desc('r.updatedAt'));

        return $this->paginate($qb->getQuery());
    }

    private function requesterHistory(QueryBuilder &$qb)
    {
        $qb->where($qb->expr()->andX(
            $qb->expr()->in('r.status', [
                ProductStatus::COMPLETED,
                ProductStatus::CLOSED,
            ]),
            $qb->expr()->eq('r.requester', ':user')
        ));
    }

    private function approverHistory(QueryBuilder &$qb)
    {
        $qb->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'));
        $qb->join(Product::class, 'p', Join::WITH, $qb->expr()->eq('r.product', 'p'));
        $qb->where($qb->expr()->eq(':user', 'p.approver'));
        $qb->andWhere($qb->expr()->in('r.status', [
            ProductStatus::APPROVED,
            ProductStatus::REJECTED,
            ProductStatus::CLOSED,
            ProductStatus::COMPLETED,
            ProductStatus::CONFIRMED,
            ProductStatus::DELIVERED,
            ProductStatus::EVALUATED,
            ProductStatus::REVIEWED,
        ]));
        $qb->andWhere($qb->expr()->eq('c.centralized', true));
    }

    private function supplierHistory(QueryBuilder &$qb)
    {
        $qb->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'));
        $qb->join(Supplier::class, 's', Join::WITH, $qb->expr()->eq('c.supplier', 's'));
        $qb->where($qb->expr()->isMemberOf(':user', 's.representatives'));
        $qb->andWhere($qb->expr()->in('r.status', [
            //ProductStatus::REVIEWED,
            ProductStatus::COMPLETED,
            ProductStatus::EVALUATED,
            ProductStatus::CLOSED,
            'cerrado',
            'evaluacion'
        ]));
    }

    public function findOpen()
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->where($qb->expr()->andX(
                $qb->expr()->in('r.status', [
                    ProductStatus::CREATED,
                    ProductStatus::APPROVED,
                    ProductStatus::ACCEPTED,
                    ProductStatus::DELIVERED,
                    ProductStatus::REJECTED,
                    ProductStatus::DECLINED,
                    ProductStatus::DENIED,
                    ProductStatus::APPEAL,
                ]),
                $qb->expr()->eq('r.requester', ':user')
            ))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getResult();
    }

    public function findRequesterPendingEvaluation()
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->where($qb->expr()->andX(
                $qb->expr()->in('r.status', [
                    ProductStatus::REVIEWED,
                    'evaluacion'
                ]),
                $qb->expr()->eq('r.requester', ':user')
            ))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getResult();
    }

    public function findHistory()
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->where($qb->expr()->andX(
                $qb->expr()->in('r.status', [
                    ProductStatus::COMPLETED,
                    ProductStatus::CLOSED,
                ]),
                $qb->expr()->eq('r.requester', ':user')
            ))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getResult();
    }

    public function getCartItemsCount()
    {
        $qb = $this->createQueryBuilder('o');
        $qb
            ->select($qb->expr()->count('o.id'))
            ->where($qb->expr()->andX(
                $qb->expr()->eq('o.requester', ':user'),
                $qb->expr()->orX(
                    $qb->expr()->eq('o.status', $qb->expr()->literal(ProductStatus::DRAFT)),
                    $qb->expr()->eq('o.status', $qb->expr()->literal('borrador'))
                )
            ))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findInCart()
    {
        $qb = $this->createQueryBuilder('o');
        $qb
            ->where($qb->expr()->andX(
                $qb->expr()->eq('o.requester', ':user'),
                $qb->expr()->orX(
                    $qb->expr()->eq('o.status', $qb->expr()->literal(ProductStatus::DRAFT)),
                    $qb->expr()->eq('o.status', $qb->expr()->literal('borrador'))
                )
            ))
            ->andWhere('o.type != :virtual_type')
            ->setParameter('virtual_type', 'requisition.virtual')
            ->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getResult();
    }

    public function findApproverPendingEvaluation()
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->join(Product::class, 'p', Join::WITH, $qb->expr()->eq('r.product', 'p'))
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->where($qb->expr()->andX(
                $qb->expr()->in('r.status', [
                    ProductStatus::CREATED,
                    ProductStatus::APPEAL,
                    ProductStatus::DENIED,
                    ProductStatus::HOLD,
                ]),
                $qb->expr()->eq(':user', 'p.approver'),
                $qb->expr()->eq('c.centralized', true)
            ))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getResult();
    }

    public function findApproverPendingCertification()
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->join(Product::class, 'p', Join::WITH, $qb->expr()->eq('r.product', 'p'))
            ->where($qb->expr()->andX(
                $qb->expr()->eq('c.centralized', true),
                $qb->expr()->eq(':user', 'p.approver'),
                $qb->expr()->like('r.status', $qb->expr()->literal(ProductStatus::CONFIRMED))
            ))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getResult();
    }

    public function findRequesterPendingCertification()
    {
        return true;
    }

    public function findSupplierPendingEvaluation()
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->join(Supplier::class, 's', Join::WITH, $qb->expr()->eq('c.supplier', 's'))
            ->where($qb->expr()->andX(
                $qb->expr()->in('r.status', [
                    ProductStatus::APPROVED,
                    ProductStatus::ACCEPTED,
                    'ejecucion'
                    //ProductStatus::CONFIRMED,
                ]),
                $qb->expr()->isMemberOf(':user', 's.representatives')
            ))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser())
            ->orderBy($qb->expr()->desc('r.createdAt'));

        return $qb->getQuery()->getResult();
    }

    public function findSupplierPendingCertification()
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->join(Supplier::class, 's', Join::WITH, $qb->expr()->eq('c.supplier', 's'))
            ->where($qb->expr()->andX(
                $qb->expr()->isMemberOf(':user', 's.representatives'),
                $qb->expr()->in('r.status', [
                    ProductStatus::RETURNED,
                    ProductStatus::DELIVERED
                ])
            ))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser())
            ->orderBy($qb->expr()->desc('r.updatedAt'));

        return $qb->getQuery()->getResult();
    }

    public function searchRequisitions($filter, $type)
    {
        if (count(array_filter($filter)) <= 3 &&
            $filter['region'] === 'region.all' &&
            $filter['city'] === 'cities.all' &&
            !$filter['category'] &&
            $filter['status'] === 'requisition.all'
        ) {
            $requisition['error'] = 'Debe llenar al menos un filtro, o seleccionar un estado diferente a todos';
            return $requisition;
        }

        $qb = $this->createQueryBuilder('r');
        $qb->leftJoin(User::class, 'u', Join::WITH, $qb->expr()->eq('r.requester', 'u'))
            ->leftJoin(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->leftJoin(Product::class, 'p', Join::WITH, $qb->expr()->eq('c.product', 'p'))
            ->leftJoin(Employee::class, 'e', Join::WITH, $qb->expr()->eq('u.employee', 'e'))
            ->leftJoin(Office::class, 'o', Join::WITH, $qb->expr()->eq('e.office', 'o'))
            ->leftJoin(Category::class, 'city', Join::WITH, $qb->expr()->eq('o.city', 'city'))
            ->leftJoin(Category::class, 'region', Join::WITH, $qb->expr()->eq('region', 'city.parent'))
            ->leftJoin(Category::class, 'prodCat', Join::WITH, $qb->expr()->eq('prodCat', 'p.category'))
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->notLike('r.status', ':statusDraft'),
                    $qb->expr()->like('r.status', ':status'),
                    $qb->expr()->isNull('o.specialType')
//                    $qb->expr()->notlike('r.type', ':type')
                )
            )
            ->setParameter('statusDraft', '%'.ProductStatus::DRAFT.'%')
            ->setParameter('status', $filter['status'] === 'requisition.all' ? '%%' : '%'.$filter['status'].'%')
//            ->setParameter('type', '%requisition.product.vcard%')
        ;

        //Review the filters
        if ($filter['orderNumber']) {
            $qb->andWhere($qb->expr()->like('r.number', ':number'))
                ->setParameter('number', '%'.$filter['orderNumber'].'%');
        }
        //Review the filters
        if ($filter['type']) {
            $qb->andWhere($qb->expr()->like('r.type', ':type'))
                ->setParameter('type', '%'.$filter['type'].'%');
        }

        if ($filter['office']) {
            $qb->andWhere($qb->expr()->eq('o', ':requesterOffice'))
                ->setParameter('requesterOffice', $filter['office']);
        }
        if ($filter['approver']) {
            $qb->andWhere($qb->expr()->eq('p.approver', ':approver'))
                ->setParameter('approver', $filter['approver']);
        }

        if ($filter['supplier']) {
            $qb->andWhere($qb->expr()->eq('c.supplier', ':supplier'))
                ->setParameter('supplier', $filter['supplier']);
        }

        if ($filter['region'] !== 'region.all') {
            $qb->andWhere($qb->expr()->eq('region', ':selectedRegion'))
                ->setParameter('selectedRegion', $filter['region']);
        }

        if ($filter['city'] !== 'cities.all') {
            $qb->andWhere($qb->expr()->eq('city', ':selectedCity'))
                ->setParameter('selectedCity', $filter['city']);
        }

        if ($filter['createdAt']) {
            $qb->andWhere($qb->expr()->eq('DATE(r.createdAt)', ':createdAt'))
                ->setParameter('createdAt', $filter['createdAt']->format('Y/m/d'));
        }

        if ($filter['requesterName']) {
            $qb->andWhere($qb->expr()->eq('u', ':user'))
                ->setParameter('user', $filter['requesterName']);
        }

        if ($filter['requesterUserName']) {
            $qb->andWhere($qb->expr()->eq('u', ':user'))
                ->setParameter('user', $filter['requesterUserName']);
        }

        if( $filter['sub_category_3'] ){
            $qb->andWhere($qb->expr()->eq('p.category', ':category'))
                ->orWhere($qb->expr()->eq('prodCat.parent', ':category'))
                ->setParameter('category', $filter['sub_category_3']);
        }

        if($filter['sub_category_2'] && !$filter['sub_category_3']){
            $qb->andWhere($qb->expr()->eq('p.category', ':category'))
                ->orWhere($qb->expr()->eq('prodCat.parent', ':category'))
                ->setParameter('category', $filter['sub_category_2']);
        }

        if($filter['sub_category_1'] && !$filter['sub_category_3'] && !$filter['sub_category_2'] ){
            $qb->andWhere($qb->expr()->eq('p.category', ':category'))
                ->orWhere($qb->expr()->eq('prodCat.parent', ':category'))
                ->setParameter('category', $filter['sub_category_1']);
        }

        if($filter['category']
            && !$filter['sub_category_3']
            && !$filter['sub_category_2']
            && !$filter['sub_category_1']){
            $qb->andWhere($qb->expr()->eq('p.category', ':category'))
                ->orWhere($qb->expr()->eq('prodCat.parent', ':category'))
                ->setParameter('category', $filter['category']);
        }

        return $qb->getQuery()->getResult();
    }

    public function findProductTypeDeliveredStatus($number) {
        return $this->findOneBy(
            ['number'=>$number,'status'=>ProductStatus::DELIVERED,'type'=>Requisition::TYPE_PRODUCT]
        );
    }

    public function setNextNumber(Requisition $requisition) {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $requisition->setNumber($this->getNextNumber());
            $em->flush();
            $em->getConnection()->commit();
        }
        catch(\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
    }

    public function getNextNumber()
    {
        $allowedClasses = [
            Requisition::class,
            SpecialRequest::class
        ];
        $numbers = [];

        foreach ($allowedClasses as $cls) {
            $qb = $this->getEntityManager()->getRepository($cls)
                ->createQueryBuilder('o');

            $qb->select('o.number');
            if (Requisition::class === $cls) {
                $qb->where('o.deletedAt is null');
            }
            $qb->orderBy('o.number', 'DESC');

            $result = $qb->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_READ)
                ->setMaxResults(1)
                ->getOneOrNullResult();

            $number = $result['number'];
            $numbers[] = is_numeric($number) ? $number : 10000;
        }

        return max($numbers) +1;
    }
}
