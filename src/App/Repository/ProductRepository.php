<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Contract;
use App\Entity\Office;
use App\Entity\Employee;
use App\Entity\User;
use BE\UniformBundle\Util\UniformUserConstraint;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ProductRepository
 * @package App\Repository
 */
class ProductRepository extends Repository
{
    /**
     * @var int Category tree temp unique index
     */
    private $counter = 0;

    /**
     * @param Employee $employee
     * @return array
     */
    public function findVcardsForEmployee(Employee $employee)
    {
        $codes = $this->get('vcard_query_helper')
            ->getVcardCodes($employee->getPosition(), $employee->getOffice());

        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('p');
        $qb->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('c.product', 'p'));
        $qb->join(Office::class, 'o', Join::WITH, $qb->expr()->eq('o', 'c.office'));
        $qb->join(Employee::class, 'e', Join::WITH, $qb->expr()->eq('e.office', 'o'));
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq('e.user', ':user'),
            $qb->expr()->in('p.code', $codes)
        ));
        $qb->setParameter('user', $employee->getUser());

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function findHavingProducts(Category $category)
    {
        $qb = $this->buildCategoryQuery($category);
        $qb->select($qb->expr()->count('p.id'));

        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true);

        return (bool)$query->getSingleScalarResult();
    }

    /**
     * @param Category $category
     * @return mixed
     */
    public function findByCategory(Category $category)
    {
        $qb = $this->buildCategoryQuery($category);

        return $this->paginate($qb->getQuery());
    }

    /**
     * @param Category $category
     * @return QueryBuilder
     */
    public function buildCategoryQuery(Category $category)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('p');
        $qb->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('c.product', 'p'));
        $qb->join(Office::class, 'o', Join::WITH, $qb->expr()->eq('o', 'c.office'));
        $qb->join(Employee::class, 'e', Join::WITH, $qb->expr()->eq('e.office', 'o'));
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq('e.user', ':user'),
            $qb->expr()->eq('c.expired', 0),
            $this->buildCategoryOr($category, $qb)
        ));
        $qb->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb;
    }

    /**
     * @param Category $category
     * @param QueryBuilder $qb
     * @return Orx
     */
    public function buildCategoryOr(Category $category, QueryBuilder $qb)
    {
        /** @var Orx $orX */
        $orX = $qb->expr()->orX();

        /** @var User $user */
        if ($user = $this->get('user_helper')->getCurrentUser()) {
            $employee = $user->getEmployee();
        }

        $this->addCategory($qb, $category, $orX, $employee ?? null);
        $orX->add($qb->expr()->eq(':cat', 'p.category'));
        $qb->setParameter('cat', $category);

        return $orX;
    }

    /**
     * @param QueryBuilder $qb
     * @param Category $root
     * @param Orx $orX
     * @param Employee|null $employee
     */
    private function addCategory(QueryBuilder $qb, Category $root, Orx $orX, Employee $employee = null)
    {
        $slug = $root->getSlug();

        if ($employee && preg_match('/uniformes/', $slug)) {
            /** @var UniformUserConstraint $uniformConstraint */
            $uic = $this->get('uniform_user_constraint');
            if (!$uic->isAllowed($employee)) {
                return;
            }
        }

        if (preg_match('/(mantencion-sanitarios)$/', $slug)) {
            return;
        }

        /** @var Category $child */
        foreach ($root->getChildren() as $child) {
            $id = sprintf('child_%s', $this->counter++);
            $orX->add($qb->expr()->eq(':' . $id, 'p.category'));
            $qb->setParameter($id, $child);
            $this->addCategory($qb, $child, $orX);
        }
    }
}
