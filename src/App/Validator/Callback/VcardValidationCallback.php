<?php

namespace App\Validator\Callback;

use App\Model\VcardContact;
use App\Model\VcardRequisition;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class VcardValidationCallback
{
    /**
     * @param VcardRequisition|VcardContact $object
     * @param ExecutionContextInterface $context
     */
    public static function validate($object, ExecutionContextInterface $context)
    {
        $phoneLen = $object->getAreaCode() === 2 ? 8 : 7;
        
        $addViolation = function ($path, $len) use ($context) {
            $context->buildViolation(sprintf('El número debe tener %s dígitos', $len))
                ->atPath($path)
                ->addViolation()
            ;
        };
        
        if (strlen($object->getPhone()) !== $phoneLen) {
            $addViolation('phone', $phoneLen);
        }
        
        if ($object instanceof VcardRequisition) {
            if ($object->getFax() && strlen($object->getFax()) !== $phoneLen) {
                $addViolation('fax', $phoneLen);
            }

            if ($object->getMobile() && strlen($object->getMobile()) !== 8) {
                $addViolation('mobile', 8);
            }
        }
    }
}