<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Rut extends Constraint
{
    public $message = 'RUT Inválido (%s)';
    public $invalidRegex = 'El RUT debe tener el formato 00000000-0';
}
