<?php

namespace App\Validator\Constraints;

use App\Util\RutUtils;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JMS\DiExtraBundle\Annotation\Validator;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @Validator("App\Validator\Constraints\SupplierValidator")
 */
class SupplierValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $manager;

    public function validate($value, Constraint $constraint)
    {
        if (!preg_match('/^\d*$/', $value)) {
            $this->context->buildViolation($constraint->invalidRegex)->addViolation();
            return;
        }

        if(!empty($value)) {
            $rut = sprintf('%s-%s', $value, RutUtils::calculateVerifier($value));
            $supplier = $this->manager->getRepository('App:Supplier')->findOneByRut($rut);

            if(!$supplier) {
                $this->context->buildViolation(sprintf($constraint->message, $value))->addViolation();
            }
        }
    }
}
