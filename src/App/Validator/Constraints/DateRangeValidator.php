<?php

namespace App\Validator\Constraints;

use JMS\DiExtraBundle\Annotation\Validator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Validator("daterange_validator")
 */
class DateRangeValidator extends ConstraintValidator
{

    public function validate($entity, Constraint $constraint)
    {
        $hasEndDate = true;
        if ($constraint->hasEndDate !== null) {
            $hasEndDate = $constraint->hasEndDate;
        }

        if ($entity->getStart() !== null) {
            if ($hasEndDate) {
                if ($entity->getEnd() !== null) {
                    if ($entity->getStart() > $entity->getEnd()) {
                        $this->context->buildViolation($constraint->message)->addViolation();
                        return;
                    }
                } else {
                    $this->context->buildViolation($constraint->emptyEndDate)->addViolation();
                    return;
                }
            } else {
                if ($entity->getEnd() !== null) {
                    if ($entity->getStart() > $entity->getEnd()) {
                        $this->context->buildViolation($constraint->message)->addViolation();
                        return;
                    }
                }
            }
        } else {
            $this->context->buildViolation($constraint->emptyStartDate)->addViolation();
            return;
        }
    }
}
