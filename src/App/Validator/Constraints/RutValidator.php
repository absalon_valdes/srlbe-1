<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JMS\DiExtraBundle\Annotation\Validator;

/**
 * @Validator("App\Validator\Constraints\RutValidator")
 */
class RutValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!preg_match('/^\d{5,8}-[\dkK]$/', $value)) {
            $this->context->buildViolation($constraint->invalidRegex)->addViolation();
            return;
        }

        $input = explode('-', $value);
        $rut = str_split($input[0]);

        $factor = 2;
        $accum  = 0;
        for ($i = count($rut) - 1; $i >= 0; $i--) {
            $factor = $factor > 7 ? 2 : $factor;
            $accum += intval($rut[$i]) * $factor++;
        }

        $dv = 11 - ($accum % 11);
        $dv = $dv == 11 ? 0 : ($dv == 10 ? 'k' : $dv);

        if ($dv != strtolower($input[1])) {
            $this->context->buildViolation(sprintf($constraint->message, $value))->addViolation();
        }
    }
}
