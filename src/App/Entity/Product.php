<?php

namespace App\Entity;

use BE\UniformBundle\Entity\Garment;
use BE\UniformBundle\Entity\Tag;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Hateoas\Configuration\Annotation as Hateoas;
use Hateoas\Configuration\Metadata\ClassMetadataInterface;
use Hateoas\Configuration\Relation;
use Hateoas\Configuration\Route;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Serializer\ExclusionPolicy("ALL")
 * @Hateoas\Relation(
 *     "self", 
 *     href=@Hateoas\Route(
 *         "resource_product",
 *         parameters={
 *             "id" = "expr(object.getId())"
 *         }
 *     )
 * )
 * @Hateoas\RelationProvider("addImagesRelation")
 * @ORM\Table(name="products")
 * @Gedmo\SoftDeleteable
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @UniqueEntity(fields="code", message="El Codigo tecnico ya existe")
 */
class Product
{
    use SoftDeleteableEntity;
    
    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $centralized = false;
    
    /**
     * @Assert\NotBlank(message="El Nombre no puede estar en blanco")
     * @ORM\Column(type="string")
     * @Serializer\Expose() 
     */
    protected $name;

    /**
     * @Assert\NotBlank(message="El Codigo no puede estar en blanco")
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $code;

    /**
     * @Assert\NotBlank(message="Seleccione al menos una categioria")
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $category;

    /**
     * @Assert\NotBlank(message="La descripción no puede estar en blanco")
     * @ORM\Column(type="text")
     * @Serializer\Expose()
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Serializer\Expose()
     */
    protected $unitOfMeasure;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $approver;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     */
    protected $technicalSpecs;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\SerializedName("specs")
     */
    protected $applySpecs;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     */
    protected $warnings;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     */
    protected $instructions;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $images;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $attachments;

    /**
     * @var Garment[]
     * @ORM\ManyToMany(targetEntity="\BE\UniformBundle\Entity\Garment",mappedBy="products")
     */
    protected $garments;

    /**
     * @var Tag[]
     * @ORM\ManyToMany(targetEntity="\BE\UniformBundle\Entity\Tag",mappedBy="products")
     */
    protected $uniformTags;

    public function __construct()
    {
        $this->garments = new ArrayCollection();
        $this->uniformTags = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return boolean
     */
    public function isCentralized()
    {
        return $this->centralized;
    }

    /**
     * @param boolean $centralized
     */
    public function setCentralized($centralized)
    {
        $this->centralized = $centralized;
    }
    
    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $unitOfMeasure
     * @return $this
     */
    public function setUnitOfMeasure($unitOfMeasure)
    {
        $this->unitOfMeasure = $unitOfMeasure;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnitOfMeasure()
    {
        return $this->unitOfMeasure;
    }

    /**
     * @param $technicalSpecs
     * @return $this
     */
    public function setTechnicalSpecs($technicalSpecs)
    {
        $this->technicalSpecs = $technicalSpecs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTechnicalSpecs()
    {
        return $this->technicalSpecs;
    }

    /**
     * @param $applySpecs
     * @return $this
     */
    public function setApplySpecs($applySpecs)
    {
        $this->applySpecs = $applySpecs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getApplySpecs()
    {
        return $this->applySpecs;
    }

    /**
     * @param $warnings
     * @return $this
     */
    public function setWarnings($warnings)
    {
        $this->warnings = $warnings;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWarnings()
    {
        return $this->warnings;
    }

    /**
     * @param $instructions
     * @return $this
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * @param User|null $approver
     * @return $this
     */
    public function setApprover(User $approver = null)
    {
        $this->approver = $approver;
        
        return $this;
    }
    
    /**
     * @return User
     */
    public function getApprover()
    {
        return $this->approver;
    }

    /**
     * @param $image
     * @return $this
     */
    public function addImage($image)
    {
        $this->images = [$image];

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param $name
     * @param $attachment
     * @return $this
     */
    public function addAttachment($name, $attachment)
    {
        $this->attachments[$name] = $attachment;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;
        
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @param $images
     * @return $this
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * @param $attachments
     * @return $this
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * @param $name
     * @return bool
     */
    public function belongsToCategory($name)
    {
        /** @var Category $category */
        $category = $this->category;

        do {
            if ($category && $category->getName() === $name) {
                return true;
            }
        } while ($category && $category = $category->getParent());
        
        return false;
    }

    /**
     * @return string|null
     */
    public function getCategoryPath()
    {
        if (!$this->category) {
            return null;
        }
        
        $path = [];
        $category = $this->category;
        
        do {
            $path[] = $category->getName();    
        } while ($category && $category = $category->getParent());
        
        return implode('/', array_reverse($path));
    }

    /**
     * @return string|null
     */
    public function getApproverId()
    {   
        if (!$approver = $this->approver) {
            return null;
        }
        
        if (!$employee = $approver->getEmployee()) {
            return null;
        }
        
        return $employee->getRut();
    }

    public function addImagesRelation($object, ClassMetadataInterface $classMetadata)
    {
        if (!count($this->images)) {
            return [];
        }
        
        return [
            new Relation('image', 
                new Route('download_product_file', [
                    'fileName' => $this->images[0]
                ])
            )
        ];
    }

    /**
     * @return Garment[]
     */
    public function getGarments()
    {
        return $this->garments;
    }

    /**
     * @param Garment $garment
     * @return Product
     */
    public function addGarment(Garment $garment){
        if(!$this->garments->exists(function($key, $element) use ($garment){
            return $garment->getName() === $element->getName();
        })){
            $this->garments->add($garment);
        }
        return $this;
    }

    /**
     * @param Garment[] $garments
     * @return Product
     */
    public function setGarments($garments)
    {
        $this->garments = $garments;
        return $this;
    }

    /**
     * @param Garment $garment
     * @return Product
     */
    public function removeGarment($garment)
    {
        $this->garments->removeElement($garment);
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getUniformTags()
    {
        return $this->uniformTags;
    }

    /**
     * @param Tag $tag
     * @return Product
     */
    public function addUniformTags(Tag $tag){
        if(!$this->uniformTags->exists(function($key, $element) use ($tag){
            return $tag->getSlug() === $element->getSlug();
        })){
            $this->uniformTags->add($tag);
        }
        return $this;
    }

    /**
     * @param Tag $tags
     * @return Product
     */
    public function setUniformTags($tags)
    {
        $this->uniformTags = $tags;
        return $this;
    }

    /**
     * @param Tag $tag
     * @return Product
     */
    public function removeUniformTag(Tag $tag)
    {
        $this->uniformTags->removeElement($tag);
        return $this;
    }

}
