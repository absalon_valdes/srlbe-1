<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @Serializer\ExclusionPolicy("ALL")
 * @Gedmo\SoftDeleteable()
 * @Gedmo\Loggable
 */
class User extends BaseUser
{
    use SoftDeleteableEntity;
    
    const TYPE_NORMAL = 'user.normal';
    const TYPE_VIP    = 'user.vip';
    const TYPE_MAIN   = 'requester.main';
    const TYPE_DEPUTY = 'requester.deputy';
    const TYPE_ALTER  = 'requester.alter';
    const TYPE_NONE   = 'requester.none';

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $type = self::TYPE_NORMAL;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isEmployee = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Employee", mappedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $employee;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $secured = false;

    /**
     * The salt to use for hashing.
     * @Gedmo\Versioned
     *
     * @var string
     */
    protected $salt;

    /**
     *
     * @Gedmo\Versioned
     * @var string
     */
    protected $password;

    /**
     *
     * @Gedmo\Versioned
     * @var string
     */
    protected $username;

    /**
     * @Gedmo\Versioned
     * @var array
     */
    protected $roles;

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $isEmployee
     * @return $this
     */
    public function setIsEmployee($isEmployee)
    {
        $this->isEmployee = $isEmployee;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsEmployee()
    {
        return $this->isEmployee;
    }

    /**
     * @param $secured
     * @return $this
     */
    public function setSecured($secured)
    {
        $this->secured = $secured;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSecured()
    {
        return $this->secured;
    }

    /**
     * @return bool
     */
    public function isSecured()
    {
        return $this->secured;
    }

    /**
     * @param Employee|null $employee
     * @return $this
     */
    public function setEmployee(Employee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }
}
