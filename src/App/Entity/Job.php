<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Job
 * 
 * @ORM\Entity(repositoryClass="App\Repository\JobRepository")
 * @ORM\Table(name="queue_jobs")
 */
class Job
{
    const QUEUED     = 'job.queued';
    const RETRY      = 'job.retry';
    const LOADING    = 'job.loading';
    const VALIDATING = 'job.validating';
    const PREPARING  = 'job.preparing';
    const RUNNING    = 'job.running';
    const STOPPED    = 'job.stopped';
    const CANCELLED  = 'job.cancelled';
    const COMPLETED  = 'job.completed';
    const FAILED     = 'job.failed';
    
    const STATUSES = [
        self::QUEUED,
        self::RETRY,
        self::LOADING,
        self::VALIDATING,
        self::PREPARING,
        self::RUNNING,
        self::STOPPED,
        self::CANCELLED,
        self::COMPLETED,
        self::FAILED,
    ];
    
    /**
     * @ORM\Id @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;
    
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Type("DateTime<'U'>")
     */
    protected $startedAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $finishedAt;
    
    /**
     * @var array
     * @ORM\Column(type="array")
     */
    protected $payload = array();
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $queue;
    
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $size = 0;
    
    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $processed = 0;
   
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $report;
    
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $state = self::QUEUED;
    
    /**
     * Job constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime;
    }
    
    /**
     * @param $queue
     * @param array $payload
     * @return Job
     */
    public static function create($queue, array $payload = [])
    {
        $job = new self;
        $job->setQueue($queue);
        $job->setPayload($payload);
        
        return $job;
    }
    
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @param null $key
     * @return array
     */
    public function getPayload($key = null)
    {
        if ($key) {
            return $this->payload[$key] ?? null;
        }
        
        return $this->payload;
    }
    
    /**
     * @param array $payload
     * @return $this
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getQueue()
    {
        return $this->queue;
    }
    
    /**
     * @param string $queue
     * @return $this
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;
        
        return $this;
    }
    
    /**
     * Increment progress counter
     * @return $this
     */
    public function stepProgress()
    {
        $this->processed++;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }
    
    /**
     * @param string $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        
        return $this;
    }
    
    /**
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }
    
    /**
     * @param \DateTime $startedAt
     * @return $this
     */
    public function setStartedAt(\DateTime $startedAt)
    {
        $this->startedAt = $startedAt;
        
        return $this;
    }
    
    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * @return \DateTime
     */
    public function getFinishedAt()
    {
        return $this->finishedAt;
    }
    
    /**
     * @param \DateTime $finishedAt
     * @return $this
     */
    public function setFinishedAt(\DateTime $finishedAt)
    {
        $this->finishedAt = $finishedAt;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getProcessed()
    {
        return $this->processed;
    }
    
    /**
     * @param int $processed
     * @return $this
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getReport()
    {
        return $this->report;
    }
    
    /**
     * @param string $report
     * @return $this
     */
    public function setReport($report)
    {
        $this->report = $report;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }
    
    /**
     * @param int $size
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;
        
        return $this;
    }
    
    /**
     * @return float|int
     */
    public function getProgress()
    {
        if (0 === $this->size || 0 === $this->processed) {
            return 0;
        }
        
        return $this->processed / $this->size;
    }

    /**
     * @return $this
     */
    public function retry() {
        $this->setState($this::RETRY);
        return $this;
    }

    /**
     * @return $this
     */
    public function stop() {
        $this->setState($this::STOPPED);
        return $this;
    }

    /**
     * @return $this
     */
    public function cancel() {
        $this->setState($this::CANCELLED);
        return $this;
    }

    public function isCanceled() {
        return self::CANCELLED === $this->state;
    }

    public function isStopped() {
        return self::STOPPED === $this->state;
    }

    public function isRetried() {
        return self::RETRY === $this->state;
    }
}