<?php

namespace App\Entity;

use App\Helper\RatingCalculator;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Validator\Constraints as Validate;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @Serializer\ExclusionPolicy("ALL")
 * @Hateoas\Relation("self", href=@Hateoas\Route("resource_supplier", parameters={"id" = "expr(object.getId())"}))
 * @ORM\Entity(repositoryClass="App\Repository\SupplierRepository")
 * @ORM\Table(name="suppliers")
 * @Gedmo\SoftDeleteable
 * @UniqueEntity(fields="rut", message="El RUT ya existe")
 */
class Supplier
{
    use SoftDeleteableEntity;
    
    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="El Nombre no puede estar en blanco")
     * @ORM\Column(type="string", length=300)
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @Assert\NotBlank(message="El rut no puede estar en blanco")
     * @Validate\Rut()
     * @ORM\Column(type="string", length=10)
     * @Serializer\Expose()
     */
    protected $rut;

    /**
     * @ORM\Column(type="string", length=60)
     * @Serializer\Expose()
     */
    protected $phone;

    /**
     * @Assert\NotBlank(message="La calle no puede estar en blanco")
     * @ORM\Column(type="string", length=300)
     * @Serializer\Expose()
     */
    protected $street;

    /**
     * @Assert\NotBlank(message="La comuna no puede estar en blanco")
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @Serializer\Expose()
     */
    protected $city;

    /**
     * @Assert\Url(message = "La url no es válida")
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @Serializer\Expose()
     */
    protected $website;

    /**
     * @Assert\Email(message="Email inválido")
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Serializer\Expose()
     */
    protected $email;

    /**
     * @ORM\Column(type="text")
     * @Serializer\Expose()
     */
    protected $excerpt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     */
    protected $activity;

    /**
     * @Assert\NotBlank(message="Debe tener por lo menos un representante")
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     * @ORM\JoinTable(name="supplier_representatives")
     */
    protected $representatives;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Accessor(getter="getRatings")
     * @Serializer\Expose()
     */
    protected $rating;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->representatives = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $rut
     *
     * @return $this
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param $street
     *
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param $website
     *
     * @return $this
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $excerpt
     *
     * @return $this
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @param Category|null $city
     *
     * @return $this
     */
    public function setCity(Category $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param Category|null $activity
     *
     * @return $this
     */
    public function setActivity(Category $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * @param User $representative
     *
     * @return $this
     */
    public function addRepresentative(User $representative)
    {
        $this->representatives[] = $representative;

        return $this;
    }

    /**
     * @param User $representative
     *
     * @return bool
     */
    public function removeRepresentative(User $representative)
    {
        return $this->representatives->removeElement($representative);
    }

    /**
     * @return ArrayCollection
     */
    public function getRepresentatives()
    {
        return $this->representatives;
    }

    /**
     * @param $rating
     *
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param array $new
     *
     * @return $this
     */
    public function addRating(array $new)
    {
        $this->rating[] = $new;

        return $this;
    }

    public function getRatings()
    {
        return RatingCalculator::getAverage($this->rating);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return sprintf('%s, %s, %s', $this->street, $this->city->getName(), $this->city->getParent()->getName());
    }
}
