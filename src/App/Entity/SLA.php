<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SLA
 * @package App\Entity
 * @ORM\Embeddable
 */
class SLA
{
    const CALENDAR_HOUR = 'sla.calendar.hour';
    const CALENDAR_DAY  = 'sla.calendar.day';
    const BUSINESS_HOUR = 'sla.business.hour';
    const BUSINESS_DAY  = 'sla.business.day';

    /**
     * @ORM\Column(type="integer")
     */
    protected $value;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * SLA constructor.
     * @param int $value
     * @param string $type
     */
    public function __construct(int $value, string $type)
    {
        if (!in_array($type, [
            self::BUSINESS_DAY, 
            self::BUSINESS_HOUR, 
            self::CALENDAR_DAY, 
            self::CALENDAR_HOUR
        ], true)) {
            throw new \InvalidArgumentException('Invalid SLA type');
        }

        $this->value = $value;
        $this->type  = $type;
    }

    /**
     * @param int $value
     * @param string $type
     * @return SLA
     */
    public static function create(int $value, string $type)
    {
        return new self($value, $type);
    }

    /**
     * @param $string
     * @return SLA
     */
    public static function fromString($string)
    {
        return new self(self::getSlaValue($string), self::getSlaType($string));
    }

    /**
     * @param $sla
     * @return string
     */
    public static function getSlaType($sla)
    {
        if (false !== stripos($sla, 'habil')) {
            if (false !== stripos($sla, 'dia')) { 
                return self::BUSINESS_DAY;
            } elseif (false !== stripos($sla, 'hora')) { 
                return self::BUSINESS_HOUR;
            }
        } elseif (false !== stripos($sla, 'corrid')) { 
            if (false !== stripos($sla, 'dia')) { 
                return self::CALENDAR_DAY;
            } elseif (false !== stripos($sla, 'hora')) { 
                return self::CALENDAR_HOUR;
            }
        }

        return self::BUSINESS_DAY; 
    }

    /**
     * @param $value
     * @return int
     */
    public static function getSlaValue($value)
    {
        $val = (int) preg_replace('/\D/','', $value);
        
        return is_int($val) ? $val : 0;
    }
    
    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isBusiness()
    {
        return $this->type === self::BUSINESS_DAY || $this->type === self::BUSINESS_HOUR; 
    }

    /**
     * @return bool
     */
    public function isCalendar()
    {
        return $this->type === self::CALENDAR_DAY || $this->type === self::CALENDAR_HOUR;
    }

    /**
     * @return bool
     */
    public function inDays()
    {
        return $this->type === self::BUSINESS_DAY || $this->type === self::CALENDAR_DAY; 
    }

    /**
     * @return bool
     */
    public function inHours()
    {
        return $this->type === self::CALENDAR_HOUR || $this->type === self::BUSINESS_HOUR; 
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s %s', $this->value, $this->type);
    }
}