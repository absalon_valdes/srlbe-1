<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @Serializer\ExclusionPolicy("ALL")
 * @Gedmo\SoftDeleteable
 * @ORM\Table(name="offices")
 * @ORM\Entity(repositoryClass="App\Repository\OfficeRepository")
 * @UniqueEntity(fields="code", message="La sucursal ya esta registrada")
 */
class Office implements \JsonSerializable
{
    use SoftDeleteableEntity;
    
    const TYPE_CENTRAL  = 'unit.central';
    const TYPE_OFFICE   = 'unit.office';
    const TYPE_DIVISION = 'unit.division';
    const TYPE_BRANCH   = 'unit.branch';

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="El Nombre no puede estar en blanco")
     * @ORM\Column(type="string", length=300)
     * @Serializer\Expose()
    */
    protected $name;

    /**
     * @Assert\NotBlank(message="El codigo no puede estar en blanco")
     * @ORM\Column(type="string", length=50)
     * @Serializer\Expose()
    */
    protected $code;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $type = Office::TYPE_CENTRAL;

    /**
     * @Assert\NotBlank(message="El codigo de dependencia no puede estar en blanco")
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $dependencyCode;

    /**
     * @Assert\NotBlank(message="La dirección no puede estar en blanco")
     * @ORM\Column(type="string", length=200)
     * @Serializer\Expose()
    */
    protected $address;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     * @Serializer\Expose()
     */
    protected $addressDetail;

    /**
     * @Assert\NotBlank(message="Debe seleccionar una comuna")
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @Serializer\Expose()
     */
    protected $city;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Serializer\Expose()
     */
    protected $costCenter;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Office", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Office", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $category;


    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Expose()
     */
    protected $specialType;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s', $this->code, $this->name);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getAddressDetail()
    {
        return $this->addressDetail;
    }

    /**
     * @param string $addressDetail
     */
    public function setAddressDetail($addressDetail)
    {
        $this->addressDetail = $addressDetail;
    }

    /**
     * @param Category|null $city
     * @return $this
     */
    public function setCity(Category $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $costCenter
     * @return $this
     */
    public function setCostCenter($costCenter)
    {
        $this->costCenter = $costCenter;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCostCenter()
    {
        return $this->costCenter;
    }

    /**
     * @param $dependencyCode
     * @return $this
     */
    public function setDependencyCode($dependencyCode)
    {
        $this->dependencyCode = $dependencyCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDependencyCode()
    {
        return $this->dependencyCode;
    }

    /**
     * @param $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Office $child
     * @return $this
     */
    public function addChild(Office $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * @param Office $child
     * @return bool
     */
    public function removeChild(Office $child)
    {
        return $this->children->removeElement($child);
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Office|null $parent
     * @return $this
     */
    public function setParent(Office $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param $specialType
     * @return $this
     */
    public function setSpecialType($specialType)
    {
        $this->specialType = $specialType;

        return $this;
    }

    /**
     * @return string
     */
    public function getSpecialType()
    {
        return $this->specialType;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'code' => $this->code,
            'name' => $this->name,
            'dependency_code' => $this->dependencyCode,
            'address' => $this->address,
            'address_detail' => $this->addressDetail,
            'city' => $this->city ? $this->city->getName() : '-',
            'region' => $this->city ? $this->city->getParent() ? $this->city->getParent()->getName() : '' : '',
            'cost_center' => $this->costCenter,
        ];
    }
}
