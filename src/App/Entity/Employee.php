<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as Validate;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 * @ORM\Table(name="employees")
 * @Gedmo\SoftDeleteable
 * @UniqueEntity(fields="rut", message="El RUT ya existe")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Employee implements \JsonSerializable
{
    use SoftDeleteableEntity;
    
    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="employee")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    /**
     * @Assert\NotBlank(message="El Nombre no puede estar en blanco")
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @Assert\NotBlank(message="El Apellido no puede estar en blanco")
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $lastName;

    /**
     * @Assert\NotBlank(message="El Segundo Apellido no puede estar en blanco")
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $secondLastName;

    /**
     * @Assert\NotBlank(message="El rut no puede estar en blanco")
     * @Validate\Rut()
     * @ORM\Column(type="string", length=100)
     * @Serializer\Expose()
     */
    protected $rut;

    /**
     * @ORM\Column(type="string", nullable=true,length=100)
     * @Serializer\Expose()
     */
    protected $alternateAddress;

    /**
     * @ORM\Column(type="string", nullable=true,length=100)
     * @Serializer\Expose()
     */
    protected $secondPhone;

    /**
     * @Assert\NotBlank(message="El cargo no puede estar en blanco")
     * @ORM\ManyToOne(targetEntity="App\Entity\Position")
     * @Serializer\Expose()
     */
    protected $position;

    /**
     * @Assert\NotBlank(message="La sucursal no puede estar en blanco")
     * @ORM\ManyToOne(targetEntity="App\Entity\Office")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $office;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Office")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $deliveryAddress;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Expose()
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Expose()
     */
    protected $gender;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("email")
     */
    public function getEmail()
    {
        return $this->user->getEmail();
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param $secondLastName
     * @return $this
     */
    public function setSecondLastName($secondLastName)
    {
        $this->secondLastName = $secondLastName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSecondLastName()
    {
        return $this->secondLastName;
    }

    /**
     * @param $rut
     * @return $this
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param Position|null $position
     * @return $this
     */
    public function setPosition(Position $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return ucwords(mb_strtolower(sprintf('%s %s %s', $this->name, $this->lastName, $this->secondLastName,'UTF-8')));
    }

    /**
     * @return string
     */
    public function getApproverCode()
    {
        return sprintf("E-%'.03d", $this->id);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s %s', $this->rut, $this->getFullName());
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param Office|null $office
     * @return $this
     */
    public function setOffice(Office $office = null)
    {
        $this->office = $office;

        return $this;
    }

    /**
     * @return Office
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'last_name' => $this->lastName,
            'second_last_name' => $this->secondLastName,
            'email' => $this->getEmail(),
            'phone' => $this->phone,
        ];
    }

    /**
     * Set alternateAddress.
     *
     * @param string|null $alternateAddress
     *
     * @return Employee
     */
    public function setAlternateAddress($alternateAddress = null)
    {
        $this->alternateAddress = $alternateAddress;

        return $this;
    }

    /**
     * Get alternateAddress.
     *
     * @return string|null
     */
    public function getAlternateAddress()
    {
        return $this->alternateAddress;
    }

    /**
     * Set secondPhone.
     *
     * @param string|null $secondPhone
     *
     * @return Employee
     */
    public function setSecondPhone($secondPhone = null)
    {
        $this->secondPhone = $secondPhone;

        return $this;
    }

    /**
     * Get secondPhone.
     *
     * @return string|null
     */
    public function getSecondPhone()
    {
        return $this->secondPhone;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param mixed $deliveryAddress
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }


}
