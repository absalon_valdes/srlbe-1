<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * Class SpecialRequest
 * @package App\Entity
 * @ORM\Table(name="special_requests")
 * @ORM\Entity(repositoryClass="App\Repository\SpecialRequestRepository")
 * @Gedmo\Loggable
 * @Gedmo\SoftDeleteable
 */
class SpecialRequest
{
    use SoftDeleteableEntity;

    const STATE_CART = 'state.cart';
    const STATE_EVALUATION = 'state.evaluation';
    const STATE_IN_PROGRESS = 'state.in_progress';
    const STATE_CLOSED = 'state.closed';
    const STATE_REJECTED = 'state.rejected';
    const STATE_REJECTED_FINAL = 'state.rejected_final';
    const STATE_APPEALED = 'state.appealed';

    const CLOSED_STATUSES = [
        self::STATE_CLOSED,
        self::STATE_REJECTED_FINAL
    ];
    
    /**
     * @ORM\Id @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $closedAt;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $number;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $state = self::STATE_CART;
    
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $requester;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $assignee;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     */
    protected $request;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     */
    protected $description;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Date(message="Debe ser una fecha válida")
     */
    protected $date;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    protected $files = array();
    
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $supplier;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $costCenter;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cost;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $rejectionComment;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $rejectionApeal;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    protected $comments = array();

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param string $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @return User
     */
    public function getAssignee()
    {
        return $this->assignee;
    }

    /**
     * @param User $assignee
     */
    public function setAssignee(User $assignee)
    {
        $this->assignee = $assignee;
    }

    /**
     * @return User
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * @param User $requester
     */
    public function setRequester(User $requester)
    {
        $this->requester = $requester;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return string
     */
    public function getCostCenter()
    {
        return $this->costCenter;
    }

    /**
     * @param string $costCenter
     */
    public function setCostCenter($costCenter)
    {
        $this->costCenter = $costCenter;
    }

    /**
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param int $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getClosedAt()
    {
        return $this->closedAt;
    }

    /**
     * @param mixed $closedAt
     */
    public function setClosedAt($closedAt)
    {
        $this->closedAt = $closedAt;
    }

    /**
     * @return array
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param array $comments
     * @return $this
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @param $comment
     * @return $this
     */
    public function addComment($comment)
    {
        $this->comments[] = $comment;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getRejectionComment()
    {
        return $this->rejectionComment;
    }

    /**
     * @param string $rejectionComment
     */
    public function setRejectionComment($rejectionComment)
    {
        $this->rejectionComment = $rejectionComment;
    }

    /**
     * @return string
     */
    public function getRejectionApeal()
    {
        return $this->rejectionApeal;
    }

    /**
     * @param string $rejectionApeal
     */
    public function setRejectionApeal($rejectionApeal)
    {
        $this->rejectionApeal = $rejectionApeal;
    }

    /**
     * @return Requisition
     */
    public function mockRequisition()
    {
        $p = new Requisition;
        $p->setRequester($this->getRequester());
        $p->setType('special_request');
        $p->setStatus($this->getState());
        $p->setNumber($this->getNumber());
        $p->setCreatedAt($this->getCreatedAt());
        $p->setUpdatedAt($this->getUpdatedAt());
        $p->setAssignee($this->getAssignee());
        $p->setStateDueDate(new \DateTime);
        
        $ref = new \ReflectionObject($p);
        $id = $ref->getProperty('id');
        $id->setAccessible(true);
        $id->setValue($p, $this->getId());

        $x = new Product();
        $x->setName($this->getDescription());
        
        $u = new Contract();
        $u->setCostCenter($this->getCostCenter());
        
        $p->setProduct($x);
        $p->setContract($u);
        
        return $p;
    }
}