<?php

namespace App\Entity;

use App\Annotations\NoGetter;
use App\Annotations\NoSetter;
use Doctrine\Common\Annotations\AnnotationReader;

trait AccesorTraits
{
    public function __call($method, $args)
    {
        if (!preg_match('/(?P<accessor>set|get)(?P<property>[A-Z][a-zA-Z0-9]*)/', $method, $match) ||
            !property_exists(__CLASS__, $match['property'] = lcfirst($match['property']))
        ) {
            throw new \BadMethodCallException(sprintf(
                "'%s' does not exist in '%s'.", $method, get_class($this)
            ));
        }
        
        $property = $match['property'];

        $reader = new AnnotationReader();
        $ref = new \ReflectionProperty($this, $property);
        $annotations = $reader->getPropertyAnnotations($ref);
        $classes = array_map('get_class', $annotations);
        
        if ('get' === $match['accessor']) {
            if (in_array(NoGetter::class, $classes, true)) {
                throw new \BadMethodCallException(sprintf(
                    "'%s' not allowed in '%s'.", $method, get_class($this)
                ));
            }
            
            return $this->{$property};
        }
        
        if ('set' === $match['accessor']) {
            if (in_array(NoSetter::class, $classes, true)) {
                throw new \BadMethodCallException(sprintf(
                    "'%s' not allowed in '%s'.", $method, get_class($this)
                ));
            }
            
            if (!$args) {
                throw new InvalidArgumentException(sprintf(
                    "'%s' requires an argument value.", $method
                ));
            }
            
            $this->{$property} = $args[0];
            return $this;
        }
    }
}