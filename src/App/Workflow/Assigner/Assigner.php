<?php

namespace App\Workflow\Assigner;

use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class RequisitionAssigner
 * @package App\Workflow\Assigner
 * 
 */
abstract class Assigner implements AssignerInterface
{
    /**
     * @var ExpressionLanguage
     */
    private $expression;

    /**
     * RequisitionAssigner constructor.
     */
    public function __construct()
    {
        $this->expression = new ExpressionLanguage;
    }
    
    /**
     * @return array
     */
    abstract public function getAssignationRules();

    /**
     * @param AssignableInterface $object
     * @param $event
     */
    public function assign(AssignableInterface $object, $event)
    {
        $rules = $this->getAssignationRules();
        $eventKey = str_replace('.reached', '', $event);
        
        if (!isset($rules[$eventKey])) {
            return;
        }
        
        try {
            /** @var UserInterface $assignee */
            $assignee = $this->expression->evaluate($rules[$eventKey], [
                'object' => $object         
            ]);
            
            $object->setAssignee($assignee);
        } catch (\Exception $e) {
            // NOOP    
        }
    }
}