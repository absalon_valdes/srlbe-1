<?php

namespace App\Workflow\Assigner;

use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequisitionAssigner
 * @package App\Workflow\Assigner
 * 
 * @Di\Service("requisition.assigner")
 */
class RequisitionAssigner extends Assigner
{
    /**
     * @return array
     */
    public function getAssignationRules()
    {
        return [
            ProductStatus::CREATED   => 'object.getApprover()',
            ProductStatus::APPROVED  => 'object.getContract().getSupplierUser()',
            ProductStatus::REJECTED  => 'object.getRequester()',
            ProductStatus::DECLINED  => 'object.getApprover() ? object.getApprover() : object.getRequester()',
            ProductStatus::APPEAL    => 'object.getApprover()',
            ProductStatus::DENIED    => 'object.getRequester()', 
            ProductStatus::CONFIRMED => 'object.isCentralized() ? object.getApprover() : object.getRequester()',
            ProductStatus::HOLD      => 'object.getApprover()',      
            ProductStatus::REVIEWED  => 'object.getRequester()',
            ProductStatus::CLOSED    => 'object.getRequester()',
            ProductStatus::RETURNED  => 'object.getSupplier()',
        ];
    }
}