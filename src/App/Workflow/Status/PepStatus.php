<?php 

namespace App\Workflow\Status;

class PepStatus
{
    const CREATED   = 'pep.created';   // Registro de proveedores creado
    const VALIDATED = 'pep.validated'; // Proveedores validados contra base de datos de PEP's
    const ACCEPTED  = 'pep.accepted';  // Política de validación ha sido aceptada y está en espera
    const SELECTED  = 'pep.selected';
    const GRANTED   = 'pep.granted';   // Proveedor adjudicado y certificado
}