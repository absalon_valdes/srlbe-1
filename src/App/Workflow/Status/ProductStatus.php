<?php 

namespace App\Workflow\Status;

/**
 * Class ProductStatus
 * @package App\Workflow\Status
 * @deprecated App\Workflow\Status\ProductStatus debe reemplazarse con App\Model\RequisitionInterface
 */

use App\Model\RequisitionInterface;

trigger_error('App\Workflow\Status\ProductStatus debe reemplazarse con App\Model\RequisitionInterface',E_USER_DEPRECATED);

final class ProductStatus implements RequisitionInterface
{
    const DRAFT     = 'product.'.self::STATUS_DRAFT;     // en borrador (carro)
    const CREATED   = 'product.'.self::STATUS_CREATED;   // emitida
    const APPROVED  = 'product.'.self::STATUS_APPROVED;  // aprobada por el evaluador
    const ORDERED   = 'product.'.self::STATUS_ORDERED;   // emisión de orden de compra
    const REJECTED  = 'product.'.self::STATUS_REJECTED;  // rechazada por el evaluador
    const APPEAL    = 'product.'.self::STATUS_APPEAL;    // en apelación
    const DENIED    = 'product.'.self::STATUS_DENIED;    // apelación rechazada
    const ACCEPTED  = 'product.'.self::STATUS_ACCEPTED;  // aceptada por el proveedor
    const CONFIRMED = 'product.'.self::STATUS_CONFIRMED; // recepción confirmada por el proveedor
    const DECLINED  = 'product.'.self::STATUS_DECLINED;  // rechazada por el proveedor
    const HOLD      = 'product.'.self::STATUS_HOLD;      // esperando que evaluador acepte rechazo de proveedor
    const DELIVERED = 'product.'.self::STATUS_DELIVERED; // completada y entregada por el proveedor
    const REVIEWED  = 'product.'.self::STATUS_REVIEWED;  // certificada por el evaluador
    const RETURNED  = 'product.'.self::STATUS_RETURNED;  // certificacion rechazada por el evaluador
    const EVALUATED = 'product.'.self::STATUS_EVALUATED; // certificada por el demandante
    const CLOSED    = 'product.'.self::STATUS_CLOSED;    // proceso cerrado incompleto
    const CLOSED_BY_APPROVER = 'product.'.self::STATUS_CLOSED_BY_APPROVER; // Cerrado por el evaluador
    const COMPLETED = 'product.'.self::STATUS_COMPLETED; // proceso completado con éxito
    const TIMEOUT   = 'product.'.self::STATUS_TIMEOUT;   // cerrado por timeout
    const SHIPPED   = 'product.'.self::STATUS_SHIPPED;   // en camino
}