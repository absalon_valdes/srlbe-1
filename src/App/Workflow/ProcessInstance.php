<?php

namespace App\Workflow;

use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\WorkflowBundle\Flow\Process;
use Lexik\Bundle\WorkflowBundle\Handler\ProcessHandlerInterface;

/**
 * Class ProcessInstance
 * @package App\Workflow
 */
class ProcessInstance implements ProcessInstanceInterface
{
    protected $name;
    protected $handler;
    protected $om;
    protected $modelClass;
    protected $process;

    /**
     * ProcessInstance constructor.
     * 
     * @param ProcessHandlerInterface $handler
     * @param $name
     * @param ObjectManager $om
     * @param $modelClass
     */
    public function __construct(ProcessHandlerInterface $handler,
                                $name, ObjectManager $om, $modelClass)
    {
        $this->name = $name;
        $this->handler = $handler;
        $this->om = $om;
        $this->modelClass = $modelClass ?: ProcessModel::class;
    }

    /**
     * @param $model
     * @return \Lexik\Bundle\WorkflowBundle\Entity\ModelState
     */
    public function start($model)
    {
        $model = $this->getModel($model);
        $entity = $model->getWorkflowObject();
        $state = $this->handler->start($model);

        if ($state->getSuccessful()) {
            $this->om->persist($entity);
            $this->om->flush($entity);
        }

        return $state;
    }

    /**
     * @param $model
     * @param $state
     * @return \Lexik\Bundle\WorkflowBundle\Entity\ModelState
     */
    public function proceed($model, $state)
    {
        $model = $this->getModel($model);
        $entity = $model->getWorkflowObject();
        $state = $this->handler->reachNextState($model, $state);

        if ($state->getSuccessful()) {
            $this->om->flush($entity);
        }

        return $state;
    }

    /**
     * @param $model
     * @return \Lexik\Bundle\WorkflowBundle\Entity\ModelState
     */
    public function getCurrentState($model)
    {
        $model = $this->getModel($model);

        return $this->handler->getCurrentState($model);
    }

    /**
     * @param $model
     * @return array
     */
    public function getHistory($model)
    {
        $model = $this->getModel($model);

        return $this->handler->getAllStates($model, $this->name);
    }

    /**
     * @param $model
     * @return bool
     */
    public function isComplete($model)
    {
        $model = $this->getModel($model);

        return $this->handler->isProcessComplete($model);
    }

    public function getProcess()
    {
        return $this->handler->getProcess();
    }

    /**
     * @param $object
     * @return mixed
     */
    private function getModel($object)
    {
        $modelClass = $this->modelClass;

        if (is_subclass_of($object, $modelClass)) {
            return $object;
        }

        return new $modelClass($object);
    }
}
