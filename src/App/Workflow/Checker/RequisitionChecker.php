<?php 

namespace App\Workflow\Checker;

use App\Entity\Contract;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Form\WorkEvaluationType;
use JMS\DiExtraBundle\Annotation as Di;
use Lexik\Bundle\WorkflowBundle\Model\ModelInterface;

/**
 * @Di\Service("requisition_checker")
 *
 * @deprecated App\Workflow\Checker\RequisitionChecker debe reemplazarse con App\Entity\Requisition
 */

trigger_error('App\Workflow\Checker\RequisitionChecker debe reemplazarse con App\Entity\Requisition',E_USER_DEPRECATED);

class RequisitionChecker
{
    /**
     * @param ModelInterface $model
     * @return bool
     */
    public function isMaintenance(ModelInterface $model)
    {
        return $model->getWorkflowObject()->isMaintenance();
    }
    
    /**
     * @param $model ModelInterface
     * @return bool
     */
    public function withoutApprover(ModelInterface $model)
    {
        return $model->getWorkflowObject()->withoutApprover();
    }
    
    /**
     * @param $model ModelInterface
     * @return bool
     */
    public function isEvaluationOk(ModelInterface $model)
    {
        return $model->getWorkflowObject()->isEvaluationOk();
    }
    
    /**
     * @param $model ModelInterface
     * @return bool
     */
    public function revalidateNext(ModelInterface $model)
    {
        return $model->getWorkflowObject()->revalidateNext();
    }
}