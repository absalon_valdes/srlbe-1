<?php

namespace App\Workflow;

/**
 * Interface ProcessTokenInterface
 * @package App\Workflow
 */
interface ProcessTokenInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getObjectName();

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @return mixed
     */
    public function getWorkflowData();

    /**
     * @return string
     */
    public function getProcessName();
}
