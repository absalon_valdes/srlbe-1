<?php

namespace App\Queue;

use App\Entity\Job;

interface Worker
{
    public function execute(Job $job);
}