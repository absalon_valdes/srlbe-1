<?php

namespace App\Queue\Workers;

use App\Entity\Employee;
use App\Entity\Job;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Entity\Supplier;
use App\Helper\ExcelBatchProcessor;
use App\Helper\RequisitionTypeResolver;
use App\Helper\UserHelper;
use App\Import\Helper\RenderImporterErrorMessage;
use App\Model\JobReport;
use App\Queue\Workers\AbstractWorker;
use App\Repository\ContractRepository;
use App\Repository\EmployeeRepository;
use App\Repository\ProductRepository;
use App\Repository\Repository;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ContractValidatorWorker
 *
 * @DI\Service()
 */
class RequisitionImporterWorker extends AbstractWorker
{
    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var ProductRepository
     * @Di\Inject("product_repo")
     */
    public $productRepo;

    /**
     * @var Repository
     * @Di\Inject("supplier_repo")
     */
    public $supplierRepo;

    /**
     * @var EmployeeRepository
     * @Di\Inject("employee_repo")
     */
    public $employeeRepo;

    /**
     * @var ContractRepository
     * @Di\Inject("contract_repo")
     */
    public $contractRepo;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var RenderImporterErrorMessage
     * @Di\Inject("render_importer_error_message")
     */
    public $errorMessageRender;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    const HEADERS = [
        'FECHA CREACION',
        'HORA CREACION',
        'CODIGO PRODUCTO',
        'CODIGOS DE INVENTARIO',
        'COMENTARIOS',
        'RUT PROVEEDOR',
        'RUT DEMANDANTE',
        'CANTIDAD'
    ];

    /**
     * ContractImporterWorker constructor.
     *
     * @param EntityManager $em
     * @param $cacheDir
     *
     * @DI\InjectParams({
     *     "em"=@DI\Inject("doctrine.orm.entity_manager"),
     *     "cacheDir"=@DI\Inject("%kernel.var_dir%"),
     *     "validator"=@DI\Inject("validator")
     * })
     */
    public function __construct(EntityManager $em, $cacheDir)
    {
        parent::__construct($em, $cacheDir);
    }

    /**
     * @inheritDoc
     */
    public function process(Job $job)
    {
        $logfile = sprintf('%s/%s.log', $this->tempDir, $job->getId());
        $tempcsv = sprintf('%s/%s.csv', $this->tempDir, $job->getId());

        ExcelBatchProcessor::excelToCsv($job->getPayload('file'), $tempcsv);

        if (!file_exists($tempcsv) && !filesize($tempcsv)) {
            throw new \RuntimeException(sprintf('Cannot create file %s', $tempcsv));
        }

        $total = (int)exec(sprintf('wc -l < "%s"', $tempcsv));
        $this->setSize($job, $total);
        $this->report($job, $logfile);
        $cursor = 1;
        $logBatch = '';

        if ($handler = fopen($tempcsv, 'r')) {
            while (!feof($handler)) {

                if (!is_array($data = fgetcsv($handler))) {
                    break;
                }

                if (empty($data)) {
                    continue;
                }

                $report = JobReport::create($cursor++, $data);

                try {
                    $report = $this->processLine($report,$cursor);
                } catch (\Exception $e) {
                    $report->addError($e->getMessage());
                }

                $logBatch .= serialize($report) . PHP_EOL;
                $this->advance($job);

                if ($cursor % 25000) {
                    file_put_contents($logfile, $logBatch, FILE_APPEND);
                    $logBatch = '';
                    $this->em->clear(Requisition::class);
                }

                if ($cursor % 1000) {
                    $this->em->flush();
                }

                if(in_array($this::HEADER_ERROR_MESSAGE,$report->getErrors())) {
                    $job->setSize(1);
                    break;
                }
            }
            fclose($handler);
        }
    }

    private function processLine(JobReport $report,$line)
    {
        if($line == 2) {
            $this->hasCorrectHeaders(array_map([$this,'normalizeString'],$report->getData()))?:$report->addError($this::HEADER_ERROR_MESSAGE);
        }
        else {
            $requisitionData = $this->normalize($report->getData());

            if(!$requisitionData['created_at']) {
                $message = 'Fecha y/o hora con formato incorrecto ( formato: dd-mm-aaaa hh:mm)';
                $report->addError($message);
            }

            /** @var Employee $requester */
            $requester = $this->employeeRepo->findOneByRut($requisitionData['requester_rut']);
            if (!$requester) {
                $message = sprintf('Demandante %s no existe', $requisitionData['requester_rut']);
                $report->addError($message);
            }

            /** @var Supplier $supplier */
            $supplier = $this->supplierRepo->findOneByRut($requisitionData['supplier_rut']);
            if (!$supplier) {
                $message = sprintf('Proveedor %s no existe', $requisitionData['supplier_rut']);
                $report->addError($message);
            }

            /** @var Product $product */
            $product = $this->productRepo->findOneByCode($requisitionData['product_code']);
            if (!$product) {
                $message = sprintf('Producto %s no existe', $requisitionData['product_code']);
                $report->addError($message);
            }

            if (0 === count($report->getErrors())) {

                $contract = $this->contractRepo->findOneBy(
                    [
                        'product' => $product,
                        'office' => $requester->getOffice(),
                        'supplier' => $supplier,
                    ]
                );

                if (!$contract) {
                    $message = sprintf(
                        'Contrato con proveedor %s, producto %s no existe en la sucursal %s',
                        $requisitionData['supplier_code'],
                        $requisitionData['product_code'],
                        $requester->getOffice()->getName()
                    );
                    $report->addError($message);
                }

                /** @var Requisition $requisition */
                $requisition = $this->requisitionRepo->create();
                $requisition->setType($this->typeResolver->getType($product));
                $requisition->setProduct($product);
                $requisition->setContract($contract);
                $requisition->setStatus(ProductStatus::DRAFT);
                $requisition->setQuantity($requisitionData['quantity']);
                $requisition->setComments($requisitionData['comments']);
                $requisition->setRequester($requester->getUser());
                $requisition->setStateDueDate(new \DateTime());
                $requisition->setCreatedAt($requisitionData['created_at']);
                if (is_array($requisitionData['inventory_codes'])) {
                    $requisition->addMetadata('inventory_code', array_filter($requisitionData['inventory_codes']));
                }

                //impersonate user to start the workflow process
                $this->userHelper->impersonateUser($requester->getUser()->getUsername());

                $this->requisitionRepo->persist($requisition, true);
                $report->setAction(JobReport::CREATED);
                if (!$this->workflow->start($requisition)->getSuccessful()) {
                    $message = sprintf('Error al workflow en el paso %s', ProductStatus::DRAFT);
                    $report->addError($message);
                }

                if (!$this->workflow->proceed($requisition, 'create')) {
                    $message = sprintf('Error al workflow en el paso %s', ProductStatus::CREATED);
                    $report->addError($message);
                }
                $report->setAction(count($report->getErrors())?JobReport::SKIPPED:JobReport::CREATED);
            }
        }
        return $report;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function normalize(array $data)
    {
        return [
            'created_at' => \DateTime::createFromFormat('Y-m-d H:i:s', sprintf('%s %s', trim($data[0]), trim($data[1]))),
            'product_code' => trim($data[2]),
            'inventory_codes' => explode(',', trim($data[3])),
            'comments' => trim($data[4]),
            'supplier_rut' => trim($data[5]),
            'requester_rut' => trim($data[6]),
            'quantity' => trim($data[7]),
        ];
    }
}
