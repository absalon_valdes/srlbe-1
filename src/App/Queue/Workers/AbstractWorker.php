<?php

namespace App\Queue\Workers;

use App\Entity\Job;
use App\Queue\JobRepository;
use App\Queue\Worker;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractWorker
 */
class AbstractWorker implements Worker
{
    /**
     * @var LoggerInterface
     * @DI\Inject("logger")
     */
    public $logger;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var string
     */
    public $tempDir;

    const HEADERS = [];
    const HEADER_ERROR_MESSAGE = 'El formato de las celdas no coincide con el formato especificado.';

    /**
     * AbstractWorker constructor.
     * @param EntityManager $em
     * @param $cacheDir
     */
    public function __construct(EntityManager $em, $cacheDir)
    {
        $this->em = $em;
        $this->tempDir = sprintf('%s/importer', $cacheDir);

        if (!@mkdir($this->tempDir, 0777, true) && !is_dir($this->tempDir)) {
            throw new \RuntimeException(sprintf(
                'Cannot create folder %s', $this->tempDir
            ));
        }

        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    /**
     * @param Job $job
     */
    public function execute(Job $job)
    {
        if (!$job->isRetried()) {
            $this->update($job, Job::RUNNING);
            $job->setStartedAt(new \DateTime);
            $this->em->flush($job);
        }
        try {
            $this->process($job);
            $this->update($job, Job::COMPLETED);
        } catch(\Exception $e) {
            $this->logger->error($e->getMessage());
            $this->update($job, Job::FAILED);
        }
        $job->setFinishedAt(new \DateTime);
        $this->em->flush($job);
    }

    /**
     * @param Job $job
     * @param $report
     */
    public function report(Job $job, $report)
    {
        $this->em->flush($job->setReport($report));
    }

    /**
     * @param Job $job
     * @param $size
     */
    public function setSize(Job $job, $size)
    {
        $this->em->flush($job->setSize($size));
    }

    /**
     * @param Job $job
     * @param $state
     */
    private function update(Job $job, $state)
    {
        $this->em->flush($job->setState($state));
    }

    /**
     * @param Job $job
     * @return mixed
     */
    public function process(Job $job)
    {
    }

    /**
     * Advance progress
     * @param Job $job
     */
    protected function advance(Job $job)
    {
        $this->em->flush($job->stepProgress());
    }

    protected function hasCorrectHeaders($data) {
        return $data === $this::HEADERS? true: false;
    }

    protected function normalizeString($string) {
        $forbidden= ["á","é","í","ó","ú","Á","É","Í","Ó","Ú"];
        $allowed= ["a","e","i","o","u","A","E","I","O","U"];
        $string = str_replace($forbidden, $allowed ,$string);
        return strtoupper($string);
    }
}