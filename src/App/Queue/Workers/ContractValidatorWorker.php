<?php

namespace App\Queue\Workers;

use App\Entity\Contract;
use App\Entity\DateRange;
use App\Entity\Job;
use App\Entity\Money;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\SLA;
use App\Entity\Supplier;
use App\Helper\ExcelBatchProcessor;
use App\Model\ContractXLSValidate;
use App\Model\JobReport;
use App\Util\RutUtils;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\DiExtraBundle\Exception\InvalidAnnotationException;
use Recurr\Exception;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ContractValidatorWorker
 *
 * @DI\Service()
 */
class ContractValidatorWorker extends AbstractWorker
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * ContractImporterWorker constructor.
     *
     * @param EntityManager $em
     * @param $cacheDir
     * @param ValidatorInterface $validator
     *
     * @DI\InjectParams({
     *     "em"=@DI\Inject("doctrine.orm.entity_manager"),
     *     "cacheDir"=@DI\Inject("%kernel.var_dir%"),
     *     "validator"=@DI\Inject("validator")
     * })
     */
    public function __construct(EntityManager $em, $cacheDir, ValidatorInterface $validator)
    {
        parent::__construct($em, $cacheDir);
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     */
    public function process(Job $job)
    {
        $logfile = sprintf('%s/%s.log', $this->tempDir, $job->getId());
        $tempcsv = sprintf('%s/%s.csv', $this->tempDir, $job->getId());

        ExcelBatchProcessor::excelToCsv($job->getPayload('file'), $tempcsv);

        if (!file_exists($tempcsv) && !filesize($tempcsv)) {
            throw new \RuntimeException(sprintf('Cannot create file %s', $tempcsv));
        }

        $total = (int) exec(sprintf('wc -l < "%s"', $tempcsv));
        $this->setSize($job, $total);
        $this->report($job, $logfile);

        $cursor = 1;
        $logBatch = '';
        if ($handler = fopen($tempcsv, 'r')) {
            while (!feof($handler)) {
                if (!is_array($data = fgetcsv($handler))) {
                    break;
                }

                if (empty($data)) {
                    continue;
                }
                $report = JobReport::create($cursor++, $data);

                try {
                    $report = $this->processLine($cursor-1,$report);
                } catch (\Exception $e) {
                    $report->addError($e->getMessage());
                }

                if(count($report->getErrors()) > 0) {
                    $report->setAction(JobReport::VALIDATED);
                    $logBatch .= serialize($report).PHP_EOL;
                }

                $this->advance($job);

                if ($cursor % 25000) {
                    file_put_contents($logfile, $logBatch, FILE_APPEND);
                    $logBatch = '';
                }

                if ($cursor % 1000) {
                    $this->em->flush();
                }
                if(in_array($this::HEADER_ERROR_MESSAGE,$report->getErrors())) {
                    $job->setSize(1);
                    break;
                }
            }
            fclose($handler);
        }

        $this->report($job, $logfile);
    }

    /**
     * @param $line
     * @param JobReport $report
     * @return string
     * @throws Exception
     */
    private function processLine($line,JobReport $report)
    {
        if($line == 1) {
            if(!ContractXLSValidate::validateHeaders($report->getData())) {
                throw new Exception\InvalidArgument($this::HEADER_ERROR_MESSAGE);
            }
        }
        else {
            $contractObject = new ContractXLSValidate($report->getData());
            /** @var $errors ConstraintViolationList */
            $errors = $this->validator->validate($contractObject);
            $data = [];
            foreach ($errors as $err) {
                $data[] = ContractXLSValidate::NAME_PROPERTIES[$err->getPropertyPath()].": ".$err->getInvalidValue();
                $report->addError($err);
            }

            $report->setData($data);
        }
        return $report;
   }
}