<?php

namespace App\Export;

use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\PepGrant;
use App\Entity\User;
use App\Helper\SlaCalculator;
use App\Repository\PepGrantRepository;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class RequisitionExporter.
 *
 * @Di\Service("backoffice.pep.exporter")
 */
class PepBackOfficeExporter extends Exporter
{
    const TYPE = 'PEP';
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var PepGrantRepository $pepGrantRepo
     * @Di\Inject("pep_grant_repo")
     */
    public $pepGrantRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    public function __construct()
    {
        $this->fileName = sprintf('reporte-pep-%s.xls', time());
        $this->tabName = 'PEP';
        $this->title = 'Reporte de pep '.date('d-m-Y H:i:s');
    }

    protected function fetchData($type, $data)
    {
        $this->data = $data;
    }

    protected function fillRows()
    {

        $pepRequester = function (PepGrant $pep) {
            if ($requester = $pep->getRequester()->getEmployee()) {
                return $requester;
            }

            return $pep->getRequester();
        };

        $employeeOffice = function (Employee $employee = null) {
            if ($employee && ($office = $employee->getOffice())) {
                return $office;
            }

            return null;
        };
                 

        $rowCount = 2;
        /** @var PepGrant $pep */
        foreach ($this->data as $pep) {
            
            $requester = $pepRequester($pep);
            
            if ($requester instanceof User) {
                $name = $requester->getUsername();
                $requesterOffice = null; 
            } else {
                $name = $requester->getFullName();
                /** @var Office $requesterOffice */
                $requesterOffice = $employeeOffice($requester);
            }
            

            


            $this->report->getActiveSheet()
                ->setCellValue('A'.$rowCount, $pep->getCreatedAt()->format('d/m/Y'))
                ->setCellValue('B'.$rowCount, $pep->getFormNumber())
                ->setCellValue('C'.$rowCount, self::TYPE)
                ->setCellValue('D'.$rowCount, $pep->getDescription())
                ->setCellValue('E'.$rowCount, $this->translator->trans($pep->getStatus()))
                ->setCellValue('F'.$rowCount, $requesterOffice ? $requesterOffice->getName() : null)
                ->setCellValue('G'.$rowCount, $name)
            ;

            ++$rowCount;
        }

        foreach (range('A', 'G') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    /**
     * @throws \PHPExcel_Exception
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Fecha de ingreso')
            ->setCellValue('B1', 'Solicitud')
            ->setCellValue('C1', 'Tipo')
            ->setCellValue('D1', 'Descripción')
            ->setCellValue('E1', 'Estado')
            ->setCellValue('F1', 'Sucursal')
            ->setCellValue('G1', 'Usuario')
        ;
    }
}
