<?php

namespace App\Export;

use App\Entity\Category;
use App\Entity\Contract;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Entity\User;
use App\Helper\RequisitionTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Helper\UserHelper;
use App\Repository\RequisitionRepository;
use App\Dashboard\Query\Requisition\SupplierAll;
use App\Util\DateUtils;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SupplierRequisitionExporter.
 *
 * @Di\Service("supplier.requisition.exporter")
 */
class SupplierRequisitionExporter extends Exporter
{
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;
    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var SupplierAll
     * @Di\Inject("app.dashboard.query.merge.supplier_all")
     */
    public $querySupplier;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionsRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;
    
    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;
    
    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;
    
    /**
     * @var LoggerInterface
     * @Di\Inject("logger")
     */
    public $logger;

    private $possibleStatuses = [
        ProductStatus::CREATED,
        ProductStatus::ORDERED,
        ProductStatus::REJECTED,
        ProductStatus::APPEAL,
        ProductStatus::DENIED,
        ProductStatus::APPROVED,
        ProductStatus::ACCEPTED,
        ProductStatus::CONFIRMED,
        ProductStatus::DECLINED,
        ProductStatus::HOLD,
        ProductStatus::DELIVERED,
        ProductStatus::REVIEWED,
        ProductStatus::RETURNED,
        ProductStatus::EVALUATED,
        ProductStatus::CLOSED,
        ProductStatus::COMPLETED,
        ProductStatus::TIMEOUT
    ];

    protected function fetchData($type, $data)
    {
        $qb = $this->em->createQueryBuilder();
        $this->data = $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->join('r.contract', 'c')
            ->join('c.supplier', 's')
            ->where(':user member of s.representatives')
            ->andWhere($qb->expr()->notIn('r.status', [
                ProductStatus::CLOSED,
                ProductStatus::REJECTED,
                ProductStatus::APPEAL,
                ProductStatus::DENIED,
                ProductStatus::CLOSED_BY_APPROVER,
            ]))
            ->setParameter('user', $this->userHelper->getCurrentUser())
            ->getQuery()
            ->getResult()
        ;
            
        $this->fileName = sprintf('reporte-%s-%s.xls', 'general', time());
        $this->tabName = ucfirst('general');
        $this->title = sprintf('Reporte de ficha de %s (%s)', 'general', date('d-m-Y H:i:s'));

        $this->setColumnInfo();
        $this->setHeaderInfo();
    }

    protected function setColumnInfo(){
        $contractType = function (Contract $contract) {
            return $this->translator->trans(
                $contract->getCentralized() ?
                    Contract::TYPE_CENTRALIZED :
                    Contract::TYPE_DESCENTRALIZED
            );
        };

        $productCategory = function (Product $product) {
            if ($category = $product->getCategory()) {
                return $category->getName();
            }

            return null;
        };

        $productApprover = function (Product $product) {
            if (($approver = $product->getApprover()) &&
                ($employee = $approver->getEmployee())) {
                return $employee;
            }

            return null;
        };

        $employeeOffice = function (Employee $employee = null) {
            if ($employee && ($office = $employee->getOffice())) {
                return $office;
            }

            return null;
        };

        $requisitionState = function (Requisition $requisition, $state) {
            $states = $this->workflow->getHistory($requisition);
            foreach ($states as $s){
                $n = $s->getProcessName().".".$s->getStepName();
                if($n == $state){
                    return $s;
                }
            }
            return null;
        };

        $initStatus = function (Requisition $requisition) {
            $contract = $requisition->getContract();
            switch ($requisition->getType()) {
                case Requisition::TYPE_MAINTENANCE:
                    return ProductStatus::APPROVED;
                    break;
                case Requisition::TYPE_PRODUCT:
                    return $contract->getCentralized() ? ProductStatus::CREATED : ProductStatus::APPROVED;
                    break;
                case Requisition::TYPE_SELF_MANAGEMENT:
                    return $contract->getCentralized() ? ProductStatus::CREATED : ProductStatus::APPROVED;
                    break;
            }
            return null;
        };
    
        $checkNuevoFormato = function (Requisition $requisition) {
            if (!$requisition->hasMetadata('details')) {
                return false;
            }
        
            $detalles = $requisition->getMetadata('details');
        
            return isset($detalles['caracteristicas']);
        };
    
        $totalPorColor = function (Requisition $requisition, $color) use ($checkNuevoFormato) {
            if (!$checkNuevoFormato($requisition)) {
                return null;
            }
        
            $items = $requisition->getMetadata('details')['caracteristicas'];
        
            return array_reduce($items, function ($carry, $item) use ($color) {
                if (isset($item['color']) && $color === $item['color']) {
                    $carry += $item['cantidad'] ?? 0;
                }
            
                return $carry;
            }, 0);
        };
    
        $totalPorColorModelo = function (Requisition $requisition, $color, $modelo) use ($checkNuevoFormato) {
            if (!$checkNuevoFormato($requisition)) {
                return null;
            }
        
            $items = $requisition->getMetadata('details')['caracteristicas'];
        
            return array_reduce($items, function ($carry, $item) use ($color, $modelo) {
                if ((isset($item['color']) && $color === $item['color']) &&
                    (isset($item['modelo']) && $modelo === $item['modelo'])
                ) {
                    $carry += $item['cantidad'] ?? 0;
                }
            
                return $carry;
            }, 0);
        };

        $requisitionStatus = function (Requisition $requisition,$status = null) use ($contractType) {
            $type = $requisition->getType();
            $status = $status ? $status : $requisition->getStatus();
            $centr = $requisition->getContract()->getCentralized() ? Contract::TYPE_CENTRALIZED : Contract::TYPE_DESCENTRALIZED;

            return sprintf('%s.%s.%s', $status, str_replace('requisition.', '', $type), str_replace('type.', '', $centr));
        };

        $requisitionsData = [];

        /** @var Requisition $requisition */
        foreach ($this->data as $i => $requisition) {
            $requisitionData = [];

            $initialStatus = $initStatus($requisition);

            //Requisition hasn't been created yet (product.draft status)
            if(!$requisitionState($requisition,$initialStatus)){
                continue;
            }
            /** @var Contract $contract */
            $contract = $requisition->getContract();

            $states = $this->workflow->getHistory($requisition);

            /** @var Product $product */
            $product = $requisition->getProduct();

            /** @var User $user*/
            $user = $requisition->getRequester();

            /** @var Employee $requester */
            $requester = $user ? $user->getEmployee() : null;

            /** @var Office $requesterOffice */
            $requesterOffice = $employeeOffice($requester);

            $evaluation = $requisition->getMetadata('evaluation');

            /* @var \DateTime $startDate */
            $startDate = $requisitionState($requisition,$initialStatus)->getCreatedAt();

            /* @var \DateTime $estimatedSLA */
            $estimatedSLA = $this->slaCalculator->getEstimate(
                $startDate,
                $requisition->getSlaProduct(),
                $requisition->getSlaAccept(),
                $requisition->getSlaApprover(),
                $requisition->getSlaApproverEval(),
                $requisition->getSlaRequesterEval());

            $endedAt = null;
            //Support for legacy evaluation metadata format
            if ($eval = $requisition->getMetadata('evaluation')) {
                if (isset($eval['evaluation'])) {
                    $endedAt = $eval['evaluated_at'];
                } else {
                    $endedAt = end($evaluation)['evaluated_at'];
                }
            }

            if($requisition->getStatus() == ProductStatus::CLOSED){
                $endedAt = $requisition->getClosedAt();
            }


            $phone = function (Employee $e = null) {
                if($e){
                    $parts = explode('/', str_replace(['\\', '//', '-', ')'], '/', $e->getPhone()));
                    return trim($parts[count($parts) === 1 ? 0 : count($parts) - 1]);
                }
                return null;
            };

            $requisitionData[] = $this->translator->trans($requisition->getType());
            $requisitionData[] =  $requisition->getNumber();
            $requisitionData[] = $product->getCode();
            $requisitionData[] = $product->getName();
            $requisitionData[] = $requisition->getQuantity();
            $requisitionData[] = $requester && $requester ? $requester->getRut() : null;
            $requisitionData[] = $requester ? $requester->getFullName() : null;
            $requisitionData[] = $phone($requester);
            $requisitionData[] = $user ? $user->getEmail() : '';
            $requisitionData[] = $requesterOffice ? $requesterOffice->getAddress(): null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCity()->getName() : null;
            $requisitionData[] = $requesterOffice ? $this->regionToRoman($requesterOffice) : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCity()->getParent()->getName() : null;
            $requisitionData[] = $requester ? $requester->getPosition()->getTitle() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCode() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getName() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCostCenter() : null;
            $requisitionData[] = $requester ? $this->translator->trans($requesterOffice->getType()) : null;
            $requisitionData[] = $requisition->isClosed() ? 'CERRADO' : 'ABIERTO';
            $requisitionData[] = $this->translator->trans($requisitionStatus($requisition));
            $requisitionData[] = $startDate->format('d/m/Y H:i:s');//fecha creacion

            $acceptedInfo = $productInfo = $approvedInfo = $declined = null;
            foreach ($this->possibleStatuses as $status){
                $diff = null;
                $slaData = $this->filterStates($states, $status,$requisition->getSlas(),$endedAt);
                if($status == ProductStatus::ACCEPTED){
                    $acceptedInfo["createdAt"] = $slaData["createdAt"];
                    $acceptedInfo["sla"] = $slaData["sla"];
                }elseif($status == ProductStatus::DELIVERED){
                    $productInfo["createdAt"] = $slaData["createdAt"];
                    $productInfo["sla"] = $slaData["sla"];
                }elseif($status == ProductStatus::APPROVED){
                    $approvedInfo["createdAt"] = $slaData["createdAt"];
                    $approvedInfo["finishedAt"] = $slaData["finishedAt"];
                    $approvedInfo["sla"] = $slaData["sla"];
                }
            }

            /***** SLA ejecución proveedor ***/
            $estimadedExecution = $this->slaCalculator->getEstimate(
                $requisitionState($requisition,$initialStatus)->getCreatedAt(),
                $requisition->getSlaProduct(),
                $requisition->getSlaAccept(),
                $requisition->getSlaApprover());

            $daysSlaEstimated = $this->getDaysInFormat($estimadedExecution, $startDate);
            $inSla = $endedAt ? $estimatedSLA > $endedAt : $estimatedSLA > new \DateTime();
            $slaTimeout = $requisition->isClosed() ? DateUtils::dateIntervalFormat($startDate->diff($requisition->getClosedAt())) : DateUtils::dateIntervalFormat($estimadedExecution->diff((new \DateTime)));

            $requisitionData[] = $estimadedExecution->format('d/m/Y  H:i:s');
            $requisitionData[] = $endedAt ? $endedAt->format('d/m/Y H:i:s') : null;
            $requisitionData[] = $inSla ? 'DENTRO SLA' : 'FUERA SLA'; //calcular con esta fecha estimada
            $requisitionData[] = $inSla && !$requisition->isClosed() ? $this->getDaysInFormat($estimadedExecution) : 0;
            $requisitionData[] = $daysSlaEstimated;
            $requisitionData[] = !$requisition->isClosed() ? DateUtils::dateIntervalFormat($startDate->diff((new \DateTime))) : DateUtils::dateIntervalFormat($startDate->diff($endedAt));
            $requisitionData[] = !$inSla ? $slaTimeout : 0;

            $requisitionData[] = "SLA Ejecución del proveedor";
            $requisitionData[] = $this->addValues($productInfo,$approvedInfo);
            $requisitionData[] = $productInfo["sla"] ? $this->translator->trans($productInfo["sla"]->getType()) : null;

            $requisitionData[] = $this->translator->trans($requisition->getCreatedAt()->format('F'))." ".$requisition->getCreatedAt()->format('Y');//mes creacion
            $requisitionData[] = $requisition->getMetadata('work_order') ?? '-';
            $requisitionData[] = $totalPorColor($requisition, 'rojo');
            $requisitionData[] = $totalPorColor($requisition, 'gris');
            $requisitionData[] = $totalPorColorModelo($requisition, 'rojo', 'meson');
            $requisitionData[] = $totalPorColorModelo($requisition, 'gris', 'meson');
            $requisitionData[] = $totalPorColorModelo($requisition, 'rojo', 'cajero');
            $requisitionData[] = $totalPorColorModelo($requisition, 'gris', 'cajero');
            
            /***** SLA ejecución proveedor ***/
            $requisitionsData[] = $requisitionData;
        }
        $this->columns = $requisitionsData;

    }

    protected function setHeaderInfo(){
        $headerInfo = [];
        $headerInfo[] = 'Tipo';
        $headerInfo[] = 'N° solicitud';
        $headerInfo[] = 'Codigo técnico';
        $headerInfo[] = 'Producto';
        $headerInfo[] = 'Cantidad';
        $headerInfo[] = 'RUT Solicitante';
        $headerInfo[] = 'Solicitante';
        $headerInfo[] = 'Anexo';
        $headerInfo[] = 'Correo electrónico';
        $headerInfo[] = 'Dirección';
        $headerInfo[] = 'Comuna';
        $headerInfo[] = 'N° Región';
        $headerInfo[] = 'Región';
        $headerInfo[] = 'Cargo Solicitante';
        $headerInfo[] = 'Codigo Unidad';
        $headerInfo[] = 'Nombre Sucursal';
        $headerInfo[] = 'Centro de costo';
        $headerInfo[] = 'Tipo unidad';
        $headerInfo[] = 'Estado';
        $headerInfo[] = 'Estado en sistema';
        $headerInfo[] = 'Fecha de creación';
        $headerInfo[] = 'Fecha posible de entrega';
        $headerInfo[] = 'Fecha entrega real';
        $headerInfo[] = 'En SLA';
        $headerInfo[] = 'Días SLA restante';
        $headerInfo[] = 'SLA total esperado';
        $headerInfo[] = 'SLA total real';
        $headerInfo[] = 'Tpo Excedido respecto de SLA';

        $headerInfo[] = "Tipo SLA";
        $headerInfo[] = "SLA esperado";
        $headerInfo[] = "SLA unidad";
        $headerInfo[] = 'Mes creación';
        $headerInfo[] = 'Nro orden de trabajo';
        $headerInfo[] = 'Rojo';
        $headerInfo[] = 'Gris';
        $headerInfo[] = 'Mesón Rojo';
        $headerInfo[] = 'Mesón Gris';
        $headerInfo[] = 'Cajero Rojo';
        $headerInfo[] = 'Cajero Gris';
        
        $this->headers = $headerInfo;
    }

    protected function fillRows()
    {
        $this->fillCellValues($this->columns);
    }

    private function addValues($val1,$val2){
        $val1 = $val1["sla"] ? $val1["sla"]->getValue() : 0;
        $val2 = $val2["sla"] ? $val2["sla"]->getValue() : 0;
        return $val1 + $val2;
    }

    private function regionToRoman(Office $office)
    {
        if (!$city = $office->getCity()) {
            return null;
        }

        /** @var Category $region */
        if (!$region = $city->getParent()) {
            return null;
        }

        $regionToRoman = [
            'arica' => 'XV',
            'tarapaca' => 'I',
            'antofagasta' => 'II',
            'atacama' => 'III',
            'coquimbo' => 'IV',
            'valparaiso' => 'V',
            'libertador' => 'VI',
            'maule' => 'VII',
            'biobio' => 'VIII',
            'araucania' => 'IX',
            'rios' => 'XIV',
            'lagos' => 'X',
            'aisen' => 'XI',
            'magallanes' => 'XII',
            'metropolitana' => 'XIII',
        ];

        foreach ($regionToRoman as $slug => $no) {
            if ((bool) strstr($region->getSlug(), $slug)) {
                return $no;
            }
        }
        return null;
    }

    /**
     * @throws \PHPExcel_Exception
     */
    protected function fillColumnHeaders()
    {
        foreach ($this->headers as $key => $value){
            $this->report->setActiveSheetIndex(0)
                ->setCellValue($this->getNameFromNumber($key)."1", $value);
        }
    }

    private function fillCellValues($requisitionsData){
        $cRow = 2;
        foreach ($requisitionsData as $row){
            foreach ($row as $key => $value){
                $this->report->getActiveSheet()
                    ->setCellValue($this->getNameFromNumber($key).$cRow, $value);
            }
            ++$cRow;
        }
    }

    private function filterStates($states, $status,$contractSlas){
        $s = $result = null;
        $result = [
            "status" => $status,
            "createdAt" =>  null,
            "finishedAt" =>  null,
            "slaExpectedValue" =>  array_key_exists($status,$contractSlas) ? $contractSlas[$status]->getValue() : null,
            "slaExpectedType" =>  array_key_exists($status,$contractSlas) ? $contractSlas[$status]->getType() : null,
            "sla" => array_key_exists($status,$contractSlas) ? $contractSlas[$status] : null
        ];

        foreach ($states as $state){
            $stateN = $state->getProcessName().".".$state->getStepName();
            if($stateN != $status){
                continue;
            }
            $s[] = $state;
        }

        if(count($s) > 0 && $s !== null) {
            $result["createdAt"] =  $s[0]->getCreatedAt();
            $result["finishedAt"] =  $s[count($s) - 1]->getFinishedAt();
            $result["quantity"] =  count($s);
        }

        return $result;
    }

    private function getNameFromNumber($num) {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
}
