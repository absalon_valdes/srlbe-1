<?php

namespace App\Export;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class ExporterAggregator
 * @package App\Export
 * @DI\Service("exporter_aggregator")
 */
class ExporterAggregator
{
    /**
     * @var array|null
     */
    protected $exporters = [];

    /**
     * @inheritdoc
     */
    public function registerExporter($name, Exporter $exporter)
    {
        $this->exporters[$name] = $exporter;
    }

    /**
     * @inheritdoc
     */
    public function getExporter($name)
    {
        if (!isset($this->exporters[$name])) {
            throw new \InvalidArgumentException(sprintf(
                'Exporter "%s" not found in (%s)', $name, implode(', ', array_keys($this->exporters))
            ));
        }
        
        return $this->exporters[$name];
    }
}