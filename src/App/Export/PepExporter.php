<?php

namespace App\Export;

use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\PepGrant;
use App\Entity\Position;
use App\Helper\SlaCalculator;
use App\Model\Company;
use App\Model\Person;
use App\Repository\PepGrantRepository;
use App\Util\PhpExcelUtils;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class RequisitionExporter.
 *
 * @Di\Service("pep.exporter")
 */
class PepExporter extends Exporter
{
    const TYPE = 'PEP';
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var PepGrantRepository $pepGrantRepo
     * @Di\Inject("pep_grant_repo")
     */
    public $pepGrantRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;
    
    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    public function __construct()
    {
        $this->fileName = sprintf('reporte-pep-%s.xls', time());
        $this->tabName = 'PEP';
        $this->title = 'Reporte de pep '.date('d-m-Y H:i:s');
    }

    protected function fetchData($type, $data)
    {
        $this->em->getFilters()->disable('soft_deleteable');
        
        $this->data = $this->pepGrantRepo
            ->createQueryBuilder('p')
            ->leftJoin('p.requester', 'r')
            ->leftJoin('r.employee', 'e')
            ->where('e.deletedAt is null')
            ->andWhere('e.position is not null')
            ->getQuery()
            ->getResult()
        ;
    }

    protected function fillRows()
    {

        $pepRequester = function (PepGrant $pep) {
            if ($requester = $pep->getRequester()) {
                return $requester->getEmployee();
            }

            return null;
        };

        $employeeOffice = function (Employee $employee = null) {
            if ($employee && ($office = $employee->getOffice())) {
                return $office;
            }

            return null;
        };

        $rowCount = 2;
        /** @var PepGrant $pep */
        foreach ($this->data as $pep) {

            /** @var Employee $requester */
            $requester = $pepRequester($pep);

            if (is_null($requester)) {
                continue;
            }

            /** @var Position $position */
            $position = $requester->getPosition();

            /** @var Office $requesterOffice */
            $requesterOffice = $employeeOffice($requester);
            
            /**@var Company $selectedCompany */
            $selectedCompany = $pep->getSelectedCompany();


            $this->report->getActiveSheet()
                ->setCellValue('A'.$rowCount, $pep->getFormNumber())
                ->setCellValue('B'.$rowCount, self::TYPE)
                ->setCellValue('C'.$rowCount, $pep->getCreatedAt()->format('d/m/Y'))
                ->setCellValue('D'.$rowCount, $requester ? $requester->getRut() : null)
                ->setCellValue('E'.$rowCount, $requester ? $requester->getFullName() : null)
                ->setCellValue('F'.$rowCount, $position ? $position->getTitle() : null)
                ->setCellValue('G'.$rowCount, $requesterOffice ? $requesterOffice->getCode() : null)
                ->setCellValue('H'.$rowCount, $requesterOffice ? $requesterOffice->getName() : null)
                ->setCellValue('I'.$rowCount, $requesterOffice ? $requesterOffice->getCity()->getName() : null)
                ->setCellValue(
                    'J'.$rowCount,
                    $requesterOffice ? $requesterOffice->getCity()->getParent()->getName() : null
                )
                ->setCellValue('K'.$rowCount, $requesterOffice ? $requesterOffice->getCostCenter() : null)
                ->setCellValue('L'.$rowCount, $this->translator->trans($pep->getStatus()))
                ->setCellValue('M'.$rowCount, $requesterOffice->getAddress())
                ->setCellValue('N'.$rowCount, $this->translator->trans($requesterOffice->getType()));
            if ($selectedCompany) {
                $this->report->getActiveSheet()            
                    ->setCellValue('O'.$rowCount, $selectedCompany->hasPeps() ? 'Si' : 'No')
                    ->setCellValue('P'.$rowCount, $selectedCompany->getType() === Company::TYPE_PERSON ? 'Natural' : 'Juridica')
                    ->setCellValue('Q'.$rowCount, $selectedCompany->getRut())
                    ->setCellValue('R'.$rowCount, $selectedCompany->getName())
                ;
                
                if ($selectedCompany->getType() !== Company::TYPE_PERSON) {
                    $representatives = $selectedCompany->getRepresentatives();
                    $rowLetter = ord('S') - 65  ;
                    /** @var Person $representative */
                    foreach ($representatives as $representative) {
                        $this->renderInfo($rowLetter, $rowCount, $representative->getName(), 'Nombre Representante Legal');
                        $rowLetter++;

                        $this->renderInfo($rowLetter, $rowCount, $representative->getRut(), 'Rut Representante Legal');
                        $rowLetter++;

                        $this->renderInfo($rowLetter, $rowCount, $representative->isPep() ? 'PEP' : 'No es PEP', 'Estado PEP');
                        $rowLetter++;
                    }
                    $shareholders = $selectedCompany->getShareholders();
                    
                    /** @var Person $shareholder */
                    foreach ($shareholders as $shareholder) {
                        if (!$shareholder->getRut()) {
                            continue;
                        }
                        $this->renderInfo($rowLetter, $rowCount, $shareholder->getName(), 'Nombre Accionista');
                        $rowLetter++;

                        $this->renderInfo($rowLetter, $rowCount, $shareholder->getRut(), 'Rut Accionista');
                        $rowLetter++;

                        $this->renderInfo($rowLetter, $rowCount, $shareholder->isPep() ? 'PEP' : 'No es PEP', 'Estado PEP');
                        $rowLetter++;
                    }
                }
                
            }
            
            ++$rowCount;
        }

        foreach (range('A', 'AA') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }
    
    private function renderInfo($rowLetter, $rowCount, $info, $title) 
    {
        $letter = PhpExcelUtils::getNextLetterByNumber($rowLetter);
        $this->report->getActiveSheet()->setCellValue($letter .'1', $title);
        $this->report->getActiveSheet()->setCellValue($letter.$rowCount, $info);
    }

    /**
     * @throws \PHPExcel_Exception
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'N° solicitud')
            ->setCellValue('B1', 'Tipo')
            ->setCellValue('C1', 'Fecha de creación')
            ->setCellValue('D1', 'RUT Solicitante')
            ->setCellValue('E1', 'Solicitante')
            ->setCellValue('F1', 'Cargo Solicitante')
            ->setCellValue('G1', 'Codigo Unidad')
            ->setCellValue('H1', 'Nombre Sucursal')
            ->setCellValue('I1', 'Región')
            ->setCellValue('J1', 'Comuna')
            ->setCellValue('K1', 'Centro de costo')
            ->setCellValue('L1', 'Estado en sistema')
            ->setCellValue('M1', 'Dirección unidad demandante')
            ->setCellValue('N1', 'Tipo unidad')
            ->setCellValue('O1', 'Estado PEP Proveedor adjudicado')
            ->setCellValue('P1', 'Tipo Provedor Adjudicado')
            ->setCellValue('Q1', 'Rut Provedor Adjudicado')
            ->setCellValue('R1', 'Nombre Provedor Adjudicado')
            ->setCellValue('S1', 'Tipo Provedor Adjudicado')
        ;
    }
}
