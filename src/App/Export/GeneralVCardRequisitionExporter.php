<?php

namespace App\Export;

use App\Entity\Category;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Entity\SLA;
use App\Model\VcardRequisition;
use App\Entity\User;
use App\Helper\RequisitionTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Repository\RequisitionRepository;
use App\Util\DateUtils;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\PersistentCollection;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use JMS\Serializer\Serializer;

/**
 * Class GeneralVCardRequisitionExporter.
 *
 * @Di\Service("general.vcard.requisition.exporter")
 */
class GeneralVCardRequisitionExporter extends Exporter
{
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var string
     * @DI\Inject("%kernel.cache_dir%")
     */
    public $cacheDir;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionsRepo;

    /**
     * @var RequisitionRepository
     * @Di\Inject("category_repo")
     */
    public $categoryRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;
    /**
     * @var Container
     * @Di\Inject("service_container")
     */
    public $container;

    /**
     */
    public $serializer;

    private $possibleStatuses = [
        'evaluacion'           => ['enviar_a_evaluacion','en_evaluacion'],
        'cerrado'              => ['en_apelacion_cerrada'],
        'aceptado'             => ['recibir_despacho'],
        'rechazado'            => ['en_rechazo'],
        'denegado'             => ['en_apelacion_rechazada'],
        'apelacion'            => ['en_apelacion'],
        'ejecucion'            => ['enviar_a_ejecucion'],
        'en_despacho'          => ['despachar_pedido'],
        'solicitud_evaluada'   => ['evaluar_entrega'],
        'proveedor_calificado' => ['calificar_proveedor'],
        //'solicitud_completa'   => ['finalizar_solicitud']
    ];

    protected function fetchData($type, $data)
    {
        $this->data = $this->requisitionsRepo->findBy([
            'type' => Requisition::TYPE_VCARD],
            ['updatedAt' => 'DESC']
        );

        $this->fileName = sprintf('reporte-%s-%s.csv', 'general-tarjetas', date('dmYHi'));
        $this->tabName = ucfirst('general');
        $this->title = sprintf('Reporte de ficha de %s (%s)', 'general-tarjetas', date('d-m-Y H:i:s'));

        $this->setColumnInfo();
        $this->setHeaderInfo();
    }

    public function getResponse()
    {
        $tempDir = sprintf('%s/vcard_report', $this->cacheDir);

        if (!@mkdir($tempDir, 0777, true) && !is_dir($tempDir)) {
            throw new \RuntimeException(sprintf('Cannot create folder %s', $tempDir));
        }

        $file = new \SplFileObject($tempDir.'/'.$this->fileName, 'w');
        $file->fputcsv($this->headers);

        foreach ($this->columns as $column) {
            $file->fputcsv($column);
        }

        $response = new BinaryFileResponse($file);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Pragma', 'private');
        $response->headers->set('Cache-Control', 'maxage=-1');
        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->fileName
        ));

        return $response;
    }

    protected function setColumnInfo()
    {
        $phone = function (Employee $e = null) {
            if($e){
                $parts = explode('/', str_replace(['\\', '//', '-', ')'], '/', $e->getPhone()));
                return trim($parts[count($parts) === 1 ? 0 : count($parts) - 1]);
            }
            return null;
        };

        $this->serializer = $this->container->get('jms_serializer');

        $requisitionsData = [];
        /** @var Requisition $requisition */
        foreach ($this->data as $i => $requisition) {

            /**@var PersistentCollection $states**/
            $states = $this->getOrderedStates($requisition->getActivities());
            $requisitionData = [];

            /**@var VcardRequisition $vcards**/
            $vcards = $this->serializer->deserialize($requisition->getMetadata('vcards'), VcardRequisition::class, 'json');

            $addressOffice = $vcards->getAddressOffice() ?: $vcards->getOffice();

            if ($addressOffice) {
                if ($realCity = $this->categoryRepo->findOneByName($addressOffice->getCity()->getName())) {
                    $addressOffice->setCity($realCity);
                }
                $address = $addressOffice->getAddress();
                $addressDetail = $vcards->getAddressDetail();
                $addressCity = $addressOffice->getCity()->getName();
                $addressFull = $address.($addressDetail ? ', '.$addressDetail : '').', '.$addressCity;
            } else {
                $addressFull = '';
            }

            $timeStamps = $requisition->getMetadata('timestamps');
            $certify = json_decode($requisition->getMetadata('certificacion') ?? "{}",true);

            /** @var Product $product */
            $product = $requisition->getProduct();

            /** @var User $user */
            $user = $requisition->getRequester();

            /** @var Employee $requester */
            $requester = $vcards->getEmployee();

            /** @var Office $requesterOffice */
            $requesterOffice = $vcards->getOffice();

            /* @var \DateTime $startDate */
            $startDate = $timeStamps["requester"]["started"];

            $endedAt = null;
            //Support for legacy evaluation metadata format
            if ($eval = $requisition->getMetadata('evaluation')) {
                if (isset($eval['evaluation'])) {
                    $endedAt = $eval['evaluated_at'];
                } else {
                    $endedAt = end($eval)['evaluated_at'];
                }
            }

            if ($requisition->getStatus() == 'solicitud_completa' && array_key_exists('delivered_at',$certify)) {
                $endedAt = new \DateTime($certify["delivered_at"]);
            }

            if (in_array($requisition->getStatus(), ['cerrado','rechazado'])) {
                $declined = $states[count($states)-1];
                $endedAt = $declined->getCreatedAt();
            }

            $requisitionData[] = $this->translator->trans($requisition->getType());
            $requisitionData[] = $requisition->getNumber();
            $requisitionData[] = $product->getCode();
            $requisitionData[] = $product->getName();
            $requisitionData[] = $requisition->getQuantity();
            $requisitionData[] = $requester && $requester ? $requester->getRut() : null;
            $requisitionData[] = $requester ? $requester->getFullName() : null;
            $requisitionData[] = $phone($requester);
            $requisitionData[] = $user ? $user->getEmail() : '';

            $requisitionData[] = $addressFull;
            $requisitionData[] = $addressOffice ? $addressOffice->getCity()->getName() : null;
            $requisitionData[] = $addressOffice ? $this->regionToRoman($addressOffice) : null;
            $requisitionData[] = $addressOffice ? ($addressOffice->getCity() ? ($addressOffice->getCity()->getParent() ? $addressOffice->getCity()->getParent()->getName() : null) : null) : null;

            $requisitionData[] = $requester ? ($requester->getPosition() ? $requester->getPosition()->getTitle(): null) : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCode() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getName() : null;
            $requisitionData[] = $vcards->getCostCenter();

            $requisitionData[] = $vcards->isShowOffice() ? 'Si':'No';
            $requisitionData[] = $vcards->isShowAddress() ? 'Si':'No';

            $requisitionData[] = $requester ? $this->translator->trans($requesterOffice->getType()) : null;
            $requisitionData[] = $endedAt ? 'CERRADO' : 'ABIERTO';
            $requisitionData[] = $this->translator->trans(str_replace('requisition',$requisition->getStatus(),$requisition->getType()).'.centralized');
            $requisitionData[] = $startDate->format('d/m/Y H:i:s');//fecha creacion

            $estimadedExecution = $this->slaCalculator->getEstimate(
                $startDate,
                $requisition->getContract()->getSLA(ProductStatus::CREATED),
                $requisition->getContract()->getSLA(ProductStatus::DELIVERED),
                $requisition->getContract()->getSLA('en_despacho')
                );

            $daysSlaEstimated = $this->getDaysInFormat($estimadedExecution, $startDate);
            $inSla = $endedAt ? $estimadedExecution > $endedAt : $estimadedExecution > new \DateTime();
            $slaTimeout = $endedAt ? DateUtils::dateIntervalFormat($startDate->diff($endedAt)) : DateUtils::dateIntervalFormat($estimadedExecution->diff((new \DateTime)));

            $requisitionData[] = $estimadedExecution->format('d/m/Y H:i:s');
            $requisitionData[] = $endedAt ? $endedAt->format('d/m/Y H:i:s') : null;
            $requisitionData[] = $inSla ? 'DENTRO SLA' : 'FUERA SLA';
            $requisitionData[] = $inSla && !$endedAt ? $this->getDaysInFormat($estimadedExecution) : 0;
            $requisitionData[] = $daysSlaEstimated;
            $requisitionData[] = !$endedAt ? DateUtils::dateIntervalFormat($startDate->diff((new \DateTime))) : DateUtils::dateIntervalFormat($startDate->diff($endedAt));
            $requisitionData[] = !$inSla ? $slaTimeout : 0;

            foreach ($this->possibleStatuses as $status => $transitions) {
                $expectedSLA   = $this->getCorrectSLA($status, $requisition);
                $key           = $this->getActivity($states, $status);
                $stateActivity = $key !== null ? $states[$key] : null;
                $nextState     = $key !== null ? $this->getNext($key, $states) : null;
                $estimatedSLA  = $stateActivity ? $this->slaCalculator->getEstimate(
                    $stateActivity->getStartedAt(),
                    $expectedSLA) : null;
                $inSlaState = $nextState ? $estimatedSLA > $nextState->getStartedAt() : $estimatedSLA > new \DateTime();
                $endStateDate = $nextState ? $nextState->getStartedAt() : new \DateTime();

                $requisitionData[] = 'SLA ' . $this->translator->trans(str_replace('requisition', $status, $requisition->getType()) . '.centralized'); //tipo
                //valor
                $requisitionData[] = $expectedSLA ? $expectedSLA->getValue() : null;
                //unidad
                $requisitionData[] = $expectedSLA ? $this->translator->trans($expectedSLA->getType()) : null;
                //sla real
                $requisitionData[] = $stateActivity ? DateUtils::dateIntervalFormat($stateActivity->getStartedAt()->diff($endStateDate)) : null;
                //inicio
                $requisitionData[] = $stateActivity ? $stateActivity->getStartedAt()->format('d/m/Y H:i:s') : null;
                //fin
                $requisitionData[] = $nextState ? $nextState->getStartedAt()->format('d/m/Y H:i:s') : null;
                //exceso
                $requisitionData[] =  !$inSlaState ?  !$stateActivity ? null : DateUtils::dateIntervalFormat($stateActivity->getStartedAt()->diff($endStateDate)) : null;
            }
            $requisitionData[]  = $this->translator->trans($requisition->getCreatedAt()->format('F')) . " " . $requisition->getCreatedAt()->format('Y');//mes creacion
            $requisitionsData[] = array_map('utf8_decode', $requisitionData);
        }
        $this->columns = $requisitionsData;

    }

    protected function setHeaderInfo()
    {
        $headerInfo = [];
        $headerInfo[] = 'Tipo';
        $headerInfo[] = 'N° solicitud';
        $headerInfo[] = 'Codigo técnico';
        $headerInfo[] = 'Producto';
        $headerInfo[] = 'Cantidad';
        $headerInfo[] = 'RUT Solicitante';
        $headerInfo[] = 'Solicitante';
        $headerInfo[] = 'Anexo';
        $headerInfo[] = 'Correo electrónico';
        $headerInfo[] = 'Dirección';
        $headerInfo[] = 'Comuna';
        $headerInfo[] = 'N° Región';
        $headerInfo[] = 'Región';
        $headerInfo[] = 'Cargo Solicitante';
        $headerInfo[] = 'Codigo Unidad';
        $headerInfo[] = 'Nombre Sucursal';
        $headerInfo[] = 'Centro de costo';
        $headerInfo[] = 'Mostrar dependencia';
        $headerInfo[] = 'Mostrar dirección';
        $headerInfo[] = 'Tipo unidad';
        $headerInfo[] = 'Estado';
        $headerInfo[] = 'Estado en sistema';
        $headerInfo[] = 'Fecha de creación';
        $headerInfo[] = 'Fecha posible de entrega';
        $headerInfo[] = 'Fecha entrega real';
        $headerInfo[] = 'En SLA';
        $headerInfo[] = 'Días SLA restante';
        $headerInfo[] = 'SLA total esperado';
        $headerInfo[] = 'SLA total real';
        $headerInfo[] = 'Tpo Excedido respecto de SLA';

        foreach ($this->possibleStatuses as $status){
            $headerInfo[] = "Tipo SLA";
            $headerInfo[] = "SLA esperado";
            $headerInfo[] = "SLA unidad";
            $headerInfo[] = "SLA real";
            $headerInfo[] = "SLA fecha inicio";
            $headerInfo[] = "SLA fecha fin";
            $headerInfo[] = 'Tpo Excedido respecto de SLA';
        }
        $headerInfo[] = 'Mes creación';
        $this->headers = array_map('utf8_decode', $headerInfo);
    }

    protected function fillRows()
    {
        $this->fillCellValues($this->columns);
    }

    /**
     * @param SLA[] $slas
     * @return integer
     */
    private function addValues(...$slas)
    {
        $result = 0;
        foreach (array_filter($slas) as $sla) {
            if (!$sla instanceof SLA) {
                continue;
            }
            $result += $sla->getValue();
        }
        return $result;
    }

    private function regionToRoman(Office $office)
    {
        if (!$city = $office->getCity()) {
            return null;
        }

        /** @var Category $region */
        if (!$region = $city->getParent()) {
            return null;
        }

        $regionToRoman = [
            'arica' => 'XV',
            'tarapaca' => 'I',
            'antofagasta' => 'II',
            'atacama' => 'III',
            'coquimbo' => 'IV',
            'valparaiso' => 'V',
            'libertador' => 'VI',
            'maule' => 'VII',
            'biobio' => 'VIII',
            'araucania' => 'IX',
            'rios' => 'XIV',
            'lagos' => 'X',
            'aisen' => 'XI',
            'magallanes' => 'XII',
            'metropolitana' => 'XIII',
        ];

        foreach ($regionToRoman as $slug => $no) {
            if ((bool)strstr($region->getSlug(), $slug)) {
                return $no;
            }
        }
        return null;
    }

    /**
     * @throws \PHPExcel_Exception
     */
    protected function fillColumnHeaders()
    {
        foreach ($this->headers as $key => $value) {
            $this->report->setActiveSheetIndex(0)
                ->setCellValue($this->getNameFromNumber($key) . "1", $value);
        }
    }

    private function fillCellValues($requisitionsData)
    {
        $cRow = 2;
        foreach ($requisitionsData as $row) {
            foreach ($row as $key => $value) {
                $this->report->getActiveSheet()
                    ->setCellValue($this->getNameFromNumber($key) . $cRow, $value);
            }
            ++$cRow;
        }
    }

    private function filterStates($states, $status, $contractSlas)
    {
        $s = $result = null;
        $result = [
            "status" => $status,
            "createdAt" => null,
            "finishedAt" => null,
            "slaExpectedValue" => array_key_exists($status, $contractSlas) ? $contractSlas[$status]->getValue() : null,
            "slaExpectedType" => array_key_exists($status, $contractSlas) ? $contractSlas[$status]->getType() : null,
            "sla" => array_key_exists($status, $contractSlas) ? $contractSlas[$status] : null
        ];

        foreach ($states as $state) {
            $stateN = $state->getProcessName() . "." . $state->getStepName();
            if ($stateN != $status) {
                continue;
            }
            $s[] = $state;
        }

        if (count($s) > 0 && $s !== null) {
            $result["createdAt"] = $s[0]->getCreatedAt();
            $result["finishedAt"] = $s[count($s) - 1]->getFinishedAt();
            $result["quantity"] = count($s);
        }

        return $result;
    }

    private function getNameFromNumber($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }

    private function getActivity($activities, $status){
        foreach ($activities as $key => $activity){
            if( in_array( $activity->getName(), $this->possibleStatuses[$status], true ) ){
                return $key;
            }
        }
        return null;
    }

    private function getCorrectSLA($status,Requisition $requisition){
        switch ($status) {
            case "evaluacion":
                return $requisition->getContract()->getSla(ProductStatus::CREATED);
                break;
            case "ejecucion":
                return $requisition->getContract()->getSla(ProductStatus::DELIVERED);
                break;
            case "en_despacho":
                return $requisition->getContract()->getSla('en_despacho');
                break;
            default:
                return null;
        }
    }

    private function getOrderedStates($states){
        $iterator = $states->getIterator();
        $iterator->uasort(function ($a, $b) {
            return ($a->getStartedAt() < $b->getStartedAt()) ? -1 : 1;
        });
        return $this->deleteRepeated(array_values(iterator_to_array($iterator)));
    }

    private function getPrevious($key,$states){
        if($key == 0){
            return;
        }
        return $states[$key - 1];
    }

    private function getNext($key,$states){
        if($key == (count($states) - 1) ){
            return;
        }
        return $states[$key + 1];
    }

    private function deleteRepeated($states){
        $result = $states;
        foreach ($states as $key => $state){
            if($key == 0){
                continue;
            }
            if($state->getName() == $states[$key -1]->getName()){
                unset($result[$key]);
            }
        }
        return array_values($result);
    }
}
