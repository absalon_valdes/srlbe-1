<?php
/**
 * Created by PhpStorm.
 * User: steven
 * Date: 30-12-16
 * Time: 10:57
 */

namespace App\Export;

use App\Entity\Contract;
use App\Entity\Requisition;
use App\Repository\RequisitionRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class VcardExporter
 * @package App\Export
 * @Di\Service("vcard.exporter")
 */
class VcardExporter extends Exporter
{
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisition;

    public function __construct()
    {
        $this->fileName = sprintf('reporte-tarjeta-%s.xls', time());
        $this->tabName = 'Datos tarjeta';
        $this->title = 'Reporte de tarjetas '.date('d-m-Y H:i:s');
    }

    protected function fetchData($type, $data)
    {
        /** @var EntityManager data */
        $this->data = $this->requisition->findBy([
            'type' => Requisition::TYPE_VCARD
        ]);
    }

    protected function fillRows()
    {
        $contracts = function (Requisition $requisition) {
            if ($contract = $requisition->getMetadata()) {
                return $contract['vcards'];
            }
            return null;
        };

        $rowCount = 2;

        if (!empty($this->data)) {

            /** @var Requisition $d */
            foreach ($this->data as $d) {

                $contract = $contracts($d);

                $this->report->getActiveSheet()
                    ->setCellValue('A' . $rowCount, $contract['name'])
                    ->setCellValue('B' . $rowCount, $contract['charge'])
                    ->setCellValue('C' . $rowCount, $contract['office'])
                    ->setCellValue('D' . $rowCount, $contract['address'])
                    ->setCellValue('E' . $rowCount, '')
                    ->setCellValue('F' . $rowCount, $contract['code_area_phone'])
                    ->setCellValue('G' . $rowCount, $contract['telephone'])
                    ->setCellValue('H' . $rowCount, $contract['fax'])
                    ->setCellValue('I' . $rowCount, $contract['cell_phone'])
                    ->setCellValue('J' . $rowCount, $contract['email'])
                    ->setCellValue('K' . $rowCount, '')
                    ->setCellValue('L' . $rowCount, $contract['quantity'])
                    ->setCellValue('M' . $rowCount, $contract['observations'])
                    ->setCellValue('N' . $rowCount, $contract['managament_unit'])
                    ->setCellValue('O' . $rowCount, $contract['name_authorizes'])
                    ->setCellValue('P' . $rowCount, '')
                    ->setCellValue('Q' . $rowCount, '');

                ++$rowCount;
            }

            foreach (range('A', 'AA') as $columnID) {
                $this->report
                    ->getActiveSheet()
                    ->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
        }
    }

    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Nombre')
            ->setCellValue('B1', 'Cargo')
            ->setCellValue('C1', 'Dependencia')
            ->setCellValue('D1', 'Dirección')
            ->setCellValue('E1', 'Codigo de país')
            ->setCellValue('F1', 'Codigo de área')
            ->setCellValue('G1', 'Teléfono')
            ->setCellValue('H1', 'Fax')
            ->setCellValue('I1', 'Celular')
            ->setCellValue('J1', 'Email')
            ->setCellValue('K1', 'Sitio')
            ->setCellValue('L1', 'Cantidad')
            ->setCellValue('M1', 'Observaciones')
            ->setCellValue('N1', 'Unidad de gestión')
            ->setCellValue('O1', 'Nombre de jefe')
            ->setCellValue('P1', 'Login')
            ->setCellValue('Q1', 'Fecha');
    }
}