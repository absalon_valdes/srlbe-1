<?php

namespace App\Export;

use App\Entity\Supplier;
use App\Repository\SupplierRepository;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class SupplierDatabaseExporter
 * @package App\Export
 * @DI\Service("supplier_exporter")
 * @DI\Tag("exporter", attributes={"alias"="suppliers"})
 */
class SupplierDatabaseExporter extends Exporter
{
    /**
     * @var SupplierRepository
     * @DI\Inject("supplier_repo")
     */
    public $supplierRepo;
    
    /**
     * SupplierDatabaseExporter constructor.
     */
    public function __construct()
    {
        $this->fileName = sprintf('base-datos-proveedores-%s.xls', time());
        $this->tabName = 'Proveedores';
        $this->title = 'Base de datos proveedores '.date('d-m-Y H:i:s');
    }

    /**
     * @inheritdoc
     */
    protected function fetchData($type, $data)
    {
        $this->data = $this->supplierRepo->findAll();
    }

    /**
     * @inheritdoc
     */
    protected function fillRows()
    {
        $index = 2;

        /** @var Supplier $supplier */
        foreach ($this->data as $supplier) {
            $this->report->getActiveSheet()
                ->setCellValue('A'.$index, $supplier->getRut()) 
                ->setCellValue('B'.$index, $supplier->getName())
                ->setCellValue('C'.$index, implode(',',$supplier->getRepresentatives()->toArray()))
                ->setCellValue('D'.$index, $supplier->getExcerpt())
                ->setCellValue('E'.$index, $supplier->getAddress())
                ->setCellValue('F'.$index, $supplier->getPhone())
                ->setCellValue('G'.$index, $supplier->getWebsite())
                ->setCellValue('H'.$index, $supplier->getEmail())
                ->setCellValue('I'.$index, $supplier->getCity())
            ;
                
            ++$index;
        }
        
        foreach (range('A', 'I') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    /**
     * @inheritdoc
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Rut')
            ->setCellValue('B1', 'Razón Social')
            ->setCellValue('C1', 'Representantes')
            ->setCellValue('D1', 'Descripción')
            ->setCellValue('E1', 'Dirección')
            ->setCellValue('F1', 'Teléfono')
            ->setCellValue('G1', 'Pagina Web')
            ->setCellValue('H1', 'E-mail')
            ->setCellValue('I1', 'Comuna')
        ;
    }
}