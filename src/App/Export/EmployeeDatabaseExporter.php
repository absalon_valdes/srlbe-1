<?php

namespace App\Export;

use App\Entity\Employee;
use App\Repository\EmployeeRepository;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class EmployeeDatabaseExporter
 * @package App\Export
 * @DI\Service("employee_exporter")
 * @DI\Tag("exporter", attributes={"alias"="employees"})
 */
class EmployeeDatabaseExporter extends Exporter
{
    /**
     * @var EmployeeRepository
     * @DI\Inject("employee_repo")
     */
    public $employeeRepo;

    /**
     * SupplierDatabaseExporter constructor.
     */
    public function __construct()
    {
        $this->fileName = sprintf('base-datos-funcionarios-%s.xls', time());
        $this->tabName = 'Funcionarios';
        $this->title = 'Base de datos funcionarios '.date('d-m-Y H:i:s');
    }

    /**
     * @inheritdoc
     */
    protected function fetchData($type, $data)
    {
        $this->data = $this->employeeRepo->findAll();
    }

    /**
     * @inheritdoc
     */
    protected function fillRows()
    {
        $index = 2;

        /** @var Employee $employee */
        foreach ($this->data as $employee) {
            $this->report->getActiveSheet()
                ->setCellValue('A'.$index, $employee->getRut())
                ->setCellValue('B'.$index, $employee->getName())
                ->setCellValue('C'.$index, $employee->getLastName())
                ->setCellValue('D'.$index, $employee->getSecondLastName())
                ->setCellValue('E'.$index, $employee->getPhone())
                ->setCellValue('F'.$index, $employee->getPosition()?$employee->getPosition()->getTitle():'')
                ->setCellValue('G'.$index, $employee->getOffice())
                ->setCellValue('H'.$index, $employee->getUser())
                ->setCellValue('I'.$index, $employee->getGender())
            ;

            ++$index;
        }

        foreach (range('A', 'H') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    /**
     * @inheritdoc
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'RUT')
            ->setCellValue('B1', 'Nombre')
            ->setCellValue('C1', 'Apellido Paterno')
            ->setCellValue('D1', 'Apellido Materno')
            ->setCellValue('E1', 'Teléfono')
            ->setCellValue('F1', 'Puesto')
            ->setCellValue('G1', 'Sucursales')
            ->setCellValue('H1', 'Usuario')
            ->setCellValue('I1', 'Sexo')
        ;
    }
}