<?php

namespace App\Export;

use Behat\Transliterator\Transliterator;
use Liuggio\ExcelBundle\Factory;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class Exporter
 * @package App\Export
 */
abstract class Exporter
{

    /**
     * @var Factory
     * @Di\Inject("phpexcel")
     */
    public $excelFactory;

    /**
     * @var \PHPExcel
     */
    protected $report;

    protected $data;

    protected $title;
    protected $tabName;
    public $fileName;

    protected $columns = array();
    protected $headers = array();

    /**
     * @param $type
     * @param $data
     * @return mixed
     * @internal param type $string
     */
    abstract protected function fetchData($type, $data);

    abstract protected function fillRows();

    abstract protected function fillColumnHeaders();

    /**
     * @param null $type
     * @param $data
     * @return RequisitionExporter
     */
    public function export($type = null, $data = null)
    {
        $this->fetchData($type, $data);
        $this->generateExcel();
        return $this;
    }

    public function getResponse()
    {
        $writer = $this->excelFactory->createWriter($this->report, 'Excel5');

        $response = $this->excelFactory->createStreamedResponse($writer);
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
//        $response->headers->set('Pragma', 'private');
//        $response->headers->set('Cache-Control', 'maxage=-1');
        $response->setSharedMaxAge(60 * 15);
        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->fileName
        ));

        return $response;
    }

    protected function generateExcel()
    {
        $this->report = $this->excelFactory->createPHPExcelObject();

        $this->report
            ->getProperties()
            ->setCreator('Sistema de Requerimientos Logísticos BancoEstado')
            ->setTitle($this->title)
        ;

        $this->fillColumnHeaders();
        $this->fillRows();

        $this->report->getActiveSheet()->setTitle($this->tabName);
        $this->report->setActiveSheetIndex(0);
    }

    /**
     * @param $filepath
     */
    public function getFile($filepath)
    {
        $writer = $this->excelFactory->createWriter($this->report, 'Excel5');
        $writer->save($filepath);
    }

    protected function getDaysInFormat($to, $since = null, $format = '%ad %Hh %Im')
    {
        if (is_null($to)) {
            return null;
        }

        $toClone = clone $to;
        
        $since = !$since ? new \DateTime() : $since;
        $sign = -1 * (int) $toClone->diff($since)->format('%r%a') >= 0 ? '' : '-';
        
        return sprintf('%s'.$to->diff($since)->format($format), $sign);
    }

    public function toArray(){
        $result = array();
        $cleanHeaders  = function ($headers) {
            $h = $headers;
            $return = [];
            foreach ($h as $value){
                $return[] = $this->clean($value);
            }
            return $return;
        };

        $cleanHeaders = $cleanHeaders($this->headers);
        foreach ($this->columns as $row){
            $result[] = array_combine($cleanHeaders,$row);
        }
        return $result;
    }

    protected function clean($string) 
    {
        return Transliterator::urlize($string, '_');
    }
}
