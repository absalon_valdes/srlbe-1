<?php

namespace App\Export;

use App\Entity\Contract;
use App\Repository\ContractRepository;
use App\Repository\OfficeRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ContractDatabaseExporter
 * @package App\Export
 * @DI\Service("contract_exporter")
 * @DI\Tag("exporter", attributes={"alias"="contracts"})
 */
class ContractDatabaseExporter extends Exporter
{
    /**
     * @var ContractRepository
     * @DI\Inject("contract_repo")
     */
    public $contractRepo;

    /**
     * @var OfficeRepository
     * @DI\Inject("office_repo")
     */
    public $officeRepo;

    /**
     * @var TranslatorInterface
     * @DI\Inject("translator")
     */
    public $translator;

    /**
     * @var RequestStack
     * @DI\Inject("request_stack")
     */
    public $requestStack;
    
    /**
     * ContractDatabaseExporter constructor.
     */
    public function __construct()
    {
        $this->fileName = sprintf('base-catalogos-%s.xls', time());
        $this->tabName = 'Catálogos';
        $this->title = 'Base de datos de catálogos'.date('d-m-Y H:i:s');
    }

    /**
     * @inheritdoc
     */
    protected function fetchData($type, $data)
    {
        $criteria = null;

        /** @var Request $request */
        if ($request = $this->requestStack->getCurrentRequest()) {
            if ($officeId = $request->query->get('office_id', null)) {
                $criteria = explode(',', $officeId);
            }
        }

        $this->data = $this->contractRepo->findOffices(array_filter($criteria));
    }

    /**
     * @inheritdoc
     */
    protected function fillRows()
    {
        $index = 2;

        /** @var Contract $contract */
        foreach ($this->data as $contract) {

            if (!$contract->getProduct()) {
                continue;
            }

            $expired = $contract->getValidity()->getEnd() < new \DateTime;

            $this->report->getActiveSheet()
                ->setCellValue('A'.$index, $contract->getProduct()->getCode())
                ->setCellValue('B'.$index, $contract->getOffice()->getCode())
                ->setCellValue('C'.$index, $contract->getOffice()->getName())
                ->setCellValue('D'.$index, $contract->getOffice()->getCostCenter())
                ->setCellValue('E'.$index, $contract->getSupplier()->getRut())
                ->setCellValue('F'.$index, $contract->getValidity()->getStart()->format('d/m/Y'))
                ->setCellValue('G'.$index, $contract->getValidity()->getEnd()->format('d/m/Y'))
                ->setCellValue('H'.$index, $expired ? 'Si' : 'No')
                ->setCellValue('I'.$index, $contract->getContractNumber())
                ->setCellValue('J'.$index, sprintf('%s %s',
                    $this->translator->trans($contract->getPrice()->getCurrency()),
                    $contract->getPrice()->getValue()
                ))
                ->setCellValue('K'.$index, sprintf('%s %s',
                    $contract->getSlaApprover()->getValue(),
                    $this->translator->trans($contract->getSlaApprover()->getType())
                ))
                ->setCellValue('L'.$index, sprintf('%s %s',
                    $contract->getSlaOrder()->getValue(),
                    $this->translator->trans($contract->getSlaOrder()->getType())
                ))
                ->setCellValue('M'.$index, sprintf('%s %s',
                    $contract->getSlaAccept()->getValue(),
                    $this->translator->trans($contract->getSlaAccept()->getType())
                ))
                ->setCellValue('N'.$index, sprintf('%s %s',
                    $contract->getSlaProduct()->getValue(),
                    $this->translator->trans($contract->getSlaProduct()->getType())
                ))
            ;

            if ($expired) {
                $this->report->getActiveSheet()
                    ->getStyle(sprintf('A%s:N%s', $index, $index))
                    ->applyFromArray([
                        'fill' => [
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => 'FF0000'],
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => ['rgb' => 'FFFFFF'],
                        ]
                    ])
                ;
            }

            ++$index;
        }
        
        foreach (range('A', 'N') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    /**
     * @inheritdoc
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Código producto')
            ->setCellValue('B1', 'Código sucursal')
            ->setCellValue('C1', 'Nombre sucursal')
            ->setCellValue('D1', 'Centro de costo')
            ->setCellValue('E1', 'Rut proveedor')
            ->setCellValue('F1', 'Fecha inicio')
            ->setCellValue('G1', 'Fecha término')
            ->setCellValue('H1', 'Expirado')
            ->setCellValue('I1', 'No. contrato')
            ->setCellValue('J1', 'Precio')
            ->setCellValue('K1', 'SLA evaluador')
            ->setCellValue('L1', 'SLA orden de compra')
            ->setCellValue('M1', 'SLA proveedor aceptar')
            ->setCellValue('N1', 'SLA proveedor ejecutar')
        ;
    }
}