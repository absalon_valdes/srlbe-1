<?php

namespace App\Export;

use App\Entity\User;
use App\Repository\UserRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Translation\Translator;

/**
 * Class UserDatabaseExporter
 * @package App\Export
 * @DI\Service("user_exporter")
 * @DI\Tag("exporter", attributes={"alias"="users"})
 */
class UserDatabaseExporter extends Exporter
{
    /**
     * @var UserRepository
     * @DI\Inject("user_repo")
     */
    public $userRepo;

    /**
     * @var Translator
     * @DI\Inject("translator")
     */
    public $translator;

    /**
     * SupplierDatabaseExporter constructor.
     */
    public function __construct()
    {
        $this->fileName = sprintf('base-datos-usuarios-%s.xls', time());
        $this->tabName = 'Usuarios';
        $this->title = 'Base de datos usuarios '.date('d-m-Y H:i:s');
    }

    /**
     * @inheritdoc
     */
    protected function fetchData($type, $data)
    {
        $this->data = $this->userRepo->findAll();
    }

    /**
     * @inheritdoc
     */
    protected function fillRows()
    {
        $index = 2;

        /** @var User $user */
        foreach ($this->data as $user) {
            $this->report->getActiveSheet()
                ->setCellValue('A'.$index, $user->getUsername())
                ->setCellValue('B'.$index, $user->getEmail())
                ->setCellValue('C'.$index, $user->isEnabled()?'Si':'No')
                ->setCellValue('D'.$index, $user->getIsEmployee() && $user->getEmployee() ?$user->getEmployee()->getRut():'')
                ->setCellValue('E'.$index, $this->getTranslatedRoles($user->getRoles()))
            ;

            ++$index;
        }

        foreach (range('A', 'E') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    /**
     * @inheritdoc
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Usuario')
            ->setCellValue('B1', 'E-mail')
            ->setCellValue('C1', 'Habilitado')
            ->setCellValue('D1', 'Funcionario')
            ->setCellValue('E1', 'Roles')
        ;
    }

    private function getTranslatedRoles($roles) {
        return implode(',',array_filter(array_map(function($u){
            if($u != $this->translator->trans($u)){
                return $this->translator->trans($u);
            }
            return false;
        },$roles)));
    }
}