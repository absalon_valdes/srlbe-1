<?php

namespace App\Export;

use App\Entity\Requisition;
use App\Entity\SLA;
use App\Util\FunctionUtils;
use App\Helper\RequisitionTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Util\DateUtils;

/**
 * Class RequisitionByCategoryExporter.
 *
 * @Di\Service("requisition.bycategory.exporter")
 */
class RequisitionByCategoryExporter
{
    const TIMEOUT = 60 * 2;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionsRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var \Redis
     * @Di\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var string
     * @Di\Inject("%kernel.cache_dir%")
     */
    public $varDir;

    /**
     * @var array
     */
    private $historyPrefetch;

    /**
     * @var Requisition
     */
    private $currentRequisition;

    /**
     * @var array
     */
    private $currentContractSlas;

    /**
     * @var array
     */
    private $currentMetadata;

    /**
     * @var array
     */
    private $currentHistory;

    /**
     * @var array
     */
    private $columnNames = [];
    /**
     * @var boolean
     */
    private $error = false;

    /**
     * @var string
     */
    private $fileName = "";

    /**
     * @var Factory
     * @Di\Inject("phpexcel")
     */
    public $excelFactory;

    /**
     * @var array
     */
    private $possibleStatuses = [
        ProductStatus::CREATED,
        ProductStatus::ORDERED,
        ProductStatus::REJECTED,
        ProductStatus::APPEAL,
        ProductStatus::DENIED,
        ProductStatus::APPROVED,
        ProductStatus::ACCEPTED,
        ProductStatus::CONFIRMED,
        ProductStatus::DECLINED,
        ProductStatus::HOLD,
        ProductStatus::DELIVERED,
        ProductStatus::REVIEWED,
        ProductStatus::RETURNED,
        ProductStatus::EVALUATED
    ];

    /**
     * @param string $category
     * @param string $from
     * @param string $type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function export($category, $from,$type = null)
    {
        $types = !$type ? [
            Requisition::TYPE_MAINTENANCE,
            Requisition::TYPE_PRODUCT,
            Requisition::TYPE_SELF_MANAGEMENT,
        ] : [$type];

        $type = !$type ? 'all' : $type;

        $reportFilename = [
            Requisition::TYPE_MAINTENANCE => 'mantenciones',
            Requisition::TYPE_PRODUCT => 'pedidos',
            Requisition::TYPE_SELF_MANAGEMENT => 'autogestion',
            'all' => 'general'
        ];

        $cacheDir = $this->varDir.'/exporter_cache';

        if (!@mkdir($cacheDir, 0777, true) && !is_dir($cacheDir)) {
            throw new \RuntimeException(sprintf('Cannot create folder %s', $cacheDir));
        }

        $basefilename = sprintf('reporte-%s-%s', $reportFilename[$type], date('YmdHi'));
        $csvname = $cacheDir.'/'.$basefilename.'.csv';
        $cacheFilename = $cacheDir.'/'.$basefilename.'.csv';
        if (!file_exists($cacheFilename)) {
            $file = new \SplFileObject($csvname, 'w');
            $this->writeData($file, $this->getData($category,$from,$types));

        }
        $this->fileName = $cacheFilename;
        return $this;
    }

    public function getResponse(){

        $basefilename = basename($this->fileName);
        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($this->fileName);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $basefilename,
            iconv('UTF-8', 'ASCII//TRANSLIT', $basefilename)
        );

        return $response;
    }

    /**
     * @param string $filepath
     * @return mixed
     */
    public function getFile($filepath)
    {
        try{
            $file = file_get_contents($this->fileName);
            file_put_contents($filepath, $file);
            return $this;
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $category
     * @param $from
     * @param $types
     * @return bool|string
     */
    private function getData($category,$from,$types)
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('r.id as requisition_uid')
            ->addSelect('r.number as requisition_number')
            ->addSelect('r.type as requisition_type')
            ->addSelect('r.metadata as requisition_metadata')
            ->addSelect('r.transitions as requisition_transitions')
            ->addSelect('r.status as requisition_state')
            ->addSelect('r.created_at as requisition_created_at')
            ->addSelect('r.closed_at as requisition_closed_at')
            ->addSelect('r.quantity as requisition_quantity')
            ->addSelect('monthname(r.created_at) as requisition_month')
            ->addSelect('year(r.created_at) as requisition_year')
            ->addSelect('c.contract_number as contract_number')
            ->addSelect('c.centralized as contract_type')
            ->addSelect('r.slas as contract_slas')
            ->addSelect('u.email as requester_email')
            ->addSelect('e.rut as requester_rut')
            ->addSelect('concat_ws(" ", e.name, e.last_name, e.second_last_name) as requester_full_name')
            ->addSelect('e.phone as requester_phone')
            ->addSelect('x.title as requester_position')
            ->addSelect('o.name as requester_office_name')
            ->addSelect('o.address as requester_office_address')
            ->addSelect('o.code as requester_office_code')
            ->addSelect('o.cost_center as requester_office_cost_center')
            ->addSelect('o.type as requester_office_type')
            ->addSelect('k.name as requester_office_city')
            ->addSelect('l.name as requester_office_region_name')
            ->addSelect('l.slug as requester_office_region_slug')
            ->addSelect('co.name as request_office_name')
            ->addSelect('co.address as request_office_address')
            ->addSelect('co.code as request_office_code')
            ->addSelect('co.cost_center as request_office_cost_center')
            ->addSelect('co.type as request_office_type')
            ->addSelect('ck.name as request_office_city')
            ->addSelect('cl.name as request_office_region_name')
            ->addSelect('cl.slug as request_office_region_slug')

            ->addSelect('p.name as product_name')
            ->addSelect('p.code as product_code')
            ->addSelect('t.name as product_category')
            ->addSelect('c.price_value as product_price')
            ->addSelect('c.price_currency as product_price_currency')
            ->addSelect('concat_ws(" ", y.name, y.last_name, y.second_last_name) as approver_full_name')
            ->addSelect('y.rut as approver_rut')
            ->addSelect('z.name as supplier_name')
            ->addSelect('z.rut as supplier_rut')
            ->from('requisitions', 'r')
            ->leftJoin('r', 'contracts',  'c', 'c.id = r.contract_id')
            ->leftJoin('r', 'offices',    'co','co.id = c.office_id')
            ->leftJoin('o', 'categories', 'ck', 'ck.id = co.city_id')
            ->leftJoin('k', 'categories', 'cl', 'cl.id = ck.parent_id')
            ->leftJoin('r', 'users',      'u', 'u.id = r.requester_id')
            ->leftJoin('u', 'employees',  'e', 'e.id = u.employee_id')
            ->leftJoin('c', 'products',   'p', 'p.id = c.product_id')
            ->leftJoin('p', 'categories', 't', 't.id = p.category_id')
            ->leftJoin('e', 'offices',    'o', 'o.id = e.office_id')
            ->leftJoin('o', 'categories', 'k', 'k.id = o.city_id')
            ->leftJoin('k', 'categories', 'l', 'l.id = k.parent_id')
            ->leftJoin('e', 'positions',  'x', 'x.id = e.position_id')
            ->leftJoin('p', 'users',      'w', 'w.id = p.approver_id')
            ->leftJoin('w', 'employees',  'y', 'y.id = w.employee_id')
            ->leftJoin('c', 'suppliers',  'z', 'z.id = c.supplier_id')
            ->where('r.type in (:types)')
            ->andWhere('r.deleted_at is null')
            ->andWhere('r.product_id is not null')
            ->andWhere("r.status <> 'product.draft'")
            ->orderBy('r.number', 'desc');

            $query->andWhere($query->expr()->like('t.slug', ':slug'));
            $query->setParameter('slug', $category.'%');

            $from = \DateTime::createFromFormat('d-m-Y', $from);
            $query->andWhere('r.created_at >= :from');
            $query->setParameter('from',$from->format('Y-m-d'));

            $query->setParameter('types', $types, Connection::PARAM_STR_ARRAY);

        if (!$this->redis->exists($key = sha1($query->getSQL())) ) {

            $this->redis->setex($key, self::TIMEOUT, $query->execute()->fetchAll(\PDO::FETCH_ASSOC));
        }


        return $this->redis->get($key);
    }

    /**
     * @param \SplFileObject $file
     * @param $data
     */
    public function writeData(\SplFileObject $file, $data)
    {
        $BOM = "\xEF\xBB\xBF";
        $file->fwrite($BOM);
        $accessor = PropertyAccess::createPropertyAccessor();

        $getValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentRequisition, $key);
        };

        $getRegionNumber = function ($regionName) {
            $regionToRoman = [
                'arica' => 'XV',
                'tarapaca' => 'I',
                'antofagasta' => 'II',
                'atacama' => 'III',
                'coquimbo' => 'IV',
                'valparaiso' => 'V',
                'libertador' => 'VI',
                'maule' => 'VII',
                'biobio' => 'VIII',
                'araucania' => 'IX',
                'rios' => 'XIV',
                'lagos' => 'X',
                'aisen' => 'XI',
                'magallanes' => 'XII',
                'metropolitana' => 'XIII',
            ];

            foreach ($regionToRoman as $slug => $no) {
                if ((bool)strstr($regionName, $slug)) {
                    return $no;
                }
            }

            return null;
        };

        $getRequisitionTypeString = function ($type) {
            return $this->translator->trans($type);
        };

        $getRequisitionStateString = function ($state = null) use ($getValue) {
            $state = !$state ? $getValue('[requisition_state]') : $state;
            $cent = $getValue('[contract_type]');
            $type = $getValue('[requisition_type]');
            return $this->translator->trans(
                $state.'.'.str_replace('requisition.', '', $type).'.'.($cent ? 'centralized' : 'descentralized')
            );
        };

        $getOfficeTypeString = function ($officeType) {
            return $this->translator->trans($officeType);
        };

        $getStatus = function($status){
            return $this->translator->trans($status);
        };

        $getSLAType = function ($sla) {
            return $this->translator->trans($sla->getType());
        };

        $getEmployeePhone = function ($p) {
            if (!$p) {
                return null;
            }

            $parts = explode('/', str_replace(['\\', '//', '-', ')'], '/', $p));
            return trim($parts[count($parts) === 1 ? 0 : count($parts) - 1]);
        };

        $getMetadata = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentMetadata, $key);
        };

        $getCreationDate = function () use ($getValue) {
            $month = $getValue('[requisition_month]');
            $year = $getValue('[requisition_year]');

            return $this->translator->trans($month).' '.$year;
        };

        $getRequisitionQuantity = function () use ($getValue) {
            $mock = new Requisition();
            $mock->setMetadata($this->currentMetadata);
            $mock->setQuantity($getValue('[requisition_quantity]'));

            return $mock->getQuantity();
        };

        $unserialize = function ($key) use ($getValue) {
            return @unserialize($getValue($key) ?? 'a:0:{}');
        };

        $getWorkflowHistory = function () use ($getValue) {
            $oid = Requisition::class.$getValue('[requisition_uid]');
            $workflowId = Uuid::uuid5(Uuid::NAMESPACE_DNS, $oid)->toString();

            return $this->historyPrefetch[$workflowId];
        };

        $checkNewFormat = function () use ($getMetadata) {
            if (!$getMetadata('[details]')) {
                return false;
            }

            return (bool) $getMetadata('[details][caracteristicas]');
        };

        $totalByColor = function ($color) use ($getMetadata, $checkNewFormat) {
            if (!$checkNewFormat()) {
                return null;
            }

            $item = $getMetadata('[details][caracteristicas]') ?? [];

            return array_reduce($item, function ($carry, $item) use ($color) {
                if (isset($item['color']) && $color === $item['color']) {
                    $carry += $item['cantidad'] ?? 0;
                }

                return $carry;
            }, 0);
        };

        $totalByColorAndModel = function ($color, $model) use ($getMetadata, $checkNewFormat) {
            if (!$checkNewFormat()) {
                return null;
            }

            $items = $getMetadata('[details][caracteristicas]') ?? [];

            return array_reduce($items, function ($carry, $item) use ($color, $model) {
                if ((isset($item['color']) && $color === $item['color']) &&
                    (isset($item['modelo']) && $model === $item['modelo'])
                ) {
                    $carry += $item['cantidad'] ?? 0;
                }

                return $carry;
            }, 0);
        };

        $inventoryCodes = function() use ($getMetadata){
            $items = $getMetadata('[details][caracteristicas]') ?? [];
            if(count($items)){
                foreach ($items as $item){
                    if (array_key_exists('codigo_inventario',$item)){
                        return implode(',',$item['codigo_inventario']);
                    }
                }
            }
            return "";
        };

        $isClosed = function () use ($getValue) {
            return in_array($getValue('[requisition_state]'), Requisition::CLOSED_STATUSES, true);
        };

        $formatMysqlDatetime = function ($datetime) {
            return date('d/m/Y H:i:s', strtotime($datetime));
        };

        $mysqlDatetimeToObject = function ($datetime,$format = 'Y/m/d H:i:s') {
            if(!$datetime){
                return null;
            }
            return \DateTime::createFromFormat($format, $datetime);
        };


        $leftCartAt = function () use ($formatMysqlDatetime) {
            $index0 = 1 === count($this->currentHistory) ? 0 : 1;
            return $formatMysqlDatetime($this->currentHistory[$index0]['process_created_at']);
        };

        $getSLAValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentContractSlas, "[$key]");
        };

        $estimadedSLA = function ($start,$status = null) use ($getSLAValue,$accessor){
            if(!$status){
                return $this->slaCalculator->getEstimate(
                    $start,
                    $getSLAValue(ProductStatus::DELIVERED),
                    $getSLAValue(ProductStatus::APPROVED),
                    $getSLAValue(ProductStatus::CREATED),
                    $getSLAValue(ProductStatus::CONFIRMED),
                    $getSLAValue(ProductStatus::REVIEWED)
                );
            }

            return $accessor->isReadable($this->currentContractSlas, "[$status]") ?
                $this->slaCalculator->getEstimate($start,$getSLAValue($status)) : null;
        };

        $inSLA = function($estimatedTo,$endedAt = null){
            return $endedAt ? $estimatedTo > $endedAt : $estimatedTo > new \DateTime();

        };

        $daysInFormat = function ($to, $since = null, $format = '%ad %Hh %Im'){
            if (is_null($to)) {
                return null;
            }

            $toClone = clone $to;

            $since = !$since ? new \DateTime() : $since;
            $sign = -1 * (int) $toClone->diff($since)->format('%r%a') >= 0 ? '' : '-';

            return sprintf('%s'.$to->diff($since)->format($format), $sign);
        };

        $slaTimeOut = function ($since,$to){
            return DateUtils::dateIntervalFormat($since->diff($to ?? new \DateTime()));
        };

        $getAllAvailableTransitionsByStatus = function($status){
            $available = [];
            foreach ($this->currentHistory as $step){
                if($step["process_step_full"] == $status){
                    $available[] = $step;
                }
            }
            return $available;
        };



        $getStepByStatus = function($status) use ($getAllAvailableTransitionsByStatus){
            $available = $getAllAvailableTransitionsByStatus($status);

            if(empty($available)){
                return;
            }
            return end($available);
        };

        $getAmountRequesterRejections = function () use ($accessor,$getMetadata){
            if(!$getMetadata('[evaluation]')){
                return;
            }
            $countedValues = array_count_values(array_column($getMetadata('[evaluation]'),'evaluation'));
            return $accessor->isReadable($countedValues, '[servicio_no_recibido]') ?
                $accessor->getValue($countedValues, '[servicio_no_recibido]') : 0;
        };

        $addSLAvalues = function (...$slas){
            $add = 0;
            foreach (array_filter($slas) as $sla) {
                if (!$sla instanceof SLA) {
                    continue;
                }
                $add += $sla->getValue();
            }
            return $add;
        };

        $getKey = function($key){
            $this->columnNames[] = $key;
            return $key;
        };

        $getReqType = FunctionUtils::memoize($getRequisitionTypeString);

        if(!count($data)){
            $file->fputcsv($this->getColumnNames());
        }

        foreach ((array) $data as $i => $requisition) {

            $this->currentRequisition = $requisition;
            $this->currentMetadata = $unserialize('[requisition_metadata]');
//            $this->currentHistory = $getWorkflowHistory();
//            $this->currentContractSlas = $unserialize('[contract_slas]');

            try {
                $currency_price = $this->translator->trans($getValue('[product_price_currency]'));

                $rowData[$getKey('N° solicitud')] = $getValue('[requisition_number]');
                $rowData[$getKey('Tipo')] = $getReqType($getValue('[requisition_type]'));
                $rowData[$getKey('Codigo técnico')] = $getValue('[product_code]');
                $rowData[$getKey('Producto')] = $getValue('[product_name]');
                $rowData[$getKey('Categoría')] = $getValue('[product_category]');
                $rowData[$getKey('Precio')] = $getValue('[product_price]');
                $rowData[$getKey('Moneda')] = $currency_price == '$' ? 'Peso' : $currency_price;
                $rowData[$getKey('RUT Proveedor')] = $getValue('[supplier_rut]');
                $rowData[$getKey('Nombre Proveedor')] = $getValue('[supplier_name]');
                $rowData[$getKey('Cantidad')] = $getRequisitionQuantity();
                $rowData[$getKey('Mes creación')] = $getCreationDate();

                if ($i === 0) {
                    $file->fputcsv($this->columnNames);
                }
                $this->columnNames = [];
                $file->fputcsv($rowData);
            } catch (\Exception $e) {
//                dump($e->getMessage());
            }
        }
    }

    function getColumnNames(){
        $columns =  [];

        $columns[] = 'N° solicitud';
        $columns[] = 'Tipo';
        $columns[] = 'Codigo técnico';
        $columns[] = 'Producto';
        $columns[] = 'Categoría';
        $columns[] = 'Precio';
        $columns[] = 'Moneda';
        $columns[] = 'RUT Proveedor';
        $columns[] = 'Nombre Proveedor';
        $columns[] = 'Cantidad';
        $columns[] = 'Mes creación';
        return $columns;
    }

}