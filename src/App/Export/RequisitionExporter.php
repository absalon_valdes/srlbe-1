<?php

namespace App\Export;

use App\Entity\Requisition;
use App\Entity\SLA;
use App\Util\FunctionUtils;
use App\Helper\RequisitionTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Util\DateUtils;

/**
 * Class NewRequisitionExporter.
 *
 * @Di\Service("requisition.exporter")
 */
class RequisitionExporter
{
    const TIMEOUT = 60 * 2;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionsRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var \Redis
     * @Di\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var string
     * @Di\Inject("%kernel.cache_dir%")
     */
    public $varDir;

    /**
     * @var array
     */
    private $historyPrefetch;

    /**
     * @var Requisition
     */
    private $currentRequisition;

    /**
     * @var array
     */
    private $currentContractSlas;

    /**
     * @var array
     */
    private $currentMetadata;

    /**
     * @var array
     */
    private $currentHistory;

    /**
     * @var array
     */
    private $columnNames = [];
    /**
     * @var boolean
     */
    private $error = false;

    /**
     * @var string
     */
    private $fileName = "";

    /**
     * @var Factory
     * @Di\Inject("phpexcel")
     */
    public $excelFactory;

    /**
     * @var array
     */
    private $possibleStatuses = [
        ProductStatus::CREATED,
        ProductStatus::ORDERED,
        ProductStatus::REJECTED,
        ProductStatus::APPEAL,
        ProductStatus::DENIED,
        ProductStatus::APPROVED,
        ProductStatus::ACCEPTED,
        ProductStatus::CONFIRMED,
        ProductStatus::DECLINED,
        ProductStatus::HOLD,
        ProductStatus::DELIVERED,
        ProductStatus::REVIEWED,
        ProductStatus::RETURNED,
        ProductStatus::EVALUATED
    ];

    /**
     * @param string $type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function export($type = null,$filters = null,$reqNumbers = null)
    {
        $types = !$type ? [
            Requisition::TYPE_MAINTENANCE,
            Requisition::TYPE_PRODUCT,
            Requisition::TYPE_SELF_MANAGEMENT,
        ] : [$type];

        $type = !$type ? 'all' : $type;

        $reportFilename = [
            Requisition::TYPE_MAINTENANCE => 'mantenciones',
            Requisition::TYPE_PRODUCT => 'pedidos',
            Requisition::TYPE_SELF_MANAGEMENT => 'autogestion',
            'all' => 'general'
        ];

        if (!empty($filters) && array_key_exists('special_type',$filters)){
            $reportFilename = $filters;
            $type = 'special_type';
        }

        $cacheDir = $this->varDir.'/exporter_cache';

        if (!@mkdir($cacheDir, 0777, true) && !is_dir($cacheDir)) {
            throw new \RuntimeException(sprintf('Cannot create folder %s', $cacheDir));
        }

        $basefilename = sprintf('reporte-%s-%s', $reportFilename[$type], date('YmdHi'));
        $csvname = $cacheDir.'/'.$basefilename.'.csv';
        $cacheFilenameX = $cacheDir.'/'.$basefilename.'.xlsx';
        $cacheFilenameC = $cacheDir.'/'.$basefilename.'.csv';
        $hasFilters = $filters !== null || !empty($filters);
        if ($hasFilters || (
            (!file_exists($cacheFilenameX) || !file_exists($cacheFilenameC)))
            //&& (time() - self::TIMEOUT) < filemtime($cacheFilenameX) || time() - self::TIMEOUT) < filemtime($cacheFilenameC)
        ) {
            $file = new \SplFileObject($csvname, 'w');
            $this->writeData($file, $this->getData($types,empty($filters) ? null: array_filter($filters),$reqNumbers));

//            try{
//                $reader = \PHPExcel_IOFactory::createReader('CSV');
//                $excel = $reader->load($file->getRealPath());
//
//                $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
//                $writer->save($cacheFilenameX);
//            }catch (S Symfony\Component\Debug\Exception\OutOfMemoryException $e){
//                $this->error = true;
//            }

            //@unlink($file->getRealPath());
        }
        $this->error = true;
        if(!$this->error){
            $this->fileName = $cacheFilenameX;
        }else{
            $this->fileName = $cacheFilenameC;
        }
        return $this;
    }

    public function getResponse(){

        $basefilename = basename($this->fileName);
        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($this->fileName);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $basefilename,
            iconv('UTF-8', 'ASCII//TRANSLIT', $basefilename)
        );

        return $response;
    }

    /**
     * @param string $filepath
     * @return mixed
     */
    public function getFile($filepath)
    {
        try{
            $file = file_get_contents($this->fileName);
            file_put_contents($filepath, $file);
            return $this;
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $types
     * @param $filters
     * @param $reqNumbers
     * @return bool|string
     */
    private function getData($types,$filters,$reqNumbers)
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('r.id as requisition_uid')
            ->addSelect('r.number as requisition_number')
            ->addSelect('r.type as requisition_type')
            ->addSelect('r.metadata as requisition_metadata')
            ->addSelect('r.transitions as requisition_transitions')
            ->addSelect('r.status as requisition_state')
            ->addSelect('r.created_at as requisition_created_at')
            ->addSelect('r.closed_at as requisition_closed_at')
            ->addSelect('r.quantity as requisition_quantity')
            ->addSelect('monthname(r.created_at) as requisition_month')
            ->addSelect('year(r.created_at) as requisition_year')
            ->addSelect('c.contract_number as contract_number')
            ->addSelect('c.centralized as contract_type')
            ->addSelect('r.slas as contract_slas')
            ->addSelect('u.email as requester_email')
            ->addSelect('e.rut as requester_rut')
            ->addSelect('concat_ws(" ", e.name, e.last_name, e.second_last_name) as requester_full_name')
            ->addSelect('e.phone as requester_phone')
            ->addSelect('x.title as requester_position')
            ->addSelect('o.name as requester_office_name')
            ->addSelect('o.address as requester_office_address')
            ->addSelect('o.code as requester_office_code')
            ->addSelect('o.cost_center as requester_office_cost_center')
            ->addSelect('o.type as requester_office_type')
            ->addSelect('k.name as requester_office_city')
            ->addSelect('l.name as requester_office_region_name')
            ->addSelect('l.slug as requester_office_region_slug')
            ->addSelect('co.name as request_office_name')
            ->addSelect('co.address as request_office_address')
            ->addSelect('co.code as request_office_code')
            ->addSelect('co.cost_center as request_office_cost_center')
            ->addSelect('co.type as request_office_type')
            ->addSelect('ck.name as request_office_city')
            ->addSelect('cl.name as request_office_region_name')
            ->addSelect('cl.slug as request_office_region_slug')

            ->addSelect('p.name as product_name')
            ->addSelect('p.code as product_code')
            ->addSelect('t.name as product_category')
            ->addSelect('c.price_value as product_price')
            ->addSelect('c.price_currency as product_price_currency')
            ->addSelect('concat_ws(" ", y.name, y.last_name, y.second_last_name) as approver_full_name')
            ->addSelect('y.rut as approver_rut')
            ->addSelect('z.name as supplier_name')
            ->addSelect('z.rut as supplier_rut')
            ->from('requisitions', 'r')
            ->leftJoin('r', 'contracts',  'c', 'c.id = r.contract_id')
            ->leftJoin('r', 'offices',    'co','co.id = c.office_id')
            ->leftJoin('o', 'categories', 'ck', 'ck.id = co.city_id')
            ->leftJoin('k', 'categories', 'cl', 'cl.id = ck.parent_id')
            ->leftJoin('r', 'users',      'u', 'u.id = r.requester_id')
            ->leftJoin('u', 'employees',  'e', 'e.id = u.employee_id')
            ->leftJoin('c', 'products',   'p', 'p.id = c.product_id')
            ->leftJoin('p', 'categories', 't', 't.id = p.category_id')
            ->leftJoin('e', 'offices',    'o', 'o.id = e.office_id')
            ->leftJoin('o', 'categories', 'k', 'k.id = o.city_id')
            ->leftJoin('k', 'categories', 'l', 'l.id = k.parent_id')
            ->leftJoin('e', 'positions',  'x', 'x.id = e.position_id')
            ->leftJoin('p', 'users',      'w', 'w.id = p.approver_id')
            ->leftJoin('w', 'employees',  'y', 'y.id = w.employee_id')
            ->leftJoin('c', 'suppliers',  'z', 'z.id = c.supplier_id')
            ->where('r.type in (:types)')
            ->andWhere('r.deleted_at is null')
            ->andWhere('r.product_id is not null')
            ->andWhere("r.status <> 'product.draft'")
            ->orderBy('r.number', 'desc');

        if($reqNumbers){
            $query->andWhere('r.number in (:numbers)');
            $query->setParameter('numbers',$reqNumbers, Connection::PARAM_STR_ARRAY);
        }

        if(!empty($filters)){
            if(array_key_exists('from',$filters)){
                $from = \DateTime::createFromFormat('d-m-Y', $filters["from"]);
                $query->andWhere('r.created_at >= :from');
                $query->setParameter('from',$from->format('Y-m-d'));
            }
            if(array_key_exists('to',$filters)){
                $to = \DateTime::createFromFormat('d-m-Y', $filters["to"]);
                $query->andWhere('r.created_at <= :to');
                $query->setParameter('to',$to->format('Y-m-d'));
            }
            if(array_key_exists('status',$filters)){
                $query->andWhere("r.status in (:status)");
                $query->setParameter('status',$filters["status"],Connection::PARAM_STR_ARRAY);
            }
            if(array_key_exists('special_type',$filters)){
                $query->andWhere("co.special_type in (:specialType)");
                $query->setParameter('specialType',[$filters["special_type"]],Connection::PARAM_STR_ARRAY);
            }else{
                $query->andWhere('co.special_type is null');
            }
            if(array_key_exists('type',$filters)){
                $types = $filters["type"];
            }
        }else{
            $query->andWhere('co.special_type is null');
        }

        $query->setParameter('types', $types, Connection::PARAM_STR_ARRAY);

        if (!$this->redis->exists($key = sha1($query->getSQL())) || !empty($filters)) {

            $this->redis->setex($key, self::TIMEOUT, $query->execute()->fetchAll(\PDO::FETCH_ASSOC));
        }

        $this->historyPrefetch = $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('s.workflow_identifier as process_uid')
            ->addSelect('s.process_name as process_name')
            ->addSelect('s.step_name as process_step')
            ->addSelect('concat_ws(".", s.process_name, s.step_name) as process_step_full')
            ->addSelect('s.created_at as process_created_at')
            ->addSelect('s.finished_at as process_finished_at')
            ->from('workflow_state', 's')
            ->where('s.successful <> 0')
            ->execute()
            ->fetchAll(\PDO::FETCH_GROUP | \PDO::FETCH_ASSOC)
        ;

        return $this->redis->get($key);
    }

    /**
     * @param \SplFileObject $file
     * @param $data
     */
    public function writeData(\SplFileObject $file, $data)
    {
        $BOM = "\xEF\xBB\xBF";
        $file->fwrite($BOM);
        $accessor = PropertyAccess::createPropertyAccessor();

        $getValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentRequisition, $key);
        };

        $getRegionNumber = function ($regionName) {
            $regionToRoman = [
                'arica' => 'XV',
                'tarapaca' => 'I',
                'antofagasta' => 'II',
                'atacama' => 'III',
                'coquimbo' => 'IV',
                'valparaiso' => 'V',
                'libertador' => 'VI',
                'maule' => 'VII',
                'biobio' => 'VIII',
                'araucania' => 'IX',
                'rios' => 'XIV',
                'lagos' => 'X',
                'aisen' => 'XI',
                'magallanes' => 'XII',
                'metropolitana' => 'XIII',
            ];

            foreach ($regionToRoman as $slug => $no) {
                if ((bool)strstr($regionName, $slug)) {
                    return $no;
                }
            }

            return null;
        };

        $getRequisitionTypeString = function ($type) {
            return $this->translator->trans($type);
        };

        $getRequisitionStateString = function ($state = null) use ($getValue) {
            $state = !$state ? $getValue('[requisition_state]') : $state;
            $cent = $getValue('[contract_type]');
            $type = $getValue('[requisition_type]');

            return $this->translator->trans(
                $state.'.'.str_replace('requisition.', '', $type).'.'.($cent ? 'centralized' : 'descentralized')
            );
        };

        $getOfficeTypeString = function ($officeType) {
            return $this->translator->trans($officeType);
        };

        $getStatus = function($status){
            return $this->translator->trans($status);
        };

        $getSLAType = function ($sla) {
            return $this->translator->trans($sla->getType());
        };

        $getEmployeePhone = function ($p) {
            if (!$p) {
                return null;
            }

            $parts = explode('/', str_replace(['\\', '//', '-', ')'], '/', $p));
            return trim($parts[count($parts) === 1 ? 0 : count($parts) - 1]);
        };

        $getMetadata = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentMetadata, $key);
        };

        $getCreationDate = function ($startedAt) use ($getValue) {
            $month = $startedAt->format('F');
            $year = $startedAt->format('Y');

            return $this->translator->trans($month).' '.$year;
        };

        $getRequisitionQuantity = function () use ($getValue) {
            $mock = new Requisition();
            $mock->setMetadata($this->currentMetadata);
            $mock->setQuantity($getValue('[requisition_quantity]'));

            return $mock->getQuantity();
        };

        $unserialize = function ($key) use ($getValue) {
            return @unserialize($getValue($key) ?? 'a:0:{}');
        };

        $getWorkflowHistory = function () use ($getValue) {
            $oid = Requisition::class.$getValue('[requisition_uid]');
            $workflowId = Uuid::uuid5(Uuid::NAMESPACE_DNS, $oid)->toString();

            return $this->historyPrefetch[$workflowId];
        };

        $checkNewFormat = function () use ($getMetadata) {
            if (!$getMetadata('[details]')) {
                return false;
            }

            return (bool) $getMetadata('[details][caracteristicas]');
        };

        $totalByColor = function ($color) use ($getMetadata, $checkNewFormat) {
            if (!$checkNewFormat()) {
                return null;
            }

            $item = $getMetadata('[details][caracteristicas]') ?? [];

            return array_reduce($item, function ($carry, $item) use ($color) {
                if (isset($item['color']) && $color === $item['color']) {
                    $carry += $item['cantidad'] ?? 0;
                }

                return $carry;
            }, 0);
        };

        $totalByColorAndModel = function ($color, $model) use ($getMetadata, $checkNewFormat) {
            if (!$checkNewFormat()) {
                return null;
            }

            $items = $getMetadata('[details][caracteristicas]') ?? [];

            return array_reduce($items, function ($carry, $item) use ($color, $model) {
                if ((isset($item['color']) && $color === $item['color']) &&
                    (isset($item['modelo']) && $model === $item['modelo'])
                ) {
                    $carry += $item['cantidad'] ?? 0;
                }

                return $carry;
            }, 0);
        };

        $inventoryCodes = function() use ($getMetadata,$checkNewFormat,$accessor){
            $items = $checkNewFormat() ?  $getMetadata('[details][caracteristicas]') : $getMetadata('[details][codigo_inventario]');
            if(count($items) && $checkNewFormat()){
                foreach ($items as $item){
                    if (array_key_exists('codigo_inventario',$item)){
                        return implode(',',$item['codigo_inventario']);
                    }
                }
            }else{
                if(count($items)){
                    return implode(',', $items);
                }elseif ($accessor->isReadable($this->currentMetadata, "[inventory_code]")){
                    $items = $getMetadata('[inventory_code]');
                    if($items && is_array($items)) {
                        return implode(',', $items);
                    }
                }
            }
            return "";
        };

        $approverCloseComments = function() use ($getMetadata){
            $comments = $getMetadata('[approver_close][comments]');
            if(!empty($comments)){
                $comments = preg_replace( "/\r|\n/", " ", $comments);
//                $comments = iconv('UTF-8', 'utf-8', $comments);
                return $comments;
            }
            return "";
        };

        $isClosed = function () use ($getValue) {
            return in_array($getValue('[requisition_state]'), Requisition::CLOSED_STATUSES, true);
        };

        $formatMysqlDatetime = function ($datetime) {
            return date('d/m/Y H:i:s', strtotime($datetime));
        };

        $mysqlDatetimeToObject = function ($datetime,$format = 'Y/m/d H:i:s') {
            if(!$datetime){
                return null;
            }
            return \DateTime::createFromFormat($format, $datetime);
        };


        $leftCartAt = function () use ($formatMysqlDatetime) {
            $index0 = 1 === count($this->currentHistory) ? 0 : 1;
            return $formatMysqlDatetime($this->currentHistory[$index0]['process_created_at']);
        };

        $getSLAValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentContractSlas, "[$key]");
        };

        $estimadedSLA = function ($start,$status = null) use ($getSLAValue,$accessor){
            if(!$status){
                return $this->slaCalculator->getEstimate(
                    $start,
                    $getSLAValue(ProductStatus::DELIVERED),
                    $getSLAValue(ProductStatus::APPROVED),
                    $getSLAValue(ProductStatus::CREATED),
                    $getSLAValue(ProductStatus::CONFIRMED),
                    $getSLAValue(ProductStatus::REVIEWED)
                );
            }

            return $accessor->isReadable($this->currentContractSlas, "[$status]") ?
                $this->slaCalculator->getEstimate($start,$getSLAValue($status)) : null;
        };

        $inSLA = function($estimatedTo,$endedAt = null){
            return $endedAt ? $estimatedTo > $endedAt : $estimatedTo > new \DateTime();

        };

        $daysInFormat = function ($to, $since = null, $format = '%ad %Hh %Im'){
            if (is_null($to)) {
                return null;
            }

            $toClone = clone $to;

            $since = !$since ? new \DateTime() : $since;
            $sign = -1 * (int) $toClone->diff($since)->format('%r%a') >= 0 ? '' : '-';

            return sprintf('%s'.$to->diff($since)->format($format), $sign);
        };

        $slaTimeOut = function ($since,$to){
            return DateUtils::dateIntervalFormat($since->diff($to ?? new \DateTime()));
        };

        $getAllAvailableTransitionsByStatus = function($status){
            $available = [];
            foreach ($this->currentHistory as $step){
                if($step["process_step_full"] == $status){
                    $available[] = $step;
                }
            }
            return $available;
        };



        $getStepByStatus = function($status) use ($getAllAvailableTransitionsByStatus){
            $available = $getAllAvailableTransitionsByStatus($status);

            if(empty($available)){
                return;
            }
            return end($available);
        };

        $getAmountRequesterRejections = function () use ($accessor,$getMetadata){
            if(!$getMetadata('[evaluation]')){
                return;
            }
            $countedValues = array_count_values(array_column($getMetadata('[evaluation]'),'evaluation'));
            return $accessor->isReadable($countedValues, '[servicio_no_recibido]') ?
                $accessor->getValue($countedValues, '[servicio_no_recibido]') : 0;
        };

        $addSLAvalues = function (...$slas){
            $add = 0;
            foreach (array_filter($slas) as $sla) {
                if (!$sla instanceof SLA) {
                    continue;
                }
                $add += $sla->getValue();
            }
            return $add;
        };

        $getKey = function($key){
            $this->columnNames[] = $key;
            return $key;
        };

        $getReqType = FunctionUtils::memoize($getRequisitionTypeString);
        $getRN = FunctionUtils::memoize($getRegionNumber);
        $getTypeOffice = FunctionUtils::memoize($getOfficeTypeString);
        $getSLATypeString = FunctionUtils::memoize($getSLAType);
//        $getStatusString = FunctionUtils::memoize($getRequisitionStateString);
        $getStatusString = $getRequisitionStateString;

        if(!count($data)){
            $file->fputcsv($this->getColumnNames(),';');
        }

        foreach ((array) $data as $i => $requisition) {

            $this->currentRequisition = $requisition;
            $this->currentMetadata = $unserialize('[requisition_metadata]');
            $this->currentHistory = $getWorkflowHistory();
            $this->currentContractSlas = $unserialize('[contract_slas]');

            $startedAt = $mysqlDatetimeToObject($leftCartAt(),'d/m/Y H:i:s');
            $endedAt   = $isClosed() ? $mysqlDatetimeToObject($getValue('[requisition_closed_at]'),'Y-m-d H:i:s'): new \DateTime();
            $estimatedEnd = $estimadedSLA($startedAt);
            $inSla = $inSLA($estimatedEnd,$endedAt);
            $declined = false;

            try {
                $rowData[$getKey('N° solicitud')] = $getValue('[requisition_number]');
                $rowData[$getKey('Tipo')] = $getReqType($getValue('[requisition_type]'));
                $rowData[$getKey('RUT Solicitante')] = $getValue('[requester_rut]') ?? 'Sin Información';
                $rowData[$getKey('Solicitante')] = $getValue('[requester_full_name]') ?? 'Sin Información';
                $rowData[$getKey('Anexo')] = $getEmployeePhone($getValue('[requester_phone]')) ?? 'Sin Información';
                $rowData[$getKey('Correo electrónico')] = $getValue('[requester_email]');
                $rowData[$getKey('Dirección')] = $getValue('[request_office_address]');
                $rowData[$getKey('Comuna')] = $getValue('[request_office_city]');
                $rowData[$getKey('N° Región')] = $getRN($getValue('[request_office_region_slug]'));
                $rowData[$getKey('Región')] = $getValue('[request_office_region_name]');
                $rowData[$getKey('Cargo Solicitante')] = $getValue('[requester_position]') ?? 'Sin Información';
                $rowData[$getKey('Codigo Unidad')] = $getValue('[request_office_code]');
                $rowData[$getKey('Nombre Sucursal')] = $getValue('[request_office_name]');
                $rowData[$getKey('Centro de costo')] = $getValue('[request_office_cost_center]');
//                $rowData[$getKey('No Recibido')] = '';
                $rowData[$getKey('Estado')] = $isClosed() ? 'CERRADO' : 'ABIERTO';
                $rowData[$getKey('Estado en sistema')] = $getRequisitionStateString();
                $rowData[$getKey('Fecha de creación')] = $startedAt->format('d/m/Y H:i:s');
                $rowData[$getKey('Fecha posible de entrega')] = $estimatedEnd->format('d/m/Y H:i:s');
                $rowData[$getKey('Fecha entrega real')] = $isClosed() ? $formatMysqlDatetime($getValue('[requisition_closed_at]')) : '';
                $rowData[$getKey('En SLA')] = $inSla ? 'DENTRO SLA' : 'FUERA SLA';
                $rowData[$getKey('Días SLA restante')] = $inSla && !$isClosed()  ? $daysInFormat($estimatedEnd) : 0;
                $rowData[$getKey('SLA total esperado')] = $daysInFormat($estimatedEnd, $startedAt);
                $rowData[$getKey('SLA total real')] =  $daysInFormat($endedAt, $startedAt);
                $rowData[$getKey('Tpo Excedido respecto de SLA')] = !$inSla ? $slaTimeOut($estimatedEnd,$endedAt) : '';

                foreach ($this->possibleStatuses as $key => $status) {
                    $sla = $getSLAValue($status);
                    $step = $getStepByStatus($status);
                    $stepStartedAt = $step ? $mysqlDatetimeToObject($step["process_created_at"],'Y-m-d H:i:s') : new \DateTime();
                    $stepEndedAt = $step && $step["process_finished_at"] ? $mysqlDatetimeToObject($step["process_finished_at"],'Y-m-d H:i:s') : new \DateTime();
                    $estimatedEndSla = $sla && $step ? $estimadedSLA($stepStartedAt,$status) : null ;

                    $rowData[$key.$getKey('Tipo SLA')] = 'SLA '.$getStatusString($status);
                    $rowData[$key.$getKey('SLA esperado')] = $sla ? $sla->getValue() : '';
                    $rowData[$key.$getKey('SLA unidad')] =  $sla ? $getSLATypeString($sla): '';
                    $rowData[$key.$getKey('SLA real')] = $step ? $daysInFormat($stepEndedAt, $stepStartedAt) : '';
                    $rowData[$key.$getKey('SLA fecha inicio')] = $step ? $formatMysqlDatetime($step["process_created_at"]): '' ;
                    $rowData[$key.$getKey('SLA fecha fin')] = $step && $step["process_finished_at"] ? $formatMysqlDatetime($step["process_finished_at"]): '';
                    $inSla = $inSLA($estimatedEndSla,$stepEndedAt);

                    if ($status === ProductStatus::EVALUATED) {
                        $eval = $getMetadata('[evaluation]');
                        $evaluatedAt = $eval ? $accessor->getValue(end($eval),'[evaluated_at]') : null;
                        $rowData[$key.$getKey('Fecha certifica')] = $evaluatedAt ? $evaluatedAt->format('d/m/Y H:i:s') : '';
                    }

                    if ($status === ProductStatus::RETURNED) {
                        $rowData[$key.$getKey('N° rechazos demandante')] = $getAmountRequesterRejections();
                    }

                    if($status == ProductStatus::DECLINED && $step){
                        $declined = true;
                    }

                    if($status == ProductStatus::ACCEPTED){
                        $stepAccepted = $step;
                        $stepAccepted["sla"] = $sla;
                    }elseif($status == ProductStatus::DELIVERED){
                        $stepProduct = $step;
                        $stepProduct["sla"] = $sla;
                    }elseif($status == ProductStatus::APPROVED){
                        $stepApproved = $step;
                        $stepApproved["sla"] = $sla;
                    }

                    $rowData[$key.$getKey('Tpo Excedido respecto de SLA')] = $estimatedEndSla !== null && !$inSla ?
                        $slaTimeOut($estimatedEndSla,$stepEndedAt) : '';
                }

                /***** SLA ejecución proveedor ***/
                $execSupplierStart = $mysqlDatetimeToObject($accessor->getValue($stepApproved,'[process_finished_at]'),'Y-m-d H:i:s');
                $firstEstimadedSupExec = $this->slaCalculator->getEstimate(
                    $startedAt,
                    $getSLAValue(ProductStatus::DELIVERED),
                    $stepApproved["sla"],
                    $getSLAValue(ProductStatus::CREATED)); //tiempo estimado de acuerdo a SLA de contrato
                $ScndEstimadedSupExec = $this->slaCalculator->getEstimate(
                    $execSupplierStart,
                    $getSLAValue(ProductStatus::DELIVERED),
                    $getSLAValue(ProductStatus::CREATED));//tiempo estimado desde que el evaluador aprobó

                $firstSlaTimeOut = $execSupplierStart && !$inSLA($firstEstimadedSupExec,$endedAt) ? $slaTimeOut($firstEstimadedSupExec,$endedAt): null;
                $ScndSlaTimeOut = $execSupplierStart && !$inSLA($ScndEstimadedSupExec,$endedAt) ? $slaTimeOut($ScndEstimadedSupExec,$endedAt): null;

                $rowData[$getKey('Tipo SLA')] = 'SLA ejecución del proveedor';
                $rowData[$getKey('Proveedor acepta o rechaza')] = $declined ? 'RECHAZA' : 'ACEPTA';
                $rowData[$getKey('SLA esperado')] = $addSLAvalues($stepProduct["sla"],$stepApproved["sla"]);
                $rowData[$getKey('SLA unidad')] = $getSLATypeString($stepProduct["sla"]);
                $rowData[$getKey('SLA real')] = $execSupplierStart ? DateUtils::dateIntervalFormat($execSupplierStart->diff($endedAt ?? (new \DateTime))) : null;
                $rowData[$getKey('SLA fecha inicio')] =  $execSupplierStart ? $formatMysqlDatetime($stepApproved["process_finished_at"]) : null;
                $rowData[$getKey('SLA fecha fin')] = $endedAt && $execSupplierStart ? $endedAt->format('d/m/Y H:i:s') : null;
                $rowData[$getKey('Primera fecha estimada')] = $execSupplierStart ? $firstEstimadedSupExec->format('d/m/Y H:i:s') : null;
                $rowData[$getKey('Tpo Excedido respecto primera fecha')] = $firstSlaTimeOut;
                $rowData[$getKey('Segunda fecha estimada')] = $execSupplierStart ? $ScndEstimadedSupExec->format('d/m/Y H:i:s') : null;
                $rowData[$getKey('Tpo Excedido respecto segunda fecha')] = $ScndSlaTimeOut;
                /***** SLA ejecución proveedor ***/

                /***** SLA cerrada evaluador ***/
                $closed_by_approver_date = $getMetadata('[approver_close][datetime]');
                $reason = $getMetadata('[approver_close][reason]');
                $comments = $approverCloseComments();
                $rowData[$getKey('Fecha de cierre evaluador')] = $closed_by_approver_date ? $closed_by_approver_date->format('d/m/Y H:i:s') : '';
                $rowData[$getKey('Razón cierre evaluador')] = $closed_by_approver_date ? $this->translator->trans($reason) : '';
                $rowData[$getKey('Comentarios cierre evaluador')] = $comments;
                /***** SLA cerrada evaluador ***/

                /***** SLA cerrada por sistema ***/
                $step = $getStepByStatus(ProductStatus::TIMEOUT);
                $stepStartedAt = $step ? $mysqlDatetimeToObject($step["process_created_at"],'Y-m-d H:i:s') : '';
                $rowData[$getKey('Fecha de cierre sistema')] = $stepStartedAt ? $stepStartedAt->format('d/m/Y H:i:s') : '';
                /***** SLA cerrada por sistema ***/

                $currency_price = $this->translator->trans($getValue('[product_price_currency]'));

                $rowData[$getKey('Puntualidad')] = $getMetadata('[supplier_rating][punctuality]');
                $rowData[$getKey('Calidad')] = $getMetadata('[supplier_rating][quality]');
                $rowData[$getKey('Nivel de servicio')] = $getMetadata('[supplier_rating][service_level]');
                $rowData[$getKey('Nro Contrato')] = $getValue('[contract_number]');
                $rowData[$getKey('Tipo contrato')] = ['Descentralizado','Centralizado'][$getValue('[contract_type]')];
                $rowData[$getKey('Codigo técnico')] = $getValue('[product_code]');
                $rowData[$getKey('Producto')] = $getValue('[product_name]');
                $rowData[$getKey('Categoría')] = $getValue('[product_category]');
                $rowData[$getKey('Precio')] = $getValue('[product_price]');
                $rowData[$getKey('Moneda')] = $currency_price == '$' ? 'Peso' : $currency_price;
                $rowData[$getKey('Marcaje')] = $inventoryCodes();
                $rowData[$getKey('RUT Evaluador')] = $getValue('[approver_rut]');
                $rowData[$getKey('Nombre Evaluador')] = $getValue('[approver_full_name]');
                $rowData[$getKey('Dirección unidad demandante')] = $getValue('[requester_office_address]');
                $rowData[$getKey('Tipo unidad')] = $getTypeOffice($getValue('[requester_office_type]'));
                $rowData[$getKey('RUT Proveedor')] = $getValue('[supplier_rut]');
                $rowData[$getKey('Nombre Proveedor')] = $getValue('[supplier_name]');
                $rowData[$getKey('Cantidad')] = $getRequisitionQuantity();
                $rowData[$getKey('Mes creación')] = $getCreationDate($startedAt);
                $rowData[$getKey('Rojo')] = $totalByColor('rojo');
                $rowData[$getKey('Gris')] = $totalByColor('gris');
                $rowData[$getKey('Mesón rojo')] = $totalByColorAndModel('rojo', 'meson');
                $rowData[$getKey('Mesón gris')] = $totalByColorAndModel('gris', 'meson');
                $rowData[$getKey('Cajero rojo')] = $totalByColorAndModel('rojo', 'cajero');
                $rowData[$getKey('Cajero gris')] = $totalByColorAndModel('gris', 'cajero');

                if ($i === 0) {
                    $file->fputcsv($this->columnNames,';');
                }
                $this->columnNames = [];
                $file->fputcsv($rowData,';');
            } catch (\Exception $e) {
//                dump($e->getMessage());
            }
        }
    }

    function getColumnNames(){
        $columns =  [
            'N° solicitud',
            'Tipo',
            'RUT Solicitante',
            'Solicitante',
            'Anexo',
            'Correo electrónico',
            'Dirección',
            'Comuna',
            'N° Región',
            'Región',
            'Cargo Solicitante',
            'Codigo Unidad',
            'Nombre Sucursal',
            'Centro de costo',
//            'No Recibido',
            'Estado',
            'Estado en sistema',
            'Fecha de creación',
            'Fecha posible de entrega',
            'Fecha entrega real',
            'En SLA',
            'Días SLA restante',
            'SLA total esperado',
            'SLA total real',
            'Tpo Excedido respecto de SLA'
        ];
        foreach ($this->possibleStatuses as $key => $status) {
            $columns[] = 'Tipo SLA';
            $columns[] = 'SLA esperado';
            $columns[] = 'SLA unidad';
            $columns[] = 'SLA real';
            $columns[] = 'SLA fecha inicio';
            $columns[] = 'SLA fecha fin';

            if ($status === ProductStatus::EVALUATED) {
                $columns[] = 'Fecha certifica';
            }

            if ($status === ProductStatus::RETURNED) {
                $columns[] = 'N° rechazos demandante';
            }
            $columns[] = 'Tpo Excedido respecto de SLA';
        }

        $columns[] = 'Tipo SLA';
        $columns[] = 'Proveedor acepta o rechaza';
        $columns[] = 'SLA esperado';
        $columns[] = 'SLA unidad';
        $columns[] = 'SLA real';
        $columns[] = 'SLA fecha inicio';
        $columns[] = 'SLA fecha fin';
        $columns[] = 'Primera fecha estimada';
        $columns[] = 'Tpo Excedido respecto primera fecha';
        $columns[] = 'Segunda fecha estimada';
        $columns[] = 'Tpo Excedido respecto segunda fecha';
        $columns[] = 'Fecha de cierre evaluador';
        $columns[] = 'Razón cierre evaluador';
        $columns[] = 'Comentarios cierre evaluador';
        $columns[] = 'Fecha de cierre sistema';
        $columns[] = 'Puntualidad';
        $columns[] = 'Calidad';
        $columns[] = 'Nivel de servicio';
        $columns[] = 'Nro Contrato';
        $columns[] = 'Tipo contrato';
        $columns[] = 'Codigo técnico';
        $columns[] = 'Producto';
        $columns[] = 'Categoría';
        $columns[] = 'Precio';
        $columns[] = 'Moneda';
        $columns[] = 'RUT Evaluador';
        $columns[] = 'Nombre Evaluador';
        $columns[] = 'Dirección unidad demandante';
        $columns[] = 'Tipo unidad';
        $columns[] = 'RUT Proveedor';
        $columns[] = 'Nombre Proveedor';
        $columns[] = 'Cantidad';
        $columns[] = 'Mes creación';
        $columns[] = 'Rojo';
        $columns[] = 'Gris';
        $columns[] = 'Mesón rojo';
        $columns[] = 'Mesón gris';
        $columns[] = 'Cajero rojo';
        $columns[] = 'Cajero gris';
        return $columns;
    }

}