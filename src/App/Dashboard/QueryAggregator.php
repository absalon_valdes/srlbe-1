<?php

namespace App\Dashboard;

use Doctrine\ORM\Query;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class QueryAggregator
 * @package App\Dashboard
 * @Di\Service("dashboard_query_aggregator")
 */
class QueryAggregator
{
    /**
     * @var QueryInterface[]
     */
    private $queries;

    /**
     * @param QueryInterface $query
     */
    public function addQuery(QueryInterface $query)
    {
        $this->queries[$query->getName()] = $query;
    }

    /**
     * @param $key
     * @return bool
     */
    public function queryExists($key)
    {
        return isset($this->queries[$key]);
    }

    /**
     * @param $name
     * @return array
     */
    public function getQueryResult($name)
    {
        if (!isset($this->queries[$name])) {
            throw $this->createNotFoundException($name);
        }

        return $this->queries[$name]->getResult();
    }

    /**
     * @param $name
     * @return array
     */
    public function getQueryArrayResult($name)
    {
        if (!isset($this->queries[$name])) {
            throw $this->createNotFoundException($name);
        }

        return $this->queries[$name]->getArrayResult();
    }

    private function createNotFoundException($name)
    {
        return new \InvalidArgumentException(sprintf(
            'Query "%s" not found in [%s]', $name, implode(', ', array_keys($this->queries))
        ));
    }
}
