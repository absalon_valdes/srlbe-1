<?php
namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\Requisition;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class RequisitionProductCreatedQuery extends AbstractQuery
{
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('r.type', $qb->expr()->literal(Requisition::TYPE_PRODUCT)),
                    $qb->expr()->in('r.status', [
                        ProductStatus::CREATED,
                    ])
                )
            )
            ->orderBy($qb->expr()->desc('r.updatedAt'))
        ;

        return $qb->getQuery();
    }
}
