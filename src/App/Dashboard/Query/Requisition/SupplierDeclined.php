<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\DeclinedRequisition;
use App\Entity\Requisition;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query\Expr\Join;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class SupplierDeclineQuery
 * @package App\Service\DashboardQuery\Query
 * @DI\Service @DI\Tag("dashboard.query")
 */
class SupplierDeclined extends AbstractQuery
{
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->join(DeclinedRequisition::class, 'd', Join::WITH, $qb->expr()->eq('r', 'd.requisition'))
            ->where($qb->expr()->eq('d.user', ':user'))       
            ->andWhere($qb->expr()->notIn('r.status', [
                ProductStatus::COMPLETED,
                ProductStatus::EVALUATED,
                ProductStatus::CLOSED,
            ]))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->getUser())
        ;
        
        return $qb->getQuery();
    }
}
