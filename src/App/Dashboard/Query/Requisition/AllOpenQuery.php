<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Dashboard\QueryAggregator;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class AllOpenQuery extends AbstractQuery
{
    /**
     * @var QueryAggregator
     * @Di\Inject("dashboard_query_aggregator")
     */
    public $aggregator;

    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        return null;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->aggregator->getQueryResult('requester.open');
    }

    /**
     * @return array
     */
    public function getArrayResult()
    {
        return $this->aggregator->getQueryArrayResult('requester.open');
    }
}
