<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\Requisition;
use App\Entity\SpecialRequest;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class RequesterHistoryQuery extends AbstractQuery
{
    public function getQuery()
    {
    }

    public function getQueryRequisitions()
    {
        $qb = $this->getQueryBuilder();

        return $qb->select('r')
            ->from(Requisition::class, 'r')
            ->where($qb->expr()->eq('r.requester', ':user'))
            ->andWhere($qb->expr()->in('r.status', [
                ProductStatus::CLOSED,
                ProductStatus::COMPLETED,
                ProductStatus::TIMEOUT,
                ProductStatus::CLOSED_BY_APPROVER,
            ]))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter(':user', $this->getUser())
            ->getQuery()
        ;
    }

    public function getQuerySpecialRequests()
    {
        $qb = $this->getQueryBuilder();

        return $qb->select('p')
            ->from(SpecialRequest::class, 'p')
            ->where($qb->expr()->in('p.state', [
                SpecialRequest::STATE_REJECTED_FINAL,
                SpecialRequest::STATE_CLOSED,
            ]))
            ->andWhere($qb->expr()->eq('p.requester', ':user'))
            ->setParameter(':user', $this->getUser())
            ->orderBy($qb->expr()->asc('p.number'))
            ->getQuery()
        ;
    }

    public function getQueryVcardRequester()
    {
        $qb = $this->getQueryBuilder();

        return $qb->select('r')
            ->from(Requisition::class, 'r')
            ->where($qb->expr()->eq('r.requester', ':user'))
            ->andWhere($qb->expr()->in('r.status', [
                'apelacion',
                'proveedor_calificado',
                'solicitud_completa',
                'cerrado',
                'cerrada_sistema'
            ]))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter(':user', $this->getUser())
            ->getQuery()
        ;
    }

    public function getResult()
    {
        $cspecs = [];

        foreach ($this->getQuerySpecialRequests()->getResult() as $item) {
            $cspecs[] = $item->mockRequisition();
        }

        $vcards  = $this->getQueryVcardRequester()->getResult();
        $pedidos = $this->getQueryRequisitions()->getResult();
        $pedidos = array_merge($cspecs, $pedidos, $vcards);

        usort($pedidos, function ($a, $b) {
            return $a->getUpdatedAt() < $b->getUpdatedAt();
        });

        return $pedidos;
    }
}
