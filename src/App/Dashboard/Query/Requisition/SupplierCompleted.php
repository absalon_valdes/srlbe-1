<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\Contract;
use App\Entity\Requisition;
use App\Entity\Supplier;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query\Expr\Join;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class SupplierMergedHistory
 * @package App\Dashboard\Query\Requisition
 * @DI\Service() @DI\Tag("dashboard.query")
 */
class SupplierCompleted extends AbstractQuery
{
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->join(Supplier::class, 's', Join::WITH, $qb->expr()->eq('c.supplier', 's'))
            ->where($qb->expr()->isMemberOf(':user', 's.representatives'))
            ->andWhere($qb->expr()->in('r.status', [
                ProductStatus::COMPLETED,
                ProductStatus::EVALUATED,
                ProductStatus::TIMEOUT,
                'solicitud_completa',
            ]))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->getUser());

        return $qb->getQuery();
    }
}
