<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\Contract;
use App\Entity\Requisition;
use App\Entity\Supplier;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query\Expr\Join;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class VCardSupplierQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class UniformSupplierQuery extends AbstractQuery
{
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->join(Supplier::class, 's', Join::WITH, $qb->expr()->eq('c.supplier', 's'))
            ->where($qb->expr()->in('r.type', [Requisition::TYPE_UNIFORM]))
            ->andWhere($qb->expr()->isMemberOf(':user', 's.representatives'))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->getUser())
        ;

        return $qb->getQuery();
    }
}
