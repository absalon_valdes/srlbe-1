<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use Doctrine\ORM\Query\Expr\Join;
use App\Workflow\Status\ProductStatus;
use App\Entity\Contract;
use App\Entity\Supplier;
use App\Entity\Requisition;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class SupplierAwayQuery extends AbstractQuery
{
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->join(Supplier::class, 's', Join::WITH, $qb->expr()->eq('c.supplier', 's'))
            ->where($qb->expr()->isMemberOf(':user', 's.representatives'))
            ->andWhere($qb->expr()->in('r.status', [
                ProductStatus::CONFIRMED,
                ProductStatus::REVIEWED,
                'aceptado',
                'en_despacho',
                'solicitud_evaluada'
            ]))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter('user', $this->getUser())
        ;

        return $qb->getQuery();
    }
}
