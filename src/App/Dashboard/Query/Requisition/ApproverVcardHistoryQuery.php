<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\Contract;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query\Expr\Join;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class ApproverVcardHistoryQuery extends AbstractQuery
{
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->join(Product::class, 'p', Join::WITH, $qb->expr()->eq('r.product', 'p'))
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->where($qb->expr()->in('r.status', [
                'cerrado',
                'rechazado',
                'denegado',
                'apelacion'
            ]))
            ->andWhere($qb->expr()->eq(':user', 'p.approver'))
            ->setParameter('user', $this->getUser())
        ;

        return $qb->getQuery();
    }
}
