<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Dashboard\QueryAggregator;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class ApproverAll
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class ApproverAll extends AbstractQuery
{
    /**
     * @var QueryAggregator
     * @Di\Inject("dashboard_query_aggregator")
     */
    public $aggregator;

    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        return null;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return array_merge(
            $this->aggregator->getQueryResult('approver.away'),
            $this->aggregator->getQueryResult('approver.cert'),
            $this->aggregator->getQueryResult('approver.pending'),
            $this->aggregator->getQueryResult('approver.history')
        );
    }

    /**
     * @return array
     */
    public function getArrayResult()
    {
        return array_merge(
            $this->aggregator->getQueryArrayResult('approver.away'),
            $this->aggregator->getQueryArrayResult('approver.cert'),
            $this->aggregator->getQueryArrayResult('approver.pending'),
            $this->aggregator->getQueryArrayResult('approver.history')
        );
    }
}
