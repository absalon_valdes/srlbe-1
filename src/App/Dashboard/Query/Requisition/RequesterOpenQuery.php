<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\Requisition;
use App\Entity\SpecialRequest;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterOpenQuery
 * @package App\Service\DashboardQuery
 * @Di\Service @Di\Tag("dashboard.query")
 */
class RequesterOpenQuery extends AbstractQuery
{
    private static $statuses = [
        ProductStatus::DRAFT,
        ProductStatus::CLOSED,
        ProductStatus::COMPLETED,
        ProductStatus::REVIEWED,
        ProductStatus::EVALUATED,
        ProductStatus::TIMEOUT,
        ProductStatus::REJECTED,
        'cerrado',
        'cerrada_sistema',
        'aceptado',
        'solicitud_completa',
        'rechazado',
        ProductStatus::CLOSED_BY_APPROVER,
    ];

    /**
     * @inheritDoc
     */
    public function getQuery()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getQueryA()
    {
        return $this->manager->createQuery('
            select r 
            from App:Requisition r 
            where (
                r.status not in (:statuses)
                and r.requester = :requester
                and r.parent is null
            ) or r.id in (
                select sp.id
                from App:Requisition sr 
                left join sr.parent sp
                left join sr.product sq
                where r.requester = :requester
                and sp.type = :rtype
            )
            order by r.updatedAt desc
        ')->setParameters([
            'statuses' => static::$statuses,
            'requester' => $this->getUser(),
            'rtype' => 'requisition.virtual'
        ]);
    }

    public function getQueryB()
    {
        $qb = $this->getQueryBuilder();
        
        return $qb->select('p')
            ->from(SpecialRequest::class, 'p')
            ->where($qb->expr()->in('p.state', [
                SpecialRequest::STATE_EVALUATION,
                SpecialRequest::STATE_IN_PROGRESS,
                SpecialRequest::STATE_REJECTED,
                SpecialRequest::STATE_APPEALED,
            ]))
            ->andWhere($qb->expr()->eq('p.requester', ':user'))
            ->orderBy($qb->expr()->asc('p.number'))
            ->setParameter(':user', $this->getUser())
            ->getQuery()
        ;
    }

    /**
     * @inheritDoc
     */
    public function getArrayResult()
    {
        $specials = $this->getQueryB()->getArrayResult();
        
        $pedidos = $this->getQueryA()->getArrayResult();
        
        return $pedidos;
    }

    /**
     * @inheritDoc
     */
    public function getResult()
    {
        $cspecs = [];
        
        foreach ($this->getQueryB()->getResult() as $item) {
            $cspecs[] = $item->mockRequisition();
        }
        
        $pedidos = $this->getQueryA()->getResult();
        $pedidos = array_merge($cspecs, $pedidos);
        
        /*
        usort($pedidos, function ($a, $b) {
            return $a->getUpdatedAt() < $b->getUpdatedAt(); 
        });
        */
        
        return $pedidos;
    }
}
