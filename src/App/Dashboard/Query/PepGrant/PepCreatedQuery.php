<?php

namespace App\Dashboard\Query\PepGrant;

use App\Dashboard\AbstractQuery;
use App\Entity\PepGrant;
use App\Workflow\Status\PepStatus;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterOpenQuery
 * @package App\Service\DashboardQuery
 * @Di\Service @Di\Tag("dashboard.query")
 */
class PepCreatedQuery extends AbstractQuery
{
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('p')
            ->from(PepGrant::class, 'p')
            ->where($qb->expr()->eq('p.status', $qb->expr()->literal(PepStatus::CREATED)))
            ->orderBy($qb->expr()->desc('p.updatedAt'))
        ;

        return $qb->getQuery();
    }
}
