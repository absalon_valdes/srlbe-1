<?php

namespace App\Dashboard\Query\PepGrant;

use App\Dashboard\AbstractQuery;
use App\Entity\PepGrant;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterOpenQuery
 * @package App\Service\DashboardQuery
 * @Di\Service @Di\Tag("dashboard.query")
 */
class PepOpenQuery extends AbstractQuery
{
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('p')
            ->from(PepGrant::class, 'p')
            ->where($qb->expr()->eq('p.requester', ':user'))
            ->orderBy($qb->expr()->desc('p.updatedAt'))
            ->setParameter(':user', $this->getUser())
        ;

        return $qb->getQuery();
    }
}
