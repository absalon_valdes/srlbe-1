<?php

namespace App\Dashboard;
use Doctrine\ORM\Query;

/**
 * Interface QueryInterface
 * @package App\Dashboard
 */
interface QueryInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return Query;
     */
    public function getQuery();

    /**
     * @return array
     */
    public function getArrayResult();

    /**
     * @return array
     */
    public function getResult();
}
