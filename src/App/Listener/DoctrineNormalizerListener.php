<?php

namespace App\Listener;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use JMS\DiExtraBundle\Annotation as Di;
use Lexik\Bundle\WorkflowBundle\Entity\ModelState;

/**
 * @Di\Service("doctrine_normalizer")
 * @Di\Tag("doctrine.event_listener", attributes={"event"="loadClassMetadata"})
 */
class DoctrineNormalizerListener
{
    /**
     * @param LoadClassMetadataEventArgs $args
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $args)
    {
        $classMetadata = $args->getClassMetadata();

        if (ModelState::class === $classMetadata->name) {
            $classMetadata->table['name'] = 'workflow_state';
        }
    }
}
