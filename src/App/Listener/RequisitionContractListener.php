<?php

namespace App\Listener;

use App\Entity\Requisition;
use App\Helper\UploadMover;
use App\Model\Attachment;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class RequisitionContractListener
 * @package App\Listener
 * 
 * @DI\Service("requisition_contract_listener")
 * @DI\Tag("doctrine.event_listener", attributes={"event"="preUpdate"})
 */
class RequisitionContractListener
{

    /**
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        /** @var Requisition $entity */
        $entity = $args->getEntity();
        if ($entity instanceof Requisition && $args->hasChangedField('contract')) {
            $entity->setSlas($entity->getContract()->getSlas());
        }
    }
}