<?php

namespace App\Listener;

use App\Entity\Contract;
use App\Entity\Employee;
use App\Entity\Product;
use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\DiExtraBundle\Annotation as DI;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;

/**
 * @DI\Service("cached_entity_update_listener")
 * @DI\Tag("doctrine.event_listener", attributes={"event"="postPersist"})
 * @DI\Tag("doctrine.event_listener", attributes={"event"="postUpdate"})
 */
class CachedEntityUpdateListener
{
    const CACHED_ENTITIES = [
        Employee::class,
        Product::class,
        Contract::class,
    ];

    /**
     * @var \Redis
     * @DI\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var CacheManager
     * @DI\Inject("liip_imagine.cache.manager")
     */
    public $cacheManager;

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->clearEntityCache($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->clearEntityCache($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    private function clearEntityCache(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        
        if (!in_array(get_class($entity), self::CACHED_ENTITIES, true)) {
            return;
        }
        
        $this->redis->flushDB();
        $this->cacheManager->remove();
    }
}