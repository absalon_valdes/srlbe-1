<?php

namespace App\Listener;

use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface as Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Helper\UserHelper;


/**
 * Class RedirectUserListener
 * @package App\Listener
 *
 * @Di\Service("redirect_user_listener")
 *
 */
class RedirectUserListener
{

    /**
     * @var UserHelper $userHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var TokenStorageInterface $tokenStorage
     * @Di\Inject("security.token_storage")
     */
    public $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface $authorizationChecker
     * @Di\Inject("security.authorization_checker")
     */
    public $authorizationChecker;

    /**
     * @var Router $router
     * @Di\Inject("router")
     */
    public $router;

    /**
     * @Di\Observe("kernel.request")
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        try {
            if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
                $route = $event->getRequest()->get('_route');
                if (($this->authorizationChecker->isGranted('ROLE_ADMIN') || $this->authorizationChecker->isGranted('ROLE_USER'))
                    && $route == 'fos_user_security_login') {
                    $response = new RedirectResponse($this->router->generate('dashboard'));
                    $event->setResponse($response);
                }
            }
        } catch (\Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException $e) {
        }
    }
}