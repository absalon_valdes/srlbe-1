<?php

namespace App\Listener\Process;

use App\Entity\Requisition;
use App\Helper\RedisHandler;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Helper\TransitionHelper;
use App\Helper\UserHelper;
use App\Notification\Process\ProductProcessNotifier;
use App\Repository\RequisitionRepository;
use App\Workflow\Assigner\AssignerInterface;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;
use Lexik\Bundle\WorkflowBundle\Event\StepEvent;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * @Di\Service("workflow.subscriber.product")
 */
class RequisitionProcessSubscriber
{
    const CHANNEL = 'PROCESS_UPDATE';

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @var ProductProcessNotifier
     * @Di\Inject("product_process_notifier")
     */
    public $notifier;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @Di\Inject("redis_handler")
     * @var RedisHandler $redisHandler
     */
    public $redisHandler;

    /**
     * @var TransitionHelper
     * @Di\Inject("transition_helper")
     */
    public $transitionHelper;

    /**
     * @var AssignerInterface
     * @Di\Inject("requisition.assigner")
     */
    public $assigner;

    /**
     * @Di\Observe("product.*.reached")
     * 
     * @param $event
     */
    public function recalculateTimestamps($event)
    {
        /** @var Requisition $requisition */
        if ($event instanceof StepEvent) {
            $requisition = $event->getModel()->getWorkflowObject();
        } elseif ($event instanceof GenericEvent) {
            $requisition = $event->getSubject();
        }
        
        $timestamps = $this->slaResolver->getSLA($requisition);
        $requisition->addMetadata('timestamps', $timestamps);

        $this->requisitionRepo->save($requisition);
    }

    /**
     * @Di\Observe("product.accepted.reached")
     * @Di\Observe("product.delivered.reached")
     * @Di\Observe("product.appeal.reached")
     * @Di\Observe("product.approved.reached")
     * @Di\Observe("product.ordered.reached")
     * @Di\Observe("product.confirmed.reached")
     * @Di\Observe("product.created.reached")
     * @Di\Observe("product.declined.reached")
     * @Di\Observe("product.closed.reached")
     * @Di\Observe("product.denied.reached")
     * @Di\Observe("product.hold.reached")
     * @Di\Observe("product.more_info_answer.reached")
     * @Di\Observe("product.more_info_ask.reached")
     * @Di\Observe("product.rejected.reached")
     * @Di\Observe("product.returned.reached")
     * @Di\Observe("product.reviewed.reached")
     *
     * @param StepEvent $event
     * @param $eventName
     */
    public function onAllEvents($event, $eventName)
    {
        /** @var Requisition $requisition */
        if ($event instanceof StepEvent) {
            $requisition = $event->getModel()->getWorkflowObject();    
        } elseif ($event instanceof GenericEvent) {
            $requisition = $event->getSubject();
        } else {
            throw new \InvalidArgumentException('Invalid event object');
        }

        if ($requisition->isType('SOLICITUD_ESPORADICA')) {
            return;
        }
        
        $this->transitionHelper->pushTransition($requisition);
        $this->assigner->assign($requisition, $eventName);
        if($this->sendEventNotification($requisition,$eventName)){
            $this->notifier->sendNotifications($event, $eventName);
        }
        $this->slaResolver->updateSLA($requisition);

        $requisition->resetInboxStatus();
    }

    /**
     * @Di\Observe("product.requisition.view")
     * 
     * @param GenericEvent $event
     */
    public function onViewRequisition(GenericEvent $event)
    {
        $requisition = $event->getSubject();
        $requisition->clearInboxStatus($this->userHelper->getCurrentUser());
        
        $this->requisitionRepo->save($requisition);
    }

    /**
     * @Di\Observe("product.*.reached")
     *
     * @param StepEvent $event
     */
    public function sendNotificationBackOffice($event)
    {
        /** @var Requisition $requisition */
        if ($event instanceof StepEvent) {
            $requisition = $event->getModel()->getWorkflowObject();
        } elseif ($event instanceof GenericEvent) {
            $requisition = $event->getSubject();
        } else {
            throw new \InvalidArgumentException('Invalid event object');
        }
        
        if ($requisition->isStatus(ProductStatus::EVALUATED) || $requisition->isStatus(ProductStatus::CONFIRMED)) {
            if (!$requisition->hasProblems()) {
                 return; 
            }
            
            $this->processBackOfficeNotification($requisition);
            
            return;
        }
        
        // even confirmed or timeout
        $this->processBackOfficeNotification($requisition);
    }
    
    private function processBackOfficeNotification(Requisition $requisition) 
    {
        $this->redisHandler->push(self::CHANNEL, [
            'id'        => $requisition->getId(),
            'number'    => $requisition->getNumber(),
            'type'      => $requisition->getType(),
            'status'    => sprintf(
                '%s.%s.%s',
                $requisition->getStatus(),
                str_replace('requisition.', '', $requisition->getType()),
                $requisition->isCentralized() ? 'centralized' : 'descentralized'
            ),
            'updatedAt' => $requisition->getUpdatedAt(),
        ]);    
    }

    private function sendEventNotification(Requisition $requisition, $eventName){
        $send = true;
        if($requisition->getMetadata('massive_accept') !== null){
            $send = false;
        }

        if((str_replace('.reached', '', $eventName) === ProductStatus::APPROVED)
        && $requisition->isType(Requisition::TYPE_UNIFORM)){
            $send = false;
        }

        return $send;

    }
}
