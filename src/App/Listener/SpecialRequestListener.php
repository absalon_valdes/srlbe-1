<?php
/**
 * Created by PhpStorm.
 * User: steven
 * Date: 22-12-16
 * Time: 14:14
 */

namespace App\Listener;

use App\Notification\Process\SpecialRequestNotifier;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class SpecialRequestListener
 * @package App\Listener
 * @Di\Service()
 */
class SpecialRequestListener
{
    /**
     * @var SpecialRequestNotifier
     * @Di\Inject("special_request_notifier")
     */
    public $notifier;

    /**
     * @Di\Observe("special.request.*")
     * @param $event
     * @param $name
     */
    public function onEvents($event, $name)
    {
        $this->notifier->sendNotifications($event, $name);
    }
}