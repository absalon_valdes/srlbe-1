<?php

namespace App\Listener;

use App\Entity\User;
use App\Entity\Employee;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class EasyAdminPreRemoveListener
 * @package App\Listener
 * @DI\Service()
 */
class EasyAdminPreRemoveListener
{
    /**
     * @DI\Observe("easy_admin.pre_remove")
     * @param GenericEvent $event
     */
    public function preRemove(GenericEvent $event)
    {
	$subject = $event->getSubject();

        if ($subject instanceof User || $subject instanceof Employee) {
            $event->getSubject()->setDeletedAt(new \DateTime);
        }
        
    }
}
