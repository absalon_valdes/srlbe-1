<?php

namespace App\Listener;

use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * @Di\Service()
 */
class ExceptionListener
{
    /**
     * @Di\Inject("mailer")
     */
    public $mailer;

    /**
     * @Di\Inject("twig")
     */
    public $twig;

    /**
     * @var boolean
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @param GetResponseForExceptionEvent $event
     * 
     * @Di\Observe("kernel.exception")
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        /*
        if ('dev' === $this->environment) {
            return;
        }
        
        $exception = $event->getException();
        
        $content = $this->twig->render('exception.twig', [
            'exception' => $exception
        ]);
        
        $response = new Response();
        $response->setContent($content);

        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $message = \Swift_Message::newInstance()
            ->setSubject('Error de Sistema')
            ->setFrom('send@example.com')
            ->setTo('steven@bluecompany.cl')
            ->setBody($response->getContent())
            ->setBody(strip_tags($content))
            ->addPart($content, 'text/html')
        ;

        return $this->mailer->send($message);
        */
    }
}