<?php

namespace App\Pipeline\Task\Reader;

use App\Pipeline\Task\AbstractTask;

class ExcelReader extends AbstractTask
{
    public function execute(&$data)
    {
        $filename = $this->options['filename'];
        
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($filename);
        
        $worksheets = [];

        foreach ($excel->getAllSheets() as $sheet) {
            $rows = array_filter($sheet->toArray(), function ($item) {
                return !empty($item[0]);
            });
            
            $worksheets[$sheet->getTitle()] = $rows;
        }
        
        reset($worksheets);
        $data = $worksheets[key($worksheets)];
    }
}