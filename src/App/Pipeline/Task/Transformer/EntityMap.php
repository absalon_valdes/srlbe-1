<?php

namespace App\Pipeline\Task\Transformer;

use App\Pipeline\Task\AbstractTask;
use Doctrine\ORM\EntityManager;

class EntityMap extends AbstractTask
{
    public function execute(&$data)
    {
        $class = $this->options['class'];
        $column = $this->options['column'];
        $refCol = $this->options['ref_column'] ?? 'id';
        $disposeCols = $this->options['dispose_columns'] ?? true;
        $columnName = $this->options['column_name'] ?? $column;
        
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        
        $data = array_map(function ($item) use ($disposeCols, $class, $column, $em, $refCol, $columnName) {
            $ref = $em->getReference($class, $item[$column]);
            $item[$columnName] = $ref;
            
            return $item;
        }, $data);
    }
}