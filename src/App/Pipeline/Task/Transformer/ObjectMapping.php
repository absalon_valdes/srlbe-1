<?php

namespace App\Pipeline\Task\Transformer;

use App\Pipeline\Task\AbstractTask;

class ObjectMapping extends AbstractTask
{
    public function execute(&$data)
    {
        $data = array_map([$this, 'mapObject'], $data);
    }

    private function mapObject($item)
    {
        $columns = $this->options['columns'];
        $class = $this->options['class'];
        $disposeCols = $this->options['dispose_columns'] ?? true;
        $factory = $this->options['factory'];
        $columnName = $this->options['column_name'];

        $args = array_map(function ($arg) use ($item) {
            if (0 === strpos($arg, '$')) {
                return $item[substr($arg, 1)];
            }

            return $arg;
        }, $factory[1]);

        $ref = new \ReflectionClass($class);
        
        if ('__constructor' === $factory[0]) {
            $instance = $ref->newInstanceArgs($args);
        } else {
            $instance = $ref->newInstance();
        }
        
        $item[$columnName] = $instance;
        
        if ($disposeCols) {
            foreach ($columns as $column) {
                unset($item[$column]);
            }
        }
        
        return $item;
    }
}