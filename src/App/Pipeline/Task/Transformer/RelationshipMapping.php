<?php

namespace App\Pipeline\Task\Transformer;

use App\Pipeline\Task\AbstractTask;
use Doctrine\ORM\EntityManager;

class RelationshipMapping extends AbstractTask
{
    public function execute(&$data)
    {
        $entity = $this->options['entity'];
        $column = $this->options['column'];
        $map = $this->options['map'];
        $value = $this->options['value'];

        $pdo = $this->container->get('database_connection');
        $pdo->setFetchMode(\PDO::FETCH_KEY_PAIR);
        
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $table = $em->getClassMetadata($entity)->getTableName();
        
        $prefetch = $pdo->fetchAll(sprintf('select %s, id from %s', $column, $table));

        $data = array_map(function ($item) use ($prefetch, $value, $map) {
            if (0 === strpos($value, '$')) {
                $value = $item[substr($value, 1)];
            }

            $item[$map] = $prefetch[$value] ?? null;
            
            return $item;
        }, $data);
    }
}