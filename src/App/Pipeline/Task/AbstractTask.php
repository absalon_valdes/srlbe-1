<?php

namespace App\Pipeline\Task;

use Doctrine\Common\Util\Inflector;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractTask implements Task
{
    protected $container;
    protected $options = array();

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    public function getName()
    {   
        $ref = new \ReflectionClass(static::class);
        
        return Inflector::tableize($ref->getShortName());
    }

    protected function parseVariable()
    {
        
    }
}