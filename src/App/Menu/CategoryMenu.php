<?php

namespace App\Menu;

use App\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\DiExtraBundle\Annotation as Di;
use Knp\Menu\FactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Event\ConfigureMenuEvent;

/**
 * @Di\Service("menu_category_menu")
 */
class CategoryMenu 
{
    /**
     * @Di\Inject("product_repo")
     */
    public $productsRepo;

    private $current = null;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function build(FactoryInterface $factory, array $options)
    {
        $this->current = $category = $options['category'];
        
        // TODO: Fix
        $prev = $category;
        while ($category->getParent()) {
            $prev = $category;
            $category = $prev->getParent();
        } 

        $this->addLeaf($menu = $factory->createItem('root'), $prev);

        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_CATEGORY,
            new ConfigureMenuEvent($factory, $menu)
        );

        return $menu;
    }

    /**
     * TODO: Optimize recursive query for ancestors
     * 
     * @param $root
     * @param Category $category
     */
    private function addLeaf($root, Category $category)
    {
        if (!$this->productsRepo->findHavingProducts($category)) {
            return;
        }
        
        $current = $root->addChild($category->getName(), [
            'route' => 'requisitions_catalog',
            'routeParameters' => [
                'slug' => $category->getSlug()
            ]
        ]);

        if ($category === $this->current) {
            $current->setCurrent(true);
        }

        /** @var ArrayCollection $children */
        $children = $category->getChildren();

        if (preg_match('#/sillas$#', $category->getSlug())) {
            $collection = $children->toArray();
            
            // TODO: remove hack
            usort($collection, function ($a, $b) {
                return 'Funcionarios' !== $a->getName() ? 1 : -1;
            });
            
            $children = new ArrayCollection($collection);
        }

        foreach ($children as $child) {
            $this->addLeaf($current, $child);
        }
    }
}
