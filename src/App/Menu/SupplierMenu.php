<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Event\ConfigureMenuEvent;

/**
 * @Di\Service("menu_supplier_menu")
 */
class SupplierMenu
{
    /**
     * @Di\Inject("request_stack")
     */
    public $requestStack;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    public function build(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'id' => 'supplier-menu',
                'class' => 'nav navbar-nav '
            ]
        ]);
        
        $request = $this->requestStack->getCurrentRequest();

        if ('dashboard' !== $request->get('_route') && !$options['dashboard']) {
            $menu->addChild('Inicio', [
                'route' => 'dashboard',
                'linkAttributes' => [
                    'class' => 'menu-home'
                ]
            ]);
        }

        $menu->addChild('Ver ofertas', [
            'attributes' => [
                'class' => 'menu-ofertas'
            ]
        ]);

        $menu->addChild('Estado de pago', [
            'attributes' => [
                'class' => 'menu-estado-pago'
            ]
        ]);

        $menu->addChild('Desarrollo empresarial', [
            'attributes' => [
                'class' => 'menu-desarrollo'
            ]
        ]);

        $menu->addChild('Estado de solicitudes', [
            'route' => 'requisitions_index',
            'attributes' => [
                'class' => 'menu-estado-solicitudes'
            ]
        ]);
        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_SUPPLIER,
            new ConfigureMenuEvent($factory, $menu)
        );


        return $menu;
    }
}
