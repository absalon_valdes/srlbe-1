<?php

namespace App\Menu;

use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\Service;
use Knp\Menu\FactoryInterface;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Event\ConfigureMenuEvent;

/**
 * @Service("menu_notification_menu")
 */
class NotificationMenu
{
    const SHOW_MAX = 3;

    /**
     * @Inject("message_repo")
     */
    public $messagesRepo;

    /**
     * @Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    public function build(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'id' => 'notification-menu',
                'class' => 'dropdown-menu'
            ]
        ]);

        $currentUser = $this->userHelper->getCurrentUser();
        $messages = $this->messagesRepo->getUnread($currentUser);
        $showInList = array_reverse($messages);
        $showInList = array_slice($showInList, 0, self::SHOW_MAX);

        $menu->setExtras([
            'showing' => count($showInList),
            'total' => count($messages)
        ]);

        if (!count($showInList)) {
            $menu->addChild('No hay alertas nuevas', [
                'attributes' => ['class' => 'empty-list-msg']
            ]);

            return $menu;
        }

        foreach ($showInList as $message) {
            $label = sprintf('<p class="%s">%s</p>',
                $message['id'], $message['body']);

            $menu->addChild($label, [
                'route' => 'notification_detail',
                'routeParameters' => ['id' => $message['id']],
                'extras' => [
                    'source' => $message['source'],
                    'created_at' => $message['createdAt'],
                    'message_id' => $message['id'],
                    'safe_label' => true
                ]
            ]);
        }

        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_NOTIFICATION,
            new ConfigureMenuEvent($factory, $menu)
        );

        return $menu;
    }
}
