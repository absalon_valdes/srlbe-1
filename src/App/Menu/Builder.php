<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;

/**
 * @Service("menu_builder")
 * @Tag("knp_menu.menu_builder", attributes={"method"="mainMenu", "alias"="main"})
 * @Tag("knp_menu.menu_builder", attributes={"method"="notificationMenu", "alias"="notifications"})
 * @Tag("knp_menu.menu_builder", attributes={"method"="categoryMenu", "alias"="category"})
 * @Tag("knp_menu.menu_builder", attributes={"method"="userMenu", "alias"="user"})
 * @Tag("knp_menu.menu_builder", attributes={"method"="adminMenu", "alias"="admin"})
 * @Tag("knp_menu.menu_builder", attributes={"method"="supplierMenu", "alias"="supplier"})
 * @Tag("knp_menu.menu_builder", attributes={"method"="supplierTopMenu", "alias"="supplier_top"})
 * @Tag("knp_menu.menu_builder", attributes={"method"="approverMenu", "alias"="approver"})
 * @Tag("knp_menu.menu_builder", attributes={"method"="evaluatorMenu", "alias"="evaluator"})
 */
class Builder
{
    /**
     * @var array
     */
    private $menues = array();
    
    /**
     * @Inject("knp_menu.factory") 
     */
    public $factory;

    /**
     * @var CategoryMenu
     * @Inject("menu_category_menu")
     */
    public $categoryMenu;

    /**
     * @var MainMenu
     * @Inject("menu_main_menu") 
     */
    public $mainMenu;

    /**
     * @var NotificationMenu
     * @Inject("menu_notification_menu") 
     */
    public $notificationMenu;

    /**
     * @var UserMenu
     * @Inject("menu_user_menu")
     */
    public $userMenu;

    /**
     * @var AdminMenu
     * @Inject("menu_admin_menu")
     */
    public $adminMenu;

    /**
     * @var SupplierMenu
     * @Inject("menu_supplier_menu")
     */
    public $supplierMenu;

    /**
     * @var SupplierTopMenu
     * @Inject("menu_supplier_top_menu")
     */
    public $supplierTopMenu;

    /**
     * @var ApproverMenu
     * @Inject("menu_approver_menu")
     */
    public $approverMenu;

    /**
     * @var EvaluatorMenu
     * @Inject("menu_evaluator_menu")
     */
    public $evaluatorMenu;

    /**
     * @param $menu
     * @param $alias
     */
    public function registerMenu($menu, $alias)
    {
        $this->menues[$alias] = $menu;
    }

    public function __call($method, $arguments)
    {
        $menu = $this->menues[preg_replace('/Menu$/', '', $method)];
        
        return $menu->build($this->factory, $arguments);
    }

    public function categoryMenu(array $options)
    {
        return $this->categoryMenu->build($this->factory, $options);
    }

    public function mainMenu(array $options)
    {
        return $this->mainMenu->build($this->factory, $options);
    }

    public function notificationMenu(array $options)
    {
        return $this->notificationMenu->build($this->factory, $options);
    }

    public function userMenu(array $options)
    {
        return $this->userMenu->build($this->factory, $options);
    }

    /**
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     * @throws \ReflectionException
     */
    public function adminMenu(array $options)
    {
        return $this->adminMenu->build($this->factory, $options);
    }

    public function supplierMenu(array $options)
    {
        return $this->supplierMenu->build($this->factory, $options);
    }

    /**
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     * @throws \ReflectionException
     */
    public function supplierTopMenu(array $options)
    {
        return $this->supplierTopMenu->build($this->factory, $options);
    }

    public function approverMenu(array $options)
    {
        return $this->approverMenu->build($this->factory, $options);
    }

    /**
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     * @throws \ReflectionException
     */
    public function evaluatorMenu(array $options)
    {
        return $this->evaluatorMenu->build($this->factory, $options);
    }
}
