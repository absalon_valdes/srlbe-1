<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Event\ConfigureMenuEvent;

/**
 * @Di\Service("menu_user_menu")
 */
class UserMenu
{
    /**
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @Di\Inject("request_stack")
     */
    public $requestStack;

    /**
     * @var \Symfony\Component\Security\Core\Authorization\AuthorizationChecker
     * @Di\Inject("security.authorization_checker")
     */
    public $security;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function build(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'id' => 'user-menu',
                'class' => 'dropdown-menu'
            ]
        ]);

        $user = $this->userHelper->getCurrentUser();
        $request = $this->requestStack->getCurrentRequest();

        if ('first_login' !== $request->get('_route')) {
            $menu->addChild('Perfil', ['route' => 'user_profile']);
        }

        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_USER,
            new ConfigureMenuEvent($factory, $menu)
        );

        $menu->addChild('',['attributes'=>['class'=>'divider']]);

        if($this->security->isGranted('ROLE_PREVIOUS_ADMIN')) {
            $menu->addChild('Volver a admin', ['route' => 'dashboard','routeParameters' => ['_switch_user'=>'_exit']]);
        }

        $menu->addChild('Cerrar sesión', ['route' => 'fos_user_security_logout']);

        return $menu;
    }
}
