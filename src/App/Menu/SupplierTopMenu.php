<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use JMS\DiExtraBundle\Annotation as Di;
use Recurr\Transformer\TranslatorInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Event\ConfigureMenuEvent;

/**
 * @Di\Service("menu_supplier_top_menu")
 */
class SupplierTopMenu
{
    const MODULE_MENU = 'module';
    const REPORT_MENU = 'report';
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     * @throws \ReflectionException
     */
    public function build(FactoryInterface $factory, array $options)
    {

        $headerMenuConf =[
            'uri' => '#',
            'attributes'=> [
                'class' => 'dropdown',
            ],
            'childrenAttributes' => [
                'class' => 'dropdown-menu',
                "style" => "z-index: 999999999999",
            ],
            'linkAttributes' => [
                "class" => "dropdown-toggle",
                "data-toggle"=>"dropdown",
                "role"=>"button",
                "aria-haspopup"=>"true",
                "aria-expanded"=>"false",
            ],
        ];

        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'class' => 'nav navbar-nav navbar-left'
            ]
        ]);

        $menu->addChild(self::MODULE_MENU, array_merge([
            'route' => 'dashboard',
            'label'=>$this->translator->trans('dashboard')
        ]));

        $reportMenu = $menu->addChild(self::REPORT_MENU, array_merge($headerMenuConf,[
            'label' => 'Reportes'
        ]));
        $reportMenu->setAttribute('class', 'dropdown');

        $reportMenu->addChild('report_requisition_supplier', [
            'route' => 'report_requisition_supplier',
            'label'=>$this->translator->trans('Pedidos')
        ]);

        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_SUPPLIER_TOP,
            new ConfigureMenuEvent($factory, $menu, $this->translator)
        );

        return $menu;
    }
}
