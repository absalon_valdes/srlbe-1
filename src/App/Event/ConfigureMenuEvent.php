<?php
/**
 * Created by PhpStorm.
 * User: rmoreno
 * Date: 25-05-18
 * Time: 15:12
 */

namespace App\Event;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Translation\TranslatorInterface;

class ConfigureMenuEvent extends Event
{

    const CONFIGURE_ADMIN = "app.admin_menu_configure";
    const CONFIGURE_APPROVER = "app.approver_menu_configure";
    const CONFIGURE_CATEGORY = "app.category_menu_configure";
    const CONFIGURE_MAIN = "app.main_menu_configure";
    const CONFIGURE_NOTIFICATION = "app.notification_menu_configure";
    const CONFIGURE_SUPPLIER = "app.supplier_menu_configure";
    const CONFIGURE_USER = "app.user_menu_configure";
    const CONFIGURE_EVALUATOR = "app.evaluator_menu_configure";
    const CONFIGURE_SUPPLIER_TOP = "app.supplier_top_menu_configure";

    /** @var FactoryInterface */
    private $factory;
    /** @var ItemInterface  */
    private $menu;
    /** @var TranslatorInterface|null  */
    public $trans;

    /**
     * @param \Knp\Menu\FactoryInterface $factory
     * @param \Knp\Menu\ItemInterface $menu
     * @param TranslatorInterface|null $translation
     */
    public function __construct(FactoryInterface $factory, ItemInterface $menu, $translation = null)
    {
        $this->factory = $factory;
        $this->menu = $menu;
        $this->trans = $translation;
    }

    /**
     * @return \Knp\Menu\FactoryInterface
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    public function getMenu()
    {
        return $this->menu;
    }
}