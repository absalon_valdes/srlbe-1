<?php

namespace ApiBundle\Query;

use App\Entity\Message;
use App\Entity\Supplier;
use App\Entity\User;
use App\Helper\UserHelper;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class MessagesQuery
 * @package ApiBundle\Query
 * @DI\Service("messages.query")
 */
class MessagesQuery
{
    /**
     * @var EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $manager;

    /**
     * @var UserHelper
     * @DI\Inject("user_helper")
     */
    public $userHelper;
    
    public function supplierMessagesQuery($state = 'all', $sort, $order)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->manager->createQueryBuilder();
        $qb = $qb->select('m')
            ->from(Message::class, 'm')
            ->join(User::class, 'u', Join::WITH, 'u = m.receiver')
        ;
        
        switch ($state) {
            case 'pending':
                $state = [0];
                break;
            case 'viewed':
                $state = [1];
                break;
            default:
                $state = [1,0];
        }
        
        $qb->where($qb->expr()->in('m.viewed', $state));
        $qb->andWhere(':user = u');

        $qb->orderBy('m.'.$sort, $order);
        $qb->setParameter('user', $this->userHelper->getCurrentUser());
        
        return $qb->getQuery();
    }
}