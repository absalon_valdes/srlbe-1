<?php

namespace ApiBundle\Listener;

use ApiBundle\Model\RequisitionMetadata;
use App\Entity\Requisition;
use App\Helper\UserHelper;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\PreSerializeEvent;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class RequisitionMetaListener
 * 
 * @DI\Service() 
 * @DI\Tag("jms_serializer.event_subscriber")
 */
class RequisitionMetaListener implements EventSubscriberInterface
{
    /**
     * @var RequisitionMetadata
     * @DI\Inject("requisition.metadata.formatter")
     */
    public $metadataFormatter;

    /**
     * @var TranslatorInterface
     * @DI\Inject("translator")
     */
    public $translator;

    /**
     * @var UserHelper
     * @DI\Inject("user_helper")
     */
    public $userHelper;
    
    public static function getSubscribedEvents()
    {
        return [[
            'event' => Events::PRE_SERIALIZE,
            'method' => 'onPreSerialize',
            'class' => Requisition::class
        ]];
    }

    public function onPreSerialize(PreSerializeEvent $event)
    {
        /** @var Requisition $requisition */
        $requisition = $event->getObject();
        
        $metadata = $this->metadataFormatter->getMetadata($requisition);
        $requisition->setSerializedMeta($metadata);
        
        $statusLabel = $this->translator->trans($requisition->getTranslatableStatus());
        $requisition->setStatusLabel($statusLabel);
        
        $status = $requisition->getInboxStatus($this->userHelper->getCurrentUser());
        $requisition->setViewed((bool) $status);
    }
}