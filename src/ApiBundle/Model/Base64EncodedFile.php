<?php

namespace ApiBundle\Model;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class Base64EncodedFile extends File
{
    public function __construct($encoded, $strict = true, $checkPath = true)
    {
        parent::__construct($this->createFromTemp($encoded, $strict), $checkPath);
    }

    private function createFromTemp($encoded, $strict = true)
    {
        if (false === $decoded = base64_decode($encoded, $strict)) {
            throw new FileException('Unable to decode strings as base64');
        }

        if (false === $path = tempnam($directory = sys_get_temp_dir(), 'Base64EncodedFile')) {
            throw new FileException(sprintf('Unable to create a file into the "%s" directory', $directory));
        }

        if (false === file_put_contents($path, $decoded, FILE_BINARY)) {
            throw new FileException(sprintf('Unable to write the file "%s"', $path));
        }

        return $path;
    }
}