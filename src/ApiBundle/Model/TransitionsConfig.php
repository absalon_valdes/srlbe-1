<?php

namespace ApiBundle\Model;

use App\Entity\Requisition;
use App\Workflow\Status\ProductStatus;

class TransitionsConfig
{
    public static function getRules(Requisition $requisition)
    {
        $isMaintenance = $requisition->isType(Requisition::TYPE_MAINTENANCE);
        $isSelfManagement = $requisition->isType(Requisition::TYPE_SELF_MANAGEMENT);
        $isProduct = $requisition->isType(Requisition::TYPE_PRODUCT);
        
        return [
            ProductStatus::APPROVED => [
                'decline' => [
                    'precondition' => $isMaintenance,
                    'title' => 'Rechazar',
                    'attrs' => [
                        'reason' => [
                            'precondition' => !$isMaintenance,
                            'type' => 'choice',
                            'choices' => [
                                'Sin stock' => 'sin_stock',
                                'Sin personal' => 'sin_personal',
                                'Motivos técnicos' => 'motivos_tecnicos',
                            ],
                            'constraints' => [
                                'choice' => [
                                    'choices' => [
                                        'sin_stock', 
                                        'motivos_tecnicos',
                                        'sin_personal', 
                                    ]
                                ],
                                'not_blank' => []
                            ]
                        ],
                        'comments' => [
                            'precondition' => true,
                            'type' => 'textarea',
                            'constraints' => [
                                'length' => ['min' => 10],
                                'not_blank' => []
                            ]
                        ]
                    ],
                    'callback' => function (Requisition $r, $meta) {
                        $r->addSubMetadata('declined', $r->getSupplier()->getId());
                        $r->addMetadata('decline', $meta);
                    }
                ], 
                'accept' => [
                    'precondition' => true,
                    'title' => 'Aceptar',
                    'attrs' => [
                        'service_responsible' => [
                            'precondition' => $isMaintenance || true,
                            'type' => 'text',
                            'constraints' => [
                                'length' => [
                                    'min' => 7
                                ],
                                'not_blank' => []
                            ]
                        ],
                        /**/
                        'certification' => [
                            'precondition' => true,
                            'type' => 'collection',
                            'constraints' => [
                                'collection' => [
                                    'fields' => [
                                        'comments' => [
                                            'length' => [
                                                'min' => 10
                                            ],
                                            'not_blank' => [],
                                        ],
                                        'attachment' => [
                                            'not_blank' => [],
                                        ]
                                    ],
                                    'allowMissingFields' => true,
                                ], 
                            ],
                            'item' => [
                                'comment' => [
                                    'type' => 'textarea'
                                ],
                                'attachment' => [
                                    'type' => 'file'
                                ]
                            ]
                        ]
                        /**/
                    ],
                    'callback' => function (Requisition $r, $meta) use ($isMaintenance) {
                        $r->addMetadata('work_order', time());
                        
                        if (isset($meta['service_responsible'])) {
                            $r->addMetadata('responsible', $meta);
                        }
                    }
                ]
            ],
            ProductStatus::ACCEPTED => [
                'deliver' => [
                    'precondition' => true,
                    'title' => $isMaintenance ? 'Aceptar orden de trabajo' : 'Confirmar recepción',
                ]
            ],
            ProductStatus::DELIVERED => [
                'confirm' => [
                    'precondition' => true,
                    'title' => 'Confirmar trabajo',
                    'attrs' => [
                        'certification' => [
                            'precondition' => true,
                            'type' => 'collection',
                            'item' => [
                                'comment' => [
                                    'type' => 'textarea',
                                    'constraints' => [
                                        'length' => ['min' => 10],
                                    ]
                                ],
                                'attachment' => [
                                    'type' => 'file',
                                ]
                            ]
                        ]
                    ],
                    'callback' => function (Requisition $r, $meta) {
                        
                    }
                ]
            ],
            ProductStatus::CONFIRMED => [],
            ProductStatus::RETURNED => [],
        ];
    }
}
