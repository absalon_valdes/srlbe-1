<?php

namespace ApiBundle\Model;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Metadata
 * @Serializer\ExclusionPolicy("ALL")
 */
class Metadata
{
    /**
     * @Serializer\Expose()
     */
    public $key;
    
    /**
     * @Serializer\Expose()
     */
    public $label;

    /**
     * @Serializer\Expose()
     */
    public $data;

    public function __construct($key, $label, $data)
    {
        $this->key = $key;
        $this->label = $label;
        $this->data = $data;
    }

    public static function create($key, $label, $data)
    {
        return new self($key, $label, $data);
    }
}