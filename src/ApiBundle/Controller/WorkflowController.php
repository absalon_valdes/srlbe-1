<?php

namespace ApiBundle\Controller;

use ApiBundle\Model\StepActionFactory;
use ApiBundle\Model\TransitionsConfig;
use ApiBundle\Query\WorkflowsQuery;
use ApiBundle\Representation\PaginatedRepresentation;
use App\Entity\Requisition;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WorkflowController
 * @Rest\Route("/workflows")
 * @Security("has_role('ROLE_SUPPLIER')")
 */
class WorkflowController extends FOSRestController
{
    /**
     * @var WorkflowsQuery
     * @DI\Inject("workflows.query")
     */
    protected $query;

    /**
     * @var EventDispatcherInterface
     * @DI\Inject("event_dispatcher")
     */
    protected $dispatcher;

    /**
     * @var StepActionFactory
     * @DI\Inject("step.action.factory")
     */
    protected $actionFactory;
    
    /**
     * @ApiDoc(
     *     section="Workflow",
     *     description="Obtiene lista de procesos activos del usuario actual",
     *     statusCodes={200="Exito"}
     * )
     * @Rest\Get(name="workflows_index")
     * @Rest\QueryParam(name="state", 
     *     nullable=true, 
     *     requirements="(pending|progress|history|certify)", 
     *     default="", 
     *     description="Estado del proceso"
     * )
     * @Rest\QueryParam(name="limit", 
     *     requirements="\d+", 
     *     default="5", 
     *     description="Items por página"
     * )
     * @Rest\QueryParam(name="page", 
     *     requirements="\d+", 
     *     default="1", 
     *     description="Página"
     * )
     * @Rest\QueryParam(name="order", 
     *     nullable=true, 
     *     requirements="(asc|desc)", 
     *     default="desc", 
     *     description="Dirección de ordenamiento"
     * )
     * @Rest\QueryParam(name="sort", 
     *     nullable=true, 
     *     requirements="[a-zA-Z0-9]+", 
     *     default="updatedAt", 
     *     description="Campo de ordenamiento"
     * )
     * @Rest\View(serializerGroups={"Default"})
     * @param ParamFetcherInterface $paramFetcher
     * @return static
     */
    public function processListAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = (int) $paramFetcher->get('limit');
        $page  = (int) $paramFetcher->get('page');
        
        $state = $paramFetcher->get('state');
        $sort  = $paramFetcher->get('sort');
        $order = $paramFetcher->get('order');
        
        $query = $this->query->supplierRequisitionsQuery($state, $sort, $order);
        
        return PaginatedRepresentation::create($query, 'workflows_index', $limit, $page);
    }

    /**
     * @ApiDoc(
     *     section="Workflow",
     *     description="Obtiene detalle de un proceso"
     * )
     * @Rest\Get("/{id}", name="workflow_detail")
     * @Security("is_granted('requisition_view', requisition)")
     * @param Requisition $requisition
     * @return static
     */
    public function processDetailAction(Requisition $requisition)
    {
        $this->dispatcher->dispatch('product.requisition.view', new GenericEvent($requisition));
        
        return $requisition;
    }

    /**
     * @ApiDoc(
     *     section="Workflow",
     *     description="Ejecuta una accion sobre un proceso"
     * )
     * @Rest\Post("/{id}/proceed/{from}/{step}", name="workflow_step")
     * @Security("is_granted('requisition_view', requisition)")
     * @param Request $request
     * @param Requisition $requisition
     * @param $from
     * @param $step
     * @return array
     */
    public function processStepAction(Request $request, Requisition $requisition, $from, $step)
    {
        $from = hex2bin($from);
        $step = hex2bin($step);
        $available = TransitionsConfig::getRules($requisition);
        
        if (!array_key_exists($from, $available)) {
            throw new \InvalidArgumentException;
        }
        
        /*$cfg = array_filter($available[$from][$step]['attrs'], function ($cfg) {
            return $cfg['precondition'];
        });*/
        
        $action = $this->actionFactory->create($requisition, $step, $available[$from], $request);
        
        if ($errors = $action->execute()) {
            return View::create($errors, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}